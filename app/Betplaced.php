<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Betplaced extends Model
{
   
	use SoftDeletes;


	protected $dates = ['deleted_at'];

	 public function user(){
        return $this->hasOne('App\User','id','user_id');
    }


    public function selection(){

        return $this->hasOne('App\MarketRunner','selection_id','selection_id');
    }





    public function event(){

        return $this->hasOne('App\Event','event_id','event_id');
    }

}
