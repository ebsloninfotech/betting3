<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Competition;
use App\Event;
use App\EventsMarket;
use App\MarketRunner;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('competition:corn')
                 ->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {


    $a = ['1','2','4'];

    foreach ($a as $b) {
            
      

        // $params = '{"filter":{"eventTypeIds":["'.$b.'"],
        //               "marketStartTime":{"from":"' . date('c') . '"}},
        //                "sort":"FIRST_TO_START"}';


        //  $result = listCompetitions($params);              
         
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'http://128.199.24.35/v1-api/match/getCompetitions?sportId='.$b.'');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: VKbsu';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);



         $result = json_decode($result);

        foreach($result as $value) {
            $d = Competition::where('competition_id',$value->competition_id)->first();
            if (!$d) {

                \DB::table('competitions')->insert([
                   ["name" => $value->competition_name,"competition_id" => $value->competition_id,"event_type" => $b]
                ]);
            }



            // $tparams2 = '{"filter":{"competitionIds":["' . $value->competition->id . '"],"maxResults": "100"},
            //                     "maxResults": "100","id": 1}';

            //                     $result2 = listEvents($tparams2);








                $ch2 = curl_init();

                curl_setopt($ch2, CURLOPT_URL, 'http://128.199.24.35/v1-api/match/getMatches?competitionId='.$value->competition_id.'');
                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, 'GET');


                $headers = array();
                $headers[] = 'Accept: application/json';
                $headers[] = 'Authorization: VKbsu';
                curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);

                $response2 = curl_exec($ch2);
                if (curl_errno($ch2)) {
                    echo 'Error:' . curl_error($ch2);
                }
                 curl_close($ch2);

                 $result2 = json_decode($response2);

             foreach ($result2 as $value2) {


                  
                 $e = Event::where('event_id',$value2->event_id)->first();


                    if (!$e) {

                        \DB::table('events')->insert([
                           ["name" => $value2->event_name,"event_id" => $value2->event_id,"competition_id" => $value->competition_id,"openDate" => $value2->open_date,"event_type" => $b,"market_id" => $value2->market_id]
                        ]);
                    }   


                    // $ch3 = curl_init();

                    // curl_setopt($ch3, CURLOPT_URL, 'http://143.110.177.103:3000/v1-api/fancy/getBetFancy/'.$value2->event_id.'');
                    // curl_setopt($ch3, CURLOPT_RETURNTRANSFER, 1);
                    // curl_setopt($ch3, CURLOPT_CUSTOMREQUEST, 'GET');



                    // $response4 = curl_exec($ch3);
                    // if (curl_errno($ch3)) {
                    //     echo 'Error:' . curl_error($ch3);
                    // }
                    //  curl_close($ch3);

                    //  $result4 = json_decode($response4);


                    // $tparams2 = '{
                    // "filter":{"eventIds":["' . $value2->event->id . '"],"maxResults": "100"},
                    // "maxResults": "100","marketProjection":["RUNNER_METADATA","RUNNER_DESCRIPTION","MARKET_START_TIME"],"id": 1}';

                    // $result3 = listMarket($tparams2);


                    // foreach ($result3 as $value3) {
                          
                    //     $m = EventsMarket::where('marketId',$value3->marketId)->first();
                    //     if (!$m) {

                    //         // if($value3->marketName == 'Match Odds' || $value3->marketName == 'Tied Match'){
                                

                    //         \DB::table('events_markets')->insert([
                    //            ["marketName" => $value3->marketName,"marketId" => $value3->marketId,"event_id" => $value2->event->id,"competition_id" => $value->competition->id,"marketStartTime" => date('Y-m-d h:i:s', strtotime($value3->marketStartTime)),"event_type" => $b]
                    //         ]);


                    //         foreach ($value3->runners as $value4) {

                    //             $check = MarketRunner::where('selection_id',$value4->selectionId)->where('marketId',$value3->marketId)->first();
                                
                    //             if(!$check){


                    //               \DB::table('market_runners')->insert([
                    //                  ["selection_id" => $value4->selectionId,"runner_name" => $value4->runnerName,"sort_priority" => $value4->sortPriority,"marketName" => $value3->marketName,"marketId" => $value3->marketId,"event_id" => $value2->event->id,"competition_id" => $value->competition->id,"event_type" => $b]
                    //               ]);
                    //             }
                    //         }


                    //         // }
                    // }    

                              





                    //  } 







             }





        }

    }









     // $tparams2 = '{"filter":{"eventTypeIds":["7"],"marketTypeCodes": ["WIN", "PLACE"]},
     //                            "maxResults": "100","id": 1}';

     //                            $result2 = listEvents($tparams2);





     // $curl3 = curl_init();

     //     curl_setopt_array($curl3, array(
     //       CURLOPT_URL => "http://128.199.24.35/v1-api/match/getCompetitions?sportId=7",
     //       CURLOPT_RETURNTRANSFER => true,
     //       // CURLOPT_ENCODING => "",
     //       CURLOPT_MAXREDIRS => 10,
     //       CURLOPT_TIMEOUT => 10,
     //       CURLOPT_FOLLOWLOCATION => true,
     //       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
     //       CURLOPT_CUSTOMREQUEST => "GET",
     //     ));

     //     $response3 = curl_exec($curl3);

     //     curl_close($curl3);


     //     $result3 = json_decode($response3);





     //         foreach ($result3 as $value2) {
                  
     //             $e = Event::where('event_id',$value2->event->id)->first();

                 // dd($value2->event->countryCode);

                 // if (empty($value2->event->countryCode)) {
                 //     $value2->event->countryCode = 'international';
                 // }
                    // if (!$e) {

                    //     \DB::table('events')->insert([
                    //        ["name" => $value2->event->name,"event_id" => $value2->event->id,"marketCount" => $value2->marketCount,"timezone" => $value2->event->timezone,"openDate" => date('Y-m-d h:i:s', strtotime($value2->event->openDate)),"countryCode" => $value2->event->countryCode,"event_type" => 7]
                    //     ]);
                    // }   



                    // $tparams3 = '{
                    // "filter":{"eventIds":["' . $value2->event->id . '"],"maxResults": "100"},
                    // "maxResults": "100","marketProjection":["RUNNER_METADATA","RUNNER_DESCRIPTION","MARKET_START_TIME"],"id": 1}';

                    // $result3 = listMarket($tparams3);

                    // foreach ($result3 as $value3) {
                          
                    //     $m = EventsMarket::where('marketId',$value3->marketId)->first();
                    //     if (!$m) {
                                

                    //         \DB::table('events_markets')->insert([
                    //            ["marketName" => $value3->marketName,"marketId" => $value3->marketId,"event_id" => $value2->event->id,"marketStartTime" => date('Y-m-d h:i:s', strtotime($value3->marketStartTime)),"event_type" => 7]
                    //         ]);

                    //         foreach ($value3->runners as $value4) {

                    //              $check = MarketRunner::where('selection_id',$value4->selectionId)->where('marketId',$value3->marketId)->first();

                    //             if(!$check){
                    //               \DB::table('market_runners')->insert([
                    //                  ["selection_id" => $value4->selectionId,"runner_name" => $value4->runnerName,"sort_priority" => $value4->sortPriority,"marketName" => $value3->marketName,"marketId" => $value3->marketId,"event_id" => $value2->event->id,"event_type" => 7]
                    //               ]);
                    //             }
                    //         }

                    //     }    

                    // }   

             // }








        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
