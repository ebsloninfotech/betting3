<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CompetitionCorn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'competition:corn';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get competition in every day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("competition Cron execution!");
        $this->info('competition:Cron Command is working fine!');
    }
}
