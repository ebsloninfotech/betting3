<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Event extends Model
{
	use SoftDeletes;



	public function selection()
    {
        return $this->hasOne('App\MarketRunner', 'selection_id','result')->where('marketName','Match Odds');
    }



	public function competition(){

		 return $this->hasOne('App\Competition', 'competition_id', 'competition_id');
	}




	public function oddmarket(){

		 return $this->hasOne('App\EventsMarket', 'event_id', 'event_id')->where('marketName','Match Odds');
	}







	public function market()
    {
        return $this->hasMany('App\EventsMarket', 'event_id','event_id')->where('marketName','To Be Placed')->where('status',1);
    }









	public function bets()
    {
        return $this->hasMany('App\Betplaced', 'event_id','event_id');
    }





}
