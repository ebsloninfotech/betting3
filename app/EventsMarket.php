<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EventsMarket extends Model
{

	use SoftDeletes;
    
	 public function event()
    {
        return $this->hasOne('App\Event', 'event_id', 'event_id');
    }




     public function runner1()
    {
        return $this->hasOne('App\MarketRunner', 'marketId', 'marketId')->where('marketName','Match Odds')->where('sort_priority','1');
    }

    
     public function runner()
    {
        return $this->hasMany('App\MarketRunner', 'marketId', 'marketId');
    }

     public function runner2()
    {
        return $this->hasOne('App\MarketRunner', 'marketId', 'marketId')->where('marketName','Match Odds')->where('sort_priority','2');
    }


     public function tied1()
    {
        return $this->hasOne('App\MarketRunner', 'marketId', 'marketId')->where('marketName','Tied Match')->where('sort_priority','1');
    }



     public function tied2()
    {
        return $this->hasOne('App\MarketRunner', 'marketId', 'marketId')->where('marketName','Tied Match')->where('sort_priority','2');
    }



      public function horserunner()
    {
        return $this->hasMany('App\MarketRunner', 'marketId', 'marketId');
    }




  public function selection()
    {
        return $this->hasOne('App\MarketRunner', 'selection_id','result')->where('marketName','Match Odds');
    }


}
