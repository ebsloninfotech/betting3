<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    

	public function index()
    {
        $news = News::all();
        return view('admin.news.index', compact('news'));
    }



    public function create()
    {
        return view('admin.news.create');
    }



    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'news' => 'required',
            ]
        );

        $data        = new News;
        $data->news = $request->news;
        $data->save();
        return redirect('master/news')->with('flash_message1', 'News Added Successfully.');
    }


     public function edit($id)
    {
        $news = News::findOrFail($id);
        return view('admin.news.edit',compact('news'));
    }



    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'news' => 'required',
            ]
        );

        $data = News::find($id);

        $data->news = $request->news;

        $data->update();
        return redirect('master/news')->with('flash_message1', 'news Updated Successfully.');
    }



    public function destroy($id)
    {
        $news = News::findOrFail($id);
        $news->delete();
        return redirect('master/news')->with('flash_message1', 'news Deleted Successfully.');
    }

    public function newsStatus(Request $request)
    {
        $id = $request->id;
        $data = News::find($id);
        if($data->status==1)
        {
            $data->status = 0;
        }
        else
        {
            $data->status = 1;
        }
        $data->update();
    }

}	
