<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DateTime;
use DateTimeZone;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activity_ar = [];
        $clientIP = request()->ip();
        if (isset(Auth::user()->id)) {
            $user = Auth::user();
            $url = 'http://ip-api.com/json/'.$clientIP;
            $data = json_decode(file_get_contents($url));

            $date = new DateTime("now", new DateTimeZone('Asia/Kolkata') );
            $now =  $date->format('Y-m-d H:i:s');

            $activity_ar['user_id'] = $user->id;
            $activity_ar['name'] = $user->name;
            $activity_ar['email'] = $user->email;
            $activity_ar['mobile_no'] = $user->mobile_no;
            $activity_ar['ipaddress'] = $clientIP;
            $activity_ar['country'] = $data->country;
            $activity_ar['countryCode'] = $data->countryCode;
            $activity_ar['regionName'] = $data->regionName;
            $activity_ar['city'] = $data->city;
            $activity_ar['zip'] = $data->zip;
            $activity_ar['time_zone'] = $data->timezone;
            $activity_ar['isp'] = $data->isp;
            $activity_ar['query'] = $data->query;
            $activity_ar['login'] = $now;
           activity()
                    ->causedBy($user)
                    ->withProperties($activity_ar)
                    ->log('Login to Website');
        }


        if(Auth::user()->hasRole('customer')){
            
            return redirect('/');

        }else{
            $rolename = Auth::user()->roles[0]->name;
            return redirect(''.$rolename.'/downline');
        }





    }
}
