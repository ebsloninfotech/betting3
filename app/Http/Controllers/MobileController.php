<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth; 
use App\Competition;
use App\Event;
use App\User;
use App\EventsMarket;
use App\Betplaced;
use App\Exposure;
use Carbon\Carbon;

class MobileController extends Controller
{
    


	public function index(){


		if (Auth::user()) {




			$cricket = EventsMarket::where('status',1)->where('marketName','Match Odds')->where('event_type',4)->where('result',null)->get();
			$soccer = EventsMarket::where('status',1)->where('marketName','Match Odds')->where('event_type',1)->where('result',null)->get();
			$tennis = EventsMarket::where('status',1)->where('marketName','Match Odds')->where('event_type',2)->where('result',null)->get();

			
			return view('mobile.index',compact('cricket','tennis','soccer'));

		}else{

			return view('mobile.login');

		}



	}



	public function inplay(){

		if (Auth::user()) {

			$currentTime = date('Y-m-d h:i:s');
			// $inplay = Event::whereDate('openDate','<',Carbon::now())->where('status',1)->where('result',null)->get();
			$today = Event::where('openDate','<',Carbon::now())->where('status',1)->where('result',null)->get();
			$tomorrow = Event::whereDate('openDate',Carbon::tomorrow())->where('status',1)->where('result',null)->get();
			

			// dd(checkInplayEvent($today[0]->event_id));
			return view('mobile.inplay',compact('today','tomorrow'));
		}else{

			return view('mobile.login');

		}



	}
	public function today(){

		if (Auth::user()) {

			$currentTime = date('Y-m-d h:i:s');
			// $today = Event::whereDate('openDate','<',Carbon::now())->where('status',1)->where('result',null)->get();
			$today = Event::where('openDate','<',Carbon::today())->where('status',1)->where('result',null)->get();
			$tomorrow = Event::whereDate('openDate',Carbon::tomorrow())->where('status',1)->where('result',null)->get();
					
			return view('mobile.today',compact('today','tomorrow'));
		}else{

			return view('mobile.login');

		}



	}
	public function tomorrow(){

		if (Auth::user()) {

			$currentTime = date('Y-m-d h:i:s');
			// $tomorrow = Event::whereDate('openDate','<',Carbon::now())->where('status',1)->where('result',null)->get();
			$today = Event::whereDate('openDate',Carbon::today())->where('status',1)->where('result',null)->get();
			$tomorrow = Event::whereDate('openDate',Carbon::tomorrow())->where('status',1)->where('result',null)->get();
					
			return view('mobile.tomorrow',compact('today','tomorrow'));
		}else{

			return view('mobile.login');

		}



	}




	public function account(){

		$id = Auth::user()->id;
		$user = User::find($id);
		return view('mobile.account',compact('user'));
	}
 	






	public function single_match_page($type,$event,$market){



		$totalMarket = EventsMarket::where('event_id',$event)->where('status',1)->get();
		$eventDetail = Event::where('event_id',$event)->where('status',1)->first();


		if(empty($totalMarket) || empty($eventDetail)){

			return redirect('mobile/index');
		}


		$user_id = Auth::user()->id;


			// $curl = curl_init();

   //       curl_setopt_array($curl, array(
   //         CURLOPT_URL => "http://178.62.105.201/api/v1/fetch_data?Action=listMarketRunner&MarketID=".$market."",
   //         CURLOPT_RETURNTRANSFER => true,
   //         // CURLOPT_ENCODING => "",
   //         CURLOPT_MAXREDIRS => 10,
   //         CURLOPT_TIMEOUT => 10,
   //         CURLOPT_FOLLOWLOCATION => true,
   //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
   //         CURLOPT_CUSTOMREQUEST => "GET",
   //       ));

   //       $response = curl_exec($curl);

   //       curl_close($curl);

   //       $MarketBook = json_decode($response);	


 			// $sparams2 = '{
		 	// 			"filter":{"marketIds":["' . $market . '"]},
		 	// 			"maxResults": "1",
		 	// 			"marketProjection": [ "EVENT","RUNNER_DESCRIPTION"
		 	// 			],"id": 1}';

		 	// 			$MarketBook = listMarket($sparams2);		    



		 $exposure = Exposure::where('event_id',$event)->where('market_id',$market)->where('user_id',$user_id)->first();


		$ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'http://128.199.24.35/v1-api/odds/getOdds/'.$market.'');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: VKbsu';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);



         $MarketBook = json_decode($result);


         	$ch2 = curl_init();

        curl_setopt($ch2, CURLOPT_URL, 'http://128.199.24.35/v1-api/fancy/getAllFancies/'.$market.'');
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, 'GET');


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: VKbsu';
        curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);

        $result2 = curl_exec($ch2);
        if (curl_errno($ch2)) {
            echo 'Error:' . curl_error($ch2);
        }
        curl_close($ch2);



         $fancy = json_decode($result2);

         // dd($fancy);

  //        if (is_array($MarketBook)) {
         	

		// if (count($MarketBook)>0) {
			
			return view('mobile.singleMatch',compact('totalMarket','eventDetail','type','event','market','MarketBook','exposure','fancy'));
		// }else{


			// foreach ($totalMarket as $value) {
			// 	$d = EventsMarket::find($value->id);
			// 	$d->delete();
			// }
			// $eventDetail->delete();
		// 	return redirect('/')->with('flash_message','Match Ended');
		// }

  //        }else{
         	
		// 	return redirect('/')->with('flash_message','Match Ended');
  //        }


	}




	public function getMyBets(){

		$user_id = Auth::user()->id;

		$data = Betplaced::where('user_id',$user_id)->where('status',1)->where('profit',null)->groupBy('selection_id')->with('selection')->get();


		return response(array([
    			'success'=>true,
    			'data'=>$data
    		]));


	}




	public function fetchSelectionBet(Request $request){

		$user_id = Auth::user()->id;

		$data = Betplaced::where('user_id',$user_id)->where('selection_id',$request->selection_id)->where('status',1)->with('selection')->get();

		return response(array([
    			'success'=>true,
    			'data'=>$data
    		]));

	}





	public function getBalance(){

		$user_id = Auth::user()->id;

		$wallet = number_format((float)get_wallet_balance(), 2, '.', '');
		$exposure = getTotalExposure();

		return response(array([
    			'success'=>true,
    			'wallet'=>$wallet,
    			'exposure'=>$exposure
    		]));


	}






}
