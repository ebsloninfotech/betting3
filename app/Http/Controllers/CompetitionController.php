<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Competition;
use App\Event;
use App\EventsMarket;
use App\MarketRunner;
use App\FancyBet;
use App\FancyBetPlaced;
use App\Betplaced;
use App\Exposure;
use App\Wallet;

class CompetitionController extends Controller
{
   	
   	public function list_comp($id){


	
		    $sport_id = $id;

		    $competition  = Competition::where('event_type',$id)->get();

		    return view('admin/competition/index',compact('competition','sport_id'));
   	}














   	 public function competitionStatus(Request $request)
    {
        $id = $request->id;
        $data = Competition::find($id);
        if($data->status==1)
        {
            $data->status = 0;
        }
        else
        {
            $data->status = 1;
        }
        $data->update();
    }




    public function list_event($type,$comp_id){


		    $event  = Event::where('competition_id',$comp_id)->with('selection')->get();

		    $competition = Competition::where('competition_id',$comp_id)->first();

		    // dd($event);

		    return view('admin/event/index',compact('event','competition'));


    	

    }


    public function list_market($type,$comp_id,$event_id){


    		// 	$p = '{
						// "filter":{"eventIds":[' . $event_id . '],"maxResults": "100"},
						// "maxResults": "100",
						// "marketProjection": [ "MARKET_START_TIME","RUNNER_DESCRIPTION"
						// ],"id": 1}';



            $ch3 = curl_init();

            curl_setopt($ch3, CURLOPT_URL, 'http://143.110.177.103:3000/v1-api/fancy/getBetFancy/'.$event_id.'');
            curl_setopt($ch3, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch3, CURLOPT_CUSTOMREQUEST, 'GET');



            $response4 = curl_exec($ch3);
            if (curl_errno($ch3)) {
                echo 'Error:' . curl_error($ch3);
            }
             curl_close($ch3);

             $market = json_decode($response4);

			$c = EventsMarket::where('id' ,'>' ,0)->where('event_id',$event_id)->pluck('marketId')->toArray();


		    foreach ($market->markets as $value) {
		    	if (in_array($value->marketId , $c)) {
		    		$data = EventsMarket::where('marketId',$value->marketId)->first();
		    		$data->marketName = $value->marketName;
		    		$data->event_type = $type;
		    		$data->competition_id = $comp_id;
		    		$data->marketStartTime =  date('Y-m-d h:i:s', strtotime($value->marketStartTime));
		    		$data->update();


		    		foreach ($value->runners as $value2) {
			    		$d = MarketRunner::where('selection_id',$value2->selectionId)->where('marketId',$value->marketId)->first();
			    		if ($d) {
			    			$d->event_id = $event_id;
			    			$d->marketName = $value->marketName;
			    			$d->marketId = $value->marketId;
				    		$d->event_type = $type;
				    		$d->competition_id = $comp_id;
				    		$d->runner_name = $value2->runnerName;
				    		$d->sort_priority = $value2->sortPriority;
				    		$d->update();
			    		}else{
			    			$d2 = new MarketRunner;
				    		$d2->selection_id = $value2->selectionId;
			    			$d2->event_id = $event_id;
			    			$d2->marketName = $value->marketName;
			    			$d2->marketId = $value->marketId;
				    		$d2->event_type = $type;
				    		$d2->competition_id = $comp_id;
				    		$d2->runner_name = $value2->runnerName;
				    		$d2->sort_priority = $value2->sortPriority;
				    		$d2->save();
			    		}
		    		}

		    	}else{
		    		$data = new EventsMarket;
		    		$data->event_id = $event_id;
		    		$data->marketId = $value->marketId;
		    		$data->marketName = $value->marketName;
		    		$data->event_type = $type;
		    		$data->competition_id = $comp_id;
		    		$data->marketStartTime = date('Y-m-d h:i:s', strtotime($value->marketStartTime));
		    		$data->save();


		    		foreach ($value->runners as $value2) {
			    			$d2 = new MarketRunner;
				    		$d2->selection_id = $value2->selectionId;
			    			$d2->event_id = $event_id;
			    			$d2->marketName = $value->marketName;
			    			$d2->marketId = $value->marketId;
				    		$d2->event_type = $type;
				    		$d2->competition_id = $comp_id;
				    		$d2->runner_name = $value2->runnerName;
				    		$d2->sort_priority = $value2->sortPriority;
				    		$d2->save();
		    		}
		    	}

		    }

		    $market  = EventsMarket::where('event_id',$event_id)->get();

		    $event = Event::where('event_id',$event_id)->first();


		    return view('admin/market/index',compact('event','market'));


    	

    }





    public function list_market_horse($type,$event_id){


                $p = '{
                        "filter":{"eventIds":[' . $event_id . '],"maxResults": "100"},
                        "maxResults": "100",
                        "marketProjection": [ "MARKET_START_TIME","RUNNER_DESCRIPTION"
                        ],"id": 1}';

                        $market = listMarket($p);
            $c = EventsMarket::where('id' ,'>' ,0)->where('event_id',$event_id)->pluck('marketId')->toArray();

            foreach ($market as $value) {
                if (in_array($value->marketId , $c)) {
                    $data = EventsMarket::where('marketId',$value->marketId)->first();
                    $data->marketName = $value->marketName;
                    $data->event_type = $type;
                    $data->marketStartTime = $value->marketStartTime;
                    $data->update();


                    foreach ($value->runners as $value2) {
                        $d = MarketRunner::where('selection_id',$value2->selectionId)->first();
                        if ($d) {
                            $d->event_id = $event_id;
                            $d->marketName = $value->marketName;
                            $d->marketId = $value->marketId;
                            $d->event_type = $type;
                            $d->runner_name = $value2->runnerName;
                            $d->sort_priority = $value2->sortPriority;
                            $d->update();
                        }else{
                            $d2 = new MarketRunner;
                            $d2->selection_id = $value2->selectionId;
                            $d2->event_id = $event_id;
                            $d2->marketName = $value->marketName;
                            $d2->marketId = $value->marketId;
                            $d2->event_type = $type;
                            $d2->runner_name = $value2->runnerName;
                            $d2->sort_priority = $value2->sortPriority;
                            $d2->save();
                        }
                    }

                }else{
                    $data = new EventsMarket;
                    $data->event_id = $event_id;
                    $data->marketId = $value->marketId;
                    $data->marketName = $value->marketName;
                    $data->event_type = $type;
                    $data->marketStartTime = $value->marketStartTime;
                    $data->save();


                    foreach ($value->runners as $value2) {
                            $d2 = new MarketRunner;
                            $d2->selection_id = $value2->selectionId;
                            $d2->event_id = $event_id;
                            $d2->marketName = $value->marketName;
                            $d2->marketId = $value->marketId;
                            $d2->event_type = $type;
                            $d2->runner_name = $value2->runnerName;
                            $d2->sort_priority = $value2->sortPriority;
                            $d2->save();
                    }
                }

            }

            $market  = EventsMarket::where('event_id',$event_id)->get();

            $event = Event::where('event_id',$event_id)->first();


            return view('admin/market/index',compact('event','market'));


        

    }











   	 public function eventStatus(Request $request)
    {
        $id = $request->id;
        $data = Event::find($id);
        if($data->status==1)
        {
            $data->status = 0;
            $market  = EventsMarket::where('event_id',$data->event_id)->get();
            foreach ($market as $value) {
            	$m = EventsMarket::find($value->id);
            	$m->status = 0;
            	$m->update();
            }

        }
        else
        {
            $data->status = 1;
            // $market  = EventsMarket::where('event_id',$data->event_id)->get();
            // dd($market);
            // foreach ($market as $value) {
            // 	$m = EventsMarket::find($value->id);
            // 	$m->status = 1;
            // 	$m->update();
            // }
        }
        $data->update();
    }





   	 public function eventBetStatus(Request $request)
    {
        $id = $request->id;
        $data = Event::find($id);
        if($data->bet_status==1)
        {
            $data->bet_status = 0;
            

        }
        else
        {
            $data->bet_status = 1;
            
        }
        $data->update();
    }








   	 public function marketStatus(Request $request)
    {
        $id = $request->id;
        $data = EventsMarket::find($id);
        if($data->status==1)
        {
            $data->status = 0;
        }
        else
        {
            $data->status = 1;
        }
        $data->update();
    }





    
    public function refreshComp($id){


    		$d = Competition::where('event_type',$id)->withTrashed()->get();
    		foreach ($d as  $value) {
    				
    				$e = Event::where('competition_id',$value->competition_id)->get();

    				foreach ($e as $v) {
    					$m = EventsMarket::where('event_id',$v->event_id)->get();

    					// foreach ($m as $market) {
    					// 	$dltMarket = EventsMarket::where('id',$market->id)->withTrashed()->first();
    					// 	$dltMarket->delete();
    					// }

    					$dltEvent = Event::where('id',$v->id)->withTrashed()->first();
    					$dltEvent->delete();
    				}

    			$dltCom = Competition::where('id',$value->id)->withTrashed()->first();
    			$dltCom->delete();	

    		}

    		// $d->delete();

    		// dd($d);

    		$params = '{"filter":{"eventTypeIds":["' . $id . '"],
		              "marketStartTime":{"from":"' . date('c') . '"}},
		               "sort":"FIRST_TO_START"}';

		    $comp = listCompetitions($params);
		  
		    foreach ($comp as $value) {

		    	$c = Competition::where('competition_id',$value->competition->id)->withTrashed()->first();
		    		
		    	if ($c){
		    		$c->name = $value->competition->name;
		    		$c->marketCount = $value->marketCount;
		    		$c->region = $value->competitionRegion;
		    		$c->deleted_at = null;
		    		$c->update();

		    		$ex = Event::where('competition_id',$c->competition_id)->withTrashed()->get();

		    		if (count($ex) > 0	) {

			    		// $cevent_id = '"'.implode('","',$ex).'"';

			    		foreach ($ex as $exvalue) {
			    			 $tparams2 = '{
								"filter":{"eventIds":["' . $exvalue->event_id . '"],"maxResults": "100"},
								"maxResults": "100","id": 1}';

								$fetchMarket = listEvents($tparams2);

								if ($fetchMarket) {
	    							
	    							$mx = EventsMarket::where('event_id',$exvalue->event_id)->withTrashed()->get();

	    							if($mx){


		    							foreach ($mx as $marketx) {

		    								$tparams2 = '{
											"filter":{"eventIds":["' . $exvalue->event_id . '"],"maxResults": "100"},
											"maxResults": "100","id": 1}';

											$fetchMarket2 = listMarket($tparams2);

											if ($fetchMarket2) {
												
					    						$mark = EventsMarket::where('id',$marketx->id)->withTrashed()->first();
					    						$mark->deleted_at = null;
					    						$mark->update();

											}

				    					}

				    					$even = Event::where('id',$exvalue->id)->withTrashed()->first();
				    					$even->deleted_at = null;
				    					$even->update();
										
	    							}
								}
			    		
			    		}


	    				// foreach ($ex as $vx) {

	    				// 	foreach ($mx as $marketx) {
	    				// 		$mark = EventsMarket::where('id',$marketx->id)->withTrashed()->first();
	    				// 		$mark->deleted_at = null;
	    				// 		$mark->update();
	    				// 	}

	    				// 	$even = Event::where('id',$vx->id)->withTrashed()->first();
	    				// 	$even->deleted_at = null;
	    				// 	$even->update();
	    				// }
		    		}

		    	}else{
		    		$data = new Competition;
		    		$data->competition_id = $value->competition->id;
		    		$data->event_type = $id;
		    		$data->name = $value->competition->name;
		    		$data->marketCount = $value->marketCount;
		    		$data->region = $value->competitionRegion;
		    		$data->save();
		    	}

		    }

		    return redirect()->back()->with('flash_message1','Success');

    }




    public function cancelBet($event_id){

    	$d = Exposure::where('event_id',$event_id)->get();


    	foreach ($d as $value) {
    		if($value->amount > $value->profit_amount){

    			if ($value->profit_amount < 0) {
    				
    				$user_id = $value->user_id;

    				$w = Wallet::where('user_id',$user_id)->first();
    				$w->wallet_amount = $w->wallet_amount - $value->profit_amount;
    				$w->update();
    			}


    		}else{
    			if ($value->amount < 0) {
    				
    				$user_id = $value->user_id;

    				$w = Wallet::where('user_id',$user_id)->first();
    				$w->wallet_amount = $w->wallet_amount - $value->amount;
    				$w->update();
    			}
    		}

    		$ex = Exposure::find($value->id);
    		$ex->delete();

    	}

    	$b = Betplaced::where('event_id',$event_id)->delete();

    	return redirect()->back()->with('flash_message1','All Bet Cancelled');

    }


    public function settleBet($market_id){

    	$d = Exposure::where('market_id',$market_id)->get();
    	

    	$market = EventsMarket::where('marketId',$market_id)->first();

    	if($market->result){

	    	foreach ($d as $value) {
	    		if($value->selection_id == $market->result){

		    		// if($value->amount < $value->profit_amount){

		    		// 	if ($value->profit_amount > 0) {
		    				
		    		// 		$user_id = $value->user_id;

		    		// 		$w = Wallet::where('user_id',$user_id)->first();
		    		// 		$w->wallet_amount = $w->wallet_amount + $value->profit_amount;
		    		// 		$w->update();
		    		// 	}


		    		// }else{
		    		// 	if ($value->amount > 0) {
		    				
		    		// 		$user_id = $value->user_id;

		    		// 		$w = Wallet::where('user_id',$user_id)->first();
		    		// 		$w->wallet_amount = $w->wallet_amount + $value->amount;
		    		// 		$w->update();
		    		// 	}
		    		// }

		    		$ex = Exposure::find($value->id);
		    		$ex->delete();
	    		}

	    	}

	    	$b = Betplaced::where('market_id',$market_id)->get();

            $user = Betplaced::where('market_id',$market_id)->groupBy('user_id')->get();


            foreach ($user as $u) {
                
                $totalLose = userEventPL($u->user_id,$market->result,$market_id);

                if ($totalLose['status'] == 'profit' ) {



                    $commission_type = $market->comm_type;
                    $commission_value = $market->comm_value;

                    if($commission_value>0){


                        if($commission_type == 'credit'){

                            $new_profit = $totalLose['net'] + (($totalLose['net']*$commission_value)/100);

                        }else{


                            $new_profit = $totalLose['net'] - (($totalLose['net']*$commission_value)/100);

                        }

                    }else{
                        $new_profit = $totalLose['net'];
                    }


                    $w = Wallet::where('user_id',$u->user_id)->first();
                    $w->wallet_amount = $w->wallet_amount + $new_profit;
                    $w->update();
                }

            }



	    	foreach($b as $be){
	    		$bet = Betplaced::find($be->id);

	    		if($bet->selection_id == $market->result){

	    			if ($bet->bet_type == 'back') {
	    				$profit = ($bet->odds * $bet->bet_amount) - $bet->bet_amount;
	    				$profit_type = 'win';
	    			}else{

	    				$profit = ($bet->odds * $bet->bet_amount) - $bet->bet_amount;
	    				$profit_type = 'lose';
	    			}


	    		}else{

	    			if ($bet->bet_type == 'back') {
	    				$profit = $bet->bet_amount;
	    				$profit_type = 'lose';
	    			}else{

	    				$profit = $bet->bet_amount;
	    				$profit_type = 'win';
	    			}
	    		}


                if($profit_type == 'win'){


                    $commission_type = $market->comm_type;
                    $commission_value = $market->comm_value;

                    if($commission_value>0){


                        if($commission_type == 'credit'){

                            $new_profit = $profit + (($profit*$commission_value)/100);

                            $bet->commission_type = 'credit';
                            $bet->commission_value = ($profit*$commission_value)/100;

                        }else{


                            $new_profit = $profit - (($profit*$commission_value)/100);

                            $bet->commission_type = 'debit';
                            $bet->commission_value = ($profit*$commission_value)/100;

                        }

                    }else{
                        $new_profit = $profit;
                    }
                }else{
                    $new_profit = $profit;
                }

	    		$bet->betstatus = 'settled';
	    		$bet->profit = $new_profit;
	    		$bet->profit_type = $profit_type;

	    		$bet->update();
	    	}




            $market->result_status = 'settled';

            $market->update();

	    	return redirect()->back()->with('flash_message1','All Bet Settled');

    	}else{


	    	return redirect()->back()->with('flash_message','Please Update Result First !!!');



    	}
    }


    public function cancelsettleBet($market_id){


        $d = Exposure::where('market_id',$market_id)->get();
        

        $market = EventsMarket::where('marketId',$market_id)->first();

        if($market->result){

            

            $b = Betplaced::where('market_id',$market_id)->get();

            $user = Betplaced::where('market_id',$market_id)->groupBy('user_id')->get();


            foreach ($user as $u) {
                
                $totalLose = userEventPL($u->user_id,$market->result,$market_id);

                if ($totalLose['status'] == 'profit' ) {



                    $commission_type = $market->comm_type;
                    $commission_value = $market->comm_value;

                    if($commission_value>0){


                        if($commission_type == 'credit'){

                            $new_profit = $totalLose['net'] + (($totalLose['net']*$commission_value)/100);

                        }else{


                            $new_profit = $totalLose['net'] - (($totalLose['net']*$commission_value)/100);

                        }

                    }else{
                        $new_profit = $totalLose['net'];
                    }


                    $w = Wallet::where('user_id',$u->user_id)->first();
                    $w->wallet_amount = $w->wallet_amount - $new_profit;
                    $w->update();
                }

            }



            foreach($b as $be){
                $bet = Betplaced::find($be->id);

            

                $bet->betstatus = null;
                $bet->profit = null;
                $bet->profit_type = null;
                $bet->commission_type = null;
                $bet->commission_value = null;

                $bet->update();
            } 

            $market->result = null;
            $market->result_status = null;

            $market->update();

            return redirect()->back()->with('flash_message1','All Settled Bet Are Cancelled');

        }else{


            return redirect()->back()->with('flash_message','Please Update Result First !!!');



        }
    }





    public function fancyBet($event_id){

         	$event = Event::where('event_id',$event_id)->first();


        $market = $event->market_id;


        $ch2 = curl_init();

        curl_setopt($ch2, CURLOPT_URL, 'http://128.199.24.35/v1-api/fancy/getAllFancies/'.$market.'');
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, 'GET');


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: VKbsu';
        curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);

        $result2 = curl_exec($ch2);
        if (curl_errno($ch2)) {
            echo 'Error:' . curl_error($ch2);
        }
        curl_close($ch2);



         $fancy = json_decode($result2);
        if(property_exists($fancy,'diamond')) {
             if(count($fancy->diamond)>0){
                 foreach ($fancy->diamond as $value) {
    	         	
    	         	$data = FancyBet::where('event_id',$market)->where('SelectionId',$value->sid)->first();
                if (empty($data)) {
                    $d = new FancyBet;
                    $d->SelectionId = $value->sid;
                    $d->event_id = $market;
                    $d->RunnerName = $value->nat;
                    $d->LayPrice1 = $value->l1;
                    $d->LaySize1 = $value->ls1;
                    $d->BackPrice1 = $value->b1;
                    $d->BackSize1 = $value->bs1;
                    $d->GameStatus = $value->gstatus;
                    $d->MarkStatus = $value->gtype;
                    $d->save();
                }

             }
         }
        }


         	$fancyBet = FancyBet::where('event_id',$market)->get();
         	return view('admin/event/fancy_bet',compact('event','fancyBet'));
    }



    public function fancyResult(Request $request){

    	$data = FancyBet::find($request->pk);
    	$data->result = $request->value;
    	$data->update();

    	echo 'Success';
    }


    public function fancySettle($SelectionId){

    	$data = FancyBet::where('SelectionId',$SelectionId)->first();



    	if ($data->result) {
    		
    		$d = Betplaced::where('selection_id',$SelectionId)->get();
    		$b = FancyBetPlaced::where('selectionId',$SelectionId)->get();


    		foreach ($d as $dvalue) {
    			if($dvalue->bet_type == 'back'){

    				$profit = ($dvalue->odds * $dvalue->bet_amount)/100;
    				$lose = $dvalue->bet_amount;

    				$upd = Betplaced::find($dvalue->id);



    				if ($dvalue->fancy_price <= $data->result) {
    					$upd->profit_type = 'win';


                        $commission_type = $data->comm_type;
                        $commission_value = $data->comm_value;

                        if($commission_value>0){


                            if($commission_type == 'credit'){

                                $new_profit = $profit + $dvalue->bet_amount + ((($profit+$dvalue->bet_amount)*$commission_value)/100);

                            }else{


                                $new_profit = $profit + $dvalue->bet_amount - ((($profit+$dvalue->bet_amount)*$commission_value)/100);

                            }

                        }else{
                            $new_profit = $profit +$dvalue->bet_amount;
                        }

    					$upd->profit = $new_profit;

                        $upd->commission_type = $commission_type;
                        $upd->commission_value = $commission_value;
    					$user = Wallet::where('user_id',$dvalue->user_id)->first();

    					$user->wallet_amount = $user->wallet_amount + $new_profit ;
    					$user->update();

    				}else{

    					$upd->profit_type = 'lose';
    					$upd->profit = $lose;

    				}
    				
    				$upd->betstatus = 'settled';
    				$upd->update();
    			}else{

    				$profit = $dvalue->bet_amount;
    				$lose = ($dvalue->odds * $dvalue->bet_amount)/100;

    				$upd = Betplaced::find($dvalue->id);

    				if ($dvalue->fancy_price > $data->result) {
    					$upd->profit_type = 'win';
    					$commission_type = $data->comm_type;
                        $commission_value = $data->comm_value;

                        if($commission_value>0){


                            if($commission_type == 'credit'){

                                $new_profit = $profit + $dvalue->bet_amount + ((($profit+$dvalue->bet_amount)*$commission_value)/100);

                            }else{


                                $new_profit = $profit + $dvalue->bet_amount - ((($profit+$dvalue->bet_amount)*$commission_value)/100);

                            }

                        }else{
                            $new_profit = $profit +$dvalue->bet_amount;
                        }

                        $upd->profit = $new_profit;
                        $upd->commission_type = $commission_type;
                        $upd->commission_value = $commission_value;
                        $user = Wallet::where('user_id',$dvalue->user_id)->first();

                        $user->wallet_amount = $user->wallet_amount + $new_profit ;
                        $user->update();

    				}else{

    					$upd->profit_type = 'lose';
    					$upd->profit = $lose;

    				}
    				
    				$upd->betstatus = 'settled';
    				$upd->update();


    			}
    		}	


    		foreach ($b as $value) {
    			
    			if($value->type == 'back'){

    				$profit = ($value->size * $value->stake)/100;
    				$lose = $value->stake;

    				$upd = FancyBetPlaced::find($value->id);



    				if ($value->prize <= $data->result) {
    					$upd->profit_status = 'win';

                        $commission_type = $data->comm_type;
                        $commission_value = $data->comm_value;

                        if($commission_value>0){


                            if($commission_type == 'credit'){

                                $new_profit = $profit  + (($profit*$commission_value)/100);

                            }else{


                                $new_profit = $profit  - (($profit*$commission_value)/100);

                            }

                        }else{
                            $new_profit = $profit;
                        }


    					$upd->profit = $new_profit;

                        $upd->commission_type = $commission_type;
                        $upd->commission_value = $commission_value;

    					// $user = Wallet::where('user_id',$value->user_id)->first();

    					// $user->wallet_amount = $user->wallet_amount + $profit;
    					// $user->update();

    				}else{

    					$upd->profit_status = 'lose';
    					$upd->profit = $lose;

    				}
    				$upd->bet_status = 'settled';
    				$upd->update();
    			}else{

    				$profit = $value->stake;
    				$lose = ($value->size * $value->stake)/100;

    				$upd = FancyBetPlaced::find($value->id);


    				if ($value->prize > $data->result) {
    					$upd->profit_status = 'win';
    					$commission_type = $data->comm_type;
                        $commission_value = $data->comm_value;

                        if($commission_value>0){


                            if($commission_type == 'credit'){

                                $new_profit = $profit  + (($profit*$commission_value)/100);

                            }else{


                                $new_profit = $profit  - (($profit*$commission_value)/100);

                            }

                        }else{
                            $new_profit = $profit;
                        }


                        $upd->profit = $new_profit;

    					// $user = Wallet::where('user_id',$value->user_id)->first();

    					// $user->wallet_amount = $user->wallet_amount + $profit;
    					// $user->update();

                        $upd->commission_type = $commission_type;
                        $upd->commission_value = $commission_value;

    				}else{

    					$upd->profit_status = 'lose';
    					$upd->profit = $lose;

    				}



    				$upd->bet_status = 'settled';
    				$upd->update();


    			}


    		}

    		$data->resultStatus = 'settled';

    		$data->update();
    		return redirect()->back()->with('flash_message1','Bet Settled Successfully !!!');
    	}else{
    		return redirect()->back()->with('flash_message','Please Update Result First!!!');
    	}
    	
    }






    public function cancelFancyBet($selectionId){

    	$data = FancyBet::where('SelectionId',$selectionId)->first();



    	if ($data->result) {
    		
    		$d = Betplaced::where('selection_id',$selectionId)->get();
    		$b = FancyBetPlaced::where('selectionId',$selectionId)->get();


    		foreach ($d as $value) {
    			$val = Betplaced::find($value->id);
    			$val->betstatus = 'voided';
    			$val->delete();
    		}


    		foreach ($b as $value2) {
    			if ($value2->type == 'back') {
    				$return = $value2->stake;
    			}else{

    				$return = ($value2->size * $value2->stake)/100;
    			}


    			$user = Wallet::where('user_id',$value2->user_id)->first();
    			$user->wallet_amount = $user->wallet_amount + $return;
    			$user->update();

    			$bet = FancyBetPlaced::find($value2->id);
    			$bet->bet_status = 'voided';
    			$bet->update();
    		}

    	}

    	$data->resultStatus = 'voided';
    	$data->update();
    	
    	return redirect()->back()->with('flash_message1','Bet Cancelled Successfully !!!');

    }









    public function cancelfancySettle($SelectionId){

        $data = FancyBet::where('SelectionId',$SelectionId)->first();



        if ($data->result) {
            
            $d = Betplaced::where('selection_id',$SelectionId)->get();
            $b = FancyBetPlaced::where('selectionId',$SelectionId)->get();


            foreach ($d as $dvalue) {
                if($dvalue->bet_type == 'back'){

                    $profit = ($dvalue->odds * $dvalue->bet_amount)/100;
                    $lose = $dvalue->bet_amount;

                    $upd = Betplaced::find($dvalue->id);



                    if ($dvalue->fancy_price <= $data->result) {
                        $upd->profit_type = null;
                        $upd->profit = null;

                        $commission_type = $dvalue->commission_type;
                        $commission_value = $dvalue->commission_value;

                        if($commission_value>0){


                            if($commission_type == 'credit'){

                                $new_profit = $profit +$dvalue->bet_amount  + (($profit +$dvalue->bet_amount*$commission_value)/100);

                            }else{


                                $new_profit = $profit +$dvalue->bet_amount  - (($profit +$dvalue->bet_amount*$commission_value)/100);

                            }

                        }else{
                            $new_profit = $profit +$dvalue->bet_amount;
                        }



                        $user = Wallet::where('user_id',$dvalue->user_id)->first();

                        $user->wallet_amount = $user->wallet_amount - $new_profit;
                        $user->update();

                    }else{

                        $upd->profit_type = null;
                        $upd->profit = null;

                    }
                    $upd->commission_type = null;
                        $upd->commission_value = null;
                    $upd->betstatus = null;
                    $upd->update();
                }else{

                    $profit = $dvalue->bet_amount;
                    $lose = ($dvalue->odds * $dvalue->bet_amount)/100;

                    $upd = Betplaced::find($dvalue->id);

                    if ($dvalue->fancy_price > $data->result) {
                        $upd->profit_type = null;
                        $upd->profit = null;
                        
                        $commission_type = $dvalue->commission_type;
                        $commission_value = $dvalue->commission_value;

                        if($commission_value>0){


                            if($commission_type == 'credit'){

                                $new_profit = $profit +$dvalue->bet_amount  + (($profit +$dvalue->bet_amount*$commission_value)/100);

                            }else{


                                $new_profit = $profit +$dvalue->bet_amount  - (($profit +$dvalue->bet_amount*$commission_value)/100);

                            }

                        }else{
                            $new_profit = $profit +$dvalue->bet_amount;
                        }



                        $user = Wallet::where('user_id',$dvalue->user_id)->first();

                        $user->wallet_amount = $user->wallet_amount - $new_profit;
                        $user->update();

                    }else{

                        $upd->profit_type = null;
                        $upd->profit = null;

                    }
                    $upd->commission_type = null;
                    $upd->commission_value = null;
                    $upd->betstatus = null;
                    $upd->update();


                }
            }   


            foreach ($b as $value) {
                
                if($value->type == 'back'){

                    $profit = ($value->size * $value->stake)/100;
                    $lose = $value->stake;

                    $upd = FancyBetPlaced::find($value->id);



                    if ($value->prize <= $data->result) {
                        $upd->profit_status = null;
                        $upd->profit = null;

                        // $user = Wallet::where('user_id',$value->user_id)->first();

                        // $user->wallet_amount = $user->wallet_amount + $profit;
                        // $user->update();

                    }else{

                        $upd->profit_status = null;
                        $upd->profit = null;

                    }
                    $upd->bet_status = null;
                    $upd->commission_type = null;
                        $upd->commission_value = null;
                    $upd->update();
                }else{

                    $profit = $value->stake;
                    $lose = ($value->size * $value->stake)/100;

                    $upd = FancyBetPlaced::find($value->id);


                    if ($value->prize > $data->result) {
                        $upd->profit_status = null;
                        $upd->profit = null;

                        // $user = Wallet::where('user_id',$value->user_id)->first();

                        // $user->wallet_amount = $user->wallet_amount + $profit;
                        // $user->update();

                    }else{

                        $upd->profit_status = null;
                        $upd->profit = null;

                    }

                    $upd->commission_type = null;
                    $upd->commission_value = null; 
                    $upd->bet_status = null;
                    $upd->update();


                }


            }

            $data->result = null;
            $data->resultStatus = null;

            $data->update();
            return redirect()->back()->with('flash_message1','Settlement Cancelled Successfully !!!');
        }else{
            return redirect()->back()->with('flash_message','Please Update Result First!!!');
        }
        
    }









        public function marketMinBet(Request $request){

            $data = EventsMarket::find($request->pk);
            $data->min_bet = $request->value;
            $data->update();

            echo 'Success';
        }


        public function marketMaxBet(Request $request){

            $data = EventsMarket::find($request->pk);
            $data->max_bet = $request->value;
            $data->update();

            echo 'Success';
        }







        public function marketcommType(Request $request){

            $data = EventsMarket::find($request->pk);
            $data->comm_type = $request->value;
            $data->update();

            echo 'Success';
        }


        public function marketcommValu(Request $request){

            $data = EventsMarket::find($request->pk);
            $data->comm_value = $request->value;
            $data->update();

            echo 'Success';
        }





        public function fancyMinBet(Request $request){

            $data = FancyBet::find($request->pk);
            $data->min_bet = $request->value;
            $data->update();

            echo 'Success';
        }


        public function fancyMaxBet(Request $request){

            $data = FancyBet::find($request->pk);
            $data->max_bet = $request->value;
            $data->update();

            echo 'Success';
        }







        public function fancycommType(Request $request){

            $data = FancyBet::find($request->pk);
            $data->comm_type = $request->value;
            $data->update();

            echo 'Success';
        }


        public function fancycommValu(Request $request){

            $data = FancyBet::find($request->pk);
            $data->comm_value = $request->value;
            $data->update();

            echo 'Success';
        }






}
