<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }




    public function username(){
    
         return 'username';
    
    }


    // public function login(Request $request)
    // {   
    //     $input = $request->all();
  
    //     $this->validate($request, [
    //         'username' => 'required',
    //         'password' => 'required',
    //     ]);
  
    //     $fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
    //     if(auth()->attempt(array($fieldType => $input['username'], 'password' => $input['password'])))
    //     {
    //         return redirect()->route('home');
    //     }else{
    //         return redirect()->route('login')
    //             ->with('error','Email-Address And Password Are Wrong.');
    //     }
          
    // }




    protected function authenticated(){

        $user = \Auth::user();
        if($user->status == 2){
            \Auth::logout();

            return redirect('/')->with('flash_message','Account is Suspended, Please contact admin!');
        }else{
            if($user->status == 3){

            \Auth::logout();
                return redirect('/')->with('flash_message','Account is Locked, Please contact admin!');
            }   
        }
    	session()->push('auth_status', 'logout');
        \Auth::logoutOtherDevices(request('password'));
    
    }





}
