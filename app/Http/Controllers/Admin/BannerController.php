<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Banner;
use Illuminate\Http\Request;

class BannerController extends Controller
{
  public function index()
    {
        $slider = Slider::all();
        return view('admin.sliders.index', compact('slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'title' => 'required',
                'tagline' => 'required',

                'slider_img.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]
        );

        $data = new Slider;

        $data->title = $request->title;
        $data->tagline = $request->tagline;
        $data->rlink = $request->rlink;

        if($request->hasfile('slider_img'))
        {
            $image = $request->file('slider_img');
            $slider_img = $image->getClientOriginalName();
            $path = 'storage/images/slider/';
            $upload = $image->move($path, $slider_img);
            $data->slider_img = $slider_img;
        }
        $data->save();
        return redirect('admin/slider')->with('flash_message', 'Slider Added Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slider = Slider::findOrFail($id);
        return view('admin.sliders.show', compact('slider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::findOrFail($id);
        return view('admin.sliders.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'title' => 'required',
                'tagline' => 'required',

                'slider_img.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]
        );

        $data = Slider::find($id);

        $data->title = $request->title;
        $data->tagline = $request->tagline;
        $data->rlink = $request->rlink;

        if($request->hasfile('slider_img'))
        {
            $image = $request->file('slider_img');
            $slider_img = $image->getClientOriginalName();
            $path = 'storage/images/slider/';
            $upload = $image->move($path, $slider_img);
            $data->slider_img = $slider_img;
        }

        $data->update();
        return redirect('admin/slider')->with('flash_message', 'Slider Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);
        $slider->delete();
        return redirect('admin/slider')->with('flash_message', 'Slider Deleted Successfully.');
    }

    public function sliderStatus(Request $request)
    {
        $id = $request->id;
        $data = Slider::find($id);
        if($data->status==1)
        {
            $data->status = 0;
        }
        else
        {
            $data->status = 1;
        }
        $data->update();
    }
	}