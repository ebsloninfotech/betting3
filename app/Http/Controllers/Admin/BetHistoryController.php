<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\User;
use App\Bet;
use App\UserParent;
use App\Betplaced;
use Auth;
use Hash;



class BetHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $id = Auth::user()->id;

        $data = User::find($id);



      if(!empty($request->get('sports'))){
        
        $bet_sports = $request->get('sports');
      }else{

        $bet_sports = 4;
      }  

      if(!empty($request->get('bet_from'))){
        $lastmonth = $request->get('bet_from');
      }else{
      $lastmonth = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

      }
      if(!empty($request->get('bet_to'))){

        $current_date = $request->get('bet_to');
      }else{
        $current_date = date('Y-m-d');

      }


      if(!empty($request->get('bet_status'))){
        $bet_status = $request->get('bet_status');
        
      }else{
        $bet_status = 'matched';

      }
      // $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();

      if($bet_status == 'matched'){
        $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit',null)->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->get();  
      }else{
        if($bet_status == 'settled'){

          $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit','!=',null)->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->get();  
        }else{
          $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->onlyTrashed()->get();  
          
        }
      }

      

        // $bets = Betplaced::where('user_id',$user_id)->get();

        return view('admin.bethistory.index',compact('data','bets','bet_sports','current_date','lastmonth','bet_status'));
    }




    public function profitLoss(Request $request)
    {

        $id = Auth::user()->id;

        $data = User::find($id);



      if(!empty($request->get('bet_from'))){
        $lastmonth = $request->get('bet_from');
      }else{
      $lastmonth = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

      }
      if(!empty($request->get('bet_to'))){

        $current_date = $request->get('bet_to');
      }else{
        $current_date = date('Y-m-d');

      }

      // $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();

      
        $bets = Betplaced::where('user_id',$id)->where('betstatus','settled')->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->groupBy('market_id')->get();  


        // $bets = Betplaced::where('user_id',$user_id)->get();

        return view('admin.bethistory.profit_lose',compact('data','bets','current_date','lastmonth'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function UpdateBitConfig(Request $request){
        $payer_id = Auth::id();
        $receiver_id = $request->receiver_id;
        $rec_name= User::find($receiver_id);
        $data='';
        $bets = Bet::where('payer_id',$payer_id)->where('receiver_id',$receiver_id)->whereStatus(1)->get();
        if(count($bets)>0){
            $data .='<div id="bet_config" class="modal fade" role="dialog">
               <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                     <form id="add_bet_config" method="post" accept-charset="utf-8">
                        <input type="hidden" name="payer_id" value="'.$payer_id.'" @endif>
                           <input type="hidden" name="receiver_id" value="'.$receiver_id.'"class="receiver_id" >
                        <div class="modal-header">
                           <h4 class="modal-title">Update Bet Configuration- '.$rec_name->name.'</h4>
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                           <table class="table table-borderless">
                                 <thead>
                                    <tr>
                                       <th>Event Type</th>
                                       <th>Min Bet</th>
                                       <th>Max Bet</th>
                                       <th>Bet Delay</th>
                                       <th>Exposure</th>
                                       <th>Profit</th>
                                    </tr>
                                 </thead>
                                 <tbody>';
                                 foreach($bets as $bet){
                                    $data .='<tr>
                                        <input type="hidden" name="bet_id[]" value="'.$bet->id.'">
                                       <td>Soccer<input type="hidden" name="event_type[]" value="'.$bet->event_type.'"></td>
                                       <td><input type="text" name="min[]" value="'.$bet->min.'" size="5"></td>
                                       <td><input type="text" name="max[]" value="'.$bet->max.'" size="5"></td>
                                       <td><input type="text" name="bet_delay[]" value="'.$bet->bet_delay.'" size="5"></td>
                                       <td><input type="text" name="exposure[]" value="'.$bet->exposure.'" size="5"></td>
                                       <td><input type="text" name="profit[]" value="'.$bet->profit.'" size="5"></td>
                                    </tr>';
                                }
                                $data .='</tbody>
                              </table>
                        </div>
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-primary">Save</button>
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <p id="res_add-bet-config"></p>
                        </div>
                     </form>
                  </div>
               </div>
            </div>';
        }else{
            $data .='<div id="bet_config" class="modal fade" role="dialog">
               <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                     <form id="add_bet_config" method="post" accept-charset="utf-8">
                        <input type="hidden" name="payer_id" value="'.$payer_id.'" @endif>
                           <input type="hidden" name="receiver_id" value="'.$receiver_id.'"class="receiver_id" >
                        <div class="modal-header">
                           <h4 class="modal-title">Add Bet Configuration- '.$rec_name->name.'</h4>
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                           <table class="table table-borderless">
                                 <thead>
                                    <tr>
                                       <th>Event Type</th>
                                       <th>Min Bet</th>
                                       <th>Max Bet</th>
                                       <th>Bet Delay</th>
                                       <th>Exposure</th>
                                       <th>Profit</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>Soccer<input type="hidden" name="event_type[]" value="Soccer"></td>
                                       <td><input type="text" name="min[]" value="10" size="5"></td>
                                       <td><input type="text" name="max[]" value="80" size="5"></td>
                                       <td><input type="text" name="bet_delay[]" value="5" size="5"></td>
                                       <td><input type="text" name="exposure[]" value="40" size="5"></td>
                                       <td><input type="text" name="profit[]" value="10" size="5"></td>
                                    </tr>
                                    <tr>
                                       <td>Tennis<input type="hidden" name="event_type[]" value="Tennis"></td>
                                       <td><input type="text" name="min[]" value="10" size="5"></td>
                                       <td><input type="text" name="max[]" value="80" size="5"></td>
                                       <td><input type="text" name="bet_delay[]" value="5" size="5"></td>
                                       <td><input type="text" name="exposure[]" value="40" size="5"></td>
                                       <td><input type="text" name="profit[]" value="10" size="5"></td>
                                    </tr>
                                    <tr>
                                       <td>Cricket<input type="hidden" name="event_type[]" value="Cricket"></td>
                                       <td><input type="text" name="min[]" value="10" size="5"></td>
                                       <td><input type="text" name="max[]" value="80" size="5"></td>
                                       <td><input type="text" name="bet_delay[]" value="5" size="5"></td>
                                       <td><input type="text" name="exposure[]" value="40" size="5"></td>
                                       <td><input type="text" name="profit[]" value="10" size="5"></td>
                                    </tr>
                                 </tbody>
                              </table>
                        </div>
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-primary">Save</button>
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <p id="res_add-bet-config"></p>
                        </div>
                     </form>
                  </div>
               </div>
            </div>';
        }
        $data .= '<script>
                $("#bet_config").modal("show");
                $("#add_bet_config").on("submit", function(e){
               e.preventDefault();
               $.ajax({
                 type: "post",
                 url: "add-bet-config",
                 headers: {
                   "X-CSRF-TOKEN": $("meta[name='."csrf-token".']").attr("content")
               },
                 data: $("#add_bet_config").serialize(),
                 success: function(res) {
                   if (res.success) {
                    $("#res_add-bet-config").html(res.message);
                        setTimeout(function(){
                            window.location.reload(true);
                      }, 2000);
                  }else{
                     $("#res_add-bet-config").html(res.message);
                  }
                 }
               });
            });
            </script>';

        return response(array('success'=>true,'message'=>$data));
    }

    public function AddBetConfig(Request $request){
        $payer_id = $request->payer_id;
        $receiver_id = $request->receiver_id;
        for ($i=0; $i <3 ; $i++) {
            if(!empty($request->bet_id[$i])){
                $data               = Bet::find($request->bet_id[$i]);
                $data->min          = $request->min[$i];
                $data->max          = $request->max[$i];
                $data->bet_delay    = $request->bet_delay[$i];
                $data->exposure     = $request->exposure[$i];
                $data->profit       = $request->profit[$i];
                $data->update();
            }else{
                $data               = new Bet;
                $data->payer_id     = $payer_id;
                $data->receiver_id  = $receiver_id;
                $data->event_type   = $request->event_type[$i];
                $data->min          = $request->min[$i];
                $data->max          = $request->max[$i];
                $data->bet_delay    = $request->bet_delay[$i];
                $data->exposure     = $request->exposure[$i];
                $data->profit       = $request->profit[$i];
                $data->status       = 1;
                $data->save();
            }
        }

        return response(array('success'=>true,'message'=>'<span style="font-size:16px;color:green">Add/Update Bet Configuration Successfull!</span>'));
    }








    public function livebets(){

      $id = Auth::user()->id;

      $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();
      $bets = Betplaced::where('user_id',$child)->where('profit',null)->get();  

      return view('admin.bethistory.livebets',compact('bets'));

    }






    public function setteledBets(){

      $id = Auth::user()->id;

      $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();
      $bets = Betplaced::where('user_id',$child)->where('profit','!=',null)->get();  
      return view('admin.bethistory.betscompeleted',compact('bets'));

    }



    public function settle_bet(Request $request){

      $bet_id = $request->id;
      $d = Betplaced::find($bet_id);

      $profit = $d->bet_amount * $d->odds;

      $lost = $d->bet_amount;

      $data = '';

      $data .='<div id="myactive" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Settle Bet ( '.$d->event_name.' )</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        <div class="error"></div>
                        <form action="#" id="settlementForm" method="post">     
                        <input type="hidden" name="bet_id" value="'.$bet_id.'">
                            <div class="form-group">                
                            <label class="row width-100">
                              <div class="col-md-1">                
                                <input type="radio" required name="status" value="win" checked="checked">
                              </div>
                            
                            <div class="col-md-3 bg-primary border-1">
                              <div>Profit</div>
                              <span>'.$profit.'</span>
                            </div>
                            <div class="col-md-3 bg-primary border-1">
                              <div>Amount</div>
                              <span>'.$d->bet_amount.'</span>
                            </div>
                            <div class="col-md-3 bg-primary border-1">
                              <div>Odds</div>
                              <span>'.$d->odds.'</span>
                            </div>
                            <div class="col-md-2 bg-primary border-1">
                              <div>Type</div>
                              <span>'.$d->bet_type.'</span>
                            </div>
                            </label>
                            <br>
                          </div>
                          <div class="form-group">                
                            <label class="row width-100">
                              <div class="col-md-1">
                                <input type="radio" required name="status" value="lost"> 
                              </div>
                              <div class="col-md-3 bg-danger border-1">
                              <div>Lost</div>
                              <span>'.$lost.'</span>
                            </div>
                            <div class="col-md-3 bg-danger border-1">
                              <div>Amount</div>
                              <span>'.$d->bet_amount.'</span>
                            </div>
                            <div class="col-md-3 bg-danger border-1">
                              <div>Odds</div>
                              <span>'.$d->odds.'</span>
                            </div>
                            <div class="col-md-2 bg-danger border-1">
                              <div>Type</div>
                              <span>'.$d->bet_type.'</span>
                            </div>

                            </label>
                            <br>
                          </div>              
                            <div class="form-group">                
                            <label for="">Enter Password:</label>
                            <input type="password" required name="password" class="form-control">
                          </div>
                              <br>
                            <input type="submit" name="submit" class="btn btn-default btn-block btn-info">
                          </form>
                        </div>
                      </div>

                    </div>
                  </div>
                  <script>
                    $("#myactive").modal("show");

                     $("#settlementForm").on("submit", function(e){
                         e.preventDefault();
                         $.ajax({
                           type: "post",
                           url: "submitBetSettle",
                           headers: {
                             "X-CSRF-TOKEN": $("meta[name='."csrf-token".']").attr("content")
                         },
                           data: $("#settlementForm").serialize(),
                           success: function(res) {
                             if (res.success) {
                              $("#Bet'.$bet_id.'").html("Settled");
                              $("#myactive").modal("hide");
                            }else{
                              $(".error").html(res.message);
                            }
                           }
                         });
                      });


                  </script>


                  ';



                   return response(array('success'=>true,'data'=>$data));





    }


    public function submit_bet_settle(Request $request){

      $logged_in_password = Auth::user()->password;
      if(Hash::check($request->password,$logged_in_password)){

        $bet_id = $request->bet_id;
        $d = Betplaced::find($bet_id);
        $d->profit_type = $request->status;
        if($request->status == 'win'){
          $d->profit =  $d->bet_amount * $d->odds;
        }else{
          $d->profit =  $d->bet_amount;
        }
        $d->update();
         return response(array('success'=>true));

      }else{
         return response(array('success'=>false,'message'=>'<span style="font-size:16px;color:red">Password Not Match. Try Agian...</span>'));
      }

    }









    public function betListClient(Request $request){


      $id = Auth::user()->id;

      if(!empty($request->get('sports'))){
        
        $bet_sports = $request->get('sports');
      }else{

        $bet_sports = 4;
      }  

      if(!empty($request->get('bet_from'))){
        $lastmonth = $request->get('bet_from');
      }else{
      $lastmonth = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

      }
      if(!empty($request->get('bet_to'))){

        $current_date = $request->get('bet_to');
      }else{
        $current_date = date('Y-m-d');

      }


      if(!empty($request->get('bet_status'))){
        $bet_status = $request->get('bet_status');

      }else{
        $bet_status = 'matched';

      }
      $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();

      if($bet_status == 'matched'){
        $bets = Betplaced::where('user_id',$child)->where('event_type',$bet_sports)->where('profit',null)->whereBetween('created_at',[$lastmonth,$current_date])->get();  
      }else{
        $bets = Betplaced::where('user_id',$child)->where('event_type',$bet_sports)->where('profit','!=',null)->whereBetween('created_at',[$lastmonth,$current_date])->get();  
      }


      return view('client.betlist',compact('bets','bet_sports','current_date','lastmonth','bet_status'));


    }










}
