<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class MyReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function downline()
    {
        //
        return view('admin.myreport.downline');
    }
    public function market()
    {
        //
        return view('admin.myreport.market');
    }
}
