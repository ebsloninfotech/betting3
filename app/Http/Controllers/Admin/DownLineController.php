<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Betplaced;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\UserParent;
use App\Credit;
use App\Wallet;
use Spatie\Activitylog\Models\Activity;

use App\Http\Controllers\Controller;

class DownLineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        // if(Auth::user()->hasRole('master')){
        // $users  = User::whereHas('roles', function($q){$q->whereIn('name', ['admin']);})->get();          
        // }
        // elseif(Auth::user()->hasRole('admin')){
        // $users  = User::whereHas('roles', function($q){$q->whereIn('name', ['client']);})->get();          
        // }
        // elseif(Auth::user()->hasRole('client')){
        //   $users  = User::whereHas('roles', function($q){$q->whereIn('name', ['customer']);})->get();          
        // }

        $keyword = $request->get('search');

        $id = Auth::user()->id;

        if (!empty($keyword)) {

          $user = User::where('username','LIKE',"%$keyword%")->get()->except($id);


        }else{

          $user = UserParent::where('parent_id',$id)->get();

        }





        return view('admin.downline.index',compact('user'));
    }

    



    public function member_Bets(Request $request,$id){

        $data = User::find($id);



      if(!empty($request->get('sports'))){
        
        $bet_sports = $request->get('sports');
      }else{

        $bet_sports = 4;
      }  

      if(!empty($request->get('bet_from'))){
        $lastmonth = $request->get('bet_from');
      }else{
      $lastmonth = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

      }
      if(!empty($request->get('bet_to'))){

        $current_date = $request->get('bet_to');
      }else{
        $current_date = date('Y-m-d');

      }


      if(!empty($request->get('bet_status'))){
        $bet_status = $request->get('bet_status');

      }else{
        $bet_status = 'matched';

      }
      // $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();

      if($bet_status == 'matched'){
        $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit',null)->whereBetween('created_at',[$lastmonth,$current_date])->get();  
      }else{
        $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit','!=',null)->whereBetween('created_at',[$lastmonth,$current_date])->get();  
      }

        return view('admin.member.clientBets',compact('data','bets','bet_sports','current_date','lastmonth','bet_status'));


    }
    public function member_Transactions($id){

        $data = User::find($id);

     $data2 = Credit::where('receiver_id',$id)->get();

        return view('admin.member.clientTransactions',compact('data','data2'));


    }
    public function member_Log($id){

        $data = User::find($id);

        return view('admin.member.clientLog',compact('data'));


    }



   

    public function masterMemberSummary($id){

        $data = User::find($id);



        return view('admin.member.admin.index',compact('data'));


    }




    public function master_member_Downline($id){

      $downline = UserParent::where('parent_id',$id)->get();

      $data = User::find($id);

      return view('admin.member.admin.downline',compact('data','downline'));
      
    }


    
    public function master_member_Bets(Request $request,$id){

      $data = User::find($id);



      if(!empty($request->get('sports'))){
        
        $bet_sports = $request->get('sports');
      }else{

        $bet_sports = 4;
      }  

      if(!empty($request->get('bet_from'))){
        $lastmonth = $request->get('bet_from');
      }else{
      $lastmonth = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

      }
      if(!empty($request->get('bet_to'))){

        $current_date = $request->get('bet_to');
      }else{
        $current_date = date('Y-m-d');

      }


      if(!empty($request->get('bet_status'))){
        $bet_status = $request->get('bet_status');

      }else{
        $bet_status = 'matched';

      }
      // $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();

      if($bet_status == 'matched'){
        $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit',null)->whereBetween('created_at',[$lastmonth,$current_date])->get();  
      }else{
        $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit','!=',null)->whereBetween('created_at',[$lastmonth,$current_date])->get();  
      }

        return view('admin.member.masterBets',compact('data','bets','bet_sports','current_date','lastmonth','bet_status'));


    }



    public function master_member_Transactions($id){

        $data = User::find($id);

     $data2 = Credit::where('receiver_id',$id)->get();

        return view('admin.member.masterTransactions',compact('data','data2'));


    }




    public function master_member_Log(Request $request,$id){

        $data = User::find($id);

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
                $activitylogs = Activity::where('causer_id',$id)->where('description', 'LIKE', "%$keyword%")
                    ->latest()->paginate($perPage);
            } else {
                $activitylogs = Activity::where('causer_id',$id)->latest()->paginate($perPage);
            }

        return view('admin.member.masterLog',compact('data','activitylogs'));


    }



      public function change_password(Request $request){
        //dd($request->all());
        $my_password           = $request->my_password;
        $new_password           = $request->new_password;
        $confirm_new_password   = $request->confirm_new_password;
        $data = Auth::user();
        if(!empty($new_password==$confirm_new_password)){
            if(Hash::check($my_password, $data->password))
            { 
               $u = User::find($request->user_id);
                $u->password = bcrypt($request->new_password);
                $u->save();

               return response(array('success'=>true,'message'=>'<span style="font-size:16px;color:green">Password Change Successfull</span>'));
            }else{
                return response(array('success'=>false,'message'=>'<span style="font-size:16px;color:red">Your Password Not Match. Try Agian...</span>'));
            }
        }else{
            return response(array('success'=>false,'message'=>'<span style="font-size:16px;color:red">Confirm Password Not Same. Try Agian...</span>'));
        }
    }




// client via admin




    public function clientMemberSummary($admin_id,$id){

        $data = User::find($id);
        $admin = User::find($admin_id);

        return view('admin.member.admin.client.index',compact('data','admin'));


    }







public function client_member_Downline($admin_id,$id){

      $downline = UserParent::where('parent_id',$id)->get();

      $data = User::find($id);

        $admin = User::find($admin_id);
      return view('admin.member.admin.client.downline',compact('data','downline','admin'));

}






public function client_member_Transactions($admin_id,$id){
$data = User::find($id);

     $data2 = Credit::where('receiver_id',$id)->get();
        $admin = User::find($admin_id);

        return view('admin.member.admin.client.clientTransactions',compact('data','data2','admin'));

}





    public function client_member_Log(Request $request,$admin_id,$id){

        $data = User::find($id);

        $admin = User::find($admin_id);


        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
                $activitylogs = Activity::where('causer_id',$id)->where('description', 'LIKE', "%$keyword%")
                    ->latest()->paginate($perPage);
            } else {
                $activitylogs = Activity::where('causer_id',$id)->latest()->paginate($perPage);
            }

        return view('admin.member.admin.client.clientLog',compact('data','activitylogs','admin'));


    }





// user via client 


public function userMemberSummary($admin_id,$client_id,$id){


  $admin = User::find($admin_id);
  $client = User::find($client_id);
  $data = User::find($id);

  return view('admin.member.admin.client.user.index',compact('data','admin','client'));


}






public function user_member_Transactions($admin_id,$client_id,$id){
      $data = User::find($id);

      $client = User::find($client_id);
      $data2 = Credit::where('receiver_id',$id)->get();
      $admin = User::find($admin_id);

      return view('admin.member.admin.client.user.userTransaction',compact('data','data2','client','admin'));

}











  public function user_member_bets(Request $request,$admin_id,$client_id,$id){

      $data = User::find($id);



      if(!empty($request->get('sports'))){
        
        $bet_sports = $request->get('sports');
      }else{

        $bet_sports = 4;
      }  

      if(!empty($request->get('bet_from'))){
        $lastmonth = $request->get('bet_from');
      }else{
      $lastmonth = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

      }
      if(!empty($request->get('bet_to'))){

        $current_date = $request->get('bet_to');
      }else{
        $current_date = date('Y-m-d');

      }


      if(!empty($request->get('bet_status'))){
        $bet_status = $request->get('bet_status');
        
      }else{
        $bet_status = 'matched';

      }
      // $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();

      // if($bet_status == 'matched'){
      //   $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit',null)->whereBetween('created_at',[$lastmonth,$current_date])->get();  
      // }else{
      //   $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit','!=',null)->whereBetween('created_at',[$lastmonth,$current_date])->get();  
      // }


      if($bet_status == 'matched'){
        $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit',null)->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->get();  
      }else{
        if($bet_status == 'settled'){

          $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit','!=',null)->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->get();  
        }else{
          $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->onlyTrashed()->get();  
          
        }
      }



            $client = User::find($client_id);

      $admin = User::find($admin_id);


        return view('admin.member.admin.client.user.userBets',compact('data','bets','bet_sports','current_date','lastmonth','bet_status','client','admin'));


    }



    public function user_member_profitLose(Request $request,$admin_id,$client_id,$id){



        $data = User::find($id);



      if(!empty($request->get('bet_from'))){
        $lastmonth = $request->get('bet_from');
      }else{
      $lastmonth = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

      }
      if(!empty($request->get('bet_to'))){

        $current_date = $request->get('bet_to');
      }else{
        $current_date = date('Y-m-d');

      }

      // $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();

      
        $bets = Betplaced::where('user_id',$id)->where('betstatus','settled')->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->groupBy('selection_id')->get();  

        $client = User::find($client_id);

        $admin = User::find($admin_id);

        return view('admin.member.admin.client.user.userProfitLose',compact('data','bets','current_date','lastmonth','client','admin'));
    }







    public function user_member_userBalance(Request $request,$admin_id,$client_id,$id){

        $data = User::find($id);

        $admin = User::find($admin_id);
        $client = User::find($client_id);

        $wallet = Wallet::where('user_id',$id)->first();

        return view('admin.member.admin.client.user.userBalance',compact('data','wallet','admin','client'));


    }

    


    public function user_member_Log(Request $request,$admin_id,$client_id,$id){

        $data = User::find($id);

        $admin = User::find($admin_id);
        $client = User::find($client_id);

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
                $activitylogs = Activity::where('causer_id',$id)->where('description', 'LIKE', "%$keyword%")
                    ->latest()->paginate($perPage);
            } else {
                $activitylogs = Activity::where('causer_id',$id)->latest()->paginate($perPage);
            }

        return view('admin.member.admin.client.user.userLog',compact('data','activitylogs','admin','client'));


    }













// user via client 



        public function fromclientMemberSummary($client_id,$id){

          $data = User::find($id);
          $client = User::find($client_id);

          return view('client.member.index',compact('data','client'));


      }





    public function userprofitLoss(Request $request,$client_id,$id){

      $client = User::find($client_id);

        $data = User::find($id);



      if(!empty($request->get('bet_from'))){
        $lastmonth = $request->get('bet_from');
      }else{
      $lastmonth = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

      }
      if(!empty($request->get('bet_to'))){

        $current_date = $request->get('bet_to');
      }else{
        $current_date = date('Y-m-d');

      }

      // $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();

      
        $bets = Betplaced::where('user_id',$id)->where('betstatus','settled')->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->groupBy('selection_id')->get();  


        

        return view('client.member.userProfitLose',compact('data','bets','current_date','lastmonth','client'));
    }





  public function client_user_Bets(Request $request,$client_id,$id){

      $data = User::find($id);



      if(!empty($request->get('sports'))){
        
        $bet_sports = $request->get('sports');
      }else{

        $bet_sports = 4;
      }  

      if(!empty($request->get('bet_from'))){
        $lastmonth = $request->get('bet_from');
      }else{
      $lastmonth = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

      }
      if(!empty($request->get('bet_to'))){

        $current_date = $request->get('bet_to');
      }else{
        $current_date = date('Y-m-d');

      }


      if(!empty($request->get('bet_status'))){
        $bet_status = $request->get('bet_status');
        
      }else{
        $bet_status = 'matched';

      }
      // $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();

      if($bet_status == 'matched'){
        $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit',null)->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->get();  
      }else{
        if($bet_status == 'settled'){

          $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit','!=',null)->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->get();  
        }else{
          $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->onlyTrashed()->get();  
          
        }
      }

            $client = User::find($client_id);

      // $admin = User::find($admin_id);


        return view('client.member.userBets',compact('data','bets','bet_sports','current_date','lastmonth','bet_status','client'));


    }








    public function client_user_member_Log(Request $request,$client_id,$id){

        $data = User::find($id);

        $client = User::find($client_id);

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
                $activitylogs = Activity::where('causer_id',$id)->where('description', 'LIKE', "%$keyword%")
                    ->latest()->paginate($perPage);
            } else {
                $activitylogs = Activity::where('causer_id',$id)->latest()->paginate($perPage);
            }

        return view('client.member.userLog',compact('data','activitylogs','client'));


    }







public function client_user_member_Transactions($client_id,$id){
      $data = User::find($id);

      $client = User::find($client_id);
      $data2 = Credit::where('receiver_id',$id)->get();

      return view('client.member.userTransactions',compact('data','data2','client'));

}
















// client via admin



    public function adminclientMemberSummary($id){

        $data = User::find($id);

        return view('admin.client.index',compact('data'));


    }







public function adminclient_member_Downline($id){

      $downline = UserParent::where('parent_id',$id)->get();

      $data = User::find($id);

      return view('admin.client.downline',compact('data','downline'));

}






public function adminclient_member_Transactions($id){
        $data = User::find($id);

        $data2 = Credit::where('receiver_id',$id)->get();
        return view('admin.client.transaction',compact('data','data2'));

}





    public function adminclient_member_Log(Request $request,$id){

        $data = User::find($id);



        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
                $activitylogs = Activity::where('causer_id',$id)->where('description', 'LIKE', "%$keyword%")
                    ->latest()->paginate($perPage);
            } else {
                $activitylogs = Activity::where('causer_id',$id)->latest()->paginate($perPage);
            }

        return view('admin.client.clientLog',compact('data','activitylogs'));


    }




// user via admin



public function admin_userMemberSummary($client_id,$id){


  $client = User::find($client_id);
  $data = User::find($id);

  return view('admin.client.user.index',compact('data','client'));


}






public function admin_user_member_Transactions($client_id,$id){
      $data = User::find($id);

      $client = User::find($client_id);
      $data2 = Credit::where('receiver_id',$id)->get();
      

      return view('admin.client.user.userTransaction',compact('data','data2','client'));

}











  public function admin_user_member_bets(Request $request,$client_id,$id){

      $data = User::find($id);



      if(!empty($request->get('sports'))){
        
        $bet_sports = $request->get('sports');
      }else{

        $bet_sports = 4;
      }  

      if(!empty($request->get('bet_from'))){
        $lastmonth = $request->get('bet_from');
      }else{
      $lastmonth = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

      }
      if(!empty($request->get('bet_to'))){

        $current_date = $request->get('bet_to');
      }else{
        $current_date = date('Y-m-d');

      }


      if(!empty($request->get('bet_status'))){
        $bet_status = $request->get('bet_status');
        
      }else{
        $bet_status = 'matched';

      }
      // $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();

      // if($bet_status == 'matched'){
      //   $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit',null)->whereBetween('created_at',[$lastmonth,$current_date])->get();  
      // }else{
      //   $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit','!=',null)->whereBetween('created_at',[$lastmonth,$current_date])->get();  
      // }


      if($bet_status == 'matched'){
        $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit',null)->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->get();  
      }else{
        if($bet_status == 'settled'){

          $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->where('profit','!=',null)->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->get();  
        }else{
          $bets = Betplaced::where('user_id',$id)->where('event_type',$bet_sports)->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->onlyTrashed()->get();  
          
        }
      }



            $client = User::find($client_id);

      


        return view('admin.client.user.userBets',compact('data','bets','bet_sports','current_date','lastmonth','bet_status','client'));


    }



    public function admin_user_member_profitLose(Request $request,$client_id,$id){



        $data = User::find($id);



      if(!empty($request->get('bet_from'))){
        $lastmonth = $request->get('bet_from');
      }else{
      $lastmonth = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

      }
      if(!empty($request->get('bet_to'))){

        $current_date = $request->get('bet_to');
      }else{
        $current_date = date('Y-m-d');

      }

      // $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();

      
        $bets = Betplaced::where('user_id',$id)->where('betstatus','settled')->whereBetween('created_at',[$lastmonth." 00:00:00",$current_date." 23:59:59"])->groupBy('selection_id')->get();  

        $client = User::find($client_id);

        

        return view('admin.client.user.userProfitLose',compact('data','bets','current_date','lastmonth','client'));
    }







    public function admin_user_member_userBalance(Request $request,$client_id,$id){

        $data = User::find($id);

        
        $client = User::find($client_id);

        $wallet = Wallet::where('user_id',$id)->first();

        return view('admin.client.user.userBalance',compact('data','wallet','client'));


    }

    


    public function admin_user_member_Log(Request $request,$client_id,$id){

        $data = User::find($id);

        
        $client = User::find($client_id);

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
                $activitylogs = Activity::where('causer_id',$id)->where('description', 'LIKE', "%$keyword%")
                    ->latest()->paginate($perPage);
            } else {
                $activitylogs = Activity::where('causer_id',$id)->latest()->paginate($perPage);
            }

        return view('admin.client.user.userLog',compact('data','activitylogs','client'));


    }





}
