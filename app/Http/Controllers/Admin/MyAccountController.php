<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Spatie\Activitylog\Models\Activity;
use App\User;
use App\Betplaced;
use App\Wallet;
use App\MarketRunner;
use App\Event;
use Auth;
use App\Credit;
use App\FancyBet;
use App\FancyBetPlaced;
use App\Transaction;
use App\UserParent;

class MyAccountController extends Controller
{
    
    public function change_password(Request $request){
        //dd($request->all());
        $user = Auth::user();

        
        if(Hash::check($request->old_password,$user->password)) {
                // Right password
                //$user->
            $user->password = bcrypt($request->new_password);
            $user->save();
            return redirect()->back()->with('flash_message','Password Updated');
           } else {
                // Wrong one
            return redirect()->back()->with('flash_message','Password Mis-Match');
            }
    }

    public function index()
    {
        return view('admin.myaccount.index');
    }

    public function CreditApply(Request $request){

        $logged_in_password = Auth::user()->password;
        $tr_id = substr(uniqid(substr(str_replace(['+', '/', '='], '', base64_encode(random_bytes(32))), 0, 32), true), 0, 32);
    if(Hash::check($request->password,$logged_in_password)){

        if (Auth::user()->hasRole('client')) {

            $w = Wallet::where('user_id',$request->receiver_id)->first();



            if($request->type == 'withdraw'){


                if($w->wallet_amount < $request->points){


                return response(array('success'=>false,'message'=>'<span style="font-size:16px;color:red">Wallet amount must be greater then withdraw amount </span>'));

                }
            }
            
            $p = Wallet::where('user_id',$request->sender_id)->first();


            if($request->type == 'deposit' && $p->wallet_amount < $request->points ){

                return response(array('success'=>false,'message'=>'<span style="font-size:16px;color:red">Wallet amount must be greater then Deposit amount </span>'));

            }
            

                $trans                      = new Transaction;
                $trans->transaction_id      = $tr_id;
                $trans->payer_id            = $request->sender_id;
                $trans->receiver_id         = $request->receiver_id;
                $trans->transaction_type    = $request->type;
                $trans->transaction_amount  = $request->points;
                $trans->remarks             = $request->remarks;
                $trans->transaction_status  = 'Complete';
                $trans->save();
                    $wa = Wallet::where('user_id',$request->receiver_id)->first();
                    if($wa){
                        
                        if($request->type == 'withdraw'){
                            $wa->wallet_amount  = $wa->wallet_amount - $request->points;
                            $p->wallet_amount  = $p->wallet_amount + $request->points;
                        }else{
                            $wa->wallet_amount  = $wa->wallet_amount + $request->points;
                            $p->wallet_amount  = $p->wallet_amount - $request->points;
                        }        
                        $wa->update();
                        $p->update();
                    }else{
                        $wallet = new Wallet;
                        $wallet->user_id = $request->receiver_id;
                        $wallet->wallet_amount = $request->points;
                        $wallet->save();
                    }


                if(!empty($tr_id)){

                    $balance = Wallet::where('user_id',$request->receiver_id)->first();

                    $credit = new Credit;
                    $credit->transaction_id  = $tr_id;
                    $credit->sender_id  = $request->sender_id;
                    $credit->receiver_id  = $request->receiver_id;
                    $credit->type = $request->type;
                    $credit->points = $request->points;
                    $credit->balance = $balance->wallet_amount;
                    $credit->remarks = $request->remarks;
                    $credit->save();
                }
                return response(array('success'=>true,'message'=>'<span style="font-size:16px;color:green">Credit Processing Successful!</span>'));



        }else{



            // $w = Wallet::where('user_id',$request->receiver_id)->first();

            // if($request->type == 'withdraw' && $w->wallet_amount > $request->points ){

            //     return response(array('success'=>false,'message'=>'<span style="font-size:16px;color:red">Wallet amount must be greater then withdraw amount </span>'));

            // }




            $trans                      = new Transaction;
            $trans->transaction_id      = $tr_id;
            $trans->payer_id            = $request->sender_id;
            $trans->receiver_id         = $request->receiver_id;
            $trans->transaction_type    = $request->type;
            $trans->transaction_amount  = $request->points;
            $trans->remarks             = $request->remarks;
            $trans->transaction_status  = 'Complete';
            $trans->save();
                $w = Wallet::where('user_id',$request->receiver_id)->first();
                if($w){
                    
                    if($request->type == 'withdraw'){
                        $w->wallet_amount  = $w->wallet_amount - $request->points;

                        $u = Wallet::where('user_id',Auth::user()->id)->first();
                        $u->wallet_amount = $u->wallet_amount + $request->points;
                        $u->update();


                    }else{
                        $w->wallet_amount  = $w->wallet_amount + $request->points;
                        $u = Wallet::where('user_id',Auth::user()->id)->first();
                        $u->wallet_amount = $u->wallet_amount - $request->points;
                        $u->update();
                    }        
                    $w->update();
                }else{
                    $wallet = new Wallet;
                    $wallet->user_id = $request->receiver_id;
                    $wallet->wallet_amount = $request->points;
                    $wallet->save();
                }


            if(!empty($tr_id)){
                
                $balance = Wallet::where('user_id',$request->receiver_id)->first();

                $credit = new Credit;
                $credit->transaction_id  = $tr_id;
                $credit->sender_id  = $request->sender_id;
                $credit->receiver_id  = $request->receiver_id;
                $credit->type = $request->type;
                $credit->points = $request->points;
                $credit->balance = $balance->wallet_amount;
                $credit->remarks = $request->remarks;
                $credit->save();
            }
            return response(array('success'=>true,'message'=>'<span style="font-size:16px;color:green">Credit Processing Successful!</span>'));
        
        }
        }else{
           return response(array('success'=>false,'message'=>'<span style="font-size:16px;color:red">Password Not Match. Try Agian...</span>'));
        }

    }

    public function MyProfile(){
        return view('admin.myaccount.myprofile');
    }






    public function balanceView(){


        return view('admin.myaccount.balance_overview');
    }








    public function ChangePassword(Request $request){
        $old_password           = $request->old_password;
        $new_password           = $request->new_password;
        $confirm_new_password   = $request->confirm_new_password;
        $data = Auth::user();
        if(!empty($new_password==$confirm_new_password)){
            if(Hash::check($old_password, $data->password))
            { 
               $data->password = Hash::make($new_password);
               $data->update();

               return response(array('success'=>true,'message'=>'<span style="font-size:16px;color:green">Password Change Successfull</span>'));
            }else{
                return response(array('success'=>false,'message'=>'<span style="font-size:16px;color:red">Old Password Not Match. Try Agian...</span>'));
            }
        }else{
            return response(array('success'=>false,'message'=>'<span style="font-size:16px;color:red">Confirm Password Not Same. Try Agian...</span>'));
        }
    }

    public function UpdateProfile(Request $request){
        $data               = Auth::user();
        $data->name         = $request->name;
        $data->username     = $request->username;
        $data->mobile_no    = $request->mobile_no;
        $data->update();
        return response(array('success'=>true,'message'=>'<span style="font-size:16px;color:green">Profile Update Successfull</span>'));
    }

    public function accountstatement(){

        $id = Auth::user()->id;

        $data = Credit::where('receiver_id',$id)->orWhere('sender_id',$id)->get();
        
        return view('admin.myaccount.myaccountstatement',compact('data'));

    }

    public function casinostatement(){
        return view('admin.myaccount.mycasinostatement');

    }

    public function loginhistory(Request $request){

        $keyword = $request->get('search');
        $perPage = 25;
        if(Auth::user()->hasRole('master')){
            if (!empty($keyword)) {
                $activitylogs = Activity::where('description', 'LIKE', "%$keyword%")
                    ->get();
            } else {
                $activitylogs = Activity::get();
            }
        }elseif(Auth::user()->hasRole('admin')){
            if (!empty($keyword)) {
                $activitylogs = Activity::where('causer_id',Auth::id())->where('description', 'LIKE', "%$keyword%")
                    ->get();
            } else {
                $activitylogs = Activity::where('causer_id',Auth::id())->get();
            }
        }elseif(Auth::user()->hasRole('client')){
            if (!empty($keyword)) {
                $activitylogs = Activity::where('causer_id',Auth::id())->where('description', 'LIKE', "%$keyword%")
                    ->get();
            } else {
                $activitylogs = Activity::where('causer_id',Auth::id())->get();
            }
        }elseif(Auth::user()->hasRole('customer')){
            if (!empty($keyword)) {
                $activitylogs = Activity::where('causer_id',Auth::id())->where('description', 'LIKE', "%$keyword%")
                    ->get();
            } else {
                $activitylogs = Activity::where('causer_id',Auth::id())->get();
            }
        }
        return view('admin.myaccount.myloginhistory',compact('activitylogs'));

    }














    public function reports(Request $request){


        $id = Auth::user()->id;

      if(!empty($request->get('sports'))){
        
        $bet_sports = $request->get('sports');
      }else{

        $bet_sports = 4;
      }  

      if(!empty($request->get('bet_from'))){
        $lastmonth = $request->get('bet_from');
      }else{
      $lastmonth = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

      }
      if(!empty($request->get('bet_to'))){

        $current_date = $request->get('bet_to');
      }else{
        $current_date = date('Y-m-d');

      }


      if(!empty($request->get('bet_status'))){
        $bet_status = $request->get('bet_status');

      }else{
        $bet_status = 'matched';

      }
      $child = UserParent::select('user_id')->where('parent_id',$id)->get()->toArray();

      if($bet_status == 'matched'){
        $bets = Betplaced::where('user_id',$child)->where('event_type',$bet_sports)->where('profit',null)->whereBetween('created_at',[$lastmonth,$current_date])->get();  
      }else{
        $bets = Betplaced::where('user_id',$child)->where('event_type',$bet_sports)->where('profit','!=',null)->whereBetween('created_at',[$lastmonth,$current_date])->get();  
      }


      return view('admin.myaccount.reports',compact('bets','bet_sports','current_date','lastmonth','bet_status'));


    } 





    public function matches($id){

        $date = date('Y-m-d H:i:s');

        $starttime = strtotime("+330 minutes", strtotime($date));

        $finaldate = date('Y-m-d H:i:s',$starttime);
        // dd($date);
        $data = Betplaced::where('event_type',$id)->with('event')->groupBy('event_id')->get();


        return view('admin.myaccount.matches',compact('data'));


    }













    public function fetchTotalBook(Request $request){

        $user_id = Auth::user()->id;

        $event_type = $request->event_type;
        $event_id = $request->event_id;
        $market_id = $request->market_id;

        $child_user = [];

        if(auth()->user()->hasRole('client')){


            $d = UserParent::where('parent_id',$user_id)->get();
            $i = 0;
            foreach ($d as $value) {
                $child_user[$i] = $value->user_id;
                $i++;
            }

        }



        if(auth()->user()->hasRole('admin')){

            $d = UserParent::where('parent_id',$user_id)->get();
            $j = 0;
            foreach ($d as $value) {
                $b = UserParent::where('parent_id',$value->user_id)->get();
                $i = $j;
                foreach ($b as $v) {
                    
                    $child_user[$i] = $v->user_id;
                    $i++;
                }

                $j = $i;
            }


        }




        if(auth()->user()->hasRole('master')){

            $d = UserParent::where('parent_id',$user_id)->get();
            $j = 0;
            foreach ($d as $value) {
                $b = UserParent::where('parent_id',$value->user_id)->get();
                $i = $j;
                foreach ($b as $v) {
                    
                    $c = UserParent::where('parent_id',$v->user_id)->get();
                    $k = $i;
                    foreach ($c as $v2) {
                        
                        $child_user[$k] = $v2->user_id;
                        $k++;
                    }

                    $i = $k;
                }

                $j = $i;
            }


        }


        if(auth()->user()->hasRole('customer')){

            return response(array([
                'success'=>false,
                'msg'=>"Invalid User",
            ]));

        }


        $event = Event::where('event_id',$event_id)->first();

        $data = Betplaced::whereIn('user_id',$child_user)->where('event_id',$event_id)->where('market_type','match_odd')->get();
        $profit = 0;
        $lose = 0;
        foreach ($data as $value) {
              
            if ($value->bet_type == 'back') {
                $profit = $profit + ($value->bet_amount * $value->odds) - $value->bet_amount;;
                $lose = $lose + $value->bet_amount;
            }else{

                $profit = $profit + $value->bet_amount;
                $lose = $lose + ($value->bet_amount * $value->odds) - $value->bet_amount;;

            }

        }

        if($event_type == '4'){
            
            $type = 'Cricket';

        }

        if($event_type == '1'){
            
            $type = 'Soccer';

        }

        if($event_type == '2'){
            
            $type = 'Tennis';

        }

        if ($event->result == null) {
            $result = 'Not Update';
        }else{
            $res = MarketRunner::where('selection_id',$event->result)->first();
            $result = $res->runner_name;
        }
        $text = '';
        $total_match = 0;
        $total_match_class = 'clsgreen';
        $total_session_class = 'clsgreen';
        if (count($data)>0) {
            
        $text .= '<div class="marketProfit">
            <p class="modalHead"> Market Profit/Loss</p>
               <table class="table table-bordered">
                       <thead>
                           <tr>
                               <th>Match</th>
                               <th class="text-center">Winner</th>
                               <th class="text-center">Profit/Loss</th>
                           </tr>
                       </thead>
                       <tbody>';

                       if($profit >= $lose){

                        $y = $profit-$lose;

                    $text .= '<tr>
                       <td>'.$type.' &gt; '.$event->name.' '.$event->openDate.'</td>
                       <td class="text-center">'.$result.'</td>
                       <td class="text-center">
                           <span id="rptMarketBook_Label2_0" data-value="233601.45">'.$y.'</span></td>
                   </tr>
               
                    <tr class="subHeadingTr" style="font-weight: bold;">
                       <td colspan="2" class="text-right" style="font-weight: bold; font-size: 16px;">Totals</td>
                       <td class="text-center" style="font-weight: bold; font-size: 16px;">
                           <span id="ttMatch" data-value="233601.45">'.$y.'</span>
                       </td>
                   </tr>';

                    $total_match = $y;

                    $total_match_class = 'clsgreen';
                 
                       }else{

                        $x = $lose-$profit;

                        $text .= '<tr>
                               <td>'.$type.' &gt; '.$event->name.' '.$event->openDate.'</td>
                               <td class="text-center">'.$result.'</td>
                               <td class="text-center">
                                   <span id="rptMarketBook_Label2_0">'.$x.'</span></td>
                           </tr>
                       
                            <tr class="subHeadingTr" style="font-weight: bold;">
                               <td colspan="2" class="text-right" style="font-weight: bold; font-size: 16px;">Totals</td>
                               <td class="text-center" style="font-weight: bold; font-size: 16px;">
                                   <span id="ttMatch" data-value="233601.45">'.$x.'</span>
                               </td>
                           </tr>';
                           $total_match = $x;
                    $total_match_class = 'clsred';
                       }

                   $text .='</tbody>
               </table>
         </div>';

        }

         $text .='<div class="sessionProfit">
            <p class="modalHead"> Session Profit/Loss</p>
               <table class="table table-bordered">
                       <thead>
                           <tr>
                               <th>Match</th>
                               <th class="text-center">Bets</th>
                               <th class="text-center">Profit/Loss</th>
                           </tr>
                       </thead>
                       <tbody>';

                       $fancy_bet = FancyBet::where('event_id',$event_id)->where('result','!=',null)->get();

                       $total_session_profit = 0;
                       $total_session_lose = 0;

                       foreach ($fancy_bet as $fancy) {

                            $sessionBet = FancyBetPlaced::where('selectionId',$fancy->SelectionId)->whereIn('user_id',$child_user)->get();

                            $amt_profit = 0;
                            $amt_lose = 0;
                            foreach ($sessionBet as $session) {

                                if ($session->type == 'back') {

                                    if ($fancy->result <= $session->prize) {
                                        
                                        $ses_profit = 0;
                                        $ses_lose = ($session->size * $session->stake)/100;

                                    }else{
                                        $ses_profit = $session->stake;
                                        $ses_lose = 0;
                                    }

                                    
                                    $amt_profit = $amt_profit + $ses_profit;
                                    $amt_lose = $amt_lose + $ses_lose;
                                }else{


                                    if ($fancy->result > $session->prize) {
                                        
                                        $ses_profit = 0;
                                        $ses_lose = $session->stake;

                                    }else{
                                        $ses_profit = ($session->size * $session->stake)/100;
                                        $ses_lose = 0;
                                    }


                                    $ses_profit = $session->stake;
                                    $ses_lose = ($session->size * $session->stake)/100;

                                    $amt_profit = $amt_profit + $ses_profit;
                                    $amt_lose = $amt_lose + $ses_lose;
                                }


                            }

                            if($amt_profit >= $amt_lose){

                            $text .='<tr>
                                       <td><a href="'.url('singlefancy/userList/'.$fancy->SelectionId.'').'" target="popup" onclick=window.open("'.url('singlefancy/userList/'.$fancy->SelectionId.'').'","popup","width=600,height=600");return false;" >'.$fancy->RunnerName.'</a> </td>
                                       <td class="text-center">'.count($sessionBet).'</td>
                                       <td class="text-center">
                                           <span id="rptMarketBook_Label2_0" class="clsgreen" data-value="'.$amt_profit.'">'.$amt_profit.'</span></td>
                                   </tr>';
                            }else{
                                $text .='<tr>
                                       <td><a href="'.url('singlefancy/userList/'.$fancy->SelectionId.'').'" target="popup" onclick=window.open("'.url('singlefancy/userList/'.$fancy->SelectionId.'').'","popup","width=600,height=600");return false;" >'.$fancy->RunnerName.'</a></td>
                                       <td class="text-center">'.count($sessionBet).'</td>
                                       <td class="text-center">
                                           <span id="rptMarketBook_Label2_0" class="clsred" data-value="'.$amt_lose.'">'.$amt_lose.'</span></td>
                                   </tr>';
                            }

                            $total_session_profit = $total_session_profit + $amt_profit;
                            $total_session_lose = $total_session_lose + $amt_lose;
                           
                       }
               
                    if($total_session_profit >= $total_session_lose){
                
                        $t = $total_session_profit - $total_session_lose;

                        $text .='<tr class="subHeadingTr" style="font-weight: bold;">
                                   <td colspan="2" class="text-right" style="font-weight: bold; font-size: 16px;">Totals</td>
                                   <td class="text-center" style="font-weight: bold; font-size: 16px;">
                                       <span id="ttMatch" class="clsred"  data-value="'.$t.'">'.$t.'</span>
                                   </td>
                               </tr>';

                            $total_session = $t;   
                            $total_session_class = 'clsred';
                    }else{
                        $t = $t;
                        $text .='<tr class="subHeadingTr" style="font-weight: bold;">
                                   <td colspan="2" class="text-right" style="font-weight: bold; font-size: 16px;">Totals</td>
                                   <td class="text-center" style="font-weight: bold; font-size: 16px;">
                                       <span id="ttMatch" class="clsgreen" data-value="'.$t.'">'.$t.'</span>
                                   </td>
                               </tr>';
                            $total_session = $t;   
                            $total_session_class = 'clsgreen';

                    }

                   $text .='</tbody>
                           </table>
                     </div>';





            return response(array([
                'success'=>true,
                'msg'=>"Success",
                'data'=>$text,
                'total_match'=>$total_match,
                'total_session'=>$total_session,
                'total_session_class'=>$total_session_class,
                'total_match_class'=>$total_match_class,
            ]));



    }



    public function singlefancy($SelectionId){
        
        $user_id = Auth::user()->id;

        $child_user = [];

        if(auth()->user()->hasRole('client')){


            $d = UserParent::where('parent_id',$user_id)->get();
            $i = 0;
            foreach ($d as $value) {
                $child_user[$i] = $value->user_id;
                $i++;
            }

        }



        if(auth()->user()->hasRole('admin')){

            $d = UserParent::where('parent_id',$user_id)->get();
            $j = 0;
            foreach ($d as $value) {
                $b = UserParent::where('parent_id',$value->user_id)->get();
                $i = $j;
                foreach ($b as $v) {
                    
                    $child_user[$i] = $v->user_id;
                    $i++;
                }

                $j = $i;
            }


        }




        if(auth()->user()->hasRole('master')){

            $d = UserParent::where('parent_id',$user_id)->get();
            $j = 0;
            foreach ($d as $value) {
                $b = UserParent::where('parent_id',$value->user_id)->get();
                $i = $j;
                foreach ($b as $v) {
                    
                    $c = UserParent::where('parent_id',$v->user_id)->get();
                    $k = $i;
                    foreach ($c as $v2) {
                        
                        $child_user[$k] = $v2->user_id;
                        $k++;
                    }

                    $i = $k;
                }

                $j = $i;
            }


        }


        if(auth()->user()->hasRole('customer')){

           abort(404);

        }


        $sessionBet = FancyBetPlaced::where('selectionId',$SelectionId)->whereIn('user_id',$child_user)->get();



        $fancy = FancyBet::where('SelectionId',$SelectionId)->first();


        return view('admin.myaccount.singleFancy',compact('fancy','sessionBet'));
    }



}
