<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Auth;
use App\UserParent;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Wallet;
use App\Sitesetting;
use App\UserDefaultBet;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.dashboard');
    }

    public function create_admin(Request $request){
    	//dd($request->all());

        $d = User::where('username',$request->username)->first();

        if($d){
    	return redirect()->back()->with('flash_message','username Already Exist!');

        }else{


    	$user = new User;
    	$user->name = $request->full_name;
    	$user->username = $request->username;
    	$user->mobile_no = $request->mobile_no;
    	$user->password = bcrypt($request->password);
    	$user->email = $request->email;
        $user->commission = $request->commission;
        $user->pass_change = 'first';
        
    	$user->save();
    	$user->assignRole('admin');
    	$userparent = new UserParent;
    	$userparent->user_id = $user->id;
    	$userparent->parent_id= Auth::user()->id;
    	$userparent->save();


        $w = new Wallet;
        $w->user_id = $user->id;
        $w->wallet_amount = 0;
        $w->save();
    	// $role = new Role;
    	// $role->user_id = $user->id;
    	// $role->
        return redirect()->back()->with('flash_message1','User Added Successfully!');
        }
    }
    public function create_client(Request $request){
    	//dd($request->all());
        $d = User::where('username',$request->username)->first();

        if($d){
        return redirect()->back()->with('flash_message','username Already Exist!');

        }else{
    	$user = new User;
    	$user->name = $request->full_name;
    	$user->username = $request->username;
    	$user->mobile_no = $request->mobile_no;
    	$user->password = bcrypt($request->password);
    	$user->email = $request->email;
        $user->commission = $request->commission;
        $user->pass_change = 'first';

    	$user->save();
    	$user->assignRole('client');
    	$userparent = new UserParent;
    	$userparent->user_id = $user->id;
    	$userparent->parent_id= Auth::user()->id;
    	$userparent->save();

        $wallet = new Wallet;
        $wallet->user_id = $user->id;
        $wallet->wallet_amount = '0';
        $wallet->save();

    	// $role = new Role;
    	// $role->user_id = $user->id;
    	// $role->
    	return redirect()->back()->with('flash_message1','User Added Successfully!');
        }
    }
    public function create_customer(Request $request){
    	//dd($request->all());
        $d = User::where('username',$request->username)->first();

        if($d){
        return redirect()->back()->with('flash_message','username Already Exist!');

        }else{
    	$user = new User;
    	$user->name = $request->full_name;
    	$user->username = $request->username;
    	$user->mobile_no = $request->mobile_no;
    	$user->password = bcrypt($request->password);
    	$user->email = $request->email;
        $user->commission = $request->commission;
        $user->pass_change = 'first';

    	$user->save();
    	$user->assignRole('customer');
    	$userparent = new UserParent;
    	$userparent->user_id = $user->id;
    	$userparent->parent_id= Auth::user()->id;
    	$userparent->save();
        $wallet = new Wallet;
        $wallet->user_id = $user->id;
        $wallet->wallet_amount = '0';
        $wallet->save();

        $bet = new UserDefaultBet;
        $bet->user_id = $user->id;
        $bet->stake1 = 10;
        $bet->stake2 = 50;
        $bet->stake3 = 100;
        $bet->stake4 = 200;
        $bet->stake5 = 500;
        $bet->stake6 = 1000;

        $bet->save();

    	// $role = new Role;
    	// $role->user_id = $user->id;
    	// $role->
    	return redirect()->back()->with('flash_message1','Customer Created Successfully!');
    }
    }

    public function edit_admin(Request $request){
    	//dd($request->all());	

        //  return $request->validate([
        //     'full_name' => ['required', 'string', 'max:255'],
        //     'username' => ['required', 'string', 'max:255'],
        //     'mobile_no' => ['required', 'string', 'max:255', 'unique:users'],
        //     'password' => ['required', 'string', 'min:6', 'confirmed'],
        // ]);
       
        	$user = User::find($request->user_id);
        	$user->name = $request->full_name;
        	$user->username = $request->username;
        	$user->mobile_no = $request->mobile_no;
        	$user->password = bcrypt($request->password);
        	$user->email = $request->email;
            $user->commission = $request->commission;
        	$user->update();
        	return redirect()->back()->with('flash_message1','Admin Updated Successfully!');
        
    }
    public function delete_admin($id){
    	$user = User::find($id);
    	$user->delete();
    	return redirect()->back()->with('flash_message1','Admin Deleted Successfully!');

    }



    public function ChangeStatus(Request $request){
        $user_id = $request->user_id;
        $status = $request->status;
        $password = $request->password;
        $old_password = Auth::User()->password;
        if(Hash::check($password, $old_password))
        {
            $udata = User::find($user_id);
            $udata->status = $status;
            $udata->update();
            return response(array('success'=>true,'message'=>'<span style="font-size:16px;color:green">Status Change Successfull</span>'));
        }else{
            return response(array('success'=>false,'message'=>'<span style="font-size:16px;color:red">Wrong password. Try Agian...</span>'));
        }
    }





    public function edit_client(Request $request){
        //dd($request->all());  
        $d = User::where('username',$request->username)->first();

       
        $user = User::find($request->user_id);
        $user->name = $request->full_name;
        $user->username = $request->username;
        $user->mobile_no = $request->mobile_no;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->commission = $request->commission;
        $user->update();
        return redirect()->back()->with('flash_message1','Client Updated Successfully!');
    }



    public function edit_customer(Request $request){
        //dd($request->all());  

        
        $user = User::find($request->user_id);
        $user->name = $request->full_name;
        $user->username = $request->username;
        $user->mobile_no = $request->mobile_no;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->commission = $request->commission;
        $user->update();
        return redirect()->back()->with('flash_message1','Client Updated Successfully!');
    }
    





    public function edit_user(Request $request){
        //dd($request->all());  
        
        $user = User::find($request->user_id);
        $user->name = $request->full_name;
        $user->username = $request->username;
        $user->mobile_no = $request->mobile_no;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->commission = $request->commission;
        $user->update();
        return redirect()->back()->with('flash_message1','User Updated Successfully!');
        
    }





      public function chPass(Request $request){
        //dd($request->all());
        // $new_password           = $request->new_password;
        // $confirm_new_password   = $request->confirm_new_password;
        // $data = Auth::user();

        // if(!empty($new_password==$confirm_new_password)){

        //         $u = User::find(Auth::user()->id);
        // dd($u);

        //         if($u){

        //             $u->password = bcrypt($request->new_password);
        //             $u->pass_change == null;
        //             $u->update();

        //             return redirect('/')->with('flash_message','Password Change Successfull');
        //         }else{
        //             return redirect('/')->with('flash_message','Unauthorised User');

        //         }
           
        // }else{
        //             return redirect('/')->with('flash_message','Confirm Password Not Same. Try Agian...');
        // }


        $old_password           = $request->old_password;
        $new_password           = $request->new_password;
        $confirm_new_password   = $request->confirm_new_password;
        $data = Auth::user();
        if(!empty($new_password==$confirm_new_password)){
            if(Hash::check($old_password, $data->password))
            { 
               $data->password = Hash::make($new_password);
               $data->pass_change = null;
               $data->update();

               return redirect('/')->with('flash_message1','Password Change Successfull');

            }else{
                return redirect('/')->with('flash_message','Old Password Not Match. Try Agian...');
            }
        }else{
             return redirect('/')->with('flash_message','Password Not Match. Try Agian...');
        }


    }




    public function betConfig(){
        $data = Sitesetting::find(1);
        return view('admin.site_setting',compact('data'));

    }



    public function configUpdate(Request $request){

        $data = Sitesetting::find(1);

        $data->cricmaxbet = $request->cricmaxbet;
        $data->cricminbet = $request->cricminbet;
        $data->cricmaxfancybet = $request->cricmaxfancybet;
        $data->cricminfancybet = $request->cricminfancybet;
        $data->soccermaxbet = $request->soccermaxbet;
        $data->soccerminbet = $request->soccerminbet;
        $data->soccermaxfancybet = $request->soccermaxfancybet;
        $data->soccerminfancybet = $request->soccerminfancybet;
        $data->tennismaxbet = $request->tennismaxbet;
        $data->tennisminbet = $request->tennisminbet;
        $data->tennismaxfancybet = $request->tennismaxfancybet;
        $data->tennisminfancybet = $request->tennisminfancybet;
        $data->horseminbet = $request->horseminbet;
        $data->horsemaxbet = $request->horsemaxbet;
        $data->horseminfancybet = $request->horseminfancybet;
        $data->horsemaxfancybet = $request->horsemaxfancybet;

        $data->update();

        return redirect()->back()->with('flash_message1','Updated Successfully!!!');

    }




}
