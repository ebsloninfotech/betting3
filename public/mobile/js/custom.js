




	$('.singleMatchSelectionRow').on('keyup','.betInput input.oddInsert',function() {

		if($(this).val()>0){
			$('.singleMatchSelectionRow .betInput .placeBetBt').removeClass('disabled');
		}else{

			$('.singleMatchSelectionRow .betInput .placeBetBt').addClass('disabled');
		}

	});



	$('.singleMatchSelectionRow').on('click','.betInput a.cancelBt',function() {
		$(this).closest("div.betInput").remove();

		$('.to-lose').hide();
		$('.to-win').hide();

	});

	$('.singleMatchSelectionRow').on('click','.betInput #stakePopupList li a',function() {

		$(this).closest("div.betInput").find("input[name='betAmount']").val($(this).attr('stake'));
		$('.singleMatchSelectionRow .betInput .placeBetBt').removeClass('disabled');

		var odds = $(this).closest("div.betInput").find("input[name='oddVal']").val();
		var type = $(this).closest("div.betInput").find("input[name='oddVal']").attr('odd_type');
		var selection_id = $(this).closest("div.betInput").find("input[name='oddVal']").attr('selection_id');
		var stake = $(this).attr('stake');	


		if(type == 'back'){		

			$('.team-row').each(function(j,item2) {
				var t = 'row_'+selection_id;
				var w =(stake * odds)-stake;


				if($(this).hasClass(t)){

					$(this).find('.d-50 > .t-name').addClass('add-to-res');
					$(this).find('.d-50 > .t-name >.to-lose').hide();
					$(this).find('.d-50 > .t-name >.to-win').html('('+w.toFixed(2)+')');
					$(this).find('.d-50 > .t-name >.to-win').show();
				}else{
					$(this).find('.d-50 > .t-name').addClass('add-to-res');
					$(this).find('.d-50 > .t-name >.to-win').hide();
					$(this).find('.d-50 > .t-name >.to-lose').html('('+parseFloat(stake).toFixed(2)+')');
					$(this).find('.d-50 > .t-name >.to-lose').show();
				}

			});

		}else{

			$('.team-row').each(function(j,item2) {
				var t = 'row_'+selection_id;
				var w =(stake * odds)-stake;
				if($(this).hasClass(t)){

					$(this).find('.d-50 > .t-name').addClass('add-to-res');
					$(this).find('.d-50 > .t-name > .to-win').hide();
					$(this).find('.d-50 > .t-name > .to-lose').html('('+w.toFixed(2)+')');
					$(this).find('.d-50 > .t-name > .to-lose').show();
				}else{
					$(this).find('.d-50 > .t-name').addClass('add-to-res');
					$(this).find('.d-50 > .t-name > .to-lose').hide();
					$(this).find('.d-50 > .t-name > .to-win').html('('+parseFloat(stake).toFixed(2)+')');
					$(this).find('.d-50 > .t-name > .to-win').show();
				}

			});

		}


	});





	$('.singleMatchSelectionRow').on('keyup','.betInput .oddInsert',function() {

		// $(this).closest("div.betInput").find("input[name='betAmount']").val($(this).attr('stake'));
		$('.singleMatchSelectionRow .betInput .placeBetBt').removeClass('disabled');

		var odds = $(this).closest("div.betInput").find("input[name='oddVal']").val();
		var type = $(this).closest("div.betInput").find("input[name='oddVal']").attr('odd_type');
		var selection_id = $(this).closest("div.betInput").find("input[name='oddVal']").attr('selection_id');
		var stake = $(this).val();	
		

		if(type == 'back'){		

			$('.team-row').each(function(j,item2) {
				var t = 'row_'+selection_id;
				var w =(stake * odds)-stake;
				if($(this).hasClass(t)){

					$(this).find('.d-50 > .t-name').addClass('add-to-res');
					$(this).find('.d-50 > .t-name >.to-lose').hide();
					$(this).find('.d-50 > .t-name >.to-win').html('('+w.toFixed(2)+')');
					$(this).find('.d-50 > .t-name >.to-win').show();
				}else{
					$(this).find('.d-50 > .t-name').addClass('add-to-res');
					$(this).find('.d-50 > .t-name >.to-win').hide();
					$(this).find('.d-50 > .t-name >.to-lose').html('('+parseFloat(stake).toFixed(2)+')');
					$(this).find('.d-50 > .t-name >.to-lose').show();
				}

			});

		}else{

			$('.team-row').each(function(j,item2) {
				var t = 'row_'+selection_id;
				var w =(stake * odds)-stake;
				if($(this).hasClass(t)){

					$(this).find('.d-50 > .t-name').addClass('add-to-res');
					$(this).find('.d-50 > .t-name > .to-win').hide();
					$(this).find('.d-50 > .t-name > .to-lose').html('('+w.toFixed(2)+')');
					$(this).find('.d-50 > .t-name > .to-lose').show();
				}else{
					$(this).find('.d-50 > .t-name').addClass('add-to-res');
					$(this).find('.d-50 > .t-name > .to-lose').hide();
					$(this).find('.d-50 > .t-name > .to-win').html('('+parseFloat(stake).toFixed(2)+')');
					$(this).find('.d-50 > .t-name > .to-win').show();
				}

			});

		}


	});










	$('.placeBtn').click(function() {
		var event_id = $(this).attr('event_id');
		var type = $(this).attr('type');
		var market_id = $(this).attr('market_id');
		var selection_id = $(this).attr('selection_id');
		var title = $(this).attr('title');
		var odds = $(this).text();

		$('.betInput').remove();

		var text = '<div class="betInput d-100"><div class="d-50"><input type="text" class="text-center oddinput" name="oddVal" value="'+odds+'" selection_id="'+selection_id+'" odd_type="'+type+'" readonly></div><div class="d-50"><input type="number" class="oddInsert" min="1" name="betAmount" value=""></div><ul id="stakePopupList" class="coin-list"> <li><a id="selectStake_1" href="#" stake="10"> 10</a></li><li><a id="selectStake_2" href="#" stake="50"> 50</a></li><li><a id="selectStake_3" href="#" stake="200"> 200</a></li><li><a id="selectStake_4" href="#" stake="300"> 300</a></li><li><a id="selectStake_5" href="#" stake="500"> 500</a></li><li><a id="selectStake_6" href="#" stake="1000"> 1000 </a></li></ul><div class="d-50"><a href="#" class="cancelBt">Cancel</a></div><div class="d-50"><a href="#" class="placeBetBt disabled" selection_id="'+selection_id+'" type="'+type+'">Place Bet</a></div></div>';

		$(text).insertAfter('.row_'+selection_id);
	});
	




	$('.a-open_bets').click(function() {

		$('.loader').show();

		$.ajax({
		        type: 'POST',
		        url: '/mobile/getMyBets',
		        headers: {
		                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		               },
		        success: function(res) {

		        		var t = '';
		              $.each(res[0].data, function(i, item) {

		              	if (item.market_type == 'fancy') {

		              		t += '<li> <a href="#" data-toggle="collapse" data-target="#collapse'+i+'" class="OpbetList "  check="collapse'+i+'"> <p>'+item.event_name+' &nbsp; ( '+item.title+') </p></a></li>'
			              	t += '<div id="collapse'+i+'" class="collapse "><p class="m-0 betinsidehead"> <a href="#"><i class="fa fa-chevron-left"></i></a> '+item.event_name+' &nbsp;  ('+item.title+')</p><p class="m-0 small text-center"> '+item.title+'</p> <p class="bMatch">Matched <span>(Fancy)</span></p>';
			              	var innerbet = fetchSelectionBet(item.selection_id);
			              	$.each(innerbet, function(j, item2) {
				              	t+= '<div class="d-100 side-head-row"><div class="d-25"><p>'+item2.bet_type+'</p></div><div class="d-25"><p>Odds</p></div><div class="d-25"><p>Stake</p></div><div class="d-25"><p>Time</p></div></div><div class="d-100 data-row"><div class="d-25"><p>';
				              	if(item2.bet_type == 'lay'){

				              		t+= '<span class="btnlay">No</span></p></div><div class="d-25"><p>'+item2.fancy_price+'/'+item2.odds+'</p></div><div class="d-25"><p>'+item2.bet_amount+'</p></div><div class="d-25"><p>'+item2.created_at+'</p></div></div>';
				              	}else{

				              		t+= '<span class="btnback">Yes</span></p></div><div class="d-25"><p>'+item2.fancy_price+'/'+item2.odds+'</p></div><div class="d-25"><p>'+item2.bet_amount+'</p></div><div class="d-25"><p>'+item2.created_at+'</p></div></div>';
				              	}

			              	});

		              	}else{


			              	t += '<li> <a href="#" data-toggle="collapse" data-target="#collapse'+i+'" class="OpbetList" check="collapse'+i+'">'+item.event_name+' &nbsp;( '+item.selection.runner_name+')</a></li>'
			              	t += '<div id="collapse'+i+'" class="collapse"><p class="m-0 betinsidehead"> <a href="#"><i class="fa fa-chevron-left"></i></a> '+item.event_name+' &nbsp;  ('+item.selection.runner_name+')</p><p class="m-0 small text-center"> '+item.selection.runner_name+'</p> <p class="bMatch">Matched</p>';
			              	var innerbet = fetchSelectionBet(item.selection_id);
			              	$.each(innerbet, function(j, item2) {
			              		t+= '<div class="d-100 side-head-row"><div class="d-25"><p>'+item2.bet_type+'</p></div><div class="d-25"><p>Odds</p></div><div class="d-25"><p>Stake</p></div><div class="d-25"><p>Time</p></div></div><div class="d-100 data-row"><div class="d-25"><p>';
			              	if(item2.bet_type == 'lay'){

			              		t+= '<span class="btnlay">'+item2.bet_type+'</span></p></div><div class="d-25"><p>'+item2.odds+'</p></div><div class="d-25"><p>'+item2.bet_amount+'</p></div><div class="d-25"><p>'+item2.created_at+'</p></div></div>';
			              	}else{

			              		t+= '<span class="btnback">'+item2.bet_type+'</span></p></div><div class="d-25"><p>'+item2.odds+'</p></div><div class="d-25"><p>'+item2.bet_amount+'</p></div><div class="d-25"><p>'+item2.created_at+'</p></div></div>';
			              	}
			              });
		              	}
		              	t+='</div>';
		              });

		              $('.betlist').html(t);
						$('.loader').hide();
						$('.sidepanel').show();
		        }
		      });

	});

	$('#closeSidePanel').click(function() {


		$('.sidepanel').hide();
	});



	$('.sidepanel').on('click','.betlist li a',function(){


		$('.betlist li').hide();




	});


	function fetchSelectionBet(selection_id){
		var d = [];
		$.ajax({
		        type: 'POST',
		        url: '/mobile/fetchSelectionBet',
		        async: false,
		        headers: {
		                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		               },
		        data:{selection_id:selection_id},       
		        success: function(res) {
		        	d =  res[0].data;
		        }

		    })
		return d;
	}

	$('.sidepanel').on('click','.betinsidehead a',function(){

		$('.betlist li').show();
		$(this).closest('.collapse').removeClass('show');




	});




	$('.balanceRefresh').click(function() {

		$('.loader').show();

		$.ajax({
		        type: 'POST',
		        url: '/mobile/getBalance',
		        headers: {
		                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		               },
		        success: function(res) {

		              $('.betCredit').html('PTH '+res[0].wallet);
		              $('.totalExposure').html(res[0].exposure);
						$('.loader').hide();
		        }
		    });

	});


