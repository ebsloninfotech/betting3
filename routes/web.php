<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	// dd(find_role());
// if(Auth::user()){




Route::group(['prefix' => 'master', 'middleware' => ['auth', 'roles'], 'roles' => ['master']], function () {  

	Route::get('/', 'Admin\\AdminController@index');
	Route::get('/index', 'Admin\\AdminController@index');
	Route::resource('/users', 'Admin\\UsersController');

	Route::get('/downline/','Admin\\DownLineController@index');

	Route::get('/myaccount/','Admin\\MyAccountController@index');
	Route::get('/myaccount/myprofile','Admin\\MyAccountController@MyProfile');
	Route::post('/myaccount/change-password','Admin\\MyAccountController@ChangePassword');
	Route::post('/myaccount/update-profile','Admin\\MyAccountController@UpdateProfile');
	Route::get('/myaccount/myaccountstatement','Admin\\MyAccountController@accountstatement');
	Route::get('/myaccount/mycasinostatement','Admin\\MyAccountController@casinostatement');
	Route::get('/myaccount/myloginhistory','Admin\\MyAccountController@loginhistory');

	Route::get('/bethistory','Admin\\BetHistoryController@index');

	Route::get('/myreport/downline','Admin\\MyReportsController@downline');

	Route::get('/myreport/market','Admin\\MyReportsController@market');

	Route::get('/livebets','Admin\\LiveBetsController@index');

	Route::get('/liveusers','Admin\\LiveUsersController@index');

	Route::get('/transfer','Admin\\TransferController@index');

	Route::post('/add_admin','Admin\\AdminController@create_admin');
	Route::post('/change-status','Admin\\AdminController@ChangeStatus');
	Route::post('/edit_admin','Admin\\AdminController@edit_admin');
	Route::delete('/delete_admin/{id}','Admin\\AdminController@delete_admin');
	Route::post('/slider-status','Admin\\SliderController@SliderStatus');
	Route::resource('/slider','Admin\\SliderController');
	Route::resource('/news','NewsController');
	Route::post('/news-status','NewsController@newsStatus');

	Route::get('competitionList/{id}','CompetitionController@list_comp');
	Route::get('competition/horse_racing','HorseController@list_event_horse_racing');
	Route::post('competitionStatus','CompetitionController@competitionStatus');
	Route::post('marketStatus','CompetitionController@marketStatus');
	Route::post('eventStatus','CompetitionController@eventStatus');
	Route::post('eventBetStatus','CompetitionController@eventBetStatus');
	Route::get('eventList/{id}/{comp_id}','CompetitionController@list_event');
	Route::get('marketList/{id}/{comp_id}/{event_id}','CompetitionController@list_market');
	Route::get('horsemarketList/{id}/{event_id}','CompetitionController@list_market_horse');

	Route::get('competition/refresh/{type}','CompetitionController@refreshComp');

	Route::get('reports','Admin\\MyAccountController@reports');

	Route::get('matches/{id}','Admin\\MyAccountController@matches');

	//admin 

	Route::get('/memberSummary/{id}','Admin\\DownLineController@masterMemberSummary');
	Route::get('/memberDwonline/{id}','Admin\\DownLineController@master_member_Downline');
	// Route::get('/memberBets/{id}','Admin\\DownLineController@master_member_Bets');
	Route::get('/memberTransactions/{id}','Admin\\DownLineController@master_member_Transactions');
	Route::get('/memberLog/{id}','Admin\\DownLineController@master_member_Log');
	Route::post('memberSummary/member-change-password','Admin\\DownLineController@change_password');


	// client


	Route::get('clientMember/{admin_id}/{id}','Admin\\DownLineController@clientMemberSummary');
	Route::get('clientDownline/{admin_id}/{id}','Admin\\DownLineController@client_member_Downline');
	Route::get('/clientTransactions/{admin_id}/{id}','Admin\\DownLineController@client_member_Transactions');
	Route::get('/clientLog/{admin_id}/{id}','Admin\\DownLineController@client_member_Log');


	//user 
	Route::get('userMember/{admin_id}/{client_id}/{id}','Admin\\DownLineController@userMemberSummary');
	Route::get('userBets/{admin_id}/{client_id}/{id}','Admin\\DownLineController@user_member_bets');
	Route::get('profitLose/{admin_id}/{client_id}/{id}','Admin\\DownLineController@user_member_profitLose');
	Route::get('userBalance/{admin_id}/{client_id}/{id}','Admin\\DownLineController@user_member_userBalance');
	Route::get('/userTransactions/{admin_id}/{client_id}/{id}','Admin\\DownLineController@user_member_Transactions');
	Route::get('/userLog/{admin_id}/{client_id}/{id}','Admin\\DownLineController@user_member_Log');




	Route::get('cancelBet/{event_id}','CompetitionController@cancelBet');
	Route::get('settleBet/{event_id}','CompetitionController@settleBet');
	Route::get('cancelsettleBet/{event_id}','CompetitionController@cancelsettleBet');

	Route::get('fancyBet/{event_id}','CompetitionController@fancyBet');

	Route::get('bet_config','Admin\\AdminController@betConfig');
	Route::post('configUpdate','Admin\\AdminController@configUpdate');

	Route::post('fancyResult','CompetitionController@fancyResult');
	Route::get('settleFancyBet/{SelectionId}','CompetitionController@fancySettle');
	Route::get('cancelfancySettle/{SelectionId}','CompetitionController@cancelfancySettle');
	Route::get('cancelFancyBet/{SelectionId}','CompetitionController@cancelFancyBet');



	Route::post('market/minBet','CompetitionController@marketMinBet');
	Route::post('market/maxBet','CompetitionController@marketMaxBet');
	Route::post('market/commType','CompetitionController@marketcommType');
	Route::post('market/commValu','CompetitionController@marketcommValu');


	Route::post('fancy/minBet','CompetitionController@fancyMinBet');
	Route::post('fancy/maxBet','CompetitionController@fancyMaxBet');
	Route::post('fancy/commType','CompetitionController@fancycommType');
	Route::post('fancy/commValu','CompetitionController@fancycommValu');

	Route::get('matches/{id}','Admin\\MyAccountController@matches');

});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'roles'], 'roles' => ['admin']], function () {  


	Route::get('/', 'Admin\\AdminController@index');
	Route::get('/index', 'Admin\\AdminController@index');
	// Route::get('/give-role-permissions', 'Admin\\AdminController@getGiveRolePermissions');
	// Route::post('admin/give-role-permissions', 'Admin\\AdminController@postGiveRolePermissions');
	// Route::resource('admin/roles', 'Admin\\RolesController');
	// Route::resource('admin/permissions', 'Admin\\PermissionsController');

	Route::resource('admin/users', 'Admin\\UsersController');

	Route::get('/downline/','Admin\\DownLineController@index');

	Route::get('/myaccount/','Admin\\MyAccountController@index');
	Route::get('/myaccount/myprofile','Admin\\MyAccountController@MyProfile');
	Route::post('/myaccount/change-password','Admin\\MyAccountController@ChangePassword');
	Route::post('/myaccount/update-profile','Admin\\MyAccountController@UpdateProfile');
	Route::get('/myaccount/myaccountstatement','Admin\\MyAccountController@accountstatement');
	Route::get('/myaccount/mycasinostatement','Admin\\MyAccountController@casinostatement');
	Route::get('/myaccount/myloginhistory','Admin\\MyAccountController@loginhistory');

	Route::get('matches/{id}','Admin\\MyAccountController@matches');

	Route::post('/edit_client','Admin\\AdminController@edit_client');

	Route::get('/bethistory','Admin\\BetHistoryController@index');

	Route::get('/myreport/downline','Admin\\MyReportsController@downline');

	Route::get('/myreport/market','Admin\\MyReportsController@market');

	Route::get('/livebets','Admin\\LiveBetsController@index');

	Route::get('/liveusers','Admin\\LiveUsersController@index');

	Route::get('/transfer','Admin\\TransferController@index');

	Route::post('/add_client','Admin\\AdminController@create_client');
	Route::post('/change-status','Admin\\AdminController@ChangeStatus');

	Route::post('/add-bet-config','Admin\\BetHistoryController@AddBetConfig');
	Route::post('/update-bit-config','Admin\\BetHistoryController@UpdateBitConfig');

	Route::get('reports','Admin\\MyAccountController@reports');


	// client

	Route::get('/memberSummary/{id}','Admin\\DownLineController@adminclientMemberSummary');
	Route::get('clientDownline/{id}','Admin\\DownLineController@adminclient_member_Downline');
	Route::get('/clientTransactions/{id}','Admin\\DownLineController@adminclient_member_Transactions');
	Route::get('/clientLog/{id}','Admin\\DownLineController@adminclient_member_Log');


	//user

	//user 
	Route::get('userMember/{client_id}/{id}','Admin\\DownLineController@admin_userMemberSummary');
	Route::get('userBets/{client_id}/{id}','Admin\\DownLineController@admin_user_member_bets');
	Route::get('profitLose/{client_id}/{id}','Admin\\DownLineController@admin_user_member_profitLose');
	Route::get('userBalance/{client_id}/{id}','Admin\\DownLineController@admin_user_member_userBalance');
	Route::get('/userTransactions/{client_id}/{id}','Admin\\DownLineController@admin_user_member_Transactions');
	Route::get('/userLog/{client_id}/{id}','Admin\\DownLineController@admin_user_member_Log');


});


Route::group(['prefix' => 'client', 'middleware' => ['auth', 'roles'], 'roles' => ['client']], function () {  


	Route::get('/', 'Admin\\AdminController@index');
	Route::get('/index', 'Admin\\AdminController@index');
	// Route::get('/give-role-permissions', 'Admin\\AdminController@getGiveRolePermissions');
	// Route::post('admin/give-role-permissions', 'Admin\\AdminController@postGiveRolePermissions');
	// Route::resource('admin/roles', 'Admin\\RolesController');
	// Route::resource('admin/permissions', 'Admin\\PermissionsController');

	Route::resource('admin/users', 'Admin\\UsersController');

	Route::get('/downline/','Admin\\DownLineController@index');

	Route::post('/edit_customer','Admin\\AdminController@edit_customer');

	Route::get('/myaccount/','Admin\\MyAccountController@index');
	Route::get('/myaccount/myprofile','Admin\\MyAccountController@MyProfile');
	Route::post('/myaccount/change-password','Admin\\MyAccountController@ChangePassword');
	Route::post('/myaccount/update-profile','Admin\\MyAccountController@UpdateProfile');
	Route::get('/myaccount/myaccountstatement','Admin\\MyAccountController@accountstatement');
	Route::get('/myaccount/mycasinostatement','Admin\\MyAccountController@casinostatement');
	Route::get('/myaccount/myloginhistory','Admin\\MyAccountController@loginhistory');

	Route::get('matches/{id}','Admin\\MyAccountController@matches');

	Route::get('/bethistory','Admin\\BetHistoryController@index');

	Route::post('/edit_user','Admin\\AdminController@edit_user');
	Route::get('/myreport/downline','Admin\\MyReportsController@downline');

	Route::get('/myreport/market','Admin\\MyReportsController@market');

	// Route::get('/livebets','Admin\\LiveBetsController@index');

	Route::get('/liveusers','Admin\\LiveUsersController@index');

	Route::get('/transfer','Admin\\TransferController@index');

	Route::post('/add_customer','Admin\\AdminController@create_customer');
	Route::post('/change-status','Admin\\AdminController@ChangeStatus');

	Route::post('/add-bet-config','Admin\\BetHistoryController@AddBetConfig');
	Route::post('/update-bit-config','Admin\\BetHistoryController@UpdateBitConfig');
	Route::get('/livebets','Admin\\BetHistoryController@livebets');
	Route::get('/setteledBets','Admin\\BetHistoryController@setteledBets');
	Route::post('/settleBet','Admin\\BetHistoryController@settle_bet');
	Route::post('/submitBetSettle','Admin\\BetHistoryController@submit_bet_settle');
	Route::get('/betlist','Admin\\BetHistoryController@betListClient');

	Route::get('/memberSummary/{client_id}/{id}','Admin\\DownLineController@fromclientMemberSummary');
	Route::get('/userBets/{client_id}/{id}','Admin\\DownLineController@client_user_Bets');
	Route::get('/userprofitLoss/{client_id}/{id}','Admin\\DownLineController@userprofitLoss');
	Route::get('/userTransactions/{client_id}/{id}','Admin\\DownLineController@client_user_member_Transactions');
	Route::get('/userLog/{client_id}/{id}','Admin\\DownLineController@client_user_member_Log');
	Route::post('memberSummary/member-change-password','Admin\\DownLineController@change_password');
	Route::get('balance_overview','Admin\\MyAccountController@balanceView');
	Route::get('reports','Admin\\MyAccountController@reports');

});

Route::group(['prefix' => 'customer', 'middleware' => ['auth', 'roles'], 'roles' => ['customer']], function () {  



	Route::get('/', 'Admin\\AdminController@index');
	Route::get('/index', 'Admin\\AdminController@index');
	// Route::resource('admin/users', 'Admin\\UsersController');
	// Route::get('/downline/','Admin\\DownLineController@index');
	Route::get('/myaccount/','Admin\\MyAccountController@index');
	Route::get('/myaccount/myprofile','Admin\\MyAccountController@MyProfile');
	Route::post('/myaccount/change-password','Admin\\MyAccountController@ChangePassword');
	Route::post('/myaccount/update-profile','Admin\\MyAccountController@UpdateProfile');
	Route::get('/myaccount/myaccountstatement','Admin\\MyAccountController@accountstatement');
	// Route::get('/myaccount/mycasinostatement','Admin\\MyAccountController@casinostatement');
	Route::get('/myaccount/myloginhistory','Admin\\MyAccountController@loginhistory');

	Route::get('/bethistory','Admin\\BetHistoryController@index');

	
	Route::get('/profitLoss','Admin\\BetHistoryController@profitLoss');

	Route::get('/myreport/downline','Admin\\MyReportsController@downline');

	Route::get('/myreport/market','Admin\\MyReportsController@market');

	Route::get('/livebets','Admin\\LiveBetsController@index');

	Route::get('/liveusers','Admin\\LiveUsersController@index');

	Route::get('/transfer','Admin\\TransferController@index');

	Route::post('/add_customer','Admin\\AdminController@create_customer');
	Route::post('/change-status','Admin\\AdminController@ChangeStatus');

	Route::get('balance_overview','Admin\\MyAccountController@balanceView');

});

Route::group(['middleware' =>['auth']], function () { 
	Route::post('/credit-apply','Admin\\MyAccountController@CreditApply');

	Route::post('/customer/BetPlaced','BetController@placed');
	Route::post('/customer/FancyBetPlaced','BetController@FancyBetPlaced');
	Route::post('/customer/SideBetPlaced','BetController@SideBetPlaced');

	Route::post('matchpl','MainController@matchpl');
	Route::post('matchedBet','MainController@matchedBet');
	Route::post('matchedTotalPoint','MainController@matchedTotalPoint');
	Route::post('UpdateBetStake','MainController@UpdateBetStake');

	Route::post('matchedSessionBet','MainController@matchedSessionBet');
	Route::post('fetchTotalBook','Admin\\MyAccountController@fetchTotalBook');
	Route::get('singlefancy/userList/{SelectionId}','Admin\\MyAccountController@singlefancy');


});

Route::get('/home', 'HomeController@index')->name('home');
Route::post('fetchSideBarData','MainController@fetch_Side_Bar_Data');
Auth::routes();
Route::get('/','MainController@main_home')->name('site');
Route::post('EventsAccMarket','MainController@Events_Acc_Market');
Route::post('SingleEventsAccMarket','MainController@SingleEvents_Acc_Market');

Route::get('/inplay', 'MainController@inplay_event')->name('home');
Route::get('fullmarket/{type}/{event}/{market}','MainController@single_match_page');

Route::get('fullmarket/{type}','MainController@SingleEventTypePAge');


Route::get('fullracingmarket/{type}/{event}/{market}','MainController@single_match_racing');

Route::post('chPass','Admin\\AdminController@chPass');


Route::post('fancyBet','MainController@SessionBet');



Route::post('matchResult','MainController@matchResult');




Route::post('checkLoginDetail','MainController@checkLoginDetail');



Route::get('checkSession','MainController@checkSession');




Route::get('valid_code','MainController@valid_code');
Route::get('check_code','MainController@check');
Route::post('searchEvent','MainController@searchEvent');

// mobile route 


Route::group(['prefix' => 'mobile'], function () {  

	Route::get('fullmarket/{type}/{event}/{market}','MobileController@single_match_page')->middleware('auth');
	Route::get('/index', 'MobileController@index')->name('index');
	Route::get('/account', 'MobileController@account')->middleware('auth');
	Route::get('inplay','MobileController@inplay')->middleware('auth');
	Route::get('today','MobileController@today')->middleware('auth');
	Route::get('tomorrow','MobileController@tomorrow')->middleware('auth');
	Route::post('getMyBets','MobileController@getMyBets')->middleware('auth');
	Route::post('fetchSelectionBet','MobileController@fetchSelectionBet')->middleware('auth');
	Route::post('getBalance','MobileController@getBalance')->middleware('auth');
	Route::post('SingleEventsAccMarket','MainController@SingleEvents_Acc_Market');
	Route::post('SingleEventsFullMarket','MainController@SingleEvents_Full_Market');
});