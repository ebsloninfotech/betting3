<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditsTable extends Migration
{

    public function up()
    {
        Schema::create('credits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('points')->nullable();            
            $table->integer('receiver_id')->nullable();
            $table->integer('sender_id')->nullable();
            $table->text('remarks')->nullable();
            $table->string('type')->nullable();
            $table->boolean('status')->default(1);
            $table->string('transaction_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('credits');
    }
}
