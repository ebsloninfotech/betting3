<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetplacedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('betplaceds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('event_type')->nullable();
            $table->string('status')->default(1);
            $table->string('bet_amount')->nullable();
            $table->string('bet_type')->nullable();
            $table->string('event_id')->nullable();
            $table->string('event_name')->nullable();
            $table->string('market_id')->nullable();
            $table->string('odds')->nullable();
            $table->float('profit',8,2)->nullable();
            $table->timestamps();
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('betplaceds');
    }
}
