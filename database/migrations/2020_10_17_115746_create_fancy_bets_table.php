<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFancyBetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fancy_bets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_id')->nullable();
            $table->string('SelectionId')->nullable();
            $table->string('RunnerName')->nullable();
            $table->string('BackPrice1')->nullable();
            $table->string('BackSize1')->nullable();
            $table->string('LayPrice1')->nullable();
            $table->string('LaySize1')->nullable();
            $table->string('GameStatus')->nullable();
            $table->string('MarkStatus')->nullable();
            $table->string('result')->nullable();
            $table->string('resultStatus')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fancy_bets');
    }
}
