<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDefaultBetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_default_bets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('stake1');
            $table->string('stake2');
            $table->string('stake3');
            $table->string('stake4');
            $table->string('stake5');
            $table->string('stake6');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_default_bets');
    }
}
