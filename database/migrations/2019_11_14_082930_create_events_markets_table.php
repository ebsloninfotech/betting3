<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsMarketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_markets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_type')->nullable();
            $table->string('competition_id')->nullable();
            $table->string('event_id')->nullable();
            $table->string('marketId')->nullable();
            $table->string('marketName')->nullable();
            $table->string('marketStartTime')->nullable();
            $table->string('status')->default(0);
            $table->string('isFancy')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events_markets');
    }
}
