<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketRunnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_runners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_id')->nullable();
            $table->string('marketId')->nullable();
            $table->string('marketName')->nullable();
            $table->string('event_type')->nullable();
            $table->string('competition_id')->nullable();
            $table->string('selection_id')->nullable();
            $table->string('runner_name')->nullable();
            $table->string('sort_priority')->nullable();
            $table->string('extra')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_runners');
    }
}
