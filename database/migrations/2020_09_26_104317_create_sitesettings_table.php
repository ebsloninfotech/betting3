<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sitesettings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cricmaxbet');
            $table->string('cricminbet');
            $table->string('cricmaxfancybet');
            $table->string('cricminfancybet');
            $table->string('soccermaxbet');
            $table->string('soccerminbet');
            $table->string('soccermaxfancybet');
            $table->string('soccerminfancybet');
            $table->string('tennismaxbet');
            $table->string('tennisminbet');
            $table->string('tennismaxfancybet');
            $table->string('tennisminfancybet');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sitesettings');
    }
}
