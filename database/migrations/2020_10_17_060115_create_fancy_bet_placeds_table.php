<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFancyBetPlacedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fancy_bet_placeds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('selectionId');
            $table->string('runnerName');
            $table->string('type');
            $table->string('prize');
            $table->string('size');
            $table->string('stake');
            $table->string('event_id')->nullable();
            $table->string('event_name')->nullable();
            $table->string('event_type')->nullable();
            $table->string('status')->nullable();
            $table->string('bet_status')->nullable();
            $table->string('profit')->nullable();
            $table->string('profit_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fancy_bet_placeds');
    }
}
