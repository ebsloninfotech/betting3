<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bets', function (Blueprint $table) {
             $table->increments('id');
            $table->integer('payer_id')->nullable();
            $table->integer('receiver_id')->nullable();
            $table->string('event_type')->nullable();
            $table->float('min',8,2)->nullable();
            $table->float('max',8,2)->nullable();
            $table->float('bet_delay',8,2)->default(0);
            $table->float('exposure',8,2)->nullable();
            $table->float('profit',8,2)->nullable();
            $table->string('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bets');
    }
}
