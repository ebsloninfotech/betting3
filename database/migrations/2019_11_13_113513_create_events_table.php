<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_id')->nullable();
            $table->string('event_type')->nullable();
            $table->string('competition_id')->nullable();
            $table->string('name')->nullable();
            $table->string('countryCode')->nullable();
            $table->string('timezone')->nullable();
            $table->string('openDate')->nullable();
            $table->string('marketCount')->nullable();
            $table->string('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
