@extends('layouts.front')


@section('content')
    <div class="container-fluid">
	
        <div class="row">
        	@guest
        		<div class="col-md-2 col-xs-2 col-sm-2 p-0">
				    @include('include/sidebar')
				</div>
        	@else
	        	@if(Auth::user()->hasRole('customer'))
				<div class="col-md-2 col-xs-2 col-sm-2 p-0">
				    @include('include/sidebar')
				</div>
				@endif
			@endguest
			

			<div class="col-md-7 col-xs-7 col-xs7">
				@if(fetchNews())
				<div class="marqueerow">    
			        <div class="marquee-box">
			                <h4><i class="fa fa-microphone"></i> News</h4>
			                <marquee>
			                	@foreach(fetchNews() as $v)
			                		<span>{{ date('d M Y',strtotime($v->created_at)) }}</span>
			                		{{ $v->news }}
			                		&nbsp;&nbsp;&nbsp;
			                	@endforeach
			                </marquee>
			        </div>
			    </div>
			    @endif
			   <div class="marketSec">
			   	<div class="SecHead">
			   		
			   		<h5>{{ $totalMarket[0]->event->name }}</h5>
			   		
			   	</div>
			   	<div class="match-odds-in">
			   		{{ $MarketBook?$MarketBook[0]->marketName: 'Match Odds' }}
			   		<img src="http://betting3.ebslon.com/images/transparent.gif" alt="">
			   	</div>
			   	<ul class="game-info list-unstyled">
			   		<li>
			   			<img class="icon-time" src="http://betting3.ebslon.com/images/transparent.gif" alt="">
			   			@php 
			   				$starttime = strtotime("+330 minutes", strtotime($totalMarket[0]->event->openDate));
			   			@endphp

			   			 {{ date('d-M-Y H:i:s',$starttime)  }}
			   		</li>
			   		@php
					$minMax = minmax($totalMarket[0]->event_type);
					@endphp
					@if(count($minMax)>0)
			   		<li><span class="game-low_liq" style="">Min/Max : {{ $minMax['min'] }}/{{ $minMax['max'] }}</span></li>
						{{-- <span class="minmaxbt">Min/Max : {{ $minMax['min'] }}/{{ $minMax['max'] }} </span> --}}
					@endif
			   	</ul>
			   {{-- 	<dl class="game-matched">
					<dt>Matched</dt>
					<dd>PTE 189</dd>
				</dl>
			   	<dl class="fancy-info">
					<dt>Min/Max</dt>
					<dd id="minMaxInfo">1 / 2400</dd>
				</dl> --}}
			   	<div class="secContent">
			   		<table class="table table-hover table-bordered text-center bg-white">
					   <thead class="table-blue-color">
					      <tr class="border-0">
					         <th class="border-0"><p>{{ $MarketBook?count($MarketBook[0]->runners):'2'}} Selections</p></th>
					         <td colspan="2" class="border-0 t-l"></td>
					         <td>Back</td>
					         <td>Lay</td>
					         <td colspan="2"></td>
					      </tr>
					   </thead>
					   <tbody class="singleMatchSelectionRow">

					{{--    	@auth
					   		 @php 
					   	 		$pl = matchPL($event);  
					   	 	@endphp

					   	@endauth --}}	
					   		@php
					   			$i=0;

					   		@endphp
					   	@foreach($totalMarket[0]->horserunner as $value)			
					   	<tr class="team-row row_{{ $value->selection_id }} " id="row_{{ $market }}" event_id ="{{ $event }}" market_id="{{ $market }}" title="{{ $value->runner_name }}" name="" selection_id="{{ $value->selection_id }}"  >
					   		<th>
					   			<p><i class="fa fa-bar-chart"></i>{{ $value->runner_name }}  
					   				@auth
					   					@php
					   						$bets = currentBets($market); 
					   						$pl = matchPL($market,$value->selection_id); 
					   					@endphp
					   					@if(count($bets)>0)

					   						@if($pl['status'] == 'lose')
					   						<span class="matchPL{{ $i }}  clsred ">{{ $pl['net_profit'] }}</span>

					   						@else
					   						<span class="matchPL{{ $i }} clsgreen ">{{ $pl['net_profit'] }}</span>
					   						@endif
					   					@else
					   					<span class="matchPL{{ $i }}"></span>	
					   					@endif
					   				@endauth
					   			</p>
					   		</th>
							@php 

							$i++;

								$params = '{"marketId":"'.$market.'",
									"selectionId":"'.$value->selection_id.'",     
									"priceProjection":{"priceData":["EX_ALL_OFFERS"]}
							}';

								$data = listRunnerBook($params);

								// $curl = curl_init();

						  //        curl_setopt_array($curl, array(
						  //          CURLOPT_URL => "http://178.62.105.201/api/v1/listMarketBookOdds?market_id=".$market."",
						  //          CURLOPT_RETURNTRANSFER => true,
						  //          // CURLOPT_ENCODING => "",
						  //          CURLOPT_MAXREDIRS => 10,
						  //          CURLOPT_TIMEOUT => 10,
						  //          CURLOPT_FOLLOWLOCATION => true,
						  //          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  //          CURLOPT_CUSTOMREQUEST => "GET",
						  //        ));

						  //        $response = curl_exec($curl);

						  //        curl_close($curl);

						  //        $data = json_decode($response);

							@endphp

							{{-- @if(property_exists($data[0],'runners') && $data[0]->status == 'ACTIVE') --}}
							@if(property_exists($data[0],'runners'))
					   		<td class="text-center two_points_single " selection_id="{{ $value->selection_id}}" >
								<div class="Msuspended d-none">Suspend</div>

					   			@if(count($data[0]->runners[0]->ex->availableToBack)>0)	
					   			<a href="#" event_id ="{{ $event }}" market_id="{{ $market }}" event_type="{{ $totalMarket[0]->event->event_type }}" title="{{ $value->runner_name }}" selection_id="{{ $value->selection_id }}"  class=" btnBack text-decoration-none fadeColorBack @if($eventDetail->bet_status != 1) betDisabled @else placeBtn @endif">-- </a>
					   			@else
					   				<a href="javascript:void(0)" class="btnBack text-decoration-none fadeColorBack"> -- </a>
								@endif	
					   		</td>
					   		<td class="text-center two_points_single " selection_id="{{ $value->selection_id}}" >
								<div class="Msuspended d-none">Suspend</div>
					   			@if(count($data[0]->runners[0]->ex->availableToBack)>1)	
					   			<a href="#" event_id ="{{ $event }}" market_id="{{ $market }}" event_type="{{ $totalMarket[0]->event->event_type }}" title="{{ $value->runner_name }}" selection_id="{{ $value->selection_id }}"  class=" btnBack text-decoration-none fadeColorBack @if($eventDetail->bet_status != 1) betDisabled @else placeBtn @endif"> --</a>
								@else
					   				<a href="javascript:void(0)" class="btnBack text-decoration-none fadeColorBack"> -- </a>
								@endif
					   		</td>
					   		<td class="text-center two_points_single " selection_id="{{ $value->selection_id}}" >
								<div class="Msuspended d-none">Suspend</div>
					   			@if(count($data[0]->runners[0]->ex->availableToBack)>2)	
					   			<a href="#" event_id ="{{ $event }}" event_type="{{ $totalMarket[0]->event->event_type }}" market_id="{{ $market }}" title="{{ $value->runner_name }}" selection_id="{{ $value->selection_id }}"  class="@if($eventDetail->bet_status != 1) betDisabled @else placeBtn @endif btnBack text-decoration-none"> --</a>
								@else
					   				<a href="javascript:void(0)" class="btnBack text-decoration-none"> -- </a>
								@endif
					   		</td>
					   		<td class="text-center two_points_single " selection_id="{{ $value->selection_id}}" >
								<div class="Msuspended d-none">Suspend</div>
								@if(count($data[0]->runners[0]->ex->availableToLay)>0)
					   			<a href="#" event_id ="{{ $event }}" market_id="{{ $market }}" event_type="{{ $totalMarket[0]->event->event_type }}" title="{{ $value->runner_name }}" selection_id="{{ $value->selection_id }}"  class="@if($eventDetail->bet_status != 1) betDisabled @else placeBtn @endif btnLay text-decoration-none"> --</a>
								@else
					   				<a href="javascript:void(0)" class="btnBack text-decoration-none"> -- </a>
								@endif
					   		</td>
					   		<td class="text-center two_points_single " selection_id="{{ $value->selection_id}}" >
								<div class="Msuspended d-none">Suspend</div>
								@if(count($data[0]->runners[0]->ex->availableToLay)>1)
					   			<a href="#" event_id ="{{ $event }}" market_id="{{ $market }}" event_type="{{ $totalMarket[0]->event->event_type }}" title="{{ $value->runner_name }}" selection_id="{{ $value->selection_id }}"  class=" btnLay text-decoration-none fadeColorlay @if($eventDetail->bet_status != 1) betDisabled @else placeBtn @endif"> --</a>
								@else
					   				<a href="javascript:void(0)" class="btnLay text-decoration-none fadeColorlay"> -- </a>
								@endif
					   		</td>
					   		<td class="text-center two_points_single " selection_id="{{ $value->selection_id}}" >
								<div class="Msuspended d-none">Suspend</div>
								@if(count($data[0]->runners[0]->ex->availableToLay)>2)
					   			<a href="#" event_id ="{{ $event }}" market_id="{{ $market }}" event_type="{{ $totalMarket[0]->event->event_type }}" title="{{ $value->runner_name }}" selection_id="{{ $value->selection_id }}"  class=" btnLay text-decoration-none fadeColorlay @if($eventDetail->bet_status != 1) betDisabled @else placeBtn @endif">--</a>
					   			@else
					   				<a href="javascript:void(0)" class="btnLay text-decoration-none fadeColorlay"> -- </a>
								@endif
					   		</td>
							{{-- {{ dd('asdf') }} --}}

					   		@else

					   			<td class="text-center two_points_single " selection_id="{{ $value->selection_id}}" >
									<div class="Msuspended">Suspend</div>	
						   		</td>
						   		<td class="text-center two_points_single " selection_id="{{ $value->selection_id}}" >
									<div class="Msuspended">Suspend</div>
						   		</td>
						   		<td class="text-center two_points_single " selection_id="{{ $value->selection_id}}" >
									<div class="Msuspended">Suspend</div>
						   		</td>
						   		<td class="text-center two_points_single " selection_id="{{ $value->selection_id}}" >
									<div class="Msuspended">Suspend</div>
						   		</td>
						   		<td class="text-center two_points_single " selection_id="{{ $value->selection_id}}" >
									<div class="Msuspended">Suspend</div>
						   		</td>
						   		<td class="text-center two_points_single " selection_id="{{ $value->selection_id}}" >
									<div class="Msuspended">Suspend</div>
						   		</td>

					   		@endif


					   	</tr>
					   	@endforeach
					   </tbody>
			   			
			   		</table>
			   	</div>
			   </div>

			 

			</div>
				@guest
					<div class="col-md-3 col-sm-3 col-xs-3 p-0">
					    	@include('include/rightSideBar')
					</div>
				@else

					@if(Auth::user()->hasRole('customer'))

						<div class="col-md-3 col-sm-3 col-xs-3 p-0">
						    	@include('include/rightSideBar')
						</div>
				    @else
						<div class="col-md-5 col-sm-5 col-xs-5 p-0">
				   			@include('include/matchBets')
						</div>
				    @endif
			    @endguest
        </div>
    </div>

@endsection


@section('scripts')

<script src="{{ asset('front/js/math.js') }}"></script>

<script>
	function c(u) {
        $('.singleMatchSelectionRow').find("#secRemain").html(MathUtil.decimal.divide(u, 10) + " sec remaining");
        if (u > 0) {
            u--;
            setTimeout(function () {
                c(u);
            }, 100);
        }
    }

</script>
<div class="sessionbookedBet">
	<div class="modal fade" id="sessionModal" tabindex="-1" role="dialog" aria-labelledby="sessionModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="sessionModalLabel">Runs Position</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <table class="table">
	        	<tr>
	        		<th>Runs</th>
	        		<th>Amount</th>
	        	</tr>
	        	<tbody class="sessionBookTable">
	        		
	        	</tbody>
	        </table>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<script src="{{ asset('front/js/math.js') }}"></script>

<script>
	function c(u) {
        $('#betloader').find("#betsecRemain").html(MathUtil.decimal.divide(u, 10) + " sec remaining");
        if (u > 0) {
            u--;
            setTimeout(function () {
                c(u);
            }, 100);
        }
    }
</script>

	<script>
		function fetchMarketOdds(){
				var eventArr = [];
					$('.singleMatchSelectionRow > tr').each(function() {
						 eventArr.push($(this).attr('selection_id'));
					});
					var event_type = $('.tab-pane.active').attr('event_type');
				if (typeof eventArr[0] !== 'undefined') {		
					$('.spark').removeClass('spark');

					$.ajax({
				        type: 'POST',
				        url: '/SingleEventsAccMarket',
				        headers: {
				                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				               },
				         data : { marketIds : '{{ $market }}', selection_id:eventArr},
				        success: function(res) {
				        		if(res.length > 0){
				                $.each(res[0].runners, function(i, item) {
				                	var k = 2;
				                	$('.row_'+item.selectionId+' > td > a').each(function(j,item2) { 
				                			if (j>2) {
				                				if(item.ex.availableToLay[j-3] != undefined){

				                				$(this).html(item.ex.availableToLay[j-3].price);

				                				$(this).addClass('spark');
				                				}
				                			}else{
				                				if(item.ex.availableToBack[k] != undefined){

				                				$(this).html(item.ex.availableToBack[k].price);
				                				$(this).addClass('spark');
				                				}


				                			}
				                		k--;
				                	});

				                });

				        		}
				        }
				      });
					}
		}
setInterval(function(){ 
		fetchMarketOdds();
}, 5000);

	$( document ).ready(function() {
		
		fetchMarketOdds();
			
	});	


@auth

@if(!Auth::user()->hasRole('customer'))

setInterval(function(){ 

	$.ajax({
	        type: 'POST',
	        url: '/matchpl',
	        headers: {
	                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	               },
	         data : { event_id : '{{ $event }}'},
	        success: function(res) {

	        	$.each(res,function(i,item){
	        		$('.matchPL'+i).html(res[i]);
	        		if(res[i]>0){
	        			$('.matchPL'+i).addClass('clsgreen');

	        		}else{

	        			$('.matchPL'+i).addClass('clsred');
	        		}
	        	})
	        }
	      });

}, 5000);

@endif

setInterval(function(){ 

    $.ajax({
            type: 'POST',
            url: '/matchedBet',
            headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   },
             data : { event_id : '{{ $event }}'},
            success: function(res) {
                var t = '';
                // $('.tblmatchbet').html(t);
                $.each(res,function(i,item){
                     t += '<tr class="';
                    if(res[i].bet_type == 'back'){
                           t += 'backtr';
                         }else{

                           t += 'laytr';
                         }   
                         var j = i+1;
                        t += '"><td> '+j+'</td><td>'+res[i].id+'</td><td>'+res[i].user.username+'</td><td>'+res[i].odds+'</td><td>'+res[i].bet_amount+'</td><td>'+res[i].bet_type+'</td><td>'+res[i].title+'</td><td>'+res[i].created_at+'</td></tr>';
                })
               	 $('.tblmatchbet').html(t);

            }
          });

}, 5000);





$('.fetchSelectionBet').click(function(){

	var event_id = $(this).attr('event_id');
	var selection_id = $(this).attr('selection_id');

	 $.ajax({
            type: 'POST',
            url: '/matchedSessionBet',
            headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   },
             data : { event_id : event_id,selection_id:selection_id},
            success: function(res) {

            	if(res[0].success == 'success'){
            		$('.sessionBookTable').html(res[0].data);
            		$('#sessionModal').modal('show');
            	}
            }
          });

});


@endauth



@auth




setInterval(function(){ 

    $.ajax({
            type: 'POST',
            url: '/matchedTotalPoint',
            headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   },
             data : { event_id : '{{ $event }}'},
            success: function(res) {
                var t = '';

                var f=0; var s = 0; var to=0;
                // $('.tblmatchbet').html(t);
                $.each(res,function(i,item){
                     t += '<tr class="';
                    if(res[i].bet_type == 'back'){
                           t += 'backtr';
                         }else{

                           t += 'laytr';
                         }   
                         var j = i+1;
                        t += '"><td> '+res[i].user.username+'</td>';
                        if(res[i].amount > 0){

                        	t+= '<td class="clsgreen">'+res[i].amount+'</td>';
                        }else{
                        	t+= '<td class="clsred">'+res[i].amount+'</td>';

                        }
                        if(res[i].profit_amount > 0){

                        	t+= '<td class="clsgreen">'+res[i].profit_amount+'</td></tr>';
                        }else{
                        	t+= '<td class="clsred">'+res[i].profit_amount+'</td></tr>';

                        }

                        f += parseFloat(res[i].amount);
		                s += parseFloat(res[i].profit_amount);
		                if(res[i].draw){

		                    to += parseFloat(res[i].draw);
		                }



                })


               	 $('.tblPoints').html(t);
                $('.team1total').html(f);
                $('.team2total').html(s);
                $('.team3total').html(to);
                if(f>0){
                	
                	$('.team1total').removeClass('clsred');
                	$('.team1total').addClass('clsgreen');

                }else{
                	$('.team1total').removeClass('clsgreen');
                	$('.team1total').addClass('clsred');
                }


                if(s>0){
                	
                	$('.team2total').removeClass('clsred');
                	$('.team2total').addClass('clsgreen');

                }else{
                	$('.team2total').removeClass('clsgreen');
                	$('.team2total').addClass('clsred');
                }


                if(to>0){
                	
                	$('.team3total').removeClass('clsred');
                	$('.team3total').addClass('clsgreen');

                }else{
                	$('.team3total').removeClass('clsgreen');
                	$('.team3total').addClass('clsred');
                }




            }
          });

}, 5000);


	$('.placeBtn').click(function() {
		var event_id = $(this).attr('event_id');
		var event_type = $(this).attr('event_type');
		var market_id = $(this).attr('market_id');
		var name = '{{ $totalMarket[0]->event->name }}';
		var selection_id = $(this).attr('selection_id');
		var title = $(this).attr('title');
		var odds = $(this).text();

		$('.SingleMatchBetSlip').html('');

		if($(this).hasClass('btnBack')){

			if ($(".BetforSlipMatch").hasClass('slip'+selection_id+'')) {

				 $('.BetforSlipMatch.slip'+selection_id+'').parent().remove();

				 if ($(".SingleMatchBetForSlip").find(".BetforSlipMatch").length < 1){ 
				 		if ($(".SingleMatchBetLaySlip").find(".BetlaySlipMatch").length < 1){ 
						    $('._3QQE9').show();
						    $('.BetSlipRow').addClass('d-none');
						}
						$('.Bet-for-slip').addClass('d-none');
					}

			}else{


			var text = '<div class="innerSlipOdds"><div class="col-md-12 BetforSlipMatch slip'+selection_id+'" id="slip'+odds+'"><div class="one_team_name"><p class="m-0"> <i class="fa fa-dot-circle-o"></i> '+name+'</p></div></div><div class="singleOddsRowBet"><div class="col-md-12 stakeInputRow"><div class="row"><div class="col-md-5 p-adjust"><div class="match-odds d-flex"><div class="closeSlipdiv"> <a href="javascript:void(0)" class="closeSlip" data-s="'+odds+'"><i class="fa fa-close"></i></a></div><div class="match-odds-countory"><p class="m-0"> '+title+'</p> <span>Match Odds</span></div></div></div><div class="col-md-2 p-adjust"><div class="input-type-area"> <input type="hidden" name="type" value="back"> <input name="stakeOdds" class="form-control" type="text"  value="'+odds+'" /></div></div><div class="col-md-2 p-adjust"><div class="input-type-area inputStake"> <input event_id="'+event_id+'" market_id="'+market_id+'" event_name="'+name+'" selection_id="'+selection_id+'" title="'+title+'" odds="'+odds+'" bet_type="back" class="form-control betStakeAmount" event_type="'+event_type+'" name="stakeAmount"  type="number" pattern= "[0-9]" maxlength="7" /></div></div><div class="col-md-3 text-right"><div class="betting-price singleLiablity"> 0 </div></div></div></div><div class="col-md-12 stakePriceRow p-adjust "><div class="match_rates"><ul class="p-0 m-0 bet_stake" ><li><a href="javascript:void(0)" class="btn">{{ user_default_stake() ? user_default_stake()->stake1 : 10  }}</a></li><li><a href="javascript:void(0)" class="btn">{{ user_default_stake() ? user_default_stake()->stake2 : 20  }}</a></li><li><a href="javascript:void(0)" class="btn">{{ user_default_stake() ? user_default_stake()->stake3 : 50  }}</a></li><li><a href="javascript:void(0)" class="btn">{{ user_default_stake() ? user_default_stake()->stake4 : 200  }}</a></li><li><a href="javascript:void(0)" class="btn">{{ user_default_stake() ? user_default_stake()->stake5 : 500  }}</a></li><li><a href="javascript:void(0)"   class="btn">{{ user_default_stake() ? user_default_stake()->stake6 : 1000  }}</a></li></ul></div></div></div></div>';
				$('._3QQE9').hide();
				$('.BetSlipRow').removeClass('d-none');
				$('.Bet-for-slip').removeClass('d-none');
				$('.Bet-for-slip > .SingleMatchBetForSlip').append(text);
			}

		}else{

			if ($(".BetlaySlipMatch").hasClass('slip'+selection_id+'')) {
				$('.BetlaySlipMatch.slip'+selection_id+'').parent().remove();
					
				 if ($(".SingleMatchBetLaySlip").find(".BetlaySlipMatch").length < 1){ 

				 		if ($(".SingleMatchBetForSlip").find(".innerSlipOdds").length < 1){ 
						    $('._3QQE9').show();
							$('.BetSlipRow').addClass('d-none');
						}

						$('.Bet-lay-slip').addClass('d-none');
					}
			}else{
				var text2 = '<div class="innerSlipOdds" ><div class="col-md-12 BetlaySlipMatch slip'+selection_id+' " id="slip'+odds+'"><div class="one_team_name"><p class="m-0"> <i class="fa fa-dot-circle-o"></i> '+name+'</p></div></div><div class="singleOddsRowBet"><div class="col-md-12 stakeInputRow"><div class="row"><div class="col-md-5 p-adjust"><div class="match-odds d-flex"><div class="closeSlipdiv" > <a href="javascript:void(0)" class="closeSlip" data-s="'+odds+'"><i class="fa fa-close"></i></a></div><div class="match-odds-countory"><p class="m-0"> '+title+'</p> <span>Match Odds</span></div></div></div><div class="col-md-2 p-adjust"><div class="input-type-area"> <input type="hidden" name="type" value="lay"> <input name="stakeOdds" class="form-control" type="text"  value="'+odds+'" /></div></div><div class="col-md-2 p-adjust"><div class="input-type-area"> <input name="stakeAmount" event_id="'+event_id+'" market_id="'+market_id+'" event_name="'+name+'" selection_id="'+selection_id+'" title="'+title+'" odds="'+odds+'" bet_type="lay" class="form-control betStakeAmount" event_type="'+event_type+'" type="number" pattern= "[0-9]" maxlength="7" /></div></div><div class="col-md-3 text-right"><div class="betting-price singleLiablity"> 0 </div></div></div></div><div class="col-md-12 stakePriceRow p-adjust "><div class="match_rates" ><ul class="p-0 m-0 bet_stake" ><li><a href="javascript:void(0)" class="btn">{{ user_default_stake() ? user_default_stake()->stake1 : 10  }}</a></li><li><a href="javascript:void(0)" class="btn">{{ user_default_stake() ? user_default_stake()->stake2 : 20  }}</a></li><li><a href="javascript:void(0)" class="btn">{{ user_default_stake() ? user_default_stake()->stake3 : 50  }}</a></li><li><a href="javascript:void(0)" class="btn">{{ user_default_stake() ? user_default_stake()->stake4 : 200  }}</a></li><li><a href="javascript:void(0)" class="btn">{{ user_default_stake() ? user_default_stake()->stake5 : 500  }}</a></li><li><a href="javascript:void(0)"   class="btn">{{ user_default_stake() ? user_default_stake()->stake6 : 1000  }}</a></li></ul></div></div></div></div>';

					$('._3QQE9').hide();
					$('.BetSlipRow').removeClass('d-none');
					$('.Bet-lay-slip').removeClass('d-none');
					$('.Bet-lay-slip > .SingleMatchBetLaySlip').append(text2);

				}
		}
	});


	$('.SingleMatchBetSlip').on('click','.closeSlip',function() {
		var sec_id = $(this).attr('data-s');
		// $('.slip'+sec_id+'').parent().remove();
	    $('[id = "slip'+sec_id+'"]').parent().remove();

	     	 if ($(".SingleMatchBetLaySlip").find(".BetlaySlipMatch").length < 1 && $(".SingleMatchBetForSlip").find(".BetforSlipMatch").length < 1){ 
	     	 	
				    $('._3QQE9').show();
				    $('.BetSlipRow').addClass('d-none');
			}else{
				if ($(".SingleMatchBetForSlip").find(".innerSlipOdds").length < 1){ 
					$('.Bet-for-slip').addClass('d-none');
				}


				if ($(".SingleMatchBetLaySlip").find(".BetlaySlipMatch").length < 1){ 
				    $('.Bet-lay-slip').addClass('d-none');
				}
			}

			
});



$('.SingleMatchBetSlip').on('click','.bet_stake > li > a',function(){
	var value = parseInt($(this).html());

	// $(this).closest('.betStakeAmount').value('112');
	var d = $(this).closest("div.singleOddsRowBet").find("input[name='stakeAmount']").val(value);
	var selection_id = $(this).closest("div.singleOddsRowBet").find("input[name='stakeAmount']").attr('selection_id');
	var bet_type = $(this).closest("div.singleOddsRowBet").find("input[name='stakeAmount']").attr('bet_type');
	var s = $(this).closest("div.singleOddsRowBet").find("input[name='stakeOdds']").val();


		var v = (value*s)-value;		

		$('.singleMatchSelectionRow > tr').each(function(j,k){

			if($(this).hasClass('row_'+selection_id+'')){
				if(bet_type == 'back'){
					$('.row_'+selection_id+' th span').html(v.toFixed(2));
					$('.row_'+selection_id+' th span').removeClass('clsred');
					$('.row_'+selection_id+' th span').addClass('clsgreen');
				}else{

					$('.row_'+selection_id+' th span').html(value);
					$('.row_'+selection_id+' th span').removeClass('clsgreen');
					$('.row_'+selection_id+' th span').addClass('clsred');
				}
			}else{

				var selection = $(this).attr('selection_id');

				if(bet_type == 'back'){

					$('.row_'+selection+' th span').html(value);
					$('.row_'+selection+' th span').removeClass('clsgreen');
					$('.row_'+selection+' th span').addClass('clsred');
				}else{

					$('.row_'+selection+' th span').html(v.toFixed(2));
					$('.row_'+selection+' th span').removeClass('clsred');
					$('.row_'+selection+' th span').addClass('clsgreen');
				}
			}

		})

		var l = $(this).closest(".stakePriceRow").prev('div.stakeInputRow').find('div.singleLiablity').html(v.toFixed(2));
		var totalLiability = 0;
		$('.singleLiablity').each(function(i, obj) {

			totalLiability = totalLiability + parseFloat($(this).text());
			$('.totalL').html(totalLiability);
			$('.BetSubmit').removeClass('disabled');
			$('.BetSubmit a').removeClass('disabled');
		});
});












$('.SingleMatchBetSlip').on('change','.betStakeAmount',function(){
	var value = parseInt($(this).val());

	// $(this).closest('.betStakeAmount').value('112');
	var d = $(this).closest("div.singleOddsRowBet").find("input[name='stakeAmount']").val(value);
	var s = $(this).closest("div.singleOddsRowBet").find("input[name='stakeOdds']").val();



		var v = (value*s)-value;	
		
		var l = $(this).closest(".stakeInputRow").find('div.singleLiablity').html(v.toFixed(2));
		var totalLiability = 0;
		$('.singleLiablity').each(function(i, obj) {

			totalLiability = totalLiability + parseFloat($(this).text());
			$('.totalL').html(totalLiability);
			$('.BetSubmit').removeClass('disabled');
			$('.BetSubmit > a').removeClass('disabled');
		});
});




	$('.BetCancelAll > a').click(function(){
		$('.SingleMatchBetSlip').html('');

		$('.bettingSlip').addClass('d-none');
		$('.BetSlipRow ').addClass('d-none');
		$('._3QQE9').show();
	})



	$('.sessionBeta').click(function(){

		if($(this).hasClass('disabled')){
			return false;
		}else{

		$('.fancy_quick_bet').remove();
		$('.loseFancy').hide();
		var type = $(this).attr('type');
		var SelectionId = $(this).attr('SelectionId');
		var prize = $(this).find('span.fancyPrize').html();
		var size = $(this).find('span.fancySize').html();

		if(type == 'back'){
			var tabclass = 'backRow';

		}else{
			var tabclass = 'layRow';
		}

		var text = '<tr class="fancy_quick_bet '+tabclass+'"><td colspan="4"><div class="row inprow"><div class="col-md-6 float-right p-5 text-left"><div class="canclFancy"><a href="javascript:void(0)" class="btn btn-primary btn-sm">Cancel</a></div><div class="oddhead"><ul class="quick-bet-confirm"><li id="runs">'+prize+'</li><li id="odds" class="quick-bet-confirm-title">'+size+'</li></ul></div><div class="col-stake"><input type="'+type+'" prize="'+prize+'" size="'+size+'" SelectionId="'+SelectionId+'" id="inputStake" name="stakeAmount" type="text" value=""></div><div class="col-send"><a id="placeBet" class="btn btn-sm btn-site disabled" style="cursor:pointer;">Place Bets</a></div></div></div><div class="row m-0"><div id="stakePopupList" class="col-stake_list"><ul><li><a  class="btn btn-default" id="selectStake_1" stake="{{ user_default_stake() ? user_default_stake()->stake1 : 10  }}" style="cursor:pointer;">{{ user_default_stake() ? user_default_stake()->stake1 : 10  }}</a></li><li><a  class="btn btn-default" id="selectStake_2" stake="{{ user_default_stake() ? user_default_stake()->stake2 : 20  }}" style="cursor:pointer;">{{ user_default_stake() ? user_default_stake()->stake2 : 20  }}</a></li><li><a  class="btn btn-default" id="selectStake_3" stake="{{ user_default_stake() ? user_default_stake()->stake3 : 50  }}" style="cursor:pointer;">{{ user_default_stake() ? user_default_stake()->stake3 : 50  }}</a></li><li><a  class="btn btn-default" id="selectStake_4" stake="{{ user_default_stake() ? user_default_stake()->stake4 : 100  }}" style="cursor:pointer;">{{ user_default_stake() ? user_default_stake()->stake4 : 100  }}</a></li><li><a  class="btn btn-default" id="selectStake_5" stake="{{ user_default_stake() ? user_default_stake()->stake5 : 200  }}" style="cursor:pointer;">{{ user_default_stake() ? user_default_stake()->stake5 : 200  }}</a></li><li><a  class="btn btn-default" id="selectStake_6" stake="{{ user_default_stake() ? user_default_stake()->stake6 : 1000  }}" style="cursor:pointer;">{{ user_default_stake() ? user_default_stake()->stake6 : 1000  }}</a></li></ul></div></div></td></tr>';


		$(text).insertAfter($(this).closest('.fancyRow'));


		}
		
	});



	$('.singleMatchSelectionRow').on('click','.canclFancy > a',function() {

		$(this).closest('.fancy_quick_bet').remove();
		$('.loseFancy').hide();
	});

@endauth






	$('.singleMatchSelectionRow').on('keyup','#inputStake',function() {

		// var val = $(this).val();

		var type = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('type');
		var size = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('size');

		if(type == 'back'){

			var val = $(this).val();
		}else{

			var val = (parseInt($(this).val()) * parseInt(size))/100 ;
		}
		var selectionId = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('SelectionId');
		if(val > 0){
			$(this).closest('.fancy_quick_bet').find("#placeBet").removeClass('disabled');
			// $(this).closest('.fancyRow').find("th > p").append('<span class="loseFancy">'+stake+'</span>');
			$('.fancy'+selectionId+' > th > p > span.loseFancy').html('(-'+val+')');
			$('.fancy'+selectionId+' > th > p > span.loseFancy').show();
		}else{
			$(this).closest('.fancy_quick_bet').find("#placeBet").addClass('disabled');
			$('.fancy'+selectionId+' > th > p > span.loseFancy').hide();
		}

	});


	$('.singleMatchSelectionRow').on('click','#stakePopupList > ul > li > a',function() {

		// console.log();

		$('.loseFancy').hide();
		var type = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('type');
		var size = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('size');

			var stake = $(this).html();
		if(type == 'back'){

			var stakelose = $(this).html();
		}else{

			var stakelose = (parseInt($(this).html()) * parseInt(size))/100 ;
		}

		$(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").val(stake);
		var selectionId = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('SelectionId');
		// $(this).closest('.fancyRow').find("th > p").append('<span class="loseFancy">'+stake+'</span>');
		$('.fancy'+selectionId+' > th > p > span.loseFancy').html('(-'+stakelose+')');
		$('.fancy'+selectionId+' > th > p > span.loseFancy').show();

		$(this).closest('.fancy_quick_bet').find("#placeBet").removeClass('disabled');


	});

	$('.singleMatchSelectionRow').on('click','#placeBet',function() {


		var selectionId = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('SelectionId');
		var type = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('type');
		var prize = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('prize');
		var size = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('size');
		var stake = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").val();
		var event_id = {{ $event }};

		

		$('.singleMatchSelectionRow').find('.fancy_quick_bet').remove();


		var text = '<tr id="fancyBetBar" class="fancy-quick-tr"><td colspan="7"><dl class="quick_bet-wrap quick_bet-progress"><dd id="progressBar" class="progress-bar" style="width: 100%;"></dd>											<dd class="progress-bar-txt">Placing your bets, Please wait <span id="secRemain">0 sec remaining…</span></dd></dl></td></tr>';

		// $('.fancy'+selectionId+'').insertAfter(text);
		$(text).insertAfter('.fancy'+selectionId+'');

		c(50);

		$.ajax({
                type: 'POST',
                url: '/customer/FancyBetPlaced',
                data: {selectionId:selectionId,type:type,prize:prize,size:size,stake:stake,event_id:event_id},
                headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                success: function(res) {
                	setTimeout(function() {
	                	if(res[0].success){
	                			
	                		swal('success',res[0].msg,'success');	
	                		location.reload();
	                	}else{
	                		swal('error',res[0].msg,'error');	
	                		$('.singleMatchSelectionRow').find('#fancyBetBar').remove();
	                	}
	                },4000);
                }
              });



	});






	$('#sideBetFetch').change(function(){
		var selectionId = $(this).val(); 
		var event_id = $(this).attr('event_id'); 


		$.ajax({
		        type: 'POST',
		        url: '/mobile/fetchSelectionBet',
		        async: false,
		        headers: {
		                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		               },
		        data:{selection_id:selectionId},       
		        success: function(res) {

		        	var innerbet = res[0].data
		        	var t = '';

		        	t = '<p class="bMatch">Matched</p>';
		        	$.each(innerbet, function(j, item2) {
	              		t+= '<div class="row side-head-row m-0"><div class="col-md-3"><p>'+item2.bet_type+'</p></div><div class="col-md-3"><p>Odds</p></div><div class="col-md-3"><p>Stake</p></div><div class="col-md-3"><p>Time</p></div></div><div class="row data-row"><div class="col-md-3"><p>';
	              	if(item2.bet_type == 'lay'){

	              		t+= '<span class="btnlay">'+item2.bet_type+'</span></p></div><div class="col-md-3"><p>'+item2.odds+'</p></div><div class="col-md-3"><p>'+item2.bet_amount+'</p></div><div class="col-md-3"><p>'+item2.created_at+'</p></div></div>';
	              	}else{

	              		t+= '<span class="btnback">'+item2.bet_type+'</span></p></div><div class="col-md-3"><p>'+item2.odds+'</p></div><div class="col-md-3"><p>'+item2.bet_amount+'</p></div><div class="col-md-3"><p>'+item2.created_at+'</p></div></div>';
	              	}

	              	$('#betDisplayed').html(t);
	              });
		        }

		    })


	})


	</script>


@endsection