@extends('layouts.front')
@section('content')
<br>
<div class="container-fluid">
<div class="row">
	<div class="col-md-12">
 		<div class="card">
                    {{-- <div class="card-header">Downline - Users <button type="button" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#add_user"> <i class="fa fa-plus"></i> Add Member</button></div> --}}
                    <div class="card-body">
                        {{-- {!! Form::open(['method' => 'GET', 'url' => '/admin/pages', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!} --}}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-border table-striped">
                                <thead>
                                    <tr>
                                        <th>Date & Time</th>
                                		<th>Receiver Name</th>
                                		<th>Credit</th>
                                		<th>Debit</th>
                                		<th>Balance</th>
                                		<th>Credit Ref.</th>
                                		<th>Status</th>
                                		<th>Remark</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $total_amt=0 @endphp
                                    @if(count($trans)>0)
                                        @foreach($trans as $item)
                                        	<tr>
                                        		<td>{{$item->created_at}}</td>
                                        		<td>{{$item->user?$item->user->name:''}}</td>
                                        		<td>
                                                @if(!empty($item->transaction_type=='withdraw'))
                                                    {{$item->transaction_amount?$item->transaction_amount:0}}
                                                @else
                                                    0
                                                @endif
                                                </td>
                                        		<td>
                                                    @if(!empty($item->transaction_type=='deposit'))
                                                        {{$item->transaction_amount?$item->transaction_amount:0}}
                                                    @else
                                                        0
                                                    @endif
                                                </td>
                                        		<td>
                                                    @if(!empty($item->transaction_type=='deposit'))
                                                        @php $total_amt+=$item->transaction_amount @endphp
                                                    @elseif(!empty($item->transaction_type=='withdraw'))
                                                        @php $total_amt-=$item->transaction_amount @endphp
                                                    @endif
                                                    {{abs($total_amt)}}
                                                </td>
                                                <td>0</td>
                                        		<td>
                                                    @if(!empty($item->transaction_status=='Complete'))
                                                    {{$item->transaction_status?$item->transaction_status:'Pending'}}
                                                    @endif
                                                </td>
                                        		<td>{{$item->remarks}}</td>
                                        	</tr>
                                        @endforeach
                                    @endif
                               {{--  @foreach($pages as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->title }}</td><td>{{ $item->content }}</td>
                                        <td>
                                            <a href="{{ url('/admin/pages/' . $item->id) }}" title="View Page"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/pages/' . $item->id . '/edit') }}" title="Edit Page"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/pages', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Page',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach --}}
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
	</div>

</div>
<br>
</div>


@endsection