@extends('layouts.front')
@section('content')
<br>
<div class="container-fluid">
<div class="row">
	
	<div class="col-md-12">
 <div class="card">
                    <div class="card-header">Live Users </div>
                    <div class="card-body">
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/pages', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>User Name</th>
                                		<th>Ip</th>
                                		<th>Device</th>
                                		<th>Issued On</th>
                                		<th>Expiring On</th>
                                		<th>Location</th>
                                		<th>Provider</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<tr>
                                		<td>1</td>
                                		<td>Vijay</td>
                                		<td>Ajay Jadeja</td>
                                		<td>PC</td>
                                		<td>02-08-2019</td>
                                		<td>02-08-2022</td>
                                		<td>West Hampton, Gerogepool</td>
                                		<td>Telstra InfoSystems</td>
                                		
                                	</tr>
                               {{--  @foreach($pages as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->title }}</td><td>{{ $item->content }}</td>
                                        <td>
                                            <a href="{{ url('/admin/pages/' . $item->id) }}" title="View Page"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/pages/' . $item->id . '/edit') }}" title="Edit Page"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/pages', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Page',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach --}}
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div id="myactive" class="modal fade" role="dialog">
					  <div class="modal-dialog">

					    <div class="modal-content">
					      <div class="modal-header">
					        <h4 class="modal-title">Change Member Status</h4>
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					      </div>
					      <div class="modal-body" style="padding: 15px 15px;">
					      	<form action="#">
					      		
					        <div class="row">
					        	<div class="col-md-4">
					        		<label for="">Current Status <i class="fa fa-arrow-down"></i></label>
			        				<label for="" style="color:green"><b>Active</b></label> <br>
					        		<label for="" style="color:red"><b>Suspend</b></label> <br>
					        		<label for="" style="color:grey"><b>Lock</b></label> <br>
					        			
					        	</div>
					        	<div class="col-md-8" style="padding: 5px 5px">
					        		<div class="row">
					        			<div class="col-md-12">
					        				
				        			<label><input type="radio" name="status" checked="checked">Active</label><br>
					        			</div>
					        			<div class="col-md-12">
					        				
					        		<label><input type="radio" name="status">Suspend</label><br>
					        			</div>
					        			<div class="col-md-12">
					        				
					        		<label><input type="radio" name="status">Lock</label>		
					        			</div>
					        		</div>
					        		
					        		<div class="row">
					        			<div class="col-md-12">
					        				
					        		<label for="">Enter Password:</label>
					        		<input type="password" name="password" class="form-control">
					        			</div>
					        		</div><br>
					        		<input type="submit" name="submit" class="btn btn-default btn-block btn-info">
					        	</div>
					        </div>

					      	</form>
					      </div>
					     {{--  <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					      </div> --}}
					    </div>

					  </div>
					</div>
                    <div id="mypt" class="modal fade" role="dialog">
					  <div class="modal-dialog">

					    <div class="modal-content">
					      <div class="modal-header">
					        <h4 class="modal-title">Update PT</h4>
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					      </div>
					      <div class="modal-body">
					        <form action="#">
					      		
					        <div class="row">
					        	<div class="col-md-4">
					        		<label for="pt">Enter PT:</label><br>
					        		<input type="text" class="form-control"  name="pt">
					        	</div>
					        	<div class="col-md-8">
					        		<label for="password">Enter Password:</label><br>
					        		<input type="password" class="form-control" name="password">
					        	</div>
					        </div>
					        <br>
					        <div class="row">
					        	<div class="col-md-12">
					        		<input type="submit" name="submit" class="btn btn-default btn-block btn-info">
					        	</div>					        	
					        </div>

					      	</form>
					      </div>
					      {{-- <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					      </div> --}}
					    </div>

					  </div>
					</div>
                    <div id="add_user" class="modal fade" role="dialog">
					  <div class="modal-dialog modal-lg">

					    <div class="modal-content">
					      <div class="modal-header">
					        <h4 class="modal-title">ADD MEMBER</h4>
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					      </div>
					      <div class="modal-body">
					        <form action="#">
					      		
					        <div class="row">
					        	<div class="col-md-6">
					        		<label for="full_name">Enter Full Name:</label><br>
					        		<input type="text" class="form-control"  name="full_name">
					        		<label for="password">Enter Password:</label><br>
					        		<input type="password" class="form-control" name="password">
					        		<label for="new_credit_reference">Please Enter New Credit Reference :</label><br>
					        		<input type="text" class="form-control" name="new_credit_reference">
					        		<label for="new_credit_reference">Please Enter Mobile No.:</label><br>
					        		<input type="number" class="form-control" name="mobile_no">
					        		<label for="new_credit_reference">Please Enter PT:</label><br>
					        		<input type="number" class="form-control" name="pt">
					        	</div>
					        	<div class="col-md-6">
					        		<label for="username">Enter Username:</label><br>
					        		<input type="text" class="form-control"  name="username">

					        		<label for="confirm_password">Enter Confirm Password:</label><br>
					        		<input type="password" class="form-control" name="confirm_password">

					        		<label for="casino_credit_reference">Please Enter Casino Credit:</label><br>
					        		<input type="text" class="form-control" name="casino_credit_reference">

					        		<label for="casino_credit_reference">Please Enter Email:</label><br>
					        		<input type="email" class="form-control" name="email">
					        	</div>
					        </div>
					        <br>
					        <div class="row">
					        	<div class="col-md-12">
					        		<input type="submit" name="submit" class="btn btn-default btn-block btn-info">
					        	</div>					        	
					        </div>

					      	</form>
					      </div>
					      {{-- <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					      </div> --}}
					    </div>

					  </div>
					</div>
                    <div id="mycredit" class="modal fade" role="dialog">
					  <div class="modal-dialog">

					    <div class="modal-content">
					      <div class="modal-header">
					        <h4 class="modal-title">Banking</h4>
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					      </div>
					      <div class="modal-body">
					      	<div class="row">
					      	<label>					      		
					        <input type="radio" name="chips_casino">Chips
					      	</label>
					      	<label>
					        <input type="radio" name="chips_casino">Casino				      		
					      	</label>	
					      	</div>
					      	
					      	<div class="switch">
						      <input type="radio" class="switch-input" name="view" value="week" id="week" checked>
						      <label for="week" class="switch-label switch-label-off">D</label>
						      <input type="radio" class="switch-input" name="view" value="month" id="month">
						      <label for="month" class="switch-label switch-label-on">W</label>
						      <span class="switch-selection"></span>
						    </div>
							<div class="row">
								<div class="col-md-12 text-center">
									
					      	<input type="number" name="deposit_withraw_cost">	
								</div>
					      	</div>
					      	<div class="row">
					      		<div class="col-md-12">
					      		<label for="remarks">Please Enter Remarks</label>
					      		<input type="text" name="remarks" class="form-control">
	
					      		</div>
					      		<div class="col-md-12">
					      			
					      		<label for="remarks">Please Enter Password</label>
					      		<input type="password" name="password" class="form-control">
					      		</div>
					      		
					      	</div>
					      	
					      </div>s
					    </div>

					  </div>
					</div>
                </div>
	</div>

</div>
</div>


@endsection