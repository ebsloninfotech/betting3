@extends('layouts.front')
@section('content')
<br>
<div class="container-fluid">
   <div class="row">
   {{--    <div class="col-md-3">
            @include('sidebars.myaccount_sidebar')
        </div> --}}

      @if(Auth::user()->hasRole('master'))


        <div class="mastersearch">
           <p class="text-right">
              <form class="row m-0" action="{{ url('master/downline') }}" >
               <div class="form-group col-md-offset-7 col-sm-offset-7 col-md-5 col-sm-5">    
                  <div class="row">

                     <div class="col-md-3 col-sm-3 text-right">
                        
                        <label for="">Search in All</label>
                     
                     </div>
                     <div class="col-md-6 col-sm-6 p-0">
                        <input type="text" name="search" class="form-control" placeholder="Enter Username" value="@if(Request::get('search')) {{ Request::get('search') }} @endif">                        
                     </div>
                     <div class="col-sm-3 text-left">
                        
                        <input type="submit" class="btn btn-sm bt-primary">
                     </div>
                  </div>              
               </div>
              </form>
           </p>
        </div>


      @endif


   <div class="col-md-12">
         <div class="card">
            <div class="card-header"><h3 class="downline-h">Downline - 
               @if(Auth::user()->hasRole('master'))ADMINS [BELOW ARE ADMINS]</h3>
            	@elseif(Auth::user()->hasRole('admin')) <h3 class="downline-h">CLIENTS [BELOW ARE YOUR CLIENTS] </h3> 
         		@elseif(Auth::user()->hasRole('client')) <h3 class="downline-h">CUSTOMERS [BELOW ARE YOUR CUSTOMERS]</h3> 
         		@endif  
	        	@if(Auth::user()->hasRole('master'))
    			  <button type="button" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#add_user"> <i class="fa fa-plus"></i> Add Admin</button> 
    			@elseif(Auth::user()->hasRole('admin'))
					<button type="button" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#add_user"> <i class="fa fa-plus"></i> Add Client</button> 
    			@elseif(Auth::user()->hasRole('client'))
					<button type="button" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#add_user"> <i class="fa fa-plus"></i> Add Customer</button> 
	   		@endif
        	</div>
         <div class="card-body">
            <div class="table-responsive">
               <table class="table-balance table-border table-striped" id="dataTables">
                  <thead>
                     <tr>
                        <th>ID</th>
                        <th>Account</th>
                        <th>Type</th>
                        {{-- <th>M.Comm(%)</th>
                        <th>S.Comm(%)</th> --}}
                        {{-- @if(Auth::user()->hasRole('client') || Auth::user()->hasRole('customer')) --}}
                        <th>Balance</th>
                        {{-- @endif --}}
                        {{-- <th>P/L</th> --}}
                        {{-- <th>Liability</th> --}}
                        <th>Status</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>

                     @if(Request::get('search'))

                     @foreach($user as $u)
                     {{-- @php 
                        $u = getuser($up->user_id);
                     @endphp --}}
                     <tr>
                        <td>{{$u->id ? $u->id : '--'}}</td>
                        <td>{{$u->username ? $u->username : '--'}}</td>
                        <td>{{$u->roles[0]->name }}</td>
                        {{-- <td>{{ $u->commission }}</td> --}}
                        {{-- <td>0</td> --}}
                        {{-- @if(Auth::user()->hasRole('client') || Auth::user()->hasRole('customer')) --}}
                        <td>{{ $u->walletA ? $u->walletA->wallet_amount : '0'}}</td>
                        {{-- @endif --}}
                        {{-- <td>0</td> --}}
                        {{-- <td>0</td> --}}
                        <td>
                           @if(!empty($u->status==1))
                           <button type="button" class="btn  btn-sm chaneg_status" user_id="{{$u->id}}" status="Active" data-toggle="modal" title="Client Status" data-target="#myactive"> <i class="fa fa-edit"></i> Active</button>
                           @elseif(!empty($u->status==2))
                           <button type="button" class="btn  btn-sm chaneg_status" user_id="{{$u->id}}" status="Suspend" title="Client Status" data-toggle="modal"  data-target="#myactive"> <i class="fa fa-edit"></i> Suspend</button>
                           @elseif(!empty($u->status==3))
                           <button type="button" class="btn  btn-sm chaneg_status" user_id="{{$u->id}}" status="Locked" data-toggle="modal" title="Client Status" data-target="#myactive"><i class="fa fa-edit"></i> Locked</button>
                           @endif
                          
                             {{--  @if(Auth::user()->hasRole('client'))
                                 <button type="button" class="bit_config btn btn-success btn-sm" title="Bet Config" receiver_id="{{$u->id}}"> <i class="fa fa-credit-card"></i> Bet Config</button>
                              @endif --}}
                           </td>
                        <td>
                           {{-- @if($u->email=='master@admin.com') --  --}}
                           {{-- @else  --}}
                           {{-- <button type="button" class="edit_admin_modal btn btn-sm btn-warning" commission="{{ $u->commission }}" user_fullname="{{$u->name}}" user_email="{{$u->email}}" user_id="{{$u->id}}" user_username="{{$u->username}}" user_mobile_no="{{$u->mobile_no}}" ><i class="fa fa-edit"></i></button>  --}}
                           {{-- {!! Form::open([
                           'method' => 'DELETE',
                           'url' => ['/admin/delete_admin', $u->id],
                           'style' => 'display:inline'
                           ]) !!}
                           {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                           'type' => 'submit',
                           'class' => 'btn btn-danger btn-sm',
                           'title' => 'Delete Page',
                           'onclick'=>'return confirm("Confirm delete?")'
                           )) !!}
                           {!! Form::close() !!} --}}
                           {{-- @endif --}}
                           {{-- @if(Auth::user()->hasRole('master'))
                           @else --}}
                           {{-- <button type="button" class="credit_button btn btn-success btn-sm" data-toggle="modal" data-target="#mycredit" title="transfor Coins" receiver_id="{{$u->id}}"> <i class="fa fa-credit-card"></i> Credit</button> --}}
                           {{-- @endif --}}
                           @if($u->roles[0]->name == 'admin')
                              <a href="{{ url('/'.Auth::user()->roles[0]->name.'/memberSummary/'.$u->id.'') }}" class="btn btn-info btn-sm"> <i class="fa fa-user"></i></a>
                           @else

                              @if($u->roles[0]->name == 'client')

                                 <a href="{{ url('/'.Auth::user()->roles[0]->name.'/clientMember/'.$u->parent->parent_id.'/'.$u->id.'') }}" class="btn btn-info btn-sm"> <i class="fa fa-user"></i></a>
                              @else
                               
                                 <a href="{{ url('/'.Auth::user()->roles[0]->name.'/userMember/'.userAdmin($u->id)->user_id.'/'.$u->parent->parent_id.'/'.$u->id.'') }}" class="btn btn-info btn-sm"> <i class="fa fa-user"></i></a>

                              @endif
                           @endif

                        </td>
                     </tr>
                     @endforeach


                     @else

                     @foreach($user as $up)
                     @php 
                        $u = getuser($up->user_id);
                     @endphp
                     <tr>
                        <td>{{$u->id ? $u->id : '--'}}</td>
                        <td>{{$u->username ? $u->username : '--'}}</td>
                        <td>{{$u->roles[0]->name }}</td>
                        {{-- <td>{{ $u->commission }}</td> --}}
                        {{-- <td>0</td> --}}
                        {{-- @if(Auth::user()->hasRole('client') || Auth::user()->hasRole('customer')) --}}
                        <td>{{ $u->walletA ? $u->walletA->wallet_amount : '0'}}</td>
                        {{-- @endif --}}
                        {{-- <td>0</td> --}}
                        {{-- <td>0</td> --}}
                        <td>
                           @if(!empty($u->status==1))
                           <button type="button" class="btn  btn-sm chaneg_status" user_id="{{$u->id}}" status="Active" data-toggle="modal" title="Client Status" data-target="#myactive"> <i class="fa fa-edit"></i> Active</button>
                           @elseif(!empty($u->status==2))
                           <button type="button" class="btn  btn-sm chaneg_status" user_id="{{$u->id}}" status="Suspend" title="Client Status" data-toggle="modal"  data-target="#myactive"> <i class="fa fa-edit"></i> Suspend</button>
                           @elseif(!empty($u->status==3))
                           <button type="button" class="btn  btn-sm chaneg_status" user_id="{{$u->id}}" status="Locked" data-toggle="modal" title="Client Status" data-target="#myactive"><i class="fa fa-edit"></i> Locked</button>
                           @endif
                          
                             {{--  @if(Auth::user()->hasRole('client'))
                                 <button type="button" class="bit_config btn btn-success btn-sm" title="Bet Config" receiver_id="{{$u->id}}"> <i class="fa fa-credit-card"></i> Bet Config</button>
                              @endif --}}
                           </td>
                        <td>
                        	@if($u->email=='master@admin.com') -- 
                        	@else 
                           <button type="button" class="edit_admin_modal btn btn-sm btn-warning" commission="{{ $u->commission }}" user_fullname="{{$u->name}}" user_email="{{$u->email}}" user_id="{{$u->id}}" user_username="{{$u->username}}" user_mobile_no="{{$u->mobile_no}}" ><i class="fa fa-edit"></i></button> 
                           {{-- {!! Form::open([
                           'method' => 'DELETE',
                           'url' => ['/admin/delete_admin', $u->id],
                           'style' => 'display:inline'
                           ]) !!}
                           {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                           'type' => 'submit',
                           'class' => 'btn btn-danger btn-sm',
                           'title' => 'Delete Page',
                           'onclick'=>'return confirm("Confirm delete?")'
                           )) !!}
                           {!! Form::close() !!} --}}
                           @endif
                           {{-- @if(Auth::user()->hasRole('master'))
                           @else --}}
                           <button type="button" class="credit_button btn btn-success btn-sm" data-toggle="modal" data-target="#mycredit" title="transfor Coins" receiver_id="{{$u->id}}"> <i class="fa fa-credit-card"></i> Credit</button>
                           {{-- @endif --}}
                           @if(Auth::user()->hasRole('client'))
                           <a href="{{ url('/'.Auth::user()->roles[0]->name.'/memberSummary/'.Auth::user()->id.'/'.$u->id.'') }}" class="btn btn-info btn-sm"> <i class="fa fa-user"></i></a>
                           @else

                              <a href="{{ url('/'.Auth::user()->roles[0]->name.'/memberSummary/'.$u->id.'') }}" class="btn btn-info btn-sm"> <i class="fa fa-user"></i></a>
                           @endif

                        </td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
            </div>
         </div>
         </div>
      </div>
   </div>
</div>
<!--Start Add User-->
<div id="add_user" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Create New User</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            @if(Auth::user()->hasRole('master'))
               <form action="{{url('master/add_admin')}}" method="post">
            @elseif(Auth::user()->hasRole('admin'))
               <form action="{{url('admin/add_client')}}" method="post">
            @elseif(Auth::user()->hasRole('client'))
               <form action="{{url('client/add_customer')}}" method="post">
            @endif
               @csrf
               <div class="row">
                  <div class="col-md-6">
                     <label for="full_name">Enter Full Name:<span style="color:red" class="small-text">*required</span></label><br>
                     <input type="text" class="form-control"  name="full_name">
                  </div>
                  <div class="col-md-6">
                     <label for="username">Enter Username: <span style="color:red" class="small-text">*required | unique</span></label><br>
                     <input type="text" class="form-control"  name="username">
                  </div>   

                  <div class="col-md-6">
                     <label for="password">Enter Password: <span style="color:red" class="small-text">*required </span></label><br>
                     <input type="password" class="form-control" name="password">
                  </div>
                     {{-- <label for="new_credit_reference">Please Enter New Credit Reference :</label><br>
                     <input type="text" class="form-control" name="new_credit_reference"> --}}
                  <div class="col-md-6">
                     <label for="confirm_password">Enter Confirm Password: <span style="color:red" class="small-text">*required </span></label><br>
                     <input type="password" class="form-control" name="confirm_password">
                  </div>   
                  <div class="col-md-6">
                     <label for="">Please Enter Email:</label> 
                     <br>
                     <input type="email" class="form-control" name="email">
                  </div>
                  <div class="col-md-6">
                     <label for="new_credit_reference">Please Enter Mobile No.</label><br>
                     <input type="number" class="form-control" name="mobile_no">
                  </div>   
                  <div class="col-md-6">
                     <label for="commission">Commission:</label><br>
                     <input type="text" class="form-control" name="commission">
                  </div>
               </div>
               <br>
               <div class="row">
                  <div class="col-md-12">
                     <input type="submit" name="submit" value="Submit" class="btn btn-default btn-block btn-info">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<!--End Add User-->
<!--Start Activate User-->
<div id="myactive" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Change Member Status</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body" style="padding: 15px 15px;">
            <form id="change_status" method="post">
               <div class="row">
                  <div class="col-md-4">
                     <label for="">Current Status <i class="fa fa-arrow-down"></i></label>
                     <label for="" style="color:green"><b>Active</b></label> <br>
                     <label for="" style="color:red"><b>Suspended</b></label> <br>
                     <label for="" style="color:grey"><b>Locked</b></label> <br>
                  </div>
                  <div class="col-md-8" style="padding: 5px 5px">
                     <div class="row">
                        <div class="col-md-12">
                           <br>
                           <input type="hidden" name="user_id" id="user_id">
                        </div>
                        <div class="col-md-12">
                           <label><input type="radio"  name="status" value="1" id="Active">Activate</label><br>
                        </div>
                        <div class="col-md-12">
                           <label><input type="radio" name="status" value="2" id="Suspend">Suspend</label><br>
                        </div>
                        <div class="col-md-12">
                           <label><input type="radio" name="status" value="3" id="Locked">Locked</label>    
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <label for="">Enter Password:</label>
                           <input type="password" name="password" class="form-control" autocomplete="off" required="true" >
                        </div>
                     </div>
                     <br>
                     <input type="submit" name="submit" class="btn btn-default btn-block btn-info">
                     <p id="res_change_status"></p>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!--End Activate User-->
<!--Start Activate User-->
<div id="mycredit" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Chips</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <p id="res_credit_apply"></p>
            <form id="credit_apply" method="post">
               @csrf
               <input type="hidden" name="sender_id" @if(Auth::check()) value="{{Auth::id()}}" @endif>
               <input type="hidden" name="receiver_id" class="receiver_id" >
               <input type="hidden" name="type" value="deposit" class="withraw_or_deposit">
               <div class="togglebutton">
                  <span>I Want To Withdraw</span>
                  <label>
                    <input type="checkbox" class="withdrawStatus" id="withdrawStatus" >
                    <span class="toggle"></span>
                  </label>
              </div>
            <div class="row">
               <div class="col-md-12">
                 <label for="deposit_withraw_cost">Enter Cost To <span class="change_deposit_label"> Deposit </span></label> <input type="number" name="points" class="form-control">  
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <label for="remarks">Please Enter Remarks</label>
                  <input type="text" name="remarks" class="form-control">
               </div>
               <div class="col-md-12">
                  <label for="remarks">Please Enter Password</label>
                  <input type="password" name="password" class="form-control">
               </div>
            </div>

            <div class="row padding_top">
               <div class="col-md-12">
                  <input type="submit" value="Submit" class="btn btn-defaul btn-sm btn-success">
                  
               </div>
            </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!--End Credit User-->
<!--Start Bit Config-->
<div id="res_update_bit_config">
   
</div>
<!--End Credit User-->
<!--Start Credit User-->
<div id="editMODAL_user" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Edit ADMIN</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            {{-- <form  method="post" action="{{url('/'.Auth::user()->role->name.'/edit_admin')}}"> --}}
                 @if(Auth::user()->hasRole('master'))
                     <form action="{{url('master/edit_admin')}}" method="post">
                  @elseif(Auth::user()->hasRole('admin'))
                     <form action="{{url('admin/edit_client')}}" method="post">
                  @elseif(Auth::user()->hasRole('client'))
                     <form action="{{url('client/edit_customer')}}" method="post">
                  @endif
               @csrf
               <input type="hidden" name="user_id" id="modal_user_id">
               <div class="row">
                  <div class="col-md-6">
                     <label for="full_name">Enter Full Name:<span style="color:red" class="small-text">*required</span></label><br>
                     <input type="text" class="form-control" id="user_full_name" name="full_name">
                  </div>   
                  <div class="col-md-6">
                     <label for="username">Enter Username: <span style="color:red" class="small-text">*required | unique</span></label><br>
                     <input type="text" class="form-control" id="user_username" name="username">
                  </div>
                  <div class="col-md-6">
                     <label for="password">Enter Password: </label><br>
                     <input type="password" class="form-control" name="password">
                  </div>
                     {{-- <label for="new_credit_reference">Please Enter New Credit Reference :</label><br>
                     <input type="text" class="form-control" name="new_credit_reference"> --}}
                  <div class="col-md-6">
                     <label for="confirm_password">Enter Confirm Password: </label><br>
                     <input type="password" class="form-control" name="confirm_password">
                  </div>
                  <div class="col-md-6">
                     <label for="casino_credit_reference">Please Enter Email</label> 
                     <br>
                     <input type="email" id="user_email"  class="form-control" name="email">
                  </div>
                  <div class="col-md-6">
                     <label for="new_credit_reference">Please Enter Mobile No.</label><br>
                     <input type="number" id="user_mobile_no"  class="form-control" name="mobile_no">
                  </div>
                  <div class="col-md-6">
                     <label for="commission">Commission:</label><br>
                     <input type="text" class="form-control" id="commission" name="commission">
                  </div>
               </div>
               <br>
               <div class="row">
                  <div class="col-md-12">
                     <input type="submit" class="btn btn-default btn-block btn-info">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!--End Bit Config-->
   @section('custom_script')
      <script>
         $(document).ready(function(){
            $('.chaneg_status').on('click',function(){
               var status = $(this).attr('status');
               var user_id = $(this).attr('user_id');
               $( "#user_id").val(user_id);
               $( "#"+status).prop( "checked", true );
            });

            $('.credit_button').on('click',function(){
                  $('#credit_apply')[0].reset();
                  $('#res_credit_apply').html('');
                  var user_id = $(this).attr('receiver_id');
                  $('.receiver_id').val(user_id);


            });

            //Change Status
            $('#credit_apply').on('submit', function(e){
               e.preventDefault();
               $.ajax({
                 type: 'post',
                 url: '/credit-apply',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: $('#credit_apply').serialize(),
                 success: function(res) {
                   if (res.success) {
                    $('#res_credit_apply').html(res.message);
                        setTimeout(function(){
                            window.location.reload(true);
                      }, 2000);
                  }else{
                     $('#res_credit_apply').html(res.message);
                  }
                 }
               });
            });

            //Change Status
            $('.bit_config').on('click', function(){
               var receiver_id = $(this).attr('receiver_id');
               $.ajax({
                 type: 'post',
                 url: 'update-bit-config',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: {receiver_id:receiver_id},
                 success: function(res) {
                   if (res.success) {
                    $('#res_update_bit_config').html(res.message);
                  }
                 }
               });
            });

            //Change Status
            $('#change_status').on('submit', function(e){
               e.preventDefault();
               $.ajax({
                 type: 'post',
                 url: 'change-status',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: $('#change_status').serialize(),
                 success: function(res) {
                   if (res.success) {
                    $('#res_change_status').html(res.message);
                        setTimeout(function(){
                            window.location.reload(true);
                      }, 2000);
                  }else{
                     $('#res_change_status').html(res.message);
                  }
                 }
               });
            });

            //Change Status
            $('#add_bet_config').on('submit', function(e){
               e.preventDefault();
               $.ajax({
                 type: 'post',
                 url: 'add-bet-config',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: $('#add_bet_config').serialize(),
                 success: function(res) {
                   if (res.success) {
                    $('#res_add-bet-config').html(res.message);
                        setTimeout(function(){
                            window.location.reload(true);
                      }, 2000);
                  }else{
                     $('#res_add-bet-config').html(res.message);
                  }
                 }
               });
            });
            //Withdrwal Status
            $("#withdrawStatus").click(function() { 
               var chk = $(this).prop("checked");
                  if(chk) 
                  { 
                    $('.change_deposit_label').text('Withdraw');
                    $('.withraw_or_deposit').val('withdraw');
                  }
                  else 
                  { 
                   $('.change_deposit_label').text('Deposit');
                   $('.withraw_or_deposit').val('deposit');
                  } 
            }); 
         });
      </script>
   @endsection
@endsection