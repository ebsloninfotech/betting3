@extends('layouts.front')
@section('content')
<br>
<div class="container-fluid">
<div class="row">
    <div class="col-md-3">
            @include('sidebars.myaccount_sidebar')
        </div>
	<div class="col-md-9">
 		<div class="card">
            <div class="card-header">
                <h3 class="m-0">Competition List 
                    {{-- <a href="{{ url('master/competition/refresh/'.$sport_id.'') }}" class="float-right btn btn-sm btn-primary">Refresh</a> --}}
                </h3>
            </div>
                    <div class="card-body">
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-striped" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                		<th>Competition id</th>
                                		<th>Name</th>
                                		<th>Region</th>
                                		<th>Market Count</th>
                                		{{-- <th>Status</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i=1; @endphp
                                    @foreach($competition as $value)
                                	<tr>
                                		<td>{{ $i }}</td>
                                		<td><a href="{{ url('master/eventList/'.$value->event_type.'/'.$value->competition_id.'') }}" >{{ $value->competition_id }} </a></td>
                                		<td>{{ $value->name }}</td>
                                		<td>{{ $value->region }}</td>
                                		<td>{{ $value->marketCount }}</td>
                                		{{-- <td>
                                            <input type="checkbox" class="competitionStatus check_box" id="{{ $value->id }}" @if($value->status == 1) {{ 'checked' }} @endif >     
                                        </td> --}}
                                		
                                		
                                	</tr>
                                    @php $i++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
	</div>

</div>
<br>
</div>
{{-- 
<div class="loader " style="display: none;">
    <img src="{{ asset('front/img/spinner.webp') }}" alt="">
</div>
 --}}

@endsection

@section('scripts')

    <script>
        
             // $(".competitionStatus").on('click',function() { 
               // var id = $(this).attr('id');
               // $('.loader').show();
               // $.ajax({
                    // type: "POST",
                    // url: "/master/competitionStatus",
                    // headers: {
                    //        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    //    },
                    // data: {
                    //     id: id
                    // },
                    // success: function (msg) {
                        // $('.loader').hide();
                         // alert(msg);
                    // }
                // });
           // }); 
         $('#dataTables').DataTable();
    </script>


@endsection