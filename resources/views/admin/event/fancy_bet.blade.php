@extends('layouts.front')

@section('before_body')

<link rel="stylesheet" href="https://vitalets.github.io/x-editable/assets/x-editable/bootstrap3-editable/css/bootstrap-editable.css">

<script src="https://vitalets.github.io/x-editable/assets/x-editable/bootstrap3-editable/js/bootstrap-editable.js"></script>
<script>
    
 $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
</script>
@endsection


@section('content')
<br>
<div class="container-fluid">
<div class="row">
    <div class="col-md-3">
            @include('sidebars.myaccount_sidebar')
        </div>
	<div class="col-md-9">
 		<div class="card">
                    <div class="card-body">
                        <div class="card-head">
                            <h4>Fancy Bet of {{ $event->name }}</h4>
                        </div>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-striped" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                		<th>Selection id</th>
                                		<th>Title</th>
                                        <th>Comm. Type</th>
                                        <th>Comm. Value (In %)</th>
                                        <th>Min Bet</th>
                                        <th>Max Bet</th>
                                		{{-- <th>Status</th> --}}
                                        <th>Result</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i=1; @endphp
                                    @foreach($fancyBet as $value)
                                	<tr>
                                		<td>{{ $i }}</td>
                                		<td>{{ $value->SelectionId }}</td>

                                        <td>{{ $value->RunnerName }}</td>
                                		{{-- <td><input type="checkbox" class="fancyBetStatus check_box" id="{{ $value->id }}" @if($value->bet_status == 1) {{ 'checked' }} @endif > </td> --}}

                                         <td>
                                            <a href="#" class="commType{{ $i }} editable editable-click editable-empty" data-type="select" data-pk="{{ $value->id }}"  data-title="" style="">@if(!empty($value->comm_type)) {{ $value->comm_type }} @endif </a>
                                        </td>

                                        <td>
                                            <a href="#" class="commValu{{ $i }} editable editable-click editable-empty" data-type="text" data-pk="{{ $value->id }}"  data-title="" style="">@if(!empty($value->comm_value)) {{ $value->comm_value }} @endif </a>
                                        </td>

                                        <td>
                                            <a href="#" class="minBet{{ $i }} editable editable-click editable-empty" data-type="text" data-pk="{{ $value->id }}"  data-title="" style="">@if(!empty($value->min_bet)) {{ $value->min_bet }} @endif </a>
                                        </td>

                                        <td>
                                            <a href="#" class="maxBet{{ $i }} editable editable-click editable-empty" data-type="text" data-pk="{{ $value->id }}"  data-title="" style="">@if(!empty($value->max_bet)) {{ $value->max_bet }} @endif </a>
                                        </td>

                                        <td>
                                            @if($value->result)
                                                    {{ $value->result }}
                                            @else
                                            <a href="#" class="fancyResult{{ $i }} editable editable-click editable-empty" data-type="text" data-pk="{{ $value->id }}"  data-title="Enter Result" style=""></a>
                                            @endif
                                        </td>
                                		{{-- <td>
                                            <input type="checkbox" class="eventStatus check_box" id="{{ $value->id }}" @if($value->status == 1) {{ 'checked' }} @endif > 
                                               
                                        </td> --}}
                                        <td>

                                            @if(empty($value->resultStatus))

                                            <a href="{{ url('master/cancelFancyBet/'.$value->SelectionId.'/') }}" class="btn btn-sm btn-danger custmbt"> Cancel Bets</a> 
                                                
                                            <a href="{{ url('master/settleFancyBet/'.$value->SelectionId.'/') }}" class="btn btn-sm btn-success custmbt"> Settle Bets</a>  @else
                                                @if($value->resultStatus == 'settled')
                                                    <a href="{{ url('master/cancelfancySettle/'.$value->SelectionId.'/') }}" class="btn btn-sm btn-success custmbt">Cancel Settle Bets</a>
                                                @else
                                                    Cancelled
                                                @endif 
                                            @endif
                                        </td>
                                		
                                        @if($value->result == null)
                                        <script>
                                                $('.fancyResult{{ $i }}').editable({
                                                    prepend: "Update Result",
                                                    mode:"inline",
                                                    type: "POST",
                                                    url: "/master/fancyResult",
                                                    success: function(data) {

                                                              }
                                                }); 



                                            $('.minBet{{ $i }}').editable({
                                                mode:"inline",
                                                type: "POST",
                                                url: "/master/fancy/minBet",
                                                validate: function(value) {
                                                   if($.trim(value) == '') 
                                                    return 'This field is required';
                                                },
                                                success: function(data) {

                                                          }

                                                        });  



                                        $('.maxBet{{ $i }}').editable({
                                                mode:"inline",
                                                type: "POST",
                                                url: "/master/fancy/maxBet",
                                                validate: function(value) {
                                                   if($.trim(value) == '') return 'This field is required';
                                                },
                                                success: function(data) {

                                                          }
                                        });



                                             $('.commType{{ $i }}').editable({
                                                mode:"inline",
                                                type: "POST",
                                                url: "/master/fancy/commType",
                                                source: [
                                                        {value: 'debit', text: 'Debit'}, 
                                                        {value: 'credit', text: 'Credit'}, 
                                                ],
                                                success: function(data) {

                                                          }

                                                        });  



                                            $('.commValu{{ $i }}').editable({
                                                mode:"inline",
                                                type: "POST",
                                                url: "/master/fancy/commValu",
                                                validate: function(value) {
                                                   if($.trim(value) == '') return 'This field is required';
                                                },
                                                success: function(data) {

                                                          }
                                        });


                                        </script>
                                     @endif

                                		
                                    
                                	</tr>
                                    @php $i++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
	</div>

</div>
<br>
</div>


@endsection

@section('scripts')



    <script>

       
       




         $('#dataTables').DataTable();






    </script>


@endsection