@extends('layouts.front')

@section('before_body')

<link rel="stylesheet" href="https://vitalets.github.io/x-editable/assets/x-editable/bootstrap3-editable/css/bootstrap-editable.css">

<script src="https://vitalets.github.io/x-editable/assets/x-editable/bootstrap3-editable/js/bootstrap-editable.js"></script>
<script>
    
 $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
</script>
@endsection


@section('content')
<br>
<div class="container-fluid">
<div class="row">
    <div class="col-md-3">
            @include('sidebars.myaccount_sidebar')
        </div>
	<div class="col-md-9">
 		<div class="card">
                    <div class="card-body">
                        <div class="card-head">
                            <h4>Events in {{ $competition->name }}</h4>
                        </div>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-striped" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                		<th>Event id</th>
                                		<th>Name</th>
                                		<th>openDate</th>
                                		<th>Market Count</th>
                                        <th>Bet Status</th>
                                        {{-- <th>Result</th> --}}
                                		<th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i=1; @endphp
                                    @foreach($event as $value)
                                	<tr>
                                		<td>{{ $i }}</td>
                                		<td>{{ $value->event_id   }}
                                            <!-- <a href="{{ url('master/marketList/'.$value->event_type.'/'.$value->competition_id.'/'.$value->event_id.'') }}" >{{ $value->event_id   }}  -->
                                            </a>
                                        </td>
                                		<td>{{ $value->name }}</td>
                                		<td>{{ $value->openDate }}</td>
                                        <td>{{ $value->marketCount }}</td>
                                		<td><input type="checkbox" class="eventBetStatus check_box" id="{{ $value->id }}" @if($value->bet_status == 1) {{ 'checked' }} @endif > </td>

                                        {{-- <td>
                                            @if($value->result)
                                                    {{ $value->selection->runner_name }}
                                            @else
                                            <a href="#" class="matchResult{{ $i }} editable editable-click editable-empty" data-type="select" data-pk="{{ $value->id }}"  data-title="Enter Strips" style="">@if($value->result == null) Not Updated @else {{ $value->result }} @endif </a>
                                            @endif
                                        </td> --}}
                                		<td>
                                            <input type="checkbox" class="eventStatus check_box" id="{{ $value->id }}" @if($value->status == 1) {{ 'checked' }} @endif > 
                                               
                                        </td>
                                        <td>
                                            <a href="{{ url('master/fancyBet/'.$value->event_id.'/') }}" class="btn btn-info btn-sm custmbt ">Fancy Markets</a>

                                            <a href="{{ url('master/marketList/'.$value->event_type.'/'.$value->competition_id.'/'.$value->event_id.'') }}" class="btn btn-sm custmbt btn-info"> Markets </a>

                                          {{--   @if($value->result_status == null)
                                            <a href="{{ url('master/cancelBet/'.$value->event_id.'/') }}" class="btn btn-sm btn-danger custmbt" onclick="return confirm('Are you sure you want to cancel all bets?');"> Cancel Bets</a> 

                                            <a href="{{ url('master/settleBet/'.$value->event_id.'/') }}" class="btn btn-sm btn-success custmbt" onclick="return confirm('Are you sure you want to settle all bets?');"> Settle Bets</a> 
                                            @else   

                                            <a href="{{ url('master/cancelsettleBet/'.$value->event_id.'/') }}" class="btn btn-sm btn-success custmbt" onclick="return confirm('Are you sure you want to cancel all settle bets?');"> Cancel Settle Bets</a> 


                                            @endif --}}
                                        </td>
                                		
                                		
                                    <script>
                                        @if($value->result == null)
                                            $('.matchResult{{ $i }}').editable({
                                                prepend: "Select Option",
                                                mode:"inline",
                                                type: "POST",
                                                url: "/matchResult",
                                                source: [
                                                    @foreach (getSelection($value->event_id) as $element)
                                                        {value: '{{ $element->selection_id }}', text: '{{ $element->runner_name }}'}, 
                                                    @endforeach
                                                ],
                                                success: function(data) {

                                                          }
                                            }); 
                                     @endif

                                    </script>
                                	</tr>
                                    @php $i++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
	</div>

</div>
<br>
</div>


@endsection

@section('scripts')



    <script>

       
        
             $(".eventStatus").on('click',function() { 
               var id = $(this).attr('id');

               $.ajax({
                    type: "POST",
                    url: "/master/eventStatus",
                    headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                    data: {
                        id: id
                    },
                    success: function (msg) {
                         // alert(msg);
                    }
                });
           }); 



                 $(".eventBetStatus").on('click',function() { 
               var id = $(this).attr('id');

               $.ajax({
                    type: "POST",
                    url: "/master/eventBetStatus",
                    headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                    data: {
                        id: id
                    },
                    success: function (msg) {
                         // alert(msg);
                    }
                });
           }); 




         $('#dataTables').DataTable();






    </script>


@endsection