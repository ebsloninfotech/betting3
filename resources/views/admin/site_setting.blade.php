@extends('layouts.front')
@section('content')
<br>
<div class="container-fluid">
   <div class="row">
      <div class="col-md-6 col-md-offset-3">
         <div class="card">
            <div class="card-header">
               <h5>Bet Configuration</h5>
            </div>  
            <div class="card-body" style="padding: 20px;">   
               <form action="{{ url('master/configUpdate') }}" method="post">
                  @csrf
                  <div class="row">
                     <div class="col-md-6">
                        <h4 class="sporthead">Cricket</h4>    
                        <div class="form-group row">
                           <div class="col-md-4">
                              <label for="" class="small_label">Minimum Bet Amount</label>
                           </div>
                           <div class="col-md-8">
                              <input type="number" class="form-control customeFormCon" name="cricminbet" value="{{ $data->cricminbet }}" >
                           </div>
                        </div> 
                        <div class="form-group row">
                           <div class="col-md-4">
                              <label for="" class="small_label">Maximum Bet Amount</label>
                           </div>
                           <div class="col-md-8">
                              <input type="number" class="form-control customeFormCon" name="cricmaxbet" value="{{ $data->cricmaxbet }}" >
                           </div>
                        </div> 
                        <div class="form-group row">
                           <div class="col-md-4">
                              <label for="" class="small_label">Minimum Fancy Bet Amount</label>
                           </div>
                           <div class="col-md-8">
                              <input type="number" class="form-control customeFormCon" name="cricminfancybet" value="{{ $data->cricminfancybet }}" >
                           </div>
                        </div> 
                        <div class="form-group row">
                           <div class="col-md-4">
                              <label for="" class="small_label">Maximum Fancy Bet Amount</label>
                           </div>
                           <div class="col-md-8">
                              <input type="number" class="form-control customeFormCon" name="cricmaxfancybet" value="{{ $data->cricmaxfancybet }}" >
                           </div>
                        </div> 
                     </div>
                     <div class="col-md-6">
                         <h4 class="sporthead">Soccer</h4>            
                           <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="" class="small_label">Minimum Bet Amount</label>
                              </div>
                              <div class="col-md-8">
                                 <input type="number" class="form-control customeFormCon" name="soccerminbet" value="{{ $data->soccerminbet }}" >
                              </div>
                           </div> 
                           <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="" class="small_label">Maximum  Bet Amount</label>
                              </div>
                              <div class="col-md-8">
                                 <input type="number" class="form-control customeFormCon" name="soccermaxbet" value="{{ $data->soccermaxbet }}" >
                              </div>
                           </div> 
                           <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="" class="small_label">Minimum Fancy Bet Amount</label>
                              </div>
                              <div class="col-md-8">
                                 <input type="number" class="form-control customeFormCon" name="soccerminfancybet" value="{{ $data->soccerminfancybet }}" >
                              </div>
                           </div> 
                           <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="" class="small_label">Maximum Fancy Bet Amount</label>
                              </div>
                              <div class="col-md-8">
                                 <input type="number" class="form-control customeFormCon" name="soccermaxfancybet" value="{{ $data->soccermaxfancybet }}" >
                              </div>
                           </div> 
                     </div>
                     <div class="col-md-12">
                        <h4 class="sporthead">Tennis</h4>            
                           <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="" class="small_label">Minimum Bet Amount</label>
                              </div>
                              <div class="col-md-8">
                                 <input type="number" class="form-control customeFormCon" name="tennisminbet" value="{{ $data->tennisminbet }}" >
                              </div>
                           </div> 
                           <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="" class="small_label">Maximum Bet Amount</label>
                              </div>
                              <div class="col-md-8">
                                 <input type="number" class="form-control customeFormCon" name="tennismaxbet" value="{{ $data->tennismaxbet }}" >
                              </div>
                           </div> 
                           <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="" class="small_label">Minimum Fancy Bet Amount</label>
                              </div>
                              <div class="col-md-8">
                                 <input type="number" class="form-control customeFormCon" name="tennisminfancybet" value="{{ $data->tennisminfancybet }}" >
                              </div>
                           </div> 
                           <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="" class="small_label">Maximum Fancy Bet Amount</label>
                              </div>
                              <div class="col-md-8">
                                 <input type="number" class="form-control customeFormCon" name="tennismaxfancybet" value="{{ $data->tennismaxfancybet }}" >
                              </div>
                           </div> 
                     </div>
                     <div class="col-md-6 hidden">
                        <h4 class="sporthead">Horse Racing</h4>            
                           <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="" class="small_label">Minimum Bet Amount</label>
                              </div>
                              <div class="col-md-8">
                                 <input type="number" class="form-control customeFormCon" name="horseminbet" value="{{ $data->horseminbet }}" >
                              </div>
                           </div> 
                           <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="" class="small_label">Maximum Bet Amount</label>
                              </div>
                              <div class="col-md-8">
                                 <input type="number" class="form-control customeFormCon" name="horsemaxbet" value="{{ $data->horsemaxbet }}" >
                              </div>
                           </div> 
                           <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="" class="small_label">Minimum Fancy Bet Amount</label>
                              </div>
                              <div class="col-md-8">
                                 <input type="number" class="form-control customeFormCon" name="horseminfancybet" value="{{ $data->horseminfancybet }}" >
                              </div>
                           </div> 
                           <div class="form-group row">
                              <div class="col-md-4">
                                 <label for="" class="small_label">Maximum Fancy Bet Amount</label>
                              </div>
                              <div class="col-md-8">
                                 <input type="number" class="form-control customeFormCon" name="horsemaxfancybet" value="{{ $data->horsemaxfancybet }}" >
                              </div>
                           </div> 
                     </div>
                  </div>
               
              
               
               <div class="form-group row text-center">
                  <input type="submit" class="btn btn-primary" value="Submit">
               </div>         
               </form>        
            </div>
         </div>
      </div>
   </div>
</div>
       
@endsection