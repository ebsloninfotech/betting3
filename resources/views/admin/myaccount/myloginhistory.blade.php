@extends('layouts.front')
@section('content')
<div class="container-fluid">
<div class="row">
	<div class="col-md-2 col-sm-2 col-xs-2 col-p">
        @include('sidebars.myaccount_sidebar')
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10">
        <div class="row">
            <div class="col-xs-12 col-md-12 p-7">
                <div class="acc-det">
                <h2>My Login History</h2>
              </div>
              </div>
            <div class="col-md-12 col-sm-12 col-xs-12 p-7">
                <div class="card m-10">
                    <div class="card-body">
                        {{-- <a href="{{ url('/admin/pages/create') }}" class="btn btn-success btn-sm" title="Add New Page">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
 --}}
                        
                        <div class="table-responsive">
                            <table class="table-balance table-border table-striped" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>Date & Time</th>
                                        <th>Login Status</th>
                                        <th>IP Address</th>
                                        <th>ISP</th>
                                        <th>City/Country</th>
                                        <th>Time Zone</th>
                                        {{-- <th>Actor</th> --}}
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($activitylogs as $item)
                                    <tr>
                                        <td>{{ $item->properties['login'] }}</td>
                                        <td>Login Success</td>
                                        <td>{{ $item->properties['ipaddress'] }}</td>
                                        <td>{{ $item->properties['isp'] }}</td>
                                        <td>{{ $item->properties['city'] }}/{{ $item->properties['country'] }}</td>
                                        <td>{{ $item->properties['time_zone'] }}</td>
                                       {{--  <td>
                                            @if ($item->causer)
                                                <a href="{{ url('/admin/users/' . $item->causer->id) }}">{{ $item->causer->name }}</a>
                                            @else
                                                -
                                            @endif
                                        </td> --}}
                                        <td>{{ $item->properties['login'] }}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
	</div>
</div>
</div>
@endsection