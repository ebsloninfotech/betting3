@extends('layouts.front')
@section('content')
<div class="container">
	<br>
<div class="row">
	<div class="col-md-3">
		<div class="card">
                            <div class="card-header">
                                My Account
                            </div>

                            <div class="card-body">
                                <ul class="nav flex-column" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" href="{{url('/admin/myaccount/myprofile')}}">
                                                My Profile
                                            </a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" href="#">
                                                Account Statement
                                            </a>
                                        </li>
                                        {{-- <li class="nav-item" role="presentation">
                                            <a class="nav-link" href="#">
                                                Casino Statement
                                            </a>
                                        </li> --}}
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" href="#">
                                                Login History
                                            </a>
                                        </li>
                                </ul>
                            </div>
                        </div>
	</div>
	<div class="col-md-9">
		
	</div>
</div>
</div>
@endsection