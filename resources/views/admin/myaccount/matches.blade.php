@extends('layouts.front')
@section('content')
<br>
<div class="container-fluid">
   <div class="row">
   {{--    <div class="col-md-3">
            @include('sidebars.myaccount_sidebar')
        </div> --}}
   <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <h3 class="downline-h">All Matches </h3>
        	   </div>
         <div class="card-body">
            <div class="table-responsive">
               <table class="table-balance table-border table-striped" id="dataTables">
                  <thead>
                     <tr>
                        <th>ID</th>
                        <th>Sports</th>
                        <th>Competition</th>
                        <th>Match</th>
                        <th>Open Date</th>
                        <th>Result</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     @php $i=1; @endphp
                     @foreach($data as $u)

                     @if($u->event->oddmarket)

                     <tr>
                        <td>{{ $i++ }}</td>
                        <td>
                           @if($u->event->event_type == '4')
                              Cricket
                           @endif
                           @if($u->event->event_type == '2')
                              Tennis
                           @endif
                           @if($u->event->event_type == '1')
                              Soccer
                           @endif


                        </td>
                        <td>{{ $u->event->competition->name }}</td>
                        <td>{{ $u->event->name }}</td>
                        <td>{{ $u->event->openDate }}</td>
                        <td>
                           @if($u->event->result)
                              Result Declared
                           @else
                              Match Running
                           @endif
                        </td>
                        <td>
                           <a href="{{ url('fullmarket/'.$u->event->event_type.'/'.$u->event->event_id.'/'.$u->event->oddmarket->marketId.'') }}" class="btn btn-sm btn-success" >Match Book</a>

                           {{-- <a href="javascript:void(0)" event_type="{{ $u->event->event_type }}" event_id="{{ $u->event->event_id }}" market_id="{{ $u->event->oddmarket->marketId }}"  class="totalBook btn btn-sm btn-success" >Total Book</a> --}}

                        </td>
                     </tr>
                     @endif
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
         </div>
      </div>
   </div>
</div>


 
@endsection


@section('scripts')



<!-- Modal -->
<div class="modal fade" id="totalBookModal" tabindex="-1" role="dialog" aria-labelledby="totalBookModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title clsgreen" id="totalBookModalLabel">Total Book : Match (<span class="matchedTotal"></span>) + Session (<span class="sessionTotal"> 2900</span>)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body totalBookData p-0" id="totalBookData">
         
         
      </div>
    </div>
  </div>
</div>



<script>
   
   $('.totalBook').click(function(){
      

      var event_type = $(this).attr('event_type');
      var event_id = $(this).attr('event_id');
      var market_id = $(this).attr('market_id');

      $.ajax({
              type: 'POST',
              url: '/fetchTotalBook',
              async: false,
              headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
              data:{event_type:event_type,event_id:event_id,market_id:market_id},       
              success: function(res) {

                  if(res[0].success){

                     $('#totalBookData').html(res[0].data);
                     $('.matchedTotal').html(res[0].total_match);
                     $('.sessionTotal').html(res[0].total_session);
                     $('#totalBookModal').modal('show');
                     $('.matchedTotal').addClass(res[0].total_match_class);
                     $('.sessionTotal').addClass(res[0].total_session_class);

                  }

              }

          })


   });



</script>



@endsection