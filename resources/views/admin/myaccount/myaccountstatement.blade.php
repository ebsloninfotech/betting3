@extends('layouts.front')
@section('content')
<div class="container-fluid">
<div class="row">
	<div class="col-md-2 col-sm-2 col-xs-2 col-p">
		@include('sidebars.myaccount_sidebar')
	</div>
	<div class="col-md-10 col-sm-10 col-xs-10">
        <div class="row">
            <div class="col-xs-12 col-md-12 p-7">
                <div class="acc-det">
                <h2>Account Statement</h2>
              </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12 p-7">
                <div class="card">
                   <!--  <div class="card-header">Account Statement</div> -->
                    <div class="card-body">
                        <!-- <div class="table-head">
                            <div class="dataTables_length" id="dataTables_length"><label>Show <select name="dataTables_length" aria-controls="dataTables" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div>
                            <div id="dataTables_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="dataTables"></label></div>
                        </div> -->
                        <table class="table-balance table-responsive table-border table-striped" id="dataTables">
                            <thead>
                                    <tr>
                                        <th>Date/Time</th>
                                        <th>Deposit</th>
                                        <th>Withdraw</th>
                                        <th>Balance</th>
                                        <th>Remarks</th>
                                        <th>From/to</th>
                                    </tr>
                                </thead>
                            <tbody>

                                    @foreach($data as $value)
                                    <tr>
                                        <td>{{ $value->created_at }}</td>
                                        <td>@if($value->type == 'deposit') {{ $value->points }} @else -- @endif</td>
                                        <td>@if($value->type == 'withdraw') {{ $value->points }} @else -- @endif</td>
                                        <td>{{ $value->balance }}</td>
                                        <td>{{ $value->remarks }}</td>
                                        <td>{{ userDetail($value->sender_id)->name  }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>


                        </table>
                        <!-- <div class="table-responsive">
                            <table class="table table-border table-striped" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>Date/Time</th>
                                        <th>Deposit</th>
                                        <th>Withdraw</th>
                                        <th>Balance</th>
                                        <th>Remarks</th>
                                        <th>From/to</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($data as $value)
                                    <tr>
                                        <td>{{ $value->created_at }}</td>
                                        <td>@if($value->type == 'deposit') {{ $value->points }} @else -- @endif</td>
                                        <td>@if($value->type == 'withdraw') {{ $value->points }} @else -- @endif</td>
                                        <td>{{ $value->balance }}</td>
                                        <td>{{ $value->remarks }}</td>
                                        <td>{{ userDetail($value->sender_id)->name  }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
 -->
                    </div>
                </div>
              </div>
        </div>
                
	</div>
</div>
</div>
@endsection