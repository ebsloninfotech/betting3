@extends('layouts.front')
@section('content')
<br>
<div class="container-fluid">
<div class="row">
	<div class="col-md-3">
		@include('sidebars.myaccount_sidebar')
	</div>
	<div class="col-md-9">
                <div class="card">
                    <div class="card-header">Casino Statement</div>
                    <div class="card-body">
                        {{-- <a href="{{ url('/admin/pages/create') }}" class="btn btn-success btn-sm" title="Add New Page">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
 --}}
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/pages', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-border table-striped" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>Date/Time</th>
                                        <th>Credit</th>
                                        <th>Debit</th>
                                        <th>Total</th>
                                        <th>Game Type</th>
                                        <th>Bet Type</th>
                                        <th>Round Id</th>
                                        <th>Narration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<tr>
                                		<td>02-12-2019 / 12:00 AM</td>
                                		<td>1200</td>
                                		<td>1100</td>
                                		<td>2300</td>
                                        <td>Football</td>
                                		<td>--bet-type--</td>
                                        <td>--123123--</td>
                                		<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur veritatis totam architecto in nesciunt</td>
                                		{{-- <td>
                                			{!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Page',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                        </td> --}}
                                	</tr>
                               {{--  @foreach($pages as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->title }}</td><td>{{ $item->content }}</td>
                                        <td>
                                            <a href="{{ url('/admin/pages/' . $item->id) }}" title="View Page"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/pages/' . $item->id . '/edit') }}" title="Edit Page"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/pages', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Page',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach --}}
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
	</div>
</div>
</div>
@endsection