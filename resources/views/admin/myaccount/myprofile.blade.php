@extends('layouts.front')
@section('content')
<div class="container-fluid">
    <div class="row">
    	<div class="col-md-2 col-sm-2 col-xs-2 col-p">
    		@include('sidebars.myaccount_sidebar')
    	</div>
    	<div class="col-md-10 col-sm-10 col-xs-10">
            <div class="row">
              <div class="col-xs-12 col-md-12 p-7">
                <div class="acc-det">
                <h2>Account Details</h2>
              </div>
              </div>
                <div class="col-md-7 col-sm-7 col-xs-7 p-7">
                   <div class="card">
                        <div class="card-header">
                            <h3>About You <i class="fa fa-pencil"data-toggle="modal" data-target="#update_profile"></i>
                            
                                <!-- <a href="JavaScript:void(0)" class="float-right btn btn-info btn-sm" data-toggle="modal" data-target="#update_profile">Update Profile </a> --></h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-hover pro-table">
                              <tr>
                                <td>Name</td>
                                <td>@if(Auth::check()) {{Auth::User()?Auth::user()->name:''}} @endif</td>
                              </tr>
                               <tr>
                                <td>User Name</td>
                                <td>@if(Auth::check()) {{Auth::User()?Auth::user()->username:''}} @endif</td>
                              </tr>
                               <tr>
                                <td>Email</td>
                                <td>  @if(Auth::check()) {{Auth::User()?Auth::user()->email:''}} @endif</td>
                              </tr>
                               <tr>
                                <td>Password</td>
                                <td> ******* <a href="javascript:void(0)" class="fa fa-pencil" data-toggle="modal" data-target="#change_password" >
                                   </a></td>
                              </tr>
                            </table>

                        </div>  
                    </div> 
                </div>
                <div class="col-md-5 col-sm-5 col-xs-5 p-7">
                  
                    <div class="card">
                        <div class="card-header"><h3>Contact Detail</h3> </div>
                
                        <div class="card-body">
                          <table class="table pro-table">
                            <tr>
                              <td>Mobile</td>
                              <td> -  @if(Auth::check()) {{Auth::User()?Auth::user()->mobile_no:''}} @endif</td>
                            </tr>
                          </table>
                        </div>  
                    </div>
                </div>
            </div>
            </div>

<div id="update_profile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Profile</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <form id="edit_profile" method="post">
            @csrf
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" name="name" value="{{Auth::User()->name}}" class="form-control" required="true"> <br>
                        <input type="text" name="username"  value="{{Auth::User()->username}}" class="form-control" required="true"> <br>
                        <input type="number" name="mobile_no" value="{{Auth::User()->mobile_no}}" placeholder="Mobile Number" class="form-control" required="true"><br>
                    </div>
                    <div class="col-md-12">
                        <input type="submit" value="Update Profile" class="btn btn-block btn-info">
                        <p id="res_update_profile"></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<div id="change_password" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Change Password</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <form id="reset_password" method="post">
            @csrf
                <div class="row">
                    <div class="col-md-12">
                        <input type="password" name="old_password" placeholder="Old Password" class="form-control" required="true"> <br>
                        <input type="password" name="new_password" placeholder="New Password" class="form-control" required="true"> <br>
                        <input type="password" name="confirm_new_password" placeholder="Confirm New Password" class="form-control" required="true"><br>
                    </div>
                    <div class="col-md-12">
                        <input type="submit" value="Submit" class="btn btn-block btn-info">
                        <p id="res_change_password"></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
@endsection
   @section('custom_script')
      <script>
         $(document).ready(function(){
            //Change Password
            $('#edit_profile').on('submit', function(e){
               e.preventDefault();
               $.ajax({
                 type: 'post',
                 url: 'update-profile',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: $('#edit_profile').serialize(),
                 success: function(res) {
                   if (res.success) {
                    $('#res_update_profile').html(res.message);
                        setTimeout(function(){
                            window.location.reload(true);
                    }, 2000);
                  }
                 }
               });
            });


            $('#reset_password').on('submit', function(e){
               e.preventDefault();
               $.ajax({
                 type: 'post',
                 url: 'change-password',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: $('#reset_password').serialize(),
                 success: function(res) {
                   if (res.success) {
                    $('#res_change_password').html(res.message);
                        setTimeout(function(){
                            window.location.reload(true);
                      }, 2000);
                  }else{
                     $('#res_change_password').html(res.message);
                  }
                 }
               });
            });
         });
      </script>
   @endsection