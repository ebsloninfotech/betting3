@extends('layouts.front')
@section('content')
<br>
<div class="container-fluid">
<div class="row">
	<div class="col-md-3">
            @include('sidebars.myaccount_sidebar')
        </div>
	<div class="col-md-9">
 <div class="card">
                    <div class="card-header">Live Bets</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-borderless" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>Bet ID</th>
                                        <th>User Name</th>
                                        <th>Market Name</th>
                                		<th>Runner Name</th>
                                		<th>Side</th>
                                		<th>Odds</th>
                                		<th>Amount</th>
                                        <th>Profit</th>
                                		<th>Created Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($bets as $value)
                                	<tr>
                                		<td>{{ $value->id }}</td>
                                		<td>{{ $value->user->name }}</td>
                                		<td>{{ $value->event_name }}</td>
                                		<td>{{ $value->title }}</td>
                                		<td>{{ $value->bet_type }}</td>
                                		<td>{{ $value->odds }}</td>
                                		<td>{{ $value->bet_amount }}</td>
                                        <td>{{ $value->profit }} ({{ $value->profit_type }})</td>
                                		<td>{{ $value->created_at }}</td>
                                		
                                	</tr>
                                	@endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
	</div>

</div>
</div>







@endsection