@extends('layouts.front')
@section('content')
<br>
<div class="container-fluid">
<div class="row">
	<div class="col-md-3">
            @include('sidebars.myaccount_sidebar')
        </div>
	<div class="col-md-9">
 <div class="card">
                    <div class="card-header">Live Bets</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-borderless" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>Bet ID</th>
                                        <th>User Name</th>
                                        <th>Market Name</th>
                                		<th>Runner Name</th>
                                		<th>Side</th>
                                		<th>Odds</th>
                                		<th>Amount</th>
                                		<th>Created Date</th>
                                		<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($bets as $value)
                                	<tr>
                                		<td>{{ $value->id }}</td>
                                		<td>{{ $value->user->name }}</td>
                                		<td>{{ $value->event_name }}</td>
                                		<td>{{ $value->title }}</td>
                                		<td>{{ $value->bet_type }}</td>
                                		<td>{{ $value->odds }}</td>
                                		<td>{{ $value->bet_amount }}</td>
                                		<td>{{ $value->created_at }}</td>
                                		<td class="betSettle" id="Bet{{ $value->id }}" ><a href="javascript:void(0)" bet-id="{{ $value->id }}"  class="settleBet btn btn-sm btn-primary">Settle</a></td>
                                		
                                	</tr>
                                	@endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
	</div>

</div>
</div>







@endsection


@section('custom_script')

<div id="betSett">
	
</div>



<script>
	

	$('.settleBet').click(function(){

		var id = $(this).attr('bet-id');

       $.ajax({
            type: "POST",
            url: "/client/settleBet",
            headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
            data: {
                id: id
            },
            success: function (msg) {
            	if(msg.success){
            		$('#betSett').html(msg.data);


            	}else{
            		alert('something went wrong');
            	}
            }
        });


	})


</script>
@endsection