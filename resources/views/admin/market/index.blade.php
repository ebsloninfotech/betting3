@extends('layouts.front')

@section('before_body')

<link rel="stylesheet" href="https://vitalets.github.io/x-editable/assets/x-editable/bootstrap3-editable/css/bootstrap-editable.css">

<script src="https://vitalets.github.io/x-editable/assets/x-editable/bootstrap3-editable/js/bootstrap-editable.js"></script>
<script>
    
 $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
</script>
@endsection

@section('content')
<br>
<div class="container-fluid">
<div class="row">
    <div class="col-md-3">
            @include('sidebars.myaccount_sidebar')
        </div>
	<div class="col-md-9">
 		<div class="card">
                    <div class="card-body">
                        <div class="card-head">
                            <h4>Market list of {{ $event->name }}</h4>
                        </div>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-striped" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                		<th>Market id</th>
                                		<th>Name</th>
                                		<th>start time</th>
                                        <th>Comm. Type</th>
                                        <th>Comm. Value (In %)</th>
                                        <th>Min Bet</th>
                                        <th>Max Bet</th>
                                		<th>Status</th>
                                         <th>Result</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i=1; @endphp
                                    @foreach($market as $value)
                                	<tr>
                                		<td>{{ $i }}</td>
                                		<td>{{ $value->marketId }} </td>
                                		<td>{{ $value->marketName }}</td>
                                		<td>{{ $value->marketStartTime }}</td>

                                        <td>
                                            <a href="#" class="commType{{ $i }} editable editable-click editable-empty" data-type="select" data-pk="{{ $value->id }}"  data-title="" style="">@if(!empty($value->comm_type)) {{ $value->comm_type }} @endif </a>
                                        </td>

                                        <td>
                                            <a href="#" class="commValu{{ $i }} editable editable-click editable-empty" data-type="text" data-pk="{{ $value->id }}"  data-title="" style="">@if(!empty($value->comm_value)) {{ $value->comm_value }} @endif </a>
                                        </td>

                                        <td>
                                            <a href="#" class="minBet{{ $i }} editable editable-click editable-empty" data-type="text" data-pk="{{ $value->id }}"  data-title="" style="">@if(!empty($value->min_bet)) {{ $value->min_bet }} @endif </a>
                                        </td>

                                        <td>
                                            <a href="#" class="maxBet{{ $i }} editable editable-click editable-empty" data-type="text" data-pk="{{ $value->id }}"  data-title="" style="">@if(!empty($value->max_bet)) {{ $value->max_bet }} @endif </a>
                                        </td>

                                		<td>
                                            <input type="checkbox" class="marketStatus check_box" id="{{ $value->id }}" @if($value->status == 1) {{ 'checked' }} @endif >     
                                        </td>
                                        <td>
                                            @if($value->result)
                                                    {{ $value->selection->runner_name }}
                                            @else
                                            <a href="#" class="matchResult{{ $i }} editable editable-click editable-empty" data-type="select" data-pk="{{ $value->id }}"  data-title="Enter Strips" style="">@if($value->result == null) Not Updated @else {{ $value->result }} @endif </a>
                                            @endif
                                        </td>
                                		<td>

                                            @if($value->result_status == null)
                                            <a href="{{ url('master/cancelBet/'.$value->marketId.'/') }}" class="btn btn-sm btn-danger custmbt" onclick="return confirm('Are you sure you want to cancel all bets?');"> Cancel Bets</a> 

                                            <a href="{{ url('master/settleBet/'.$value->marketId.'/') }}" class="btn btn-sm btn-success custmbt" onclick="return confirm('Are you sure you want to settle all bets?');"> Settle Bets</a> 
                                            @else   

                                            <a href="{{ url('master/cancelsettleBet/'.$value->marketId.'/') }}" class="btn btn-sm btn-success custmbt" onclick="return confirm('Are you sure you want to cancel all settle bets?');"> Cancel Settle Bets</a> 


                                            @endif


                                        </td>
                                		
                                	</tr>
                                        <script>


                                             @if($value->result == null)
                                                    $('.matchResult{{ $i }}').editable({
                                                        prepend: "Select Option",
                                                        mode:"inline",
                                                        type: "POST",
                                                        url: "/matchResult",
                                                        source: [
                                                            @foreach (getSelectionviaMarket($value->marketId) as $element)
                                                                {value: '{{ $element->selection_id }}', text: '{{ $element->runner_name }}'}, 
                                                            @endforeach
                                                        ],
                                                        success: function(data) {

                                                                  }
                                                    }); 
                                             @endif


                                            $('.minBet{{ $i }}').editable({
                                                mode:"inline",
                                                type: "POST",
                                                url: "/master/market/minBet",
                                                validate: function(value) {
                                                   if($.trim(value) == '') 
                                                    return 'This field is required';
                                                },
                                                success: function(data) {

                                                          }

                                                        });  



                                        $('.maxBet{{ $i }}').editable({
                                                mode:"inline",
                                                type: "POST",
                                                url: "/master/market/maxBet",
                                                validate: function(value) {
                                                   if($.trim(value) == '') return 'This field is required';
                                                },
                                                success: function(data) {

                                                          }
                                        });



                                             $('.commType{{ $i }}').editable({
                                                mode:"inline",
                                                type: "POST",
                                                url: "/master/market/commType",
                                                source: [
                                                        {value: 'debit', text: 'Debit'}, 
                                                        {value: 'credit', text: 'Credit'}, 
                                                ],
                                                success: function(data) {

                                                          }

                                                        });  



                                            $('.commValu{{ $i }}').editable({
                                                mode:"inline",
                                                type: "POST",
                                                url: "/master/market/commValu",
                                                validate: function(value) {
                                                   if($.trim(value) == '') return 'This field is required';
                                                },
                                                success: function(data) {

                                                          }
                                        });

                                    </script>

                                    @php $i++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
	</div>

</div>
<br>
</div>


@endsection

@section('scripts')

    <script>
        
             $(".marketStatus").on('click',function() { 
               var id = $(this).attr('id');

               $.ajax({
                    type: "POST",
                    url: "/master/marketStatus",
                    headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                    data: {
                        id: id
                    },
                    success: function (msg) {
                         // alert(msg);
                    }
                });
           }); 
         $('#dataTables').DataTable();
    </script>




@endsection