@extends('layouts.front')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-xs-3 col-md-3 col-p">
            @include('admin.client.layout.clientlayout')
        </div>
    	<div class="col-sm-9 col-xs-9 col-md-9">
            <div class="row">
                <div class="col-xs-12 col-md-12 p-7">
                <div class="acc-det">
                <h2>Login History</h2>
              </div>
              </div>
              <div class="col-sm-12 col-xs-12 col-md-12 p-7">
                <div class="card m-10">
                    <!-- <div class="card-header">Login History</div> -->
                    <div class="card-body">
                         <div class="table-responsive">
                             <table class="table-balance table-border table-striped" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>Date & Time</th>
                                        <th>Login Status</th>
                                        <th>IP Address</th>
                                        <th>ISP</th>
                                        <th>City/Country</th>
                                        <th>Time Zone</th>
                                        {{-- <th>Actor</th> --}}
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($activitylogs as $item)
                                    <tr>
                                        <td>{{ $item->properties['login'] }}</td>
                                        <td>Login Success</td>
                                        <td>{{ $item->properties['ipaddress'] }}</td>
                                        <td>{{ $item->properties['isp'] }}</td>
                                        <td>{{ $item->properties['city'] }}/{{ $item->properties['country'] }}</td>
                                        <td>{{ $item->properties['time_zone'] }}</td>
                                       {{--  <td>
                                            @if ($item->causer)
                                                <a href="{{ url('/admin/users/' . $item->causer->id) }}">{{ $item->causer->name }}</a>
                                            @else
                                                -
                                            @endif
                                        </td> --}}
                                        <td>{{ $item->properties['login'] }}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
              </div>
            </div>
            
        </div>


</div>
@endsection
 