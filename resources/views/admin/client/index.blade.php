@extends('layouts.front')
@section('content')
<div class="container-fluid">
    <div class="row">
    	<div class="col-sm-3 col-xs-3 col-md-3 col-p">
    		@include('admin.client.layout.clientlayout')
    	</div>
    	<div class="col-sm-9 col-xs-9 col-md-9">
            <div class="row">
              <div class="col-xs-12 col-md-12 p-7">
                <div class="acc-det">
                <h2> Account Summary</h2>
              </div>
              </div>
                <div class="col-sm-7 col-xs-7 col-md-7 p-7">
                   <div class="card">
                        <div class="card-header">
                            <h3 class="float-left">About You </h3>
                            {{-- <div class="float-right">
                                <a href="JavaScript:void(0)" class="btn btn-info btn-sm" data-toggle="modal" data-target="#update_profile">Update Profile </a></div> --}}
                        </div>
                        <div class="card-body">
                            <table class="table table-hover pro-table">
                              <tr>
                                <td>Name</td>
                                <td> @if($data) {{$data ?$data->name:''}} @endif</td>
                              </tr>
                               <tr>
                                <td>User Name</td>
                                <td> @if($data) {{$data ?$data->username:''}} @endif</td>
                              </tr>
                               <tr>
                                <td>Email</td>
                                <td>   @if($data) {{$data ?$data->email:''}} @endif</td>
                              </tr>
                               <!-- <tr>
                                <td>Password</td>
                                <td> ******* <a href="javascript:void(0)" class="" data-toggle="modal" data-target="#change_password">
                                  <i class="fa fa-pencil"></i> </a></td>
                              </tr> -->
                            </table>

                        </div>  
                    </div> 
                </div>
                <div class="col-sm-5 col-xs-5 col-md-5 p-7">
                  
                    <div class="card">
                        <div class="card-header"><h3>Contact Detail</h3> </div>
                
                        <div class="card-body">
                          <table class="table pro-table">
                            <tr>
                              <td>Mobile</td>
                              <td>  @if($data) {{$data ?$data->mobile_no:''}} @endif</td>
                            </tr>
                          </table>
                        </div>  
                    </div>
                </div>
            </div>
            </div>

<div id="update_profile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Profile</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <form id="edit_profile" method="post">
            @csrf
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" name="name" value="{{Auth::User()->name}}" class="form-control" required="true"> <br>
                        <input type="text" name="username"  value="{{Auth::User()->username}}" class="form-control" required="true"> <br>
                        <input type="number" name="mobile_no" value="{{Auth::User()->mobile_no}}" placeholder="Mobile Number" class="form-control" required="true"><br>
                    </div>
                    <div class="col-md-12">
                        <input type="submit" value="Update Profile" class="btn btn-block btn-info">
                        <p id="res_update_profile"></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<div id="change_password" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Change Password</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <form id="reset_password" method="post">
            @csrf
            <input type="hidden" name="user_id" value="{{ $data->id }}">
                <div class="row">
                    <div class="col-md-12">
                        <input type="password" name="new_password" placeholder="New Password" class="form-control" required="true"> <br>
                        <input type="password" name="confirm_new_password" placeholder="Confirm New Password" class="form-control" required="true"><br>
                        <input type="password" name="my_password" placeholder="Your Password" class="form-control" required="true"> <br>
                    </div>
                    <div class="col-md-12">
                        <input type="submit" value="Submit" class="btn btn-block btn-info">
                        <p id="res_change_password"></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
@endsection
   @section('custom_script')
      <script>
         $(document).ready(function(){
            //Change Password
            // $('#edit_profile').on('submit', function(e){
            //    e.preventDefault();
            //    $.ajax({
            //      type: 'post',
            //      url: 'update-profile',
            //      headers: {
            //           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //       },
            //      data: $('#edit_profile').serialize(),
            //      success: function(res) {
            //        if (res.success) {
            //         $('#res_update_profile').html(res.message);
            //             setTimeout(function(){
            //                 window.location.reload(true);
            //         }, 2000);
            //       }
            //      }
            //    });
            // });


            $('#reset_password').on('submit', function(e){
               e.preventDefault();
               $.ajax({
                 type: 'post',
                 url: 'member-change-password',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: $('#reset_password').serialize(),
                 success: function(res) {
                   if (res.success) {
                    $('#res_change_password').html(res.message);
                        setTimeout(function(){
                            window.location.reload(true);
                      }, 2000);
                  }else{
                     $('#res_change_password').html(res.message);
                  }
                 }
               });
            });
         });
      </script>
   @endsection