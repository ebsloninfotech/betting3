@extends('layouts.front')
@section('content')
<div class="container-fluid">
    <div class="row">
    	<div class="col-sm-3 col-xs-3 col-md-3 col-p">
    		@include('admin.client.user.layout.user_admin_layout')
    	</div>
    	<div class="col-md-9 col-sm-9 col-xs-9">
        <div class="row">
        <div class="col-xs-12 col-md-12 p-7">    
            <div class="filter mt-10">
               <form action="" id="filterForm" method="get">
                    @csrf

                    <div class="form-group">
                        <label for="">Sports</label>
                        <br>
                        <input type="radio" name="sports" @if($bet_sports == 4 ) checked @endif value="4"> &nbsp; Cricket
                        <input type="radio" name="sports" @if($bet_sports == 1 ) checked @endif value="1"> &nbsp; Soccer
                        <input type="radio" name="sports" @if($bet_sports == 2 ) checked @endif value="2"> &nbsp; Tennis
                    </div>

                    <div class="form-group">
                        <label for="">Bet Status : </label>
                        <select name="bet_status" id="" class="select">
                            <option value="matched" @if($bet_status == 'matched') selected @endif>Matched</option>
                            <option value="settled" @if($bet_status == 'settled') selected @endif>Settled</option>
                            <option value="void" @if(Request::get('bet_status') == 'void') selected @endif>Void</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for=""> Period : </label>
                        <input type="date" name="bet_from" value="{{ $lastmonth }}">
                        <label for="">To</label>
                        <input type="date" name="bet_to" value="{{ $current_date }}">
                        <input type="submit" name="submit" class="btn btn-sm btn-site" value="Get History" >
                    </div>
                </form>
            </div>
        </div>
    </div>

           <div class="row">
          <div class="col-xs-12 col-md-12 p-7">
                 <div class="card">
                    <div class="card-header"> <h3>Bets</h3></div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table-balance table-borderless" id="dataTables">
                                    <thead>
                                        <tr>
                                            <th>Bet ID</th>
                                            <th>User Name</th>
                                            <th>Market</th>
                                            <th>Selection</th>
                                            <th>Type</th>
                                            <th>Odds</th>
                                            <th>Amount</th>
                                            <th>Bet Placed Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($bets as $value)
                                      <tr>
                                        <td>{{ $value->id }}</td>
                                        <td>{{ $value->user->name }}</td>
                                        <td> 
                                            @if($value->event_type == '4')
                                                <span>
                                                    <b>Cricket</b>
                                                    <i class="fa fa-angle-right"></i>
                                                </span>
                                            @endif
                                            @if($value->event_type == '1')
                                                <span>
                                                    <b>Soccer</b>
                                                    <i class="fa fa-angle-right"></i>
                                                </span>
                                            @endif
                                            @if($value->event_type == '2')
                                                <span>
                                                    <b>Tennis</b>
                                                    <i class="fa fa-angle-right"></i>
                                                </span>
                                            @endif
                                            {{ $value->event_name }} 
                                            @if($value->market_type == 'fancy')
                                                    <i class="fa fa-angle-right"></i>
                                                <b>Fancy</b>
                                            @endif
                                        </td>
                                        <td>{{ $value->title }}</td>
                                        <td>
                                            @if($value->market_type == 'fancy')
                                                @if($value->bet_type == 'lay')
                                                <span class="danger">No</span>
                                                @else
                                                <span class="success">Yes</span>
                                                @endif
                                            @else
                                            {{ $value->bet_type }}
                                            @endif
                                        </td>
                                        <td>
                                             @if($value->market_type == 'fancy')
                                                {{ $value->fancy_price }}/{{ $value->odds }}
                                             @else
                                                {{ $value->odds }}
                                            @endif
                                        </td>
                                        <td>{{ $value->bet_amount }}</td>
                                        <td>{{ $value->created_at }}</td>

                                        
                                      </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
      </div>
</div>
@endsection