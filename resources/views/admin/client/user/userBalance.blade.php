@extends('layouts.front')
@section('content')
<div class="container-fluid">
    <div class="row">
    	<div class="col-sm-3 col-xs-3 col-md-3 col-p">
    		@include('admin.client.user.layout.user_admin_layout')
    	</div>
    	<div class="col-md-9 col-sm-9 col-xs-9">
            <div class="row">
                <div class="col-xs-12 col-md-12 p-7">
                    <div class="acc-det">
                    <h2>Summary</h2>
                  </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 p-7">
                    <div class="card">
                        <div class="card-header"><h3>Balance Overview</h3></div>
                        <div class="card-body balance-card">
                            <div class="row">
                              <div class="col-md-4 col-sm-4 pr-0">
                                <div class="balance-wrap">
                                                          <h4>Your Balance</h4>
                            <h6> {{ $wallet->wallet_amount }}  <span>PTH</span></h6>
                                </div>
                              </div>
                              <div class="col-md-7 col-sm-7 p-0">
                                <div class="balance-wrap b-0">
                                  <h4>Welcome,</h4>
                            <p>View your account details here. You can manage funds, review and change your settings and see the performance of your betting activity.</p>
                                </div>
                              </div>
                            </div>

                        </div>
                    </div>

                  {{--   <table class="table-balance table-responsive">
                      <tbody>
                        <tr>
                        <th>Date</th>
                        <th>Transaction №</th>
                        <th>Debits</th>
                        <th>Credits</th>
                        <th>Balance</th>
                        <th>Remarks</th>
                        <th>From/To</th>
                        
                      </tr>
                      </tbody>
                    </table> --}}
                  </div>
            </div>
                    
        </div>

</div>
</div>
@endsection