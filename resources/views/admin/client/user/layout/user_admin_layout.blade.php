<div class="card bg-site">
            <div class="card-body">
                <ul class="nav sideNav flex-column" role="tablist">
                    <li class="nav-item first-head-tab dwn-li dwn-li-n">
                        <a href="{{ url('admin/myaccount/myprofile') }}" class="nav-link head-account-row">{{ Auth::user()->username }}</a>
                        <a href="{{ url('/admin/clientDownline/'.$client->id.'/') }}" class="nav-link head-account-row">{{ $client->username }}</a>
                        <a href="javascript:void(0)" class="nav-link head-account-row">{{ $data->username }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/admin/userMember/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-user"></i> Account Summary
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/admin/userBalance/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Balance Overview
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/admin/userBets/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Bets
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/admin/profitLose/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Profit &amp; Lose
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/admin/userTransactions/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Transaction History
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/admin/userLog/'.$client->id.'/'.$data->id.'')}}">
                           <i class="fa fa-book"></i> Login History
                        </a>
                    </li>
                </ul>
            </div>
        </div>