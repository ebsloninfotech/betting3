@extends('layouts.front')
@section('content')
<div class="container-fluid">
    <div class="row">
    	<div class="col-sm-3 col-xs-3 col-md-3 col-p">
    		@include('admin.client.user.layout.user_admin_layout')
    	</div>
    	<div class="col-md-9 col-sm-9 col-xs-9">
        <div class="row">
           <div class="col-xs-12 col-md-12 p-7">
                    <div class="acc-det">
                    <h2>Profit &amp; Loss</h2>
                  </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 p-7">   
                <div class="filter">
                    <form action="" id="filterForm" method="get">
                        @csrf
                        <div class="form-group">
                            <label for=""> Period : </label>
                            <input type="date" name="bet_from" value="{{ $lastmonth }}">
                            <label for="">To</label>
                            <input type="date" name="bet_to" value="{{ $current_date }}">
                            <input type="submit" name="submit" class="btn btn-sm btn-site" value="Get P &amp; L" >
                        </div>
                    </form>
                </div>
            </div>
        </div>

         <div class="row">
          <div class="col-md-12 p-7">
                 <div class="card">
                    <div class="card-header"><h3> Bets</h3></div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table-balance" id="dataTables">
                                    <thead>
                                        <tr>
                                            <th>Market</th>
                                            <th>Start Time</th>
                                            <th>Settled Date</th>
                                            <th>Profit/Loss</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($bets as $value)
                                      <tr class="proftR">
                                        <td> 
                                            @if($value->event_type == '4')
                                               
                                                    Cricket
                                                    <i class="fa fa-angle-right"></i>
                                            @endif
                                            @if($value->event_type == '1')
                                               
                                                    Soccer
                                                    <i class="fa fa-angle-right"></i>
                                            @endif
                                            @if($value->event_type == '2')
                                               
                                                    Tennis
                                                    <i class="fa fa-angle-right"></i>
                                            @endif
                                            <b>
                                                
                                            {{ $value->event_name }}
                                            </b>
                                            <i class="fa fa-angle-right"></i>
                                            @if($value->market_type == 'match_odd')
                                                Match Odds
                                            @else
                                               {{ $value->title }} 
                                            @endif    
                                        </td>
                                        <td>{{ $value->created_at }}</td>
                                        <td>{{ $value->updated_at }}</td>

                                        <td>
                                            @php
                                                $betProfitViaSelection = betProfitViaSelectionAdmin($value->selection_id,$data->id);
                                            @endphp

                                            <span class="@if($betProfitViaSelection['status'] == 'win') clsgreen @else clsred @endif">({{ $betProfitViaSelection['total'] }})</span>
                                             <a href="javascript:void(0)" class="trDesc" list={{ $value->id }}><i class="fa fa-plus"></i></a>
                                         </td>

                                        
                                      </tr>
                                      <tr class="detail{{ $value->id }} expand d-none">
                                            <td colspan="4">

                                                <table class="table-commission">
                                                    <tbody><tr>
                                                        <th width="9%">Bet ID
                                                        </th>
                                                        <th width="">Selection
                                                        </th>
                                                        <th width="9%">Odds
                                                        </th>
                                                        <th width="13%">Stake
                                                        </th>
                                                        <th width="8%">Type
                                                        </th>
                                                        <th width="16%">Placed
                                                        </th>
                                                        <th width="23%">Profit/Loss
                                                        </th>
                                                    </tr>
                                                    @php
                                                        $selectionmultipleBet = selectionmultipleBetAdmin($value->selection_id,$data->id);
                                                    @endphp

                                                    @if($selectionmultipleBet)

                                                    @php
                                                        $totalStake = 0;
                                                        $backSubTotal = 0;
                                                        $laySubTotal = 0;
                                                        $winning = 0;
                                                        $loseing = 0;
                                                    @endphp
                                                    @foreach($selectionmultipleBet as $s)

                                                        @if($s->market_type == 'fancy')
                                                            <tr id="txRow0" style="display: table-row;" class="even">
                                                                {{-- <td id="betID" style="text-align: left !important;">{{ str_pad($s->id, 5, '0', STR_PAD_LEFT) }}</td> --}}
                                                                <td id="betID" style="text-align: left !important;">{{ $s->id }}</td>
                                                                <td id="matchSelection">--</td>
                                                                <td id="txOddsMatched">{{ $s->fancy_price }}/{{ $s->odds }}</td>
                                                                <td id="txStake">{{ $s->bet_amount }}</td>
                                                                <td><span id="matchType" class="back">
                                                                    @if($s->bet_type == 'back')
                                                                        <span>Yes</span>
                                                                    @else
                                                                        <span class="text-danger">No</span>

                                                                    @endif
                                                                </span></td>
                                                                <td id="placed">{{ $s->created_at }}</td>
                                                                @if($s->profit_type == 'win')
                                                                    <td class="clsgreen">{{$s->profit }}</td>
                                                                @else
                                                                    <td class="clsred">({{$s->profit }})</td>
                                                                @endif
                                                            </tr>
                                                        @else

                                                        <tr id="txRow0" style="display: table-row;" class="even">
                                                            {{-- <td id="betID" style="text-align: left !important;">{{ str_pad($s->id, 5, '0', STR_PAD_LEFT) }}</td> --}}
                                                            <td id="betID" style="text-align: left !important;">{{ $s->id }}</td>
                                                            <td id="matchSelection">{{ $s->title }}</td>
                                                            <td id="txOddsMatched">{{ $s->odds }}</td>
                                                            <td id="txStake">{{ $s->bet_amount }}</td>
                                                            <td><span id="matchType" class="back">{{ $s->bet_type }}</span></td>
                                                            <td id="placed">{{ $s->created_at }}</td>
                                                            @if($s->profit_type == 'win')
                                                                @if($s->bet_type == 'back')
                                                                    <td id="txLiability">{{ $s->profit-$s->bet_amount }}</td>
                                                                @else
                                                                    <td id="txLiability">{{ $s->bet_amount - $s->profit }}</td>

                                                                @endif
                                                            @else
                                                                @if($s->bet_type == 'back')
                                                                    <td id="txLiability">{{ $s->bet_amount }}</td>
                                                                @else
                                                                    <td id="txLiability">{{ $s->bet_amount - (($s->odds*$s->bet_amount)-$s->bet_amount) }}</td>

                                                                @endif
                                                            @endif
                                                        </tr>
                                                        @endif
                                                        @php
                                                            if($s->bet_type == 'back'){
                                                                $backSubTotal = $backSubTotal + $s->bet_amount;
                                                            }else{

                                                                $laySubTotal = $laySubTotal + $s->bet_amount;
                                                            }

                                                            if($s->profit_type == 'win'){
                                                                $winning = $winning + $s->profit;
                                                            }else{
                                                                $loseing = $loseing + $s->profit;

                                                            }


                                                            $totalStake = $totalStake + $s->bet_amount;
                                                        @endphp


                                                    @endforeach
                                                    @endif
                                                    <tr class="sum-pl">
                                                        <td colspan="7">
                                                            <dl>
                                                                <dt>Total Stakes</dt>
                                                                <dd id="totalStakes">{{ $totalStake }}</dd>                                
                                                                
                                                                <dt id="backSubTotalTitle">Back subtotal</dt>
                                                                @if($backSubTotal > 0)
                                                                <dd id="backSubTotal">{{ $backSubTotal }}</dd>
                                                                @else
                                                                <dd id="backSubTotal">0.00</dd>
                                                                @endif
                                                                

                                                                <dt id="laySubTotalTitle">Lay subtotal</dt>
                                                                @if($laySubTotal > 0)
                                                                <dd id="laySubTotal" class="clsred">({{ $laySubTotal }})</dd>
                                                                @else
                                                                <dd id="laySubTotal">0.00</dd>
                                                                @endif
                                                                
                                                                <dt>Market subtotal</dt>
                                                                <dd id="marketTotal">{{  betProfitViaSelectionAdmin($value->selection_id,$data->id)['total'] }}</dd>
                                                                
                                                                {{-- <dt id="commissionTitle" style="display: block;">Commission</dt>
                                                                <dd id="commission" style="display: block;"><span class="red">(0.02)</span></dd> --}}
                                                                
                                                                <dt class="net_total">Net Market Total</dt>
                                                                <dd id="netTotal" class="net_total">{{  betProfitViaSelectionAdmin($value->selection_id,$data->id)['total'] }}</dd>
                                                            </dl>
                                                        </td>
                                                    </tr>
                                                </tbody></table>

                                            </td>
                                        </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            
        </div>
      </div>

</div>
</div>
@endsection
@section('scripts')

    <script>
        
        $('.trDesc').click(function() {
            var l = $(this).attr('list');
            if($('.detail'+l).hasClass('d-none')){

                $('.detail'+l).removeClass('d-none');
            }else{

                $('.detail'+l).addClass('d-none');
            }

        });

    </script>


@endsection