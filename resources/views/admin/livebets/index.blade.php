@extends('layouts.front')
@section('content')
<br>
<div class="container-fluid">
<div class="row">
	<div class="col-md-12">
 		<div class="card">
                    {{-- <div class="card-header">Downline - Users <button type="button" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#add_user"> <i class="fa fa-plus"></i> Add Member</button></div> --}}
                    <div class="card-body">
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/pages', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>Bet ID</th>
                                        
                                		<th>Name</th>
                                		<th>Market Name</th>
                                		<th>Runner Name</th>
                                		<th>Side</th>
                                		<th>Price</th>
                                		<th>Matched Size</th>
                                		<th>Remaining Size</th>
                                		<th>Created Date</th>
                                		<th>Updated Date</th>
                                		<th>IP Address</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<tr>
                                		<td>1</td>
                                		<td>fastbetter101</td>
                                		<td>Cricket</td>
                                		<td>Ajay Jadeja </td>
                                		<td>0</td>
                                		<td>0</td>
                                		<td>0</td>
                                		<td>0</td>
                                		<td>01-12-2018</td>
                                		<td>02-12-2018</td>
                                		<td>123.123.123.01</td>
                                		
                                		
                                	</tr>
                               {{--  @foreach($pages as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->title }}</td><td>{{ $item->content }}</td>
                                        <td>
                                            <a href="{{ url('/admin/pages/' . $item->id) }}" title="View Page"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/pages/' . $item->id . '/edit') }}" title="Edit Page"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/pages', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Page',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach --}}
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
	</div>

</div>
<br>
<hr style="height: 2px;background: lightblue">
<div class="row">
	<div class="col-md-12">
 		<div class="card">
                   {{--  <div class="card-header">Downline - Users <button type="button" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#add_user"> <i class="fa fa-plus"></i> Add Member</button></div> --}}
                    <div class="card-body">
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/pages', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>Bet ID</th>
                                        
                                		<th>Name</th>
                                		<th>Market Name</th>
                                		<th>Runner Name</th>
                                		<th>Side</th>
                                		<th>Price</th>
                                		<th>Matched Size</th>
                                		<th>Remaining Size</th>
                                		<th>Created Date</th>
                                		<th>Updated Date</th>
                                		<th>IP Address</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<tr>
                                		<td>1</td>
                                		<td>fastbetter101</td>
                                		<td>Cricket</td>
                                		<td>Ajay Jadeja </td>
                                		<td>0</td>
                                		<td>0</td>
                                		<td>0</td>
                                		<td>0</td>
                                		<td>01-12-2018</td>
                                		<td>02-12-2018</td>
                                		<td>123.123.123.01</td>
                                		
                                		
                                	</tr>
                               {{--  @foreach($pages as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->title }}</td><td>{{ $item->content }}</td>
                                        <td>
                                            <a href="{{ url('/admin/pages/' . $item->id) }}" title="View Page"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/pages/' . $item->id . '/edit') }}" title="Edit Page"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/pages', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Page',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach --}}
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
	</div>

</div>
</div>


@endsection