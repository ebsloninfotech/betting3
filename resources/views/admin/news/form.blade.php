<div class="form-group{{ $errors->has('news') ? ' has-error' : ''}}">
    {!! Form::label('news', 'News: (Required) ', ['class' => 'control-label', 'required'=>'required']) !!}
    <br>
    {!! Form::text('news', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('news', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>