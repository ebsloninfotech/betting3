@extends('layouts.front')

@section('content')
    <div class="page-title">
        <h3 class="breadcrumb-header">Slider </h3>
    </div>
        <div class="card">
            <div class="card-body">

                <a href="{{ url('/admin/slider') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <a href="{{ url('/admin/slider/' . $slider->id . '/edit') }}" title="Edit slider"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                {!! Form::open([
                    'method' => 'DELETE',
                    'url' => ['/admin/slider', $slider->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Delete slider',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                {!! Form::close() !!}
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table">
                            <tr>
                                <th>Title :</th>
                                <td>{{ $slider->title }}</td>
                            </tr>
                            <tr>
                                <th>Tagline :</th>
                               <td> {{ $slider->tagline }} </td>
                            </tr>
                            <tr>
                                <th>Redirect link :</th>
                               <td> {{ $slider->rlink }} </td>
                            </tr>
                            <tr>
                                <th>Image :</th>
                               <td> 
                                 @if(!empty($slider->slider_img))
                                     <img src="{{ asset('storage/images/slider/'.$slider->slider_img.'') }}" alt="{{ $slider->slider_img }}" height="100" width="100">
                                    @else
                                    No Image
                                    @endif
                               </td>
                            </tr>
                            <tr>
                                <th>Created At :</th>
                               <td> {{ $slider->created_at->diffForHumans() }} </td>
                            </tr>
                    </table>
                </div>

            </div>
        </div>
@endsection
