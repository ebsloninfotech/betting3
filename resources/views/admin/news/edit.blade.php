@extends('layouts.front')
@section('content')
<br>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            @include('sidebars.myaccount_sidebar')
        </div>
        <div class="col-md-9">
            <div class="page-title">
                <h3 class="breadcrumb-header">Update news &nbsp;
                </h3>
            </div>
            <div class="card">
                <div class="card-body">
                    <a href="{{ url('/master/news') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <br />
                    <br />

                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif

                    {!! Form::model($news, [
                        'method' => 'PATCH',
                        'url' => ['/master/news', $news->id],
                        'class' => 'form-horizontal col-md-7',
                        'enctype'=>'multipart/form-data'
                    ]) !!}

                    @include ('admin.news.form', ['formMode' => 'edit'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
