@extends('layouts.front')

@section('content')

<br>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            @include('sidebars.myaccount_sidebar')
        </div>
        <div class="col-md-9">
            <div class="page-title">
                <h3 class="breadcrumb-header">news &nbsp; 
                    <a href="{{ url('/master/news/create') }}" class="btn btn-primary btn-sm btn-dark" title="Add New">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                </h3>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-border table-striped" id="dataTables">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>News</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($news)>0)
                                    @foreach($news as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->news }}</td>
                                            <td>
                                                <div class="togglebutton">
                                                    <label>
                                                      <input type="checkbox" class="check_box newsStatus" id="{{ $item->id }}" @if($item->status) {{ 'checked' }} @endif >
                                                      <span class="toggle"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ url('/master/news/' . $item->id . '/edit') }}" title="Edit "><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                                {!! Form::open([
                                                    'method' => 'DELETE',
                                                    'url' => ['/master/news', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Remove', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger btn-sm',
                                                            'title' => 'Delete news',
                                                            'onclick'=>'return confirm("Confirm Delete?")'
                                                    )) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
   @section('custom_script')
      <script>
         $(document).ready(function(){
            //Change Password
            $('.newsStatus').on('click', function(){
               var id = $(this).attr('id');
               $.ajax({
                 type: 'post',
                 url: '/master/news-status',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: {id:id},
                 success: function(res) {
                 }
               });
            });
         });
      </script>
   @endsection
@endsection
