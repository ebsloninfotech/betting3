<div class="card bg-site">
            <div class="card-body">
                <ul class="nav sideNav flex-column" role="tablist">
                    <li class="nav-item first-head-tab dwn-li">
                        <a href="{{ url('master/myaccount/myprofile') }}" class="nav-link head-account-row">{{ Auth::user()->username }}</a>
                        <a href="{{ url('master/memberSummary/'.$admin->id.'/') }}" class="nav-link head-account-row">{{ $admin->username }}</a>
                        <a href="javascript:void(0)" class="nav-link head-account-row">{{ $data->username }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/clientMember/'.$admin->id.'/'.$data->id.'')}}">
                            <i class="fa fa-user"></i> Account Summary
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/clientDownline/'.$admin->id.'/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Downline
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/clientTransactions/'.$admin->id.'/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Transaction History
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/clientLog/'.$admin->id.'/'.$data->id.'')}}">
                           <i class="fa fa-book"></i> Login History
                        </a>
                    </li>
                </ul>
            </div>
        </div>