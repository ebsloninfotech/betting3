<div class="card bg-site">
            <div class="card-body">
                <ul class="nav sideNav flex-column" role="tablist">
                    <li class="nav-item first-head-tab dwn-li dwn-li-n">
                        <a href="{{ url('master/myaccount/myprofile') }}" class="nav-link head-account-row">{{ Auth::user()->username }}</a>
                        <a href="{{ url('master/memberSummary/'.$admin->id.'/') }}" class="nav-link head-account-row">{{ $admin->username }}</a>
                        <a href="{{ url('/master/clientDownline/'.$admin->id.'/'.$client->id.'/') }}" class="nav-link head-account-row">{{ $client->username }}</a>
                        <a href="javascript:void(0)" class="nav-link head-account-row">{{ $data->username }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/userMember/'.$admin->id.'/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-user"></i> Account Summary
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/userBalance/'.$admin->id.'/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Balance Overview
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/userBets/'.$admin->id.'/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Bets
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/profitLose/'.$admin->id.'/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Profit &amp; Lose
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/userTransactions/'.$admin->id.'/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Transaction History
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/userLog/'.$admin->id.'/'.$client->id.'/'.$data->id.'')}}">
                           <i class="fa fa-book"></i> Login History
                        </a>
                    </li>
                </ul>
            </div>
        </div>