@extends('layouts.front')
@section('content')
<div class="container-fluid">
    <div class="row">
      <!-- <div id="crumbs" class="container-fluid">
        <ul>
          <li><a href="{{ url('master/myaccount/myprofile') }}">{{ Auth::user()->username }}</a></li>
          <li><a href="javascript:void(0)"> {{ $data->username }}</a></li>
          
        </ul>
      </div> -->
    	<div class="col-sm-2 col-xs-2 col-md-2 col-p">
    		@include('admin.member.admin.layout.layout')
    	</div>
    	<div class="col-sm-10 col-xs-10 col-md-10 p-7">
         <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="acc-det">
                <h2>Downline</h2>
              </div>
              </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
               <div class="card">
                    <!-- <div class="card-header"><h3>Downline</h3></div> -->
                    <div class="card-body">
                        <div class="table-responsive">
                             <table class="table-balance table-border table-striped" id="dataTables">
                  <thead>
                     <tr>
                        <th>User Name</th>
                        {{-- <th>Email</th> --}}
                        @if(Auth::user()->hasRole('client') || Auth::user()->hasRole('customer'))
                        <th>Balance</th>
                        @endif
                        {{-- <th>Mobile No.</th> --}}
                        <th>Status</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>

                     @foreach($downline as $u)

                     <tr>
                        <td>{{$u->user->username ? $u->user->username : '--'}}</td>
                        {{-- <td>{{$u->email}}</td> --}}
                        @if(Auth::user()->hasRole('client') || Auth::user()->hasRole('customer'))
                        <td>{{ $u->user->walletA ? $u->user->walletA->wallet_amount : '0'}}</td>
                        @endif
                        {{-- <td>{{$u->user->mobile_no ? $u->user->mobile_no : '--'}}</td> --}}
                        <td>
                           @if(!empty($u->user->status==1))
                           <button type="button" class="btn  btn-sm chaneg_status" user_id="{{$u->user->id}}" status="Active" data-toggle="modal" title="Client Status" data-target="#myactive"> <i class="fa fa-edit"></i> Active</button>
                           @elseif(!empty($u->user->status==2))
                           <button type="button" class="btn  btn-sm chaneg_status" user_id="{{$u->user->id}}" status="Suspend" title="Client Status" data-toggle="modal"  data-target="#myactive"> <i class="fa fa-edit"></i> Suspend</button>
                           @elseif(!empty($u->user->status==3))
                           <button type="button" class="btn  btn-sm chaneg_status" user_id="{{$u->user->id}}" status="Locked" data-toggle="modal" title="Client Status" data-target="#myactive"><i class="fa fa-edit"></i> Locked</button>
                           @endif
                          
                             {{--  @if(Auth::user()->hasRole('client'))
                                 <button type="button" class="bit_config btn btn-success btn-sm" title="Bet Config" receiver_id="{{$u->user->id}}"> <i class="fa fa-credit-card"></i> Bet Config</button>
                              @endif --}}
                           </td>
                        <td>
                            @if($u->user->email=='master@admin.com') -- 
                            @else 
                           <button type="button" class="edit_admin_modal btn btn-sm btn-warning" user_fullname="{{$u->user->name}}" user_email="{{$u->user->email}}" user_id="{{$u->user->id}}" user_username="{{$u->user->username}}" user_mobile_no="{{$u->user->mobile_no}}" ><i class="fa fa-edit"></i></button> 
                           {{-- {!! Form::open([
                           'method' => 'DELETE',
                           'url' => ['/admin/delete_admin', $u->user->id],
                           'style' => 'display:inline'
                           ]) !!}
                           {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                           'type' => 'submit',
                           'class' => 'btn btn-danger btn-sm',
                           'title' => 'Delete Page',
                           'onclick'=>'return confirm("Confirm delete?")'
                           )) !!}
                           {!! Form::close() !!} --}}
                           @endif
                           @if(Auth::user()->hasRole('master'))
                           @else
                           <button type="button" class="credit_button btn btn-success btn-sm" data-toggle="modal" data-target="#mycredit" title="transfor Coins" receiver_id="{{$u->user->id}}"> <i class="fa fa-credit-card"></i> Credit</button>
                           @endif

                           <a href="{{ url('/'.Auth::user()->roles[0]->name.'/clientMember/'.$data->id.'/'.$u->user->id.'') }}" class="btn btn-info btn-sm"> <i class="fa fa-user"></i></a>

                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
                        </div>

                    </div>
                </div>
            </div>
         </div>
            
        </div>


</div>


<!--Start Add User-->
<!-- <div id="add_user" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Create New User</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            @if(Auth::user()->hasRole('master'))
               <form action="{{url('master/add_admin')}}" method="post">
            @elseif(Auth::user()->hasRole('admin'))
               <form action="{{url('admin/add_client')}}" method="post">
            @elseif(Auth::user()->hasRole('client'))
               <form action="{{url('client/add_customer')}}" method="post">
            @endif
               @csrf
               <div class="row">
                  <div class="col-md-6">
                     <label for="full_name">Enter Full Name:<span style="color:red" class="small-text">*required</span></label><br>
                     <input type="text" class="form-control"  name="full_name">
                     <label for="password">Enter Password: <span style="color:red" class="small-text">*required </span></label><br>
                     <input type="password" class="form-control" name="password">
                     {{-- <label for="new_credit_reference">Please Enter New Credit Reference :</label><br>
                     <input type="text" class="form-control" name="new_credit_reference"> --}}
                     <label for="new_credit_reference">Please Enter Mobile No.:<span style="color:red" class="small-text">*required | unique</span></label><br>
                     <input type="number" class="form-control" name="mobile_no">
                     {{-- <label for="new_credit_reference">Please Enter PT:</label><br>
                     <input type="number" class="form-control" name="pt"> --}}
                  </div>
                  <div class="col-md-6">
                     <label for="username">Enter Username: <span style="color:red" class="small-text">*required | unique</span></label><br>
                     <input type="text" class="form-control"  name="username">
                     <label for="confirm_password">Enter Confirm Password: <span style="color:red" class="small-text">*required </span></label><br>
                     <input type="password" class="form-control" name="confirm_password">
                     {{-- <label for="casino_credit_reference">Please Enter Casino Credit:</label><br>
                     <input type="text" class="form-control" name="casino_credit_reference">
                     --}}
                     <label for="casino_credit_reference">Please Enter Email: <span style="color:red" class="small-text">*required | unique</span></label> 
                     <br>
                     <input type="email" class="form-control" name="email">
                  </div>
               </div>
               <br>
               <div class="row">
                  <div class="col-md-12">
                     <input type="submit" name="submit" value="Submit" class="btn btn-default btn-block btn-info">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div> -->

<!--End Add User-->
<!--Start Activate User-->
<div id="myactive" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Change Member Status</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body" style="padding: 15px 15px;">
            <form id="change_status" method="post">
               <div class="row">
                  <div class="col-md-4">
                     <label for="">Current Status <i class="fa fa-arrow-down"></i></label>
                     <label for="" style="color:green"><b>Active</b></label> <br>
                     <label for="" style="color:red"><b>Suspended</b></label> <br>
                     <label for="" style="color:grey"><b>Locked</b></label> <br>
                  </div>
                  <div class="col-md-8" style="padding: 5px 5px">
                     <div class="row">
                        <div class="col-md-12">
                           <br>
                           <input type="hidden" name="user_id" id="user_id">
                        </div>
                        <div class="col-md-12">
                           <label><input type="radio"  name="status" value="1" id="Active">Activate</label><br>
                        </div>
                        <div class="col-md-12">
                           <label><input type="radio" name="status" value="2" id="Suspend">Suspend</label><br>
                        </div>
                        <div class="col-md-12">
                           <label><input type="radio" name="status" value="3" id="Locked">Locked</label>    
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <label for="">Enter Password:</label>
                           <input type="password" name="password" class="form-control" autocomplete="off" required="true" >
                        </div>
                     </div>
                     <br>
                     <input type="submit" name="submit" class="btn btn-default btn-block btn-info">
                     <p id="res_change_status"></p>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!--End Activate User-->
<!--Start Activate User-->
<div id="mycredit" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Chips</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <p id="res_credit_apply"></p>
            <form id="credit_apply" method="post">
               @csrf
               <input type="hidden" name="sender_id" @if(Auth::check()) value="{{Auth::id()}}" @endif>
               <input type="hidden" name="receiver_id" class="receiver_id" >
               <input type="hidden" name="type" value="deposit" class="withraw_or_deposit">
               <div class="togglebutton">
                  <span>I Want To Withdraw</span>
                  <label>
                    <input type="checkbox" class="withdrawStatus" id="withdrawStatus" >
                    <span class="toggle"></span>
                  </label>
              </div>
            <div class="row">
               <div class="col-md-12">
                 <label for="deposit_withraw_cost">Enter Cost To <span class="change_deposit_label"> Deposit </span></label> <input type="number" name="points" class="form-control">  
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <label for="remarks">Please Enter Remarks</label>
                  <input type="text" name="remarks" class="form-control">
               </div>
               <div class="col-md-12">
                  <label for="remarks">Please Enter Password</label>
                  <input type="password" name="password" class="form-control">
               </div>
            </div>

            <div class="row padding_top">
               <div class="col-md-12">
                  <input type="submit" value="Submit" class="btn btn-defaul btn-sm btn-success">
                  
               </div>
            </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!--End Credit User-->
<!--Start Bit Config-->
<div id="res_update_bit_config">
   
</div>
<!--End Credit User-->
<!--Start Credit User-->
<div id="editMODAL_user" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Edit ADMIN </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            {{-- <form  method="post" action="{{url('/'.Auth::user()->role->name.'/edit_admin')}}"> --}}
                 @if(Auth::user()->hasRole('master'))
                     <form action="{{url('master/edit_admin')}}" method="post">
                  @elseif(Auth::user()->hasRole('admin'))
                     <form action="{{url('admin/edit_client')}}" method="post">
                  @elseif(Auth::user()->hasRole('client'))
                     <form action="{{url('client/edit_customer')}}" method="post">
                  @endif
               @csrf
               <input type="hidden" name="user_id" id="modal_user_id">
               <div class="row">
                  <div class="col-md-6">
                     <label for="full_name">Enter Full Name:<span style="color:red" class="small-text">*required</span></label><br>
                     <input type="text" class="form-control" id="user_full_name" name="full_name">
                     <label for="password">Enter Password: </label><br>
                     <input type="password" class="form-control" name="password">
                     {{-- <label for="new_credit_reference">Please Enter New Credit Reference :</label><br>
                     <input type="text" class="form-control" name="new_credit_reference"> --}}
                     <label for="new_credit_reference">Please Enter Mobile No.:<span style="color:red" class="small-text">*required | unique</span></label><br>
                     <input type="number" id="user_mobile_no"  class="form-control" name="mobile_no">
                     {{-- <label for="new_credit_reference">Please Enter PT:</label><br>
                     <input type="number" class="form-control" name="pt"> --}}
                  </div>
                  <div class="col-md-6">
                     <label for="username">Enter Username: <span style="color:red" class="small-text">*required | unique</span></label><br>
                     <input type="text" class="form-control" id="user_username" name="username">
                     <label for="confirm_password">Enter Confirm Password: </label><br>
                     <input type="password" class="form-control" name="confirm_password">
                     <label for="casino_credit_reference">Please Enter Email: <span style="color:red" class="small-text">*required | unique</span></label> 
                     <br>
                     <input type="email" id="user_email"  class="form-control" name="email">
                  </div>
               </div>
               <br>
               <div class="row">
                  <div class="col-md-12">
                     <input type="submit" class="btn btn-default btn-block btn-info">
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<!--End Bit Config-->
   @section('custom_script')
      <script>
         $(document).ready(function(){
            $('.chaneg_status').on('click',function(){
               var status = $(this).attr('status');
               var user_id = $(this).attr('user_id');
               $( "#user_id").val(user_id);
               $( "#"+status).prop( "checked", true );
            });

            $('.credit_button').on('click',function(){
               var user_id = $(this).attr('receiver_id');
                  $('.receiver_id').val(user_id);
            });

            //Change Status
            $('#credit_apply').on('submit', function(e){
               e.preventDefault();
               $.ajax({
                 type: 'post',
                 url: '/credit-apply',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: $('#credit_apply').serialize(),
                 success: function(res) {
                   if (res.success) {
                    $('#res_credit_apply').html(res.message);
                        setTimeout(function(){
                            window.location.reload(true);
                      }, 2000);
                  }else{
                     $('#res_credit_apply').html(res.message);
                  }
                 }
               });
            });

            //Change Status
            $('.bit_config').on('click', function(){
               var receiver_id = $(this).attr('receiver_id');
               $.ajax({
                 type: 'post',
                 url: 'update-bit-config',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: {receiver_id:receiver_id},
                 success: function(res) {
                   if (res.success) {
                    $('#res_update_bit_config').html(res.message);
                  }
                 }
               });
            });

            //Change Status
            $('#change_status').on('submit', function(e){
               e.preventDefault();
               $.ajax({
                 type: 'post',
                 url: 'change-status',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: $('#change_status').serialize(),
                 success: function(res) {
                   if (res.success) {
                    $('#res_change_status').html(res.message);
                        setTimeout(function(){
                            window.location.reload(true);
                      }, 2000);
                  }else{
                     $('#res_change_status').html(res.message);
                  }
                 }
               });
            });

            //Change Status
            $('#add_bet_config').on('submit', function(e){
               e.preventDefault();
               $.ajax({
                 type: 'post',
                 url: 'add-bet-config',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: $('#add_bet_config').serialize(),
                 success: function(res) {
                   if (res.success) {
                    $('#res_add-bet-config').html(res.message);
                        setTimeout(function(){
                            window.location.reload(true);
                      }, 2000);
                  }else{
                     $('#res_add-bet-config').html(res.message);
                  }
                 }
               });
            });
            //Withdrwal Status
            $("#withdrawStatus").click(function() { 
               var chk = $(this).prop("checked");
                  if(chk) 
                  { 
                    $('.change_deposit_label').text('Withdraw');
                    $('.withraw_or_deposit').val('withdraw');
                  }
                  else 
                  { 
                   $('.change_deposit_label').text('Deposit');
                   $('.withraw_or_deposit').val('deposit');
                  } 
            }); 
         });
      </script>
   @endsection
@endsection
 