@extends('layouts.front')
@section('content')
<div class="container-fluid">
    <div class="row">
      <div id="crumbs" class="container-fluid">
        <ul>
          <li><a href="javascript:void(0)">{{ Auth::user()->username }}</a></li>
          <li><a href="javascript:void(0)"> {{ $data->username }}</a></li>
          
        </ul>
      </div>
    	<div class="col-md-3">
    		<div class="card bg-site border-0">
            <div class="card-body">
                <ul class="nav sideNav flex-column" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/memberSummary/'.$data->id.'')}}">
                            <i class="fa fa-user"></i> Account Summary
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/memberBets/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Bets
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/memberTransactions/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Transaction History
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/memberLog/'.$data->id.'')}}">
                           <i class="fa fa-book"></i> Login History
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    	</div>
    	<div class="col-md-9">
            <div class="card">
                    <div class="card-header">Account Statement</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-border table-striped" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>Date/Time</th>
                                        <th>Deposit</th>
                                        <th>Withdraw</th>
                                        <th>Balance</th>
                                        <th>Remarks</th>
                                        <th>From/to</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($data2 as $value)
                                  <tr>
                                    <td>{{ $value->created_at }}</td>
                                    <td>@if($value->type == 'deposit') {{ $value->points }} @else -- @endif</td>
                                    <td>@if($value->type == 'withdraw') {{ $value->points }} @else -- @endif</td>
                                    <td>{{ $value->balance }}</td>
                                    <td>{{ $value->remarks }}</td>
                                    <td>{{ userDetail($value->sender_id)->name  }}</td>
                                  </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
        </div>


</div>
@endsection
 