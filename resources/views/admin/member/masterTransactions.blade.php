@extends('layouts.front')
@section('content')
<div class="container-fluid">
    <div class="row">
      <!-- <div id="crumbs" class="container-fluid">
        <ul>
          <li><a href="javascript:void(0)">{{ Auth::user()->username }}</a></li>
          <li><a href="javascript:void(0)"> {{ $data->username }}</a></li>
          
        </ul>
      </div> -->
    	<div class="col-sm-2 col-xs-2 col-md-2 col-p">
            @include('admin.member.admin.layout.layout')
    	</div>
    	<div class="col-sm-10 col-xs-10 col-md-10 p-7">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                <div class="acc-det">
                <h2>Transaction History</h2>
              </div>
              </div>
              <div class="col-xs-12 col-md-12">
                <div class="card">
                    <div class="card-header"><h3>Account Statement</h3></div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table-balance table-border table-striped" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>Date/Time</th>
                                        <th>Deposit</th>
                                        <th>Withdraw</th>
                                        <th>Balance</th>
                                        <th>Remarks</th>
                                        <th>From/to</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($data2 as $value)
                                  <tr>
                                    <td>{{ $value->created_at }}</td>
                                    <td>@if($value->type == 'deposit') {{ $value->points }} @else -- @endif</td>
                                    <td>@if($value->type == 'withdraw') {{ $value->points }} @else -- @endif</td>
                                    <td>{{ $value->balance }}</td>
                                    <td>{{ $value->remarks }}</td>
                                    <td>{{ userDetail($value->sender_id)->name  }}</td>
                                  </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
              </div>
            </div>
            
        </div>


</div>
@endsection
 