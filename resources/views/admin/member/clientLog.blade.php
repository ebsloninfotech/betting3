@extends('layouts.front')
@section('content')
<br>
<div class="container-fluid">
    <div class="row">
      <div id="crumbs" class="container-fluid">
        <ul>
          <li><a href="javascript:void(0)">{{ Auth::user()->username }}</a></li>
          <li><a href="javascript:void(0)"> {{ $data->username }}</a></li>
          
        </ul>
      </div>
    	<div class="col-md-3">
    		<div class="card bg-site border-0">
            <div class="card-body">
                <ul class="nav sideNav flex-column" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/memberSummary/'.$data->id.'')}}">
                            <i class="fa fa-user"></i> Account Summary
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/memberDwonline/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Downline
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/memberTransactions/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Transaction History
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/master/memberLog/'.$data->id.'')}}">
                           <i class="fa fa-book"></i> Login History
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    	</div>
    	<div class="col-md-9">
            <div class="row">
                <div class="col-md-8">
                   <div class="card">
                        <div class="card-header">
                            <div class="float-left">About You </div>
                            {{-- <div class="float-right">
                                <a href="JavaScript:void(0)" class="btn btn-info btn-sm" data-toggle="modal" data-target="#update_profile">Update Profile </a></div> --}}
                        </div>
                        <div class="card-body">
                            <table class="table table-hover">
                              <tr>
                                <td>Name</td>
                                <td>- @if($data) {{$data ?$data->name:''}} @endif</td>
                              </tr>
                               <tr>
                                <td>User Name</td>
                                <td>- @if($data) {{$data ?$data->username:''}} @endif</td>
                              </tr>
                               <tr>
                                <td>Email</td>
                                <td> -  @if($data) {{$data ?$data->email:''}} @endif</td>
                              </tr>
                               <tr>
                                <td>Password</td>
                                <td>- ******* <a href="javascript:void(0)" class="" data-toggle="modal" data-target="#change_password" style="    border: 1px solid;    font-size: 12px;    color: black;    padding: 0px 8px;">
                                  <i class="fa fa-pencil"></i> Edit </a></td>
                              </tr>
                            </table>

                        </div>  
                    </div> 
                </div>
                <div class="col-md-4">
                  
                    <div class="card">
                        <div class="card-header">Contact Detail </div>
                
                        <div class="card-body">
                          <table class="table">
                            <tr>
                              <td>Mobile</td>
                              <td> -  @if($data) {{$data ?$data->mobile_no:''}} @endif</td>
                            </tr>
                          </table>
                        </div>  
                    </div>
                </div>
            </div>
            </div>

<div id="update_profile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Update Profile</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <form id="edit_profile" method="post">
            @csrf
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" name="name" value="{{Auth::User()->name}}" class="form-control" required="true"> <br>
                        <input type="text" name="username"  value="{{Auth::User()->username}}" class="form-control" required="true"> <br>
                        <input type="number" name="mobile_no" value="{{Auth::User()->mobile_no}}" placeholder="Mobile Number" class="form-control" required="true"><br>
                    </div>
                    <div class="col-md-12">
                        <input type="submit" value="Update Profile" class="btn btn-block btn-info">
                        <p id="res_update_profile"></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<div id="change_password" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Change Password</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <form id="reset_password" method="post">
            @csrf
            <input type="hidden" name="user_id" value="{{ $data->id }}">
                <div class="row">
                    <div class="col-md-12">
                        <input type="password" name="new_password" placeholder="New Password" class="form-control" required="true"> <br>
                        <input type="password" name="confirm_new_password" placeholder="Confirm New Password" class="form-control" required="true"><br>
                        <input type="password" name="my_password" placeholder="Your Password" class="form-control" required="true"> <br>
                    </div>
                    <div class="col-md-12">
                        <input type="submit" value="Submit" class="btn btn-block btn-info">
                        <p id="res_change_password"></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
@endsection
   @section('custom_script')
      <script>
         $(document).ready(function(){
            //Change Password
            // $('#edit_profile').on('submit', function(e){
            //    e.preventDefault();
            //    $.ajax({
            //      type: 'post',
            //      url: 'update-profile',
            //      headers: {
            //           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //       },
            //      data: $('#edit_profile').serialize(),
            //      success: function(res) {
            //        if (res.success) {
            //         $('#res_update_profile').html(res.message);
            //             setTimeout(function(){
            //                 window.location.reload(true);
            //         }, 2000);
            //       }
            //      }
            //    });
            // });


            $('#reset_password').on('submit', function(e){
               e.preventDefault();
               $.ajax({
                 type: 'post',
                 url: 'member-change-password',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: $('#reset_password').serialize(),
                 success: function(res) {
                   if (res.success) {
                    $('#res_change_password').html(res.message);
                        setTimeout(function(){
                            window.location.reload(true);
                      }, 2000);
                  }else{
                     $('#res_change_password').html(res.message);
                  }
                 }
               });
            });
         });
      </script>
   @endsection