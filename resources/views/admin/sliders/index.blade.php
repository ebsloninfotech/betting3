@extends('layouts.front')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2 col-xs-2 col-md-2 col-p">
            @include('sidebars.myaccount_sidebar')
        </div>
        <div class="col-sm-10 col-xs-10 col-md-10">
            <div class="page-title">
                <h3 class="breadcrumb-header">Sliders &nbsp; 
                    <a href="{{ url('/master/slider/create') }}" class="btn btn-primary btn-sm btn-dark" title="Add New">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                </h3>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-border table-striped" id="dataTables">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($slider)>0)
                                    @foreach($slider as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->title }}</td>
                                            <td>
                                                <img src="{{asset('images/slider/'.$item->slider_img.'')}}" style="width:100px;height: 50px" alt="">
                                            </td>
                                            <td>
                                                <div class="togglebutton">
                                                    <label>
                                                      <input type="checkbox" class="check_box sliderStatus" id="{{ $item->id }}" @if($item->status) {{ 'checked' }} @endif >
                                                      <span class="toggle"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ url('/master/slider/' . $item->id . '/edit') }}" title="Edit "><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                                {!! Form::open([
                                                    'method' => 'DELETE',
                                                    'url' => ['/master/slider', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Remove', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger btn-sm',
                                                            'title' => 'Delete Slider',
                                                            'onclick'=>'return confirm("Confirm Delete?")'
                                                    )) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
   @section('custom_script')
      <script>
         $(document).ready(function(){
            //Change Password
            $('.sliderStatus').on('click', function(){
               var id = $(this).attr('id');
               $.ajax({
                 type: 'post',
                 url: '/master/slider-status',
                 headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                 data: {id:id},
                 success: function(res) {
                   if (res.success) {
                        setTimeout(function(){
                            window.location.reload(true);
                    }, 2000);
                  }
                 }
               });
            });
         });
      </script>
   @endsection
@endsection
