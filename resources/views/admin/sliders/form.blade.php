<div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
    {!! Form::label('title', 'Title: (Required) ', ['class' => 'control-label', 'required'=>'required']) !!}
    <br>
    {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>

{{-- <div class="form-group{{ $errors->has('tagline') ? ' has-error' : ''}}">
    {!! Form::label('tagline', 'Tagline: (Required)', ['class' => 'bmd-label-floating']) !!}
    {!! Form::text('tagline', null, ['class' => 'form-control', ]) !!}
    {!! $errors->first('tagline', '<p class="help-block">:message</p>') !!}
</div> --}}


<div class="form-group{{ $errors->has('rlink') ? ' has-error' : ''}}">
    {!! Form::label('rlink', 'Redirect Link: (Required)', ['class' => 'bmd-label-floating']) !!}
    {!! Form::text('rlink', null, ['class' => 'form-control']) !!}
    {!! $errors->first('rlink', '<p class="help-block">:message</p>') !!}
</div>


<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="upload">Slider Image</label>
      <input type="file" name="slider_img" class="form-control" id="add_slider_image">
      {!! $errors->first('slider_img', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
  <div class="col-md-6">
        @if($formMode === 'edit')
            <img src="{{ asset('storage/images/slider/'.$slider->slider_img)}}" id="slider_image" alt="Slider Image" width="100%">
        @else
            <img src="{{ asset('front/img/slider.jpg')}}" id="slider_image" class=" d-none" alt="Slider Image" width="100%">
        @endif
  </div>
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>


@section('scripts')

<script>
   function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#slider_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);

                $('#slider_image').removeClass('d-none');
            }
        }
        $("#add_slider_image").change(function(){
            readURL(this);
        });
</script>

@endsection