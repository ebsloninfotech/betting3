@extends('layouts.front')


@section('content')

    <div class="container-fluid">
        <div class="row">
			<div class="col-md-2 p-0">
			        <div class="left-section">
				        <ul class="ico_dots">
				            <li class="ico_sport   ">
				                <a href="#"> <i class="fa fa-ellipsis-v" aria-hidden="true"></i> </a>
				                <a href="{{ url('/') }}" >Sports</a>
				            </li>
				        </ul>
				    </div>
				    <div class="click_on_text leftSideBarContent">
				        <ul class="sportsList">
				        	@if(isset($totalMarket))
								@foreach($totalMarket as $value)	
								<li class="ico icon_leftt">
									<a href="javascript:void(0)" class="sportInnerClick" name="{{ $value->name }}" type_id="{{ $value->event_type }}" competition_id="{{ $value->competition_id }}" event_id="{{ $value->event_id?$value->event_id:'' }}" market_id="">{{ $value->name }}<i class="fa fa-arrow-right float-right"></i></a>
								</li>
								@endforeach
							@else
								<li class="ico icon_leftt">
									<a href="javascript:void(0)" class="sportInnerClick" name="Cricket" type_id="4" competition_id="" event_id="" market_id="" >Cricket <i class="fa fa-arrow-right float-right"></i></a>
								</li>
								<li class="ico icon_leftt">
									<a href="javascript:void(0)" class="sportInnerClick" name="Soccer" type_id="1" competition_id="" event_id="" market_id="" >Soccer <i class="fa fa-arrow-right float-right"></i></a>
								</li>
								<li class="ico icon_leftt">
									<a href="javascript:void(0)" class="sportInnerClick" name="Tennis" type_id="2" competition_id="" event_id="" market_id="" >Tennis <i class="fa fa-arrow-right float-right"></i></a> 
								</li>
							@endif
				        </ul>
				    </div>
			</div>

			<div class="col-md-7 all_margin_section">
			    <div class="col_center middleSec">
				    <div class="row">
				        <div class="col-sm-12">
				            <div id="myCarousel" class="carousel slide" data-ride="carousel">
				                <!-- Indicators -->
				                <ol class="carousel-indicators">
				                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				                    <li data-target="#myCarousel" data-slide-to="1"></li>
				                    <li data-target="#myCarousel" data-slide-to="2"></li>
				                </ol>
				                <!-- Wrapper for slides -->
				                <div class="carousel-inner">
				                    <div class="item active">
				                        <img src="{{ asset('images/slider1.jpg') }}" alt="" class="img-responsive">
				                    </div>
				                    <div class="item">
				                        <img src="{{ asset('images/slider2.jpg') }}" alt="" class="img-responsive">
				                    </div>
				                    <div class="item">
				                        <img src="{{ asset('images/slider3.jpg') }}" alt="" class="img-responsive">
				                    </div>
				                </div>
				            </div>
				        </div>
				        <div class="col-sm-12 sec-tab_secxtion">
				        	{{-- <div class="tabs-buttonn" id="sportsTab" >
				                <p>Highlights </p>
				                <ul class="nav nav-tabs ">
				                    <li class="nav-item">
								      <a class="nav-link active" data-toggle="tab" href="#cricket"> Cricket </a>
								    </li>
				                </ul>
				            </div> --}}
				{{-- 

				<div class="highlightWrap">
					<div class="secHeading">
						<h2>Highlights</h2>
					</div>
				<div id="sportsTab" class="">	
				 <!-- Nav tabs -->
				  <ul class="nav nav-tabs">
				    <li class="nav-item">
				      <a class="nav-link active" data-toggle="tab" href="#cricket"> Cricket </a>
				    </li>
				    <li class="nav-item">
				      <a class="nav-link" data-toggle="tab" href="#soccer"> Soccer </a>
				    </li>
				    <li class="nav-item">
				      <a class="nav-link" data-toggle="tab" href="#tennis"> Tennis </a>
				    </li>
				  </ul> --}}

				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div id="cricket" class="tab-pane active" event_type="4">
				     <table class="table table-hover">
						   <thead class="table-blue-color">
						      <tr>
						         <th scope="col">Match</th>
						         <th scope="col" class="text-center">1</th>
						         <th scope="col" class="text-center">X</th>
						         <th scope="col" class="text-center">2</th>
						      </tr>
						   </thead>
						   <tbody id="CricketBody">
						   		{{-- @if($cricket)
							   		@foreach($cricket as $c)
								      <tr class="second-row " event_id ="{{ $c->event->id }}" id="row_{{ $c->marketId }}" market_id="{{ $c->marketId}}">
								         <th scope="row">
								         	<a href="{{ url('fullmarket/4/'.$c->event->id.'/'.$c->marketId.'') }}" class="text-decoration-none">{{ $c->event->name }}       </a>
									     </th>
								        <td class="text-center two_points firstTeam" selection_id="{{ $c->runners[0]->selectionId}}" >
								        	<div class="Msuspended">Suspend</div>
								         	<a href="javascript:void(0)" event_type="4" event_id ="{{ $c->event->id }}" market_id="{{ $c->marketId}}" name="{{ $c->event->name }}" selection_id="{{ $c->runners[0]->selectionId}}"  title="{{ $c->runners[0]->runnerName }}" class="placeBtn btnBack text-decoration-none">--</a>
								         	<a href="javascript:void(0)" event_type="4" event_id ="{{ $c->event->id }}" market_id="{{ $c->marketId}}" name="{{ $c->event->name }}" selection_id="{{ $c->runners[0]->selectionId}}"  title="{{ $c->runners[0]->runnerName }}" class="placeBtn btnLay text-decoration-none">--</a>
								         </td>
								         <td class="text-center two_points DrawPos @if(count($c->runners) < 2) disabled @endif " >
								         	<div class="Msuspended">Suspend</div>
								         	<a href="javascript:void(0)" event_type="4" event_id ="{{ $c->event->id }}" market_id="{{ $c->marketId}}" name="{{ $c->event->name }}"  @if(count($c->runners)>2) selection_id="{{ $c->runners[2]->selectionId}}" title="{{ $c->runners[2]->runnerName }}" @endif class="placeBtn btnBack text-decoration-none">--</a>
								         	<a href="javascript:void(0)" event_type="4" event_id ="{{ $c->event->id }}" market_id="{{ $c->marketId}}" name="{{ $c->event->name }}"  @if(count($c->runners)>2) selection_id="{{ $c->runners[2]->selectionId}}" title="{{ $c->runners[2]->runnerName }}" @endif class="placeBtn btnLay text-decoration-none">--</a>
								         </td>
								         <td class="text-center two_points secondTeam" >
								         	<div class="Msuspended">Suspend</div>
								         	<a href="javascript:void(0)" event_type="4" event_id ="{{ $c->event->id }}" market_id="{{ $c->marketId}}" name="{{ $c->event->name }}" selection_id="{{ $c->runners[1]->selectionId}}" title="{{ $c->runners[1]->runnerName }}" class="placeBtn btnBack text-decoration-none">--</a>
								         	<a href="javascript:void(0)" event_type="4" event_id ="{{ $c->event->id }}" market_id="{{ $c->marketId}}" name="{{ $c->event->name }}" selection_id="{{ $c->runners[1]->selectionId}}" title="{{ $c->runners[1]->runnerName }}" class="placeBtn btnLay text-decoration-none">--</a>
								         </td>

								      </tr>
						      @endforeach
						      @else
						      <tr>
						      	<td class="text-center" colspan="4">No Match Found</td>
						      </tr>
						      @endif --}}

						      @if($type == 7)
						      	@if(count($market))
							   	@foreach($market as $h)

								      <tr class="second-row " event_id ="{{ $h->event_id }}">
									         <th scope="row">
									         	{{ $h->name }}
										     </th>
									        <td class=" horseRow" >

									        	@if(count($h->market)>0)

									        		@foreach($h->market as $horseTab)


									        			<a href="{{ url('fullracingmarket/7/'.$h->event_id.'/'.$horseTab->marketId.'') }}" class="horseAnchor">
									        				{{ date('h:i',strtotime($horseTab->marketStartTime)) }}
									        			</a>


									        		@endforeach

									        	@endif

									         </td>

									      </tr>
							      @endforeach
							       @else
									      <tr>
									      	<td class="text-center" colspan="4">No Match Found</td>
									      </tr>
								      @endif
						      @else
						      @if(count($market) > 0 )
						   		@foreach($market as $c)
							      <tr class="second-row team-row" event_id ="{{ $c->event_id }}" id="row_{{ $c->marketId }}" market_id="{{ $c->marketId}}">
							         <th scope="row">
							         	<a href="{{ url('fullmarket/4/'.$c->event_id.'/'.$c->marketId.'') }}" class="text-decoration-none">{{ $c->event->name }}       </a>
								     </th>
								     @if($c->runner1)
							        <td class="text-center two_points firstTeam" selection_id="{{ $c->runner1->selection_id }}" >
							        	<div class="Msuspended">Suspend</div>
							         	<a href="javascript:void(0)" event_type="4" event_id ="{{ $c->event_id }}" market_id="{{ $c->marketId}}" name="{{ $c->event->name }}" selection_id="{{ $c->runner1->selection_id }}"  title="{{ $c->runner1->runner_name  }}" class="placeBtn btnBack text-decoration-none">--</a>
							         	<a href="javascript:void(0)" event_type="4" event_id ="{{ $c->event_id }}" market_id="{{ $c->marketId}}" name="{{ $c->event->name }}" selection_id="{{ $c->runner1->selection_id }}"  title="{{ $c->runner1->runner_name }}" class="placeBtn btnLay text-decoration-none">--</a>
							         </td>
							         @else

								        <td class="text-center two_points firstTeam">
								        	<div class="Msuspended">Suspend</div>
								        </td>
							         @endif
							         <td class="text-center two_points DrawPos @if(empty($c->tied1)) disabled @endif " >
							         	<div class="Msuspended">Suspend</div>
							         	<a href="javascript:void(0)" event_type="4" event_id ="{{ $c->event_id }}" market_id="{{ $c->marketId}}" name="{{ $c->event->name }}"  @if(!empty($c->tied1)) selection_id="{{ $c->tied1->selection_id }}" title="{{ $c->tied1->runner_name }}" @endif class="placeBtn btnBack text-decoration-none">--</a>
							         	<a href="javascript:void(0)" event_type="4" event_id ="{{ $c->event_id }}" market_id="{{ $c->marketId}}" name="{{ $c->event->name }}"  @if(!empty($c->tied2)) selection_id="{{ $c->tied2->selection_id }}" title="{{ $c->tied2->runner_name }}" @endif class="placeBtn btnLay text-decoration-none">--</a>
							         </td>
							         <td class="text-center two_points secondTeam" >
							         	<div class="Msuspended">Suspend</div>
							         	@if(!empty($c->runner2))
							         	<a href="javascript:void(0)" event_type="4" event_id ="{{ $c->event_id }}" market_id="{{ $c->marketId}}" name="{{ $c->event->name }}" selection_id="{{ $c->runner2->selection_id }}"  title="{{ $c->runner2->runner_name  }}" class="placeBtn btnBack text-decoration-none">--</a>
							         	<a href="javascript:void(0)" event_type="4" event_id ="{{ $c->event_id }}" market_id="{{ $c->marketId}}" name="{{ $c->event->name }}" selection_id="{{ $c->runner2->selection_id }}"  title="{{ $c->runner2->runner_name }}" class="placeBtn btnLay text-decoration-none">--</a>
							         	@endif
							         </td>

							      </tr>
					      		@endforeach
					      		 @else
							      <tr>
							      	<td class="text-center" colspan="4">No Match Found</td>
							      </tr>
						      @endif
						      @endif


						   </tbody>
						</table>
				    </div>
				  </div>
				  </div>
				</div>
				</div>


			</div>
			
			<div class="col-md-3 p-0">
				
			    @include('include/rightSideBar')
			</div>
        </div>
    </div>

@endsection


@section('scripts')


<script>

	$('.placeBtn').click(function() {
		var event_id = $(this).attr('event_id');
		var event_type = $(this).attr('event_type');
		var market_id = $(this).attr('market_id');
		var name = $(this).attr('name');
		var selection_id = $(this).attr('selection_id');
		var title = $(this).attr('title');
		var odds = $(this).text();

		if($(this).hasClass('btnBack')){

			if ($(".BetforSlipMatch").hasClass('slip'+selection_id+'')) {

				 $('.BetforSlipMatch.slip'+selection_id+'').parent().remove();

				 if ($(".SingleMatchBetForSlip").find(".BetforSlipMatch").length < 1){ 
				 		if ($(".SingleMatchBetLaySlip").find(".BetlaySlipMatch").length < 1){ 
						    $('._3QQE9').show();
						    $('.BetSlipRow').addClass('d-none');
						}
						$('.Bet-for-slip').addClass('d-none');
					}

			}else{


			var text = '<div class="innerSlipOdds"><div class="col-md-12 BetforSlipMatch slip'+selection_id+'" id="slip'+odds+'"><div class="one_team_name"><p class="m-0"> <i class="fa fa-dot-circle-o"></i> '+name+'</p></div></div><div class="singleOddsRowBet"><div class="col-md-12 stakeInputRow"><div class="row"><div class="col-md-5 p-adjust"><div class="match-odds d-flex"><div class="closeSlipdiv"> <a href="javascript:void(0)" class="closeSlip" data-s="'+odds+'"><i class="fa fa-close"></i></a></div><div class="match-odds-countory"><p class="m-0"> '+title+'</p> <span>Match Odds</span></div></div></div><div class="col-md-2 p-adjust"><div class="input-type-area"> <input type="hidden" name="type" value="back"> <input name="stakeOdds" class="form-control" type="text" readonly value="'+odds+'" /></div></div><div class="col-md-2 p-adjust"><div class="input-type-area inputStake"> <input event_id="'+event_id+'" event_type="'+event_type+'" market_id="'+market_id+'" event_name="'+name+'" selection_id="'+selection_id+'" title="'+title+'" odds="'+odds+'" bet_type="back" class="form-control betStakeAmount" name="stakeAmount"  type="number" pattern= "[0-9]" maxlength="7" /></div></div><div class="col-md-3 text-right"><div class="betting-price singleLiablity"> 0 </div></div></div></div><div class="col-md-12 stakePriceRow p-adjust "><div class="match_rates"><ul class="p-0 m-0 bet_stake "><li><a href="javascript:void(0)" class="btn">10</a></li><li><a href="javascript:void(0)" class="btn">30</a></li><li><a href="javascript:void(0)" class="btn">50</a></li><li><a href="javascript:void(0)" class="btn">200</a></li><li><a href="javascript:void(0)" class="btn">500</a></li><li><a href="javascript:void(0)" class="btn">1000</a></li></ul></div></div></div></div>';
				$('._3QQE9').hide();
				$('.BetSlipRow').removeClass('d-none');
				$('.Bet-for-slip').removeClass('d-none');
				$('.Bet-for-slip > .SingleMatchBetForSlip').append(text);
			}

		}else{

			if ($(".BetlaySlipMatch").hasClass('slip'+selection_id+'')) {
				$('.BetlaySlipMatch.slip'+selection_id+'').parent().remove();
					
				 if ($(".SingleMatchBetLaySlip").find(".BetlaySlipMatch").length < 1){ 

				 		if ($(".SingleMatchBetForSlip").find(".innerSlipOdds").length < 1){ 
						    $('._3QQE9').show();
							$('.BetSlipRow').addClass('d-none');
						}

						$('.Bet-lay-slip').addClass('d-none');
					}
			}else{
				var text2 = '<div class="innerSlipOdds" ><div class="col-md-12 BetlaySlipMatch slip'+selection_id+' " id="slip'+odds+'"><div class="one_team_name"><p class="m-0"> <i class="fa fa-dot-circle-o"></i> '+name+'</p></div></div><div class="singleOddsRowBet"><div class="col-md-12 stakeInputRow"><div class="row"><div class="col-md-5 p-adjust"><div class="match-odds d-flex"><div class="closeSlipdiv" > <a href="javascript:void(0)" class="closeSlip" data-s="'+odds+'"><i class="fa fa-close"></i></a></div><div class="match-odds-countory"><p class="m-0"> '+title+'</p> <span>Match Odds</span></div></div></div><div class="col-md-2 p-adjust"><div class="input-type-area"> <input type="hidden" name="type" value="lay"> <input name="stakeOdds" class="form-control" type="text" readonly value="'+odds+'" /></div></div><div class="col-md-2 p-adjust"><div class="input-type-area"> <input name="stakeAmount" event_id="'+event_id+'" event_type="'+event_type+'" market_id="'+market_id+'" event_name="'+name+'" selection_id="'+selection_id+'" title="'+title+'" odds="'+odds+'" bet_type="lay" class="form-control betStakeAmount" type="number" pattern= "[0-9]" maxlength="7" /></div></div><div class="col-md-3 text-right"><div class="betting-price singleLiablity"> 0 </div></div></div></div><div class="col-md-12 stakePriceRow p-adjust "><div class="match_rates" ><ul class="p-0 m-0 bet_stake" ><li><a href="javascript:void(0)" class="btn">10</a></li><li><a href="javascript:void(0)" class="btn">30</a></li><li><a href="javascript:void(0)" class="btn">50</a></li><li><a href="javascript:void(0)" class="btn">200</a></li><li><a href="javascript:void(0)" class="btn">500</a></li><li><a href="javascript:void(0)"   class="btn">1000</a></li></ul></div></div></div></div>';

					$('._3QQE9').hide();
					$('.BetSlipRow').removeClass('d-none');
					$('.Bet-lay-slip').removeClass('d-none');
					$('.Bet-lay-slip > .SingleMatchBetLaySlip').append(text2);

				}
		}
	});


	$('.SingleMatchBetSlip').on('click','.closeSlip',function() {
		var sec_id = $(this).attr('data-s');
		// $('.slip'+sec_id+'').parent().remove();
	    $('[id = "slip'+sec_id+'"]').parent().remove();

	     	 if ($(".SingleMatchBetLaySlip").find(".BetlaySlipMatch").length < 1 && $(".SingleMatchBetForSlip").find(".BetforSlipMatch").length < 1){ 
	     	 	
				    $('._3QQE9').show();
				    $('.BetSlipRow').addClass('d-none');
				    $('.BetSubmit').addClass('disabled');
					$('.BetSubmit > a').addClass('disabled');
			}else{
				if ($(".SingleMatchBetForSlip").find(".innerSlipOdds").length < 1){ 
					$('.Bet-for-slip').addClass('d-none');
				}


				if ($(".SingleMatchBetLaySlip").find(".BetlaySlipMatch").length < 1){ 
				    $('.Bet-lay-slip').addClass('d-none');
				}
			}

			
});



$('.SingleMatchBetSlip').on('click','.bet_stake > li > a',function(){
	var value = parseInt($(this).html());

	// $(this).closest('.betStakeAmount').value('112');
	var d = $(this).closest("div.singleOddsRowBet").find("input[name='stakeAmount']").val(value);
	var s = $(this).closest("div.singleOddsRowBet").find("input[name='stakeOdds']").val();



		var v = (value*s)-value;	
		
		var l = $(this).closest(".stakePriceRow").prev('div.stakeInputRow').find('div.singleLiablity').html(v.toFixed(2));
		var totalLiability = 0;
		$('.singleLiablity').each(function(i, obj) {

			totalLiability = totalLiability + parseFloat($(this).text());
			$('.totalL').html(totalLiability);
			$('.BetSubmit').removeClass('disabled');
			$('.BetSubmit a').removeClass('disabled');
		});
});




$('.SingleMatchBetSlip').on('change','.betStakeAmount',function(){
	var value = parseInt($(this).val());

	// $(this).closest('.betStakeAmount').value('112');
	var d = $(this).closest("div.singleOddsRowBet").find("input[name='stakeAmount']").val(value);
	var s = $(this).closest("div.singleOddsRowBet").find("input[name='stakeOdds']").val();



		var v = (value*s)-value;	
		
		var l = $(this).closest(".stakeInputRow").find('div.singleLiablity').html(v.toFixed(2));
		var totalLiability = 0;
		$('.singleLiablity').each(function(i, obj) {

			totalLiability = totalLiability + parseFloat($(this).text());
			$('.totalL').html(totalLiability);
			$('.BetSubmit').removeClass('disabled');
			$('.BetSubmit > a').removeClass('disabled');
		});
});



			

	// setInterval(function(){ 
	$( document ).ready(function() {
		var eventArr = [];
		$('.tab-pane.active > table > tbody > tr').each(function() {
			 eventArr.push($(this).attr('market_id'));
		});
		var event_type = $('.tab-pane.active').attr('event_type');

	if (typeof eventArr[0] !== 'undefined') {		

		$.ajax({
	        type: 'POST',
	        url: '/EventsAccMarket',
	        headers: {
	                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	               },
	         data : { type_id : event_type, marketIds:eventArr},
	     //     beforeSend: function() {
	     //     	 $.each(eventArr, function(i, item) {
		    //     var sectionID = 'row_'+item;
	     //      	var firstback ='<a href="javascript:void(0)" class="text-decoration-none">--</a>';
	     //       		firstback +='<a href="javascript:void(0)" class="text-decoration-none">--</a>';
	     //      	$('[id = "'+sectionID+'"] > td.firstTeam').html(firstback);
	     //  		var secondback ='<a href="javascript:void(0)" class="text-decoration-none">--</a>';
	     //        secondback +='<a href="javascript:void(0)" class="text-decoration-none">--</a>';
	     //      	$('[id = "'+sectionID+'"] > td.secondTeam').html(secondback);


	     //      	var drawback ='<a href="javascript:void(0)" class="text-decoration-none">--</a>';
	     //        	drawback +='<a href="javascript:void(0)" class="text-decoration-none">--</a>';
	     //      		$('[id = "'+sectionID+'"] > td.DrawPos').html(drawback);
	     //      });
		    // },
	        success: function(res) {
	                $.each(res, function(i, item) {

	                	var sectionID = 'row_'+item.marketId;
	                	if(item.runners[0].ex.availableToBack.length > 0 ){
	                		$('.Msuspended').css('display','none');

	                		// $('[id = "'+sectionID+'"] > td.firstTeam > a.btnBack').attr('market_id',item.marketId);
	                		// $('[id = "'+sectionID+'"] > td.firstTeam > a.btnBack').attr('selection_id',item.runners[0].selectionId);
	                		$('[id = "'+sectionID+'"] > td.firstTeam > a.btnBack').html(item.runners[0].ex.availableToBack[0].price);


	                		// $('[id = "'+sectionID+'"] > td.firstTeam > a.btnLay').attr('market_id',item.marketId);
	                		// $('[id = "'+sectionID+'"] > td.firstTeam > a.btnLay').attr('selection_id',item.runners[0].selectionId);
	                		$('[id = "'+sectionID+'"] > td.firstTeam > a.btnLay').html(item.runners[0].ex.availableToLay[0].price);

	                  		// var firstback ='<a href="javascript:void(0)" class="placeBtn btnBack" market_id="'+item.marketId+'" selection_id="'+item.runners[0].selectionId+'" class="text-decoration-none">'+ item.runners[0].ex.availableToBack[0].price +'</a>';
	                   	// 		firstback +='<a href="javascript:void(0)" class="placeBtn btnLay" market_id="'+item.marketId+'" selection_id="'+item.runners[0].selectionId+'"  class="text-decoration-none">'+ item.runners[0].ex.availableToLay[0].price +'</a>';
	                	}else{
	                		$('.Msuspended').css('display','block');
	                	}


	                	if(item.runners[1].ex.availableToBack.length > 0 ){
	                		$('.Msuspended').css('display','none');

	                		// $('[id = "'+sectionID+'"] > td.secondTeam > a.btnBack').attr('market_id',item.marketId);
	                		// $('[id = "'+sectionID+'"] > td.secondTeam > a.btnBack').attr('selection_id',item.runners[1].selectionId);
	                		$('[id = "'+sectionID+'"] > td.secondTeam > a.btnBack').html(item.runners[1].ex.availableToBack[0].price);


	                		// $('[id = "'+sectionID+'"] > td.secondTeam > a.btnLay').attr('market_id',item.marketId);
	                		// $('[id = "'+sectionID+'"] > td.secondTeam > a.btnLay').attr('selection_id',item.runners[1].selectionId);
	                		$('[id = "'+sectionID+'"] > td.secondTeam > a.btnLay').html(item.runners[1].ex.availableToLay[0].price);

	                  		// var firstback ='<a href="javascript:void(0)" class="placeBtn btnBack" market_id="'+item.marketId+'" selection_id="'+item.runners[0].selectionId+'" class="text-decoration-none">'+ item.runners[0].ex.availableToBack[0].price +'</a>';
	                   	// 		firstback +='<a href="javascript:void(0)" class="placeBtn btnLay" market_id="'+item.marketId+'" selection_id="'+item.runners[0].selectionId+'"  class="text-decoration-none">'+ item.runners[0].ex.availableToLay[0].price +'</a>';
	                	}else{
	                		$('.Msuspended').css('display','block');	
	                	}

	                  	// $('[id = "'+sectionID+'"] > td.firstTeam').html(firstback);
	              		// var secondback ='<a href="javascript:void(0)" class="placeBtn btnBack" market_id="'+item.marketId+'" selection_id="'+item.runners[1].selectionId+'"  class="text-decoration-none">'+ item.runners[1].ex.availableToBack[0].price +'</a>';
	                //     secondback +='<a href="javascript:void(0)" class="placeBtn btnLay" market_id="'+item.marketId+'" selection_id="'+item.runners[1].selectionId+'" class="text-decoration-none">'+ item.runners[1].ex.availableToLay[0].price +'</a>';
	                //   	$('[id = "'+sectionID+'"] > td.secondTeam').html(secondback);

	                  	if (event_type != 2) {                  		
		                  	if (item.runners.length > 2) {

		                  		if(item.runners[1].ex.availableToBack.length > 0 ){
			                		$('.Msuspended').css('display','none');

			                		// $('[id = "'+sectionID+'"] > td.DrawPos > a.btnBack').attr('market_id',item.marketId);
			                		// $('[id = "'+sectionID+'"] > td.DrawPos > a.btnBack').attr('selection_id',item.runners[2].selectionId);
			                		$('[id = "'+sectionID+'"] > td.DrawPos > a.btnBack').html(item.runners[2].ex.availableToBack[0].price);


			                		// $('[id = "'+sectionID+'"] > td.DrawPos > a.btnLay').attr('market_id',item.marketId);
			                		// $('[id = "'+sectionID+'"] > td.DrawPos > a.btnLay').attr('selection_id',item.runners[2].selectionId);
			                		$('[id = "'+sectionID+'"] > td.DrawPos > a.btnLay').html(item.runners[2].ex.availableToLay[0].price);

			                	}else{
			                		$('.Msuspended').css('display','block');	
			                	}	


		                  	// var drawback ='<a href="javascript:void(0)" class="placeBtn btnBack" market_id="'+item.marketId+'" selection_id="'+item.runners[2].selectionId+'" class="text-decoration-none">'+ item.runners[2].ex.availableToBack[0].price +'</a>';
		                   //  	drawback +='<a href="javascript:void(0)" class="placeBtn btnLay" market_id="'+item.marketId+'" selection_id="'+item.runners[2].selectionId+'" class="text-decoration-none">'+ item.runners[2].ex.availableToLay[0].price +'</a>';
		                  	// 	$('[id = "'+sectionID+'"] > td.DrawPos').html(drawback);
		                  	}                 	
	                  	}
	                });

	        }
	      });
		}	
	});	
// }, 5000);


	


	$('.BetCancelAll > a').click(function(){
		$('.SingleMatchBetSlip').html('');

		$('.bettingSlip').addClass('d-none');
		$('.BetSlipRow ').addClass('d-none');
		$('._3QQE9').show();
	})



</script>


@endsection

