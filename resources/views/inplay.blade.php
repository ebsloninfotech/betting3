@extends('layouts.front')


@section('content')

    <div class="container-fluid">
        <div class="row">
			<div class="col-md-9 p-0 all_margin_section">

				<div class="col_center middleSec">
	<div class="height-box">
    <div class="row">
        
        <div class="col-sm-12 sec-tab_secxtion in-play-section">
        	<div class="tabs-buttonn" id="sportsTab" >
               
                <ul class="nav nav-tabs ">
                    <li class="nav-item active">
				      <a class="nav-link" data-toggle="tab" href="#InPlay"> In-Play </a>
				    </li>
				    <li class="nav-item">
				      <a class="nav-link" data-toggle="tab" href="#Today"> Today </a>
				    </li>
				    <li class="nav-item">
				      <a class="nav-link" data-toggle="tab" href="#Tomorrow"> Tomorrow </a>
				    </li>
                </ul>
            </div>


  <!-- Tab panes -->
  <div class="tab-content">
    <div id="InPlay" class="tab-pane active" event_type="4">
     <table class="table table-hover">
		   <thead class="table-blue-color">
		      <tr>
		         <th scope="col">Cricket</th>
		         <th></th>
		         <th></th>
		         <th><a href="javascript:void(0)" class="a-btn" id="c-hide"><i class="fa fa-minus"></i></a></th>
		      </tr>
		      
		   </thead>
		   <tbody id="CricketBodys">
		   	{{-- <tr>
		      	<th></th>
			      <th scope="col" class="text-center">1</th>
			         <th scope="col" class="text-center">X</th>
			         <th scope="col" class="text-center">2</th>
			     </tr> --}}

			     		@if($today)
				     		@foreach($today as $t)
				     			@if($t->event_type == 4)
				     			@if(checkInplayEvent($t->oddmarket->marketId))

						      <tr class="second-row">
						         <th scope="row">
						         	<a href="{{ url('fullmarket/'.$t->event_type.'/'.$t->event_id.'/'.$t->oddmarket->marketId.'/') }}" class="text-decoration-none"><i class="fa fa-circle"></i>  {{ $t->name }}</a>
							     </th>
						        <td class="text-center two_points firstTeam">
						        
						         	<!-- <a href="javascript:void(0)"class="placeBtn btnBack text-decoration-none">--</a>
						         	<a href="javascript:void(0)"class="placeBtn btnLay text-decoration-none">--</a> -->
						         </td>
						         <td class="text-center two_points firstTeam">
						        	
						         	<!-- <a href="javascript:void(0)"class="placeBtn btnBack text-decoration-none">--</a>
						         	<a href="javascript:void(0)"class="placeBtn btnLay text-decoration-none">--</a> -->
						         </td>
						         <td class="text-center two_points firstTeam">
						        	
						         	<!-- <a href="javascript:void(0)"class="placeBtn btnBack text-decoration-none">--</a>
						         	<a href="javascript:void(0)"class="placeBtn btnLay text-decoration-none">--</a> -->
						         </td>
						         
						         

						      </tr>
						      @endif
						      @endif
					     	@endforeach
					      @endif
		      		
				      <!-- <tr>
				      	<td class="text-center" colspan="4">No Match Found</td>
				      </tr> -->
			      
			     
		      
		   </tbody>
		</table>

		<table class="table table-hover">
		   <thead class="table-blue-color">
		      <tr>
		         <th scope="col">Soccer</th>
		         <th></th>
		         <th></th>
		         <th><a href="javascript:void(0)" class="a-btn" id="s-hide"><i class="fa fa-minus"></i></a></th>
		      </tr>
		      
		   </thead>
		   <tbody id="SoccerBodys">
		  {{--  	<tr>
		      	<th></th>
		      <th scope="col" class="text-center">1</th>
		         <th scope="col" class="text-center">X</th>
		         <th scope="col" class="text-center">2</th>
		     </tr> --}}

		     		@if($today)

				     @foreach($today as $t)

			     			@if($t->event_type == 1)
			     			@if(checkInplayEvent($t->oddmarket->marketId))
					      <tr class="second-row">
					         <th scope="row">

					         	<a href="{{ url('fullmarket/'.$t->event_type.'/'.$t->event_id.'/'.$t->oddmarket->marketId.'/') }}" class="text-decoration-none"> <i class="fa fa-circle"></i> {{ $t->name }}</a>
						     </th>
					        <td class="text-center two_points firstTeam">
					        	
					         	<!-- <a href="javascript:void(0)"class="placeBtn btnBack text-decoration-none">--</a>
					         	<a href="javascript:void(0)"class="placeBtn btnLay text-decoration-none">--</a> -->
					         </td>
					         <td class="text-center two_points firstTeam">
					        	
					         	<!-- <a href="javascript:void(0)"class="placeBtn btnBack text-decoration-none">--</a>
					         	<a href="javascript:void(0)"class="placeBtn btnLay text-decoration-none">--</a> -->
					         </td>
					         <td class="text-center two_points firstTeam">
					        	
					         	<!-- <a href="javascript:void(0)"class="placeBtn btnBack text-decoration-none">--</a>
					         	<a href="javascript:void(0)"class="placeBtn btnLay text-decoration-none">--</a> -->
					         </td>
					         
					         

					      </tr>
					      @endif
					      @endif
				     	@endforeach
				     @endif	
		      		
				      <!-- <tr>
				      	<td class="text-center" colspan="4">No Match Found</td>
				      </tr> -->
			      
			     
		      
		   </tbody>
		</table>

		<table class="table table-hover">
		   <thead class="table-blue-color">
		      <tr>
		         <th scope="col">Tennis</th>
		         <th></th>
		         <th></th>
		         <th><a href="javascript:void(0)" class="a-btn" id="t-hide"><i class="fa fa-minus"></i></a></th>
		      </tr>
		      
		   </thead>
		   <tbody id="TennisBodys">
		   	{{-- <tr>
		      	<th></th>
		      <th scope="col" class="text-center">1</th>
		         <th scope="col" class="text-center">X</th>
		         <th scope="col" class="text-center">2</th>
		     </tr> --}}
		     	@if($today)
						@foreach($today as $t)

			     			@if($t->event_type == 2)

			     			@if(checkInplayEvent($t->event_id))
					      <tr class="second-row">
					         <th scope="row">
					         	<a href="{{ url('fullmarket/'.$t->event_type.'/'.$t->event_id.'/'.$t->oddmarket->marketId.'/') }}" class="text-decoration-none"><i class="fa fa-circle"></i>  {{ $t->name }}</a>
						     </th>
					        <td class="text-center two_points firstTeam">
					        	
					         	<!-- <a href="javascript:void(0)"class="placeBtn btnBack text-decoration-none">--</a>
					         	<a href="javascript:void(0)"class="placeBtn btnLay text-decoration-none">--</a> -->
					         </td>
					         <td class="text-center two_points firstTeam">
					        	
					         	<!-- <a href="javascript:void(0)"class="placeBtn btnBack text-decoration-none">--</a>
					         	<a href="javascript:void(0)"class="placeBtn btnLay text-decoration-none">--</a> -->
					         </td>
					         <td class="text-center two_points firstTeam">
					        	
					         	<!-- <a href="javascript:void(0)"class="placeBtn btnBack text-decoration-none">--</a>
					         	<a href="javascript:void(0)"class="placeBtn btnLay text-decoration-none">--</a> -->
					         </td>
					         
					         

					      </tr>
					      @endif
					      @endif
				     	@endforeach
				    @endif 	
		      		
				      <!-- <tr>
				      	<td class="text-center" colspan="4">No Match Found</td>
				      </tr> -->
			      
			     
		      
		   </tbody>
		</table>

		
    </div>
    <div id="Today" class=" tab-pane fade">
      <div id="sportFilter" class="function-wrap" style="display: block;">
		<ul class="filter-list">
			<li class="filter-head">Sport Filters:</li>
			<li class="filter-content">			
			<span id="sport_4" style="" class="filter-first">Cricket</span><span id="sport_1" style="">Soccer</span><span id="sport_2" style="">Tennis</span></li>
			{{-- <li class="filter-btn"><a id="sportFilterPopup" class="btn">Filter</a></li> --}}
		</ul>
		{{-- <div id="sportFilterContainer" class="filter-pop">
			<ul>
				<li><input name="sportsFilter" value="-1" id="filter-all" type="checkbox"><label for="filter-all">All</label></li>
				
				<li><input name="sportsFilter" value="1" id="filter-1" type="checkbox"><label for="filter-1">Soccer</label></li>
				
				<li><input name="sportsFilter" value="2" id="filter-2" type="checkbox"><label for="filter-2">Tennis</label></li>
				
				<li><input name="sportsFilter" value="4" id="filter-4" type="checkbox"><label for="filter-4">Cricket</label></li>
				
			</ul>
			<div class="btn-wrap">
				<a id="sportFilterSave" class="btn-send">Save</a>
				<a id="sportFilterCancel" class="btn">Cancel</a>
			</div>
		</div> --}}
	</div>

	<table class="table table-hover table-play">
		   
		   <tbody>
		   		
		   			@if($today)

				     	@foreach($today as $t)
		   	

				      <tr class="second-row">
				         <td scope="row">
				         	{{ $t->openDate }}
					     </td>
					     <td scope="row">
				         	@if($t->event_type == 1)
				         	Soccer
				         	@endif	
				         	@if($t->event_type == 2)
				         	Tennis
				         	@endif	
				         	@if($t->event_type == 4)
				         	Cricket
				         	@endif	
				         	<img class="fromto" src="http://ferrariexchange247.com/images/transparent.gif">{{ $t->competition?$t->competition->name : '--'  }}<img class="fromto" src="http://ferrariexchange247.com/images/transparent.gif"><a href="#" class="text-decoration-none">{{ $t->name }}</a>
					     </td>     
				         
				         

				      </tr>

				      @endforeach
		      		
		      		@endif
		      		
				      <!-- <tr>
				      	<td class="text-center" colspan="4">No Match Found</td>
				      </tr> -->
			      
			     
		      
		   </tbody>
		</table>
    </div>
    <div id="Tomorrow" class=" tab-pane fade">
     <div id="sportFilter" class="function-wrap" style="display: block;">
		<ul class="filter-list">
			<li class="filter-head">Sport Filters:</li>
			<li class="filter-content">			
			<span id="sport_4" style="" class="filter-first">Cricket</span><span id="sport_1" style="">Soccer</span><span id="sport_2" style="">Tennis</span></li>
			{{-- <li class="filter-btn"><a id="sportFilterPopups" class="btn">Filter</a></li> --}}
		</ul>
		{{-- <div id="sportFilterContainers" class="filter-pop">
			<ul>
				<li><input name="sportsFilter" value="-1" id="filter-all" type="checkbox"><label for="filter-all">All</label></li>
				
				<li><input name="sportsFilter" value="1" id="filter-1" type="checkbox"><label for="filter-1">Soccer</label></li>
				
				<li><input name="sportsFilter" value="2" id="filter-2" type="checkbox"><label for="filter-2">Tennis</label></li>
				
				<li><input name="sportsFilter" value="4" id="filter-4" type="checkbox"><label for="filter-4">Cricket</label></li>
				
			</ul>
			<div class="btn-wrap">
				<a id="sportFilterSave" class="btn-send">Save</a>
				<a id="sportFilterCancel" class="btn">Cancel</a>
			</div>
		</div> --}}
	</div>

	<table class="table table-hover table-play">
		   
		   <tbody>

		   		@if($tomorrow)

		   				@foreach($tomorrow as $t)
		   	

				      <tr class="second-row">
				         <td scope="row">
				         	{{ $t->openDate }}
					     </td>
					     <td scope="row">
				         	@if($t->event_type == 1)
				         	Soccer
				         	@endif	
				         	@if($t->event_type == 2)
				         	Tennis
				         	@endif	
				         	@if($t->event_type == 4)
				         	Cricket
				         	@endif	
				         	<img class="fromto" src="http://ferrariexchange247.com/images/transparent.gif">{{ $t->competition?$t->competition->name : '--'  }}<img class="fromto" src="http://ferrariexchange247.com/images/transparent.gif"><a href="#" class="text-decoration-none">{{ $t->name }}</a>
					     </td>     
				         
				         

				      </tr>

				      @endforeach

				    @endif  
		      		
				      <!-- <tr>
				      	<td class="text-center" colspan="4">No Match Found</td>
				      </tr> -->
			      
			     
		      
		   </tbody>
		</table>
    </div>
  </div>
  </div>
</div>

</div>
</div>
			</div>
			
			<div class="col-md-3 p-0">
				
			    @include('include/rightSideBar')
			</div>
        </div>
    </div>



@endsection

