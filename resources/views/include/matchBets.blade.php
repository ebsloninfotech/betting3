<div class="betHistorydata">
   <div class="topnelHeding">
      <div class="panel-heading site_color" style="user-select: text;">
         <span class="textalign-left no-border-radius setBetSlipHeader no-margin">Matched Bet</span>
         {{-- <a style="float: right; text-decoration: none;" target="_blank" class="c-white" href="{{ url('/viewAllMatchedBet/event_id') }}">View All</a> --}}
      </div>
   </div>
   <div class="bidhistorywrp table-responsive">
      <table class="tbl_history table table-bordered table-hover table-striped ">
         <thead>
            <tr>
               <th>Sr.</th>
               <th>Bet ID</th>
               <th>User</th>
               <th>Odds</th>
               <th>Stake</th>
               <th>BetType</th>
               <th>Team</th>
               <th>Date</th>
            </tr>
         </thead>
         <tbody class="tblmatchbet">
            @php $i=1; @endphp
            @foreach(matchedBethelper($event) as $v)
                <tr class="@if($v->bet_type == 'back') backtr @else laytr @endif">
                   <td>{{ $i++ }}</td>
                   <td>{{ $v->id }}</td>
                   <td>{{ $v->user->username }}</td>
                   <td>{{ $v->odds }}</td>
                   <td>{{ $v->bet_amount }}</td>
                   <td>{{ $v->bet_type }} </td>
                   <td class="textteam">
                      <p title="{{ $v->title }}">{{ $v->title }}</p>
                   </td>
                   <td>{{ $v->created_at }}</td>
                </tr>
            @endforeach
         </tbody>
      </table>
   </div>

   <div class="topnelHeding">
      <div class="panel-heading site_color" style="user-select: text;">
         <span class="textalign-left no-border-radius setBetSlipHeader no-margin">Total Points</span>
      </div>
   </div>
{{--    <div class="tablepanel">
       <div class="row border-1 m-0">
           <div class="col-md-2">
               
           </div>
           @foreach($MarketBook[0]->runners as $value)   
               <div class="col-md-4 text-center">
                   <span><b>{{ $value->runnerName }}</b></span>
               </div>
            @endforeach
       </div>
   </div>

   <div class="amountpanel">
       <div class="row border-1 m-0">
           <div class="col-md-4">
               
           </div>
       </div>
   </div> --}}


   <div class="bidhistorywrp table-responsive">
      <table class="tbl_history table table-bordered table-hover table-striped">
           <thead>
              <tr>
                 <th></th>

                 @foreach($totalMarket[0]->runner as $value)    
                 <th class="text-center">
                    <span id="ContentPlaceHolder1_team01">{{ $value->runner_name }}</span>
                 </th>
                 @endforeach
              </tr>
           </thead>
           <tbody class="tblPoints">
                @php $f=0; $s = 0; $t=0; @endphp
                    
                       @foreach(matchedTotal($market) as $v) 
                      <tr>
                         <td class="headr">{{ $v->user->username }}</td>

                          @foreach($totalMarket[0]->runner as $value)    

                              @php

                                $userPl = userEventPL($v->user_id,$value->selection_id,$market);
                              @endphp
                             <td class="text-center">
                                @if($userPl['status'] == 'profit')
                                <span class="clsgreen">{{ $userPl['net'] }}</span> 

                                @else

                                <span class="clsred">{{ $userPl['net'] }}</span> 
                                @endif
                             </td>
                             @endforeach
                      </tr>
                      @php 
                        // $f = $f + $v->amount;
                        // $s = $s + $v->profit_amount;
                        // if($v->draw){

                        //     $t = $t + $v->draw;
                        // }

                      @endphp
                      @endforeach
              
              {{-- <tr class="stuspointheading">
                 <td class="headr"><b>DL P&amp;L</b></td>
                 <td class="text-center"><span class="clsred">173871.08</span> </td>
                 <td class="text-center"><span class="clsgreen">14510.74</span></td>
              </tr>
              <tr class="stuspointheading">
                 <td class="headr"><b>MDL P&amp;L</b></td>
                 <td class="text-center"><span class="clsred">0.00</span> </td>
                 <td class="text-center"><span class="clsgreen">0.00</span></td>
              </tr> --}}
           </tbody>
           <tfoot>
             <tr class="stuspointheading">
                 <td class="headr"><b>Total P&amp;L</b></td>
                  @foreach(getTotalExposureViaEvent($market) as $totalPl)   
                     <td class="text-center">
                        @if($totalPl['status'] == 'profit')
                        <span class="clsgreen">{{ $totalPl['net'] }}</span> 

                        @else

                        <span class="clsred">{{ $totalPl['net'] }}</span> 
                        @endif
                     </td>
                  @endforeach
              </tr>
           </tfoot>
        </table>
   </div>


</div>

