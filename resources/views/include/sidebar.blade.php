{{-- <div class="leftSidebar">
	<ul class="topBar ">
		<li><a href="{{ url('/') }}" >Sports</a></li>
	</ul>
	<div class="leftSideBarContent">
		<ul class="sportsList">
			
			@if(isset($totalMarket))
				@foreach($totalMarket as $value)	
				<li class="@if($value->marketId == $market ) active @endif">
					<a href="{{ url('fullmarket/'.$type.'/'.$event.'/'.$value->marketId.'') }}"   >{{ $value->marketName }} <span class="fa fa-arrow-right float-right"></span></a>
				</li>
				@endforeach
			@else	
			<li>
				<a href="javascript:void(0)" class="sportInnerClick" name="Cricket" type_id="4" competition_id="" event_id="" market_id="" >Cricket <span class="fa fa-arrow-right float-right"></span></a>
			</li>
			<li>
				<a href="javascript:void(0)" class="sportInnerClick" name="Soccer" type_id="1" competition_id="" event_id="" market_id="" >Soccer <span class="fa fa-arrow-right float-right"></span></a>
			</li>
			<li>
				<a href="javascript:void(0)" class="sportInnerClick" name="Tennis" type_id="2" competition_id="" event_id="" market_id="" >Tennis <span class="fa fa-arrow-right float-right"></span></a> 
			</li>
			@endif
		</ul>
	</div>
</div> --}}

    <div class="left-section">
        <ul class="ico_dots">
            <li class="ico_sport">
                <div class="dropdown">
					<a class="" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
					</a>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
						<ul class="p-0 m-0 pathBack">
							<li>
								<a class="dropdown-item" href="{{ url('/') }}"><i class="fas fa-angle-up"></i> Sports</a>
							</li>
					    </ul>
					</div>
				</div>
               {{--  <a href="{{ url('/') }}" >Sports</a>
                <a href="{{ url('/') }}" >Sports</a>
                <a href="{{ url('/') }}" >Sports</a>
                <a href="{{ url('/') }}" >Sports</a> --}}
            </li>
        </ul>
    </div>
    <div class="click_on_text leftSideBarContent">
        <ul class="sportsList">
        	@if(isset($otherMarket))
				@foreach($otherMarket as $value)	
				<li class="ico icon_leftt @if(isset($market )) @if($value->marketId == $market ) active @endif @endif">
					<a href="{{ url('fullmarket/'.$type.'/'.$event.'/'.$value->marketId.'') }}"   >{{ $value->marketName }} <i class="fa fa-arrow-right float-right"></i></a>
				</li>
				@endforeach
			@else
				<li class="ico icon_leftt">
					<a href="javascript:void(0)" class="sportInnerClick" name="Cricket" type_id="4" competition_id="" event_id="" market_id="" >Cricket <i class="fa fa-arrow-right float-right"></i></a>
				</li>
				<li class="ico icon_leftt">
					<a href="javascript:void(0)" class="sportInnerClick" name="Soccer" type_id="1" competition_id="" event_id="" market_id="" >Soccer <i class="fa fa-arrow-right float-right"></i></a>
				</li>
				<li class="ico icon_leftt">
					<a href="javascript:void(0)" class="sportInnerClick" name="Tennis" type_id="2" competition_id="" event_id="" market_id="" >Tennis <i class="fa fa-arrow-right float-right"></i></a> 
				</li>
				{{-- <li class="ico icon_leftt">
					<a href="javascript:void(0)" class="sportInnerClick" name="Horse Racing" type_id="7" competition_id="" event_id="" market_id="" >Horse Racing <i class="fa fa-arrow-right float-right"></i></a> 
				</li> --}}

			@endif
            {{-- <li class="ico icon_leftt">
                <a href="#"> cricket  </a>
                <a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
            </li>
            <li class="ico icon_leftt">
                <a href="#"> Soccer  </a>
                <a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
            </li>
            <li class="ico icon_leftt">
                <a href="#"> Tennis  </a>
                <a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
            </li> --}}
        </ul>
    </div>

