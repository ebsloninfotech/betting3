
<div class="main-body-right-section">
    <div class="main-body-right-section-wrap ">
        <div class="contentBlockHeading bet-slip _1zgNi">
           <h1><span>Bet Slip</span></h1>
        </div>
           <p class="site-alert"></p>
        <div class="_3QQE9">
           <div class="noBetplaced">
               	<i class="fa fa-ticket"></i>
               <div class="_1BXvS">There are no bets on your ticket.</div>
               <div class="_1Eprm">Click the odds to add a bet</div>
           </div>
        </div>
    	<div class="bg-color BetSlipRow d-none">
            <div class="BetForSlipSec">
        		<div class="row m-0 pb-2">
                    <div class="Bet-for-slip bettingSlip pb-3 pt-2 d-none">                        
                        <div class="BetForSlipSecHead col-md-12 p-10">
                            <div class="row">                            
                        		<div class="col-xs-5 col-sm-5 col-md-5">
                        			 <div class="match_details_box">
                        			 	<p class="m-0">Back (Bet For)</p>
                        			 </div>
                        		</div>
                        		<div class="col-xs-2 col-sm-2 col-md-2">
                        			<div class="match_details_box">
                        				<p class="m-0">Odds</p>
                        			</div>
                        		</div>
                        		<div class="col-xs-2 col-sm-2 col-md-2">
                        			<div class="match_details_box">
                        				<p class="m-0">Stake</p>
                        			</div>
                        		</div>
                        		<div class="col-xs-3 col-sm-3 col-md-3 text-right">
                        			<div class="match_details_box">
                        				<p class="m-0">Profit</p>
                        			</div>
                        		</div>
                            </div>                        
                        </div>
                        <div class="SingleMatchBetSlip SingleMatchBetForSlip">  
                        </div>
                    </div>
                    <div class="Bet-lay-slip bettingSlip pb-3 pt-2 d-none">                        
                        <div class="BetLaySlipSecHead col-md-12">
                            <div class="row">                            
                                <div class="col-xs-5 col-sm-5 col-md-5">
                                     <div class="match_details_box">
                                        <p class="m-0">Back (Bet Against)</p>
                                     </div>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2">
                                    <div class="match_details_box">
                                        <p class="m-0">Odds</p>
                                    </div>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2">
                                    <div class="match_details_box">
                                        <p class="m-0">Stake</p>
                                    </div>
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 ">
                                    <div class="match_details_box">
                                        <p class="m-0">Profit</p>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                        <div class="SingleMatchBetSlip SingleMatchBetLaySlip">                        
                           
                        </div>
                    </div>
            		<div class="col-xs-12 col-sm-12 col-md-12 p-7">
            			<div class=" liability text-right">
            				<p class="m-0">Total Liability : <span class="totalL">0</span></p>
            			</div>
            		</div>
            		
            		{{-- <div class="col-md-12">
            			<div class="form-group m-0 py-3 form-check">
    					    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    					    <label class="form-check-label" for="exampleCheck1">Please confirm your bets.</label>
    					</div>
            		</div> --}}
                    <br>
            	</div>
                <div class="row m-0">
                    <div class="col-xs-6 col-sm-6 col-md-6 p-7">
                        <div class="two_buttons BetCancelAll">
                            <a href="#" class="btn" style="background-color: #5f5f5f; border-color: #5f5f5f;">Cancel All</a>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 p-7">
                        <div class="two_buttons BetSubmit disabled">
                            <a href="javascript:void(0)" class="btn disabled betPlaceSub" >Place Bets</a>
                        </div>
                    </div>
                </div>
            </div> 
        
    	</div>
       {{--  <div class="loader d-none">
            <img src="{{ asset('svg/submitting.gif') }}" alt="">
        </div> --}}
    </div>
    @auth
        @if(isset($event))
        @if(count(getbetSelection($event))>0)
            <div class="main-body-right-section-wrap ">

                <div class="secsideHead">
                    <p>Open Bets</p>
                </div>
                <div class="secsideBet">
                    <select name="bets" id="sideBetFetch" event_id="{{ $event }}" class="form-control">
                        <option value="" selected disabled>---Select Market---</option>
                            @foreach(getbetSelection($event) as $v)
                                <option value="{{ $v->selection_id }}">{{ $v->title }}</option> 
                            @endforeach
                    </select>
                </div>
                <div id="betDisplayed">
                    
                </div>
            </div>
        @endif
        @endif
    @endauth
</div>