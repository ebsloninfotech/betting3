<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css" integrity="sha512-Cv93isQdFwaKBV+Z4X8kaVBYWHST58Xb/jVOcV9aRsGSArZsgAnFIhMpDoMDcFNoUtday1hdjn0nGp3+KZyyFw==" crossorigin="anonymous" />
    
    <script  src="https://code.jquery.com/jquery-3.4.1.min.js"  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="  crossorigin="anonymous"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
     @yield('before_body')
</head>
<body>
	    <header class="header_top_full">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4 logo_header_top">
                    <h4 style="margin-top: 5px; margin-bottom: 5px;">

                        <a href="{{ url('/') }}" class="c-white p-7">Play For Exchange</a>
                         {{-- <a href="{{ url('/') }}"><img src="{{ asset('images/logo.png') }}" class="img-responsive" alt=""> </a> --}}
                        <input type="text" placeholder="Search Events" id="searchEvent" class="form-control" style="width: 50%;display: inline;position: relative;right: 0;float: right;">

                        <div class="searchList"></div>
                    </h4>
                  </div>
                {{-- <div class="col-sm-2">
                    <div class="search_bar_top">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <input type="search" name="" placeholder="Search Events">
                    </div>
                </div> --}}
                <div class="col-sm-8">
                         @guest
                         <form method="POST" action="{{ route('login') }}" onsubmit="return checkCaptcha()">
                                    @csrf
                            <ul class="nav nav-pills navbar-right main-top_nav">
                                <li> <i class="fa fa-user user_top" aria-hidden="true"></i>
                                    <input type="text" id="loginName" name="username" type="text" placeholder="Username"> 
                                    @if ($errors->has('username'))
                                        <dd id="errorMsg" class="state" style="display: block;">{{ $errors->first('username') }}</dd>
                                    @else
                                        @if ($errors->has('password'))
                                            <dd id="errorMsg" class="state" style="display: block;">{{ $errors->first('password') }}</dd>
                                        @else    
                                        <dd id="errorMsg" class="state"></dd>
                                        @endif
                                    @endif
                                </li>
                                <li>
                                    <input id="password" name="password" type="password" placeholder="Password"> 
                                </li>
                                <li>
                                    <img src="{{ url('valid_code') }}" class="validCodeImg" alt="">
                                    <input id="valid_code" type="text" placeholder="Validation" maxlength="4"> 
                                </li>
                                <li>
                                    <button type="submit" value="Login" class="btn btn-info login-btn"> Login <i class="fa fa-sign-in" aria-hidden="true"></i></button>
                                </li>
                            </ul>
                        </form>
                        @else
                         <ul class="nav nav-pills navbar-right main-top_nav">
                                    <li>
                                        <span>Main <b>PTH</b> <b class="betCredit">@if(get_wallet_balance()) {{ number_format((float)get_wallet_balance(),2, '.', '') }} @else 0.00 @endif</b> </span>
                                    </li>
                                @if(Auth::user()->hasRole('customer') || Auth::user()->hasRole('client'))

                                     <li>
                                        <span>Exposure <b class="totalExposure">@if(getTotalExposure()) {{ number_format((float)getTotalExposure(),2, '.', '') }}  @else 0.00 @endif</b> </span>
                                    </li>
                                @endif
                                    <li>
                                        <a class="nav-link balanceRefresh" href="#" role="button"> <i class="fas fa-redo"></i></a>
                                    </li> 
                                 <li class="nav-item dropdown border-0">
                                    <a class="nav-link dropdown-toggle a-icon" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-user tab-icon"></i>
                                      {{ Auth::user()?Auth::user()->username:'' }}
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="{{ url('/'.Auth::user()->roles[0]['name'].'/myaccount/myprofile')}}"><i class="fa fa-user"></i> My Profile</a></li>
                                        <li><a class="dropdown-item" href="{{ url('/'.Auth::user()->roles[0]['name'].'/myaccount/myaccountstatement')}}"><i class="fas fa-file-invoice-dollar"></i> Account Statement</a></li>
                                        @if(Auth::user()->hasRole('customer'))
                                        <li><a class="dropdown-item" href="{{ url('/'.Auth::user()->roles[0]['name'].'/balance_overview')}}"><i class="fa fa-file-text"></i> Balance Overview</a></li>
                                        <li><a class="dropdown-item" href="{{ url('/'.Auth::user()->roles[0]['name'].'/bethistory')}}"><i class="fas fa-hand-holding-usd"></i> My Bets</a></li>
                                        <li><a class="dropdown-item" href="{{ url('/'.Auth::user()->roles[0]['name'].'/profitLoss')}}"><i class="fas fa-hand-holding-usd"></i> Profit &amp; Loss </a></li>
                                        @endif


                                        <li><a class="dropdown-item dropdown-button" href="{{ route('logout') }}"  onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">LOGOUT <i class="fa fa-sign-out"></i></a>
                                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form> </li>
                                    </ul>
                                  </li>
                            </ul>
                        @endguest

                </div>
            </div>
            


    @include('topbar')
    </div>
    
</header>


@yield('content')

    
     @auth
        @if(auth()->user()->roles->first()->name == 'master' || auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'client' )
            <script src="{{asset('js/mastercustom.js')}}"></script>
        @endif
    @endauth


    @auth

        @if(Auth::user()->pass_change == 'first')


                <div id="change_password" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Change Password</h4>
                            {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
                        </div>
                        <div class="modal-body">
                            <form  action="{{ url('chPass') }}" method="post">
                            @csrf
                            {{-- <input type="hidden" name="user_id" value="{{ Auth::user()->id }}"> --}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="password" name="old_password" placeholder="Old Password" class="form-control" required="true"> <br>
                                        <input type="password" name="new_password" placeholder="New Password" class="form-control" required="true"> <br>
                                        <input type="password" name="confirm_new_password" placeholder="Confirm New Password" class="form-control" required="true"><br>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="submit" value="Submit" class="btn btn-block btn-info">
                                        <p id="res_change_password"></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                  </div>
                </div>

                <script>
                    
                    $('#change_password').modal({backdrop: 'static', keyboard: false});
                    $('#change_password').modal('show');  
                </script>

        @endif


    @endauth

        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

        <script src="{{ asset('front/js/custom.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    @yield('scripts')
<div class="loader">
        <img src="{{ asset('mobile/img/load.gif') }}" alt="">
    </div>

    <div class="loader" id="betloader">
        <img src="{{ asset('mobile/img/load.gif') }}" alt="">
        <p class="loader-inner">please Wait <span id="betsecRemain"> 0 sec remaining…</span></p>
    </div>
    <script>
       
        $('.betPlaceSub').on('click',function(){

            $('#betloader').show();

            c(50);

            @if(isset(Auth::user()->name))
                var user = "{{Auth::user()->username }}";
            @else

                var user = "";
            @endif 

            if (user) {

                var a = [];

                var b = []; 

                $(".team-row").each(function(i) {

                    b[i] = $(this).attr('selection_id');

                   
                });
                
               $('.betStakeAmount').each(function(){
                    item = {};
                    item['event_id'] = $(this).attr('event_id');
                    item['event_type'] = $(this).attr('event_type');
                    item['market_id'] = $(this).attr('market_id');
                    item['event_name'] = $(this).attr('event_name');
                    item['selection_id'] = $(this).attr('selection_id');
                    item['selections'] = b;
                    item['title'] = $(this).attr('title');
                    item['odds'] = $("input[name='stakeOdds']").val();
                    item['bet_type'] = $(this).attr('bet_type');  
                    item['bet_amount'] = $(this).val();  
                    a.push(item);

                });

               // $('.loader').removeClass('d-none');
               $('.BetSlipRow').addClass('d-none');

               $.ajax({
                type: 'POST',
                url: '/customer/BetPlaced',
                data: JSON.stringify(a),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                success: function(res) {
                    setTimeout(function() {
                    if(res[0].success){

                        $('#betloader').addClass('d-none');
                        // $('.site-alert').html(res[0].msg);
                        // $('.site-alert').show();
                        swal('',res[0].msg,'success');
                        location.reload();
                    }else{
                        $('#betloader').addClass('d-none');
                        $('.BetSlipRow').removeClass('d-none');
                        swal('',res[0].msg,'error');
                        // $('.site-alert').show();
                        // $('.site-alert').html(res[0].msg);
                        // setTimeout(function() {
                        //      $('.site-alert').hide();
                        // },5000);
                    }
                    },5000);
                }
              });


            }else{
                $('#betloader').hide();
                alert('Please Login First');
            }


        });




        $('#dataTables').DataTable();
    </script>
    <!-- <script>
    $(document).ready(function(){
       
        
        // Toggle plus minus icon on show hide of collapse element
        $(".collapses").on('show.bs.collapse', function(){
          $(this).prev("#c-hide").find(".fa").removeClass("fa-plus").addClass("fa-minus");
         
        }).on('hide.bs.collapses', function(){
          $(this).prev("#c-hide").find(".fa").removeClass("fa-minus").addClass("fa-plus");
          
        });
    });
</script> -->
    <script type="text/javascript">
$(document).ready(function(){
  $("#c-hide").click(function(){
    $("#CricketBodys").slideToggle();
    $("#CricketBodys").prev(".table-blue-color").find(".fa").addClass("fa-plus").removeClass("fa-minus");
       
   
  });
  
  $("#t-hide").click(function(){
    $("#TennisBodys").slideToggle();
    $("#TennisBodys").prev(".table-blue-color").find(".fa").addClass("fa-plus").removeClass("fa-minus");
       
   
  });
  $("#s-hide").click(function(){
    $("#SoccerBodys").slideToggle();
    $("#SoccerBodys").prev(".table-blue-color").find(".fa").addClass("fa-plus").removeClass("fa-minus");
       
   
  });
  $("#sportFilterPopup").click(function(){
    $("#sportFilterContainer").toggle();
    
  });
   $("#sportFilterPopups").click(function(){
    $("#sportFilterContainers").toggle();
    
  });
   $("#slipSet-in").click(function(){
    $("#set_pop").toggle();
    
  });
 
  });


    function checkCaptcha(){
        var loginName = $('#loginName').val();
            var password = $('#password').val();
            var valid_code = $('#valid_code').val();

            var d = '';

            $.ajax({
                    type: 'POST',
                    url: '/checkLoginDetail',
                    async:false,
                    data: {'loginName':loginName,'password':password,'valid_code':valid_code},
                    headers: {
                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                           },
                    success: function(res) {

                        if(res[0].success){

                            d =  true;

                        }else{
                            $('#errorMsg').html(res[0].msg);
                            $('#errorMsg').show();
                            $('.validCodeImg').attr('src',res[0].captcha);
                            d = false;
                        }

                    }
                });

            return d;
    }



    $('.balanceRefresh').click(function() {

        $('.loader').show();

        $.ajax({
                type: 'POST',
                url: '/mobile/getBalance',
                headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                success: function(res) {

                      $('.betCredit').html(res[0].wallet);
                      $('.totalExposure').html(res[0].exposure);
                        $('.loader').hide();
                }
            });

    });


    @auth
    //   setInterval(function(){ 


    //             $.ajax({
    //             type: 'get',
    //             url: '/checkSession',
    //             headers: {
    //                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //                    },
    //             success: function(res) {
                    
    //                 if(res[0].success){

    //                 }else{
                        
    //                     // alert('You have been logout due to inactive session or another screen login');
    //                     location.reload();
    //                 }

    //             }
    //           });

    // }, 3000);

  @endauth



  $('#searchEvent').on('keyup',function(){

       var test = $(this).val();

        if(test){

           $.ajax({
                type: 'post',
                url: '/searchEvent',
                headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                data:{'serachquery':test},       
                success: function(res) {
                   
                    $('.searchList').show();
                    $('.searchList').html(res);
                }
              });
        }else{

                    $('.searchList').hide();
        }

  });





</script>



@if (Session::has('flash_message'))
   {{--  <div class="container">
        <div class="alert alert-success toggle-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_message') }}
        </div>
    </div> --}}
    <script>
        swal('','{{ Session::get('flash_message') }}','error');
    </script>
@endif


@if (Session::has('flash_message1'))
    <script>
        swal('','{{ Session::get('flash_message1') }}','success');
    </script>
@endif

     @yield('custom_script')
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <!--  <div class="modal-header">     </div> -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6 hello-blck">
                            <img src="http://betting3.ebslon.com/mobile/img/logo.png" class="img-responsive" alt="">
                        </div>
                        <div class="col-sm-6 hello">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="login_form">
                                <p class="text-left logn_title_"> Please login to continue </p>
                                <form>
                                    <input type="text" name="name" class="form-control" placeholder="Username">
                                    <input type="text" name="name" class="form-control" placeholder="Password">
                                    <input type="text" name="name" class="form-control" placeholder="Validation Code">
                                    <button type="submit" class="login_btn"> Login</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





</body>
</html>