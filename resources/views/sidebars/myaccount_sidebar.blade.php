<div class="card bg-site border-0">
    <div class="card-body p-0">
        <ul class="nav sideNav flex-column" role="tablist">
            <li class="nav-item first-head-tab">
                <a href="javascript:void(0)" class="nav-link head-account-row">My Account</a>
            </li>
            @if(Auth::user()->hasRole('master'))
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/master/myaccount/myprofile')}}">
                         <i class="fa fa-user"></i> My Profile
                    </a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" href="{{url('/master/downline')}}">
                         Downline
                    </a>
                </li>   --}}              
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1"> <i class="fa fa-trophy"></i> Competitions</a>
                    <div class="collapse" id="submenu1" aria-expanded="false">
                        <ul class="flex-column pl-2 nav">
                            <li class="nav-item"><a class="nav-link py-0" href="{{url('/master/competitionList/4')}}"> Cricket</a></li>
                            <li class="nav-item"><a class="nav-link py-0" href="{{url('/master/competitionList/1')}}"> Soccer</a></li>
                            <li class="nav-item"><a class="nav-link py-0" href="{{url('/master/competitionList/2')}}"> Tennis</a></li>
                            {{-- <li class="nav-item"><a class="nav-link py-0" href="{{url('/master/competition/horse_racing')}}"> Horse Racing</a></li> --}}
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/master/slider')}}">
                        <i class="fas fa-sliders-h"></i> Banner Setting
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/master/bet_config')}}">
                        <i class="fas fa-cog"></i> Bet Configuration
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/master/myaccount/myloginhistory')}}">
                       <i class="fa fa-book"></i> Login History
                    </a>
                </li>
            @elseif(Auth::user()->hasRole('admin'))
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/admin/myaccount/myprofile')}}">
                        <i class="fa fa-user"></i> My Profile
                    </a>
                </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{url('/admin/downline')}}">
                         Downline
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/admin/myaccount/myaccountstatement')}}">
                       <i class="fa fa-file-text-o"></i>  Account Statement
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/admin/myaccount/myloginhistory')}}">
                       <i class="fa fa-book"></i> Login History
                    </a>
                </li>
            @elseif(Auth::user()->hasRole('client'))
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/client/myaccount/myprofile')}}">
                        <i class="fa fa-user"></i> My Profile
                    </a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" href="{{url('/client/downline')}}">
                         Downline
                    </a>
                </li> --}}
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/client/myaccount/myaccountstatement')}}">
                         Account Statement
                    </a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link collapsed" href="#submenu1" data-toggle="collapse" data-target="#submenu1"> Bets</a>
                    <div class="collapse" id="submenu1" aria-expanded="false">
                        <ul class="flex-column pl-2 nav">
                            <li class="nav-item"><a class="nav-link py-0" href="{{url('/client/livebets')}}"> Live Bets</a></li>
                            <li class="nav-item"><a class="nav-link py-0" href="{{url('/client/setteledBets')}}"> Setteled Bets</a></li>
                        </ul>
                    </div>
                </li> --}}
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/client/myaccount/myloginhistory')}}">
                       <i class="fa fa-book"></i> Login History
                    </a>
                </li>
            @elseif(Auth::user()->hasRole('customer'))
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/customer/myaccount/myprofile')}}">
                        <i class="fa fa-user"></i> My Profile
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/customer/balance_overview')}}">
                         <i class="fa fa-file-text"></i> Balance Overview
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/customer/myaccount/myaccountstatement')}}">
                        <!-- <i class="fa fa-file-text-o"></i> --><i class="fas fa-file-invoice-dollar"></i> Account Statement
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/customer/bethistory')}}">
                        <i class="fas fa-hand-holding-usd"></i> My Bets
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/customer/profitLoss')}}">
                        <i class="fas fa-hand-holding-usd"></i> Profit &amp; Loss
                    </a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/customer/myaccount/myloginhistory')}}">
                       <i class="fa fa-book"></i> Login History
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>

<style>
    body{
        background: #ebebeb;
    }
</style>