@extends('layouts.front')


@section('content')

    <div class="container-fluid">
        <div class="row">
			<div class="col-md-2 col-sm-2 col-xs-2 p-0">
			    @include('include/sidebar')
			</div>
			@guest

				<div class="col-md-7 col-sm-7 col-xs-7 all_margin_section">
				    @include('include/front_middle')
				</div>
				
				<div class="col-md-3 col-sm-3 col-xs-3 p-0">
					
				    @include('include/rightSideBar')
				</div>

			@else


				@if(Auth::user()->hasRole('customer'))

					<div class="col-md-7 col-sm-7 col-xs-7 all_margin_section">
						
					    @include('include/front_middle')
					</div>
					
					<div class="col-md-3 col-sm-3 col-xs-3 p-0">
						
					    @include('include/rightSideBar')
					</div>
				@else

					<div class="col-md-10 col-sm-10 col-xs-10 all_margin_section">
						
					    @include('include/front_middle')
					</div>

				@endif
			@endguest
        </div>
    </div>

@endsection