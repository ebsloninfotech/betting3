
    @guest
         <div class="row manu_tab">
                <div class="col-sm-6 pl-0">
                    <nav>
                        <ul id="tabMenu" class=" main_manu_bar">
                            <li id="menu_Home"><a href="{{ url('/') }}">Home</a></li>
                            <li><a href="{{ url('inplay') }}">In-Play</a></li>
                            <li><a href="{{ url('fullmarket/4') }}">Cricket</a></li>
                            <li><a href="{{ url('fullmarket/1') }}">Soccer</a></li>
                            <li><a href="{{ url('fullmarket/2') }}">Tennis</a></li>
                            {{-- <li><a href="{{ url('fullmarket/7') }}">Horse Racing</a></li> --}}
                        </ul>
                    </nav>
                </div>
                <div class="col-sm-6">
                    <ul id="tabMenu" class=" main_manu_bar text-right">
                        {{-- <li class="time_zone"><span class="time_zoon">Time Zone :</span> GMT+5:30</li> --}}

                        {{-- <li style="background-image: url(images/bg-top-skyEX.png); background-size: cover; background: #2a2828; ">
                            <a id="oneClickSetting" class="one_click" style="color: #ffb80c;">
                                <input type="" name="" class="box_input"> One Click Bet</a>
                        </li> --}}
                       {{--  <li>
                            <a id="openStreaming" class="live-menu"><i class="fa fa-play-circle live_btn_icone" aria-hidden="true"></i>Live</a>
                        </li> --}}
                        {{-- <li><a id="slipSet" class="setting" style="cursor: pointer">Setting <i class="fa fa-cog setting_icone  " data-toggle="modal" data-target="#myModal"  aria-hidden="true"></i></a> </li> --}}
                        <!------modal html cod*****************/-->
                        
                    </ul>
                </div>
            </div>
    @else
        @if (Auth::user()->hasRole('master'))
           <div class="row manu_tab">
                <div class="col-sm-12 p-7">
                    <nav>
                        <ul id="tabMenu" class=" main_manu_bar">
                            <li id="menu_Home"><a href="{{ url('/master/downline') }}">Downline List</a></li>
                            <li><a href="">Match Book</a>
                                <ul>
                                    <li><a href="{{ url('/master/matches/4') }}">Cricket</a></li>
                                    <li><a href="{{ url('/master/matches/1') }}">Soccer</a></li>
                                    <li><a href="{{ url('/master/matches/2') }}">Tennis</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ url('/master/reports') }}">My Report</a></li>
                            <li><a href="{{ url('/master/myaccount/myprofile') }}">My Account</a></li>
                            <li><a href="{{ url('/master/news') }}">News </a></li>
                        </ul>
                    </nav>
                </div>
                {{-- <div class="col-sm-6">
                    <ul id="tabMenu" class=" main_manu_bar text-right">
                        <li class="time_zone"><span class="time_zoon">Time Zone :</span> GMT+5:30</li>
                        
                    </ul>
                </div> --}}
            </div>
        @elseif(Auth::user()->hasRole('admin'))
            <div class="row manu_tab">
                <div class="col-sm-12">
                    <nav>
                        <ul id="tabMenu" class=" main_manu_bar">
                            <li id="menu_Home"><a href="{{ url('/admin/downline') }}">Downline List</a></li>
                            <li><a href="">Match Book</a>
                                <ul>
                                    <li><a href="{{ url('/admin/matches/4') }}">Cricket</a></li>
                                    <li><a href="{{ url('/admin/matches/1') }}">Soccer</a></li>
                                    <li><a href="{{ url('/admin/matches/2') }}">Tennis</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ url('/admin/reports') }}">My Report</a></li>
                            <li><a href="{{ url('/admin/myaccount/myprofile') }}">My Account</a></li>
                            {{-- <li><a href="{{ url('/admin/betlist') }}">Bet List</a></li> --}}
                        </ul>
                    </nav>
                </div>
                {{-- <div class="col-sm-6">
                    <ul id="tabMenu" class=" main_manu_bar text-right">
                        <li class="time_zone"><span class="time_zoon">Time Zone :</span> GMT+5:30</li>
                        
                    </ul>
                </div> --}}
            </div>
        @elseif(Auth::user()->hasRole('client'))
            <div class="row manu_tab">
                <div class="col-sm-12">
                    <nav>
                        <ul id="tabMenu" class=" main_manu_bar">
                            <li id="menu_Home"><a href="{{ url('/client/downline') }}">Downline List</a></li>
                            <li><a href="">Match Book</a>
                                <ul>
                                    <li><a href="{{ url('/client/matches/4') }}">Cricket</a></li>
                                    <li><a href="{{ url('/client/matches/1') }}">Soccer</a></li>
                                    <li><a href="{{ url('/client/matches/2') }}">Tennis</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ url('client/reports') }}">My Report</a></li>
                            <li><a href="{{ url('/client/myaccount/myprofile') }}">My Account</a></li>
                            {{-- <li><a href="{{ url('/client/betlist') }}">Bet List</a></li> --}}
                        </ul>
                    </nav>
                </div>
                {{-- <div class="col-sm-6">
                    <ul id="tabMenu" class=" main_manu_bar text-right">
                        <li class="time_zone"><span class="time_zoon">Time Zone :</span> GMT+5:30</li>
                        
                    </ul>
                </div> --}}
            </div>
        @elseif(Auth::user()->hasRole('customer'))
            <div class="row manu_tab">
                <div class="col-sm-6 p-7">
                    <nav>
                        <ul id="tabMenu" class=" main_manu_bar">
                            <li id="menu_Home"><a href="{{ url('/') }}">Home</a></li>
                            <li><a href="{{ url('inplay') }}">In-Play</a></li>
                            <li><a href="{{ url('fullmarket/4') }}">Cricket</a></li>
                            <li><a href="{{ url('fullmarket/1') }}">Soccer</a></li>
                            <li><a href="{{ url('fullmarket/2') }}">Tennis</a></li>
                            {{-- <li><a href="{{ url('fullmarket/7') }}">Horse Racing</a></li> --}}
                        </ul>
                    </nav>
                </div>
                <div class="col-sm-6">
                    <ul id="tabMenu" class=" main_manu_bar text-right">
                        {{-- <li class="time_zone"><span class="time_zoon">Time Zone :</span> GMT+5:30</li> --}}

                        {{-- <li style="background-image: url(images/bg-top-skyEX.png); background-size: cover; background: #2a2828; ">
                            <a id="oneClickSetting" class="one_click" style="color: #ffb80c;">
                                <input type="" name="" class="box_input"> One Click Bet</a>
                        </li> --}}
                       {{--  <li>
                            <a id="openStreaming" class="live-menu"><i class="fa fa-play-circle live_btn_icone" aria-hidden="true"></i>Live</a>
                        </li> --}}
                        <li><a id="slipSet" class="setting settingpopup" style="cursor: pointer">Setting <i class="fa fa-cog setting_icone  "   aria-hidden="true"></i></a> </li>
                        <!------modal html cod*****************/-->
                        
                    </ul>

                    <div id="set_pop" class="slip_set-pop d-none">
                        <div id="coinList" class="set-content">

                            
                            <dl id="editCustomizeStakeList" class="stake-set">

                                <dt>Stake</dt>
                                <form action="" id="betStakeForm">  
                                @csrf 
                                    @if(user_default_stake())  

                                        <dd><input id="stakeEdit_1" name="stakeEdit_1" class="stake_edit-input" type="number" value="{{ user_default_stake()->stake1 }}" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_2" name="stakeEdit_2" class="stake_edit-input" type="number" value="{{ user_default_stake()->stake2 }}" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_3" name="stakeEdit_3" class="stake_edit-input" type="number" value="{{ user_default_stake()->stake3 }}" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_4" name="stakeEdit_4" class="stake_edit-input" type="number" value="{{ user_default_stake()->stake4 }}" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_5" name="stakeEdit_5" class="stake_edit-input" type="number" value="{{ user_default_stake()->stake5 }}" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_6" name="stakeEdit_6" class="stake_edit-input" type="number" value="{{ user_default_stake()->stake6 }}" maxlength="7"></dd>  
                                    @else
                                        <dd><input id="stakeEdit_1" name="stakeEdit_1" class="stake_edit-input" type="number" value="" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_2" name="stakeEdit_2" class="stake_edit-input" type="number" value="" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_3" name="stakeEdit_3" class="stake_edit-input" type="number" value="" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_4" name="stakeEdit_4" class="stake_edit-input" type="number" value="" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_5" name="stakeEdit_5" class="stake_edit-input" type="number" value="" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_6" name="stakeEdit_6" class="stake_edit-input" type="number" value="" maxlength="7"></dd>
                                    @endif
                                </form>
                            </dl>
                            
                            
                            <ul class="btn-wrap">
                                <li class="col-send"><a id="coinSave" class="btn-primary btn-sm" style="cursor:pointer;">Update</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endguest