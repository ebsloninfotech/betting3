@extends('layouts.front')
@section('content')
<div class="container-fluid">
    <div class="row">
    	<div class="col-sm-3 col-xs-3 col-md-3 col-p">
    		@include('client.member.layouts.user_admin_layout')
    	</div>
    	<div class="col-md-9 col-sm-9 col-xs-9">
            <div class="row">
                <div class="col-xs-12 col-md-12 p-7">
                <div class="acc-det">
                <h2> Transaction History</h2>
              </div>
              </div>
              <div class="col-sm-12 col-xs-12 col-md-12 p-7">
                 <div class="card">
                    <div class="card-header"><h3>Account Statement</h3></div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table-balance table-border table-striped" id="dataTables">
                                <thead>
                                    <tr>
                                        <th>Date/Time</th>
                                        <th>Deposit</th>
                                        <th>Withdraw</th>
                                        <th>Balance</th>
                                        <th>Remarks</th>
                                        <th>From/to</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($data2 as $value)
                                  <tr>
                                    <td>{{ $value->created_at }}</td>
                                    <td class="deposit">@if($value->type == 'deposit') {{ $value->points }} @else -- @endif</td>
                                    <td class="withdraw">@if($value->type == 'withdraw') {{ $value->points }} @else -- @endif</td>
                                    <td>{{ $value->balance }}</td>
                                    <td>{{ $value->remarks }}</td>
                                    <td>{{ userDetail($value->sender_id)->name  }}</td>
                                  </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
              </div>
            </div>
           
        </div>


</div>
@endsection
 