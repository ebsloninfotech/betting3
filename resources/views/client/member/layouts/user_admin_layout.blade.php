<div class="card bg-site">
            <div class="card-body">
                <ul class="nav sideNav flex-column" role="tablist">
                    <li class="nav-item first-head-tab dwn-li dwn-li-n">
                        <a href="{{ url('client/myaccount/myprofile') }}" class="nav-link head-account-row">{{ Auth::user()->username }}</a>
                        <a href="javascript:void(0)" class="nav-link head-account-row">{{ $data->username }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/client/memberSummary/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-user"></i> Account Summary
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/client/userBets/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Bets
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/client/userprofitLoss/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Betting Profit &amp; Loss
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/client/userTransactions/'.$client->id.'/'.$data->id.'')}}">
                            <i class="fa fa-circle"></i> Transaction History
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/client/userLog/'.$client->id.'/'.$data->id.'')}}">
                           <i class="fa fa-book"></i> Login History
                        </a>
                    </li>
                </ul>
            </div>
        </div>