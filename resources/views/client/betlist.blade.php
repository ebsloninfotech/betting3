@extends('layouts.front')
@section('content')
<br>
<div class="container-fluid">
    
    <div class="filter">
        <form action="" id="filterForm" method="get">
            @csrf

            <div class="form-group">
                <label for="">Sports</label>
                <br>
                <input type="radio" name="sports" @if($bet_sports == 4 ) checked @endif value="4"> &nbsp; Cricket
                <input type="radio" name="sports" @if($bet_sports == 1 ) checked @endif value="1"> &nbsp; Soccer
                <input type="radio" name="sports" @if($bet_sports == 2 ) checked @endif value="2"> &nbsp; Tennis
            </div>

            <div class="form-group">
                <label for="">Bet Status : </label>
                <select name="bet_status" id="">
                    <option value="matched" @if($bet_status == 'matched') selected @endif>Matched</option>
                    <option value="settled" @if($bet_status == 'settled') selected @endif>Settled</option>
                </select>
            </div>
            <div class="form-group">
                <label for=""> Period : </label>
                <input type="date" name="bet_from" value="{{ $lastmonth }}">
                <label for="">To</label>
                <input type="date" name="bet_to" value="{{ $current_date }}">
                <input type="submit" name="submit" class="btn btn-sm btn-site" value="Get History" >
            </div>
        </form>
    </div>

<div class="row">
	<div class="col-md-12">
         <div class="card">
            <div class="card-header"> Bets</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-borderless" id="dataTables">
                            <thead>
                                <tr>
                                    <th>Bet ID</th>
                                    <th>User Name</th>
                                    <th>Market Name</th>
                            		<th>Runner Name</th>
                            		<th>Side</th>
                            		<th>Odds</th>
                            		<th>Amount</th>
                            		<th>Created Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($bets as $value)
                            	<tr>
                            		<td>{{ $value->id }}</td>
                            		<td>{{ $value->user->name }}</td>
                            		<td>{{ $value->event_name }}</td>
                            		<td>{{ $value->title }}</td>
                            		<td>{{ $value->bet_type }}</td>
                            		<td>{{ $value->odds }}</td>
                            		<td>{{ $value->bet_amount }}</td>
                            		<td>{{ $value->created_at }}</td>

                            		
                            	</tr>
                            	@endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>







@endsection


@section('custom_script')

<div id="betSett">
	
</div>



<script>
	

	$('.settleBet').click(function(){

		var id = $(this).attr('bet-id');

       $.ajax({
            type: "POST",
            url: "/client/settleBet",
            headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
            data: {
                id: id
            },
            success: function (msg) {
            	if(msg.success){
            		$('#betSett').html(msg.data);


            	}else{
            		alert('something went wrong');
            	}
            }
        });


	})


</script>
@endsection