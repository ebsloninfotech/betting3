@extends('mobile.layouts.app')

@section('content')

<div class="main-body">
	
	<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
	  <div class="carousel-inner">
	    <div class="carousel-item active">
	      <img class="d-block w-100" src="{{ asset('images/slider1.jpg') }}" alt="First slide">
	    </div>
	    <div class="carousel-item">
	      <img class="d-block w-100" src="{{ asset('images/slider2.jpg') }}" alt="Second slide">
	    </div>
	    <div class="carousel-item">
	      <img class="d-block w-100" src="{{ asset('images/slider3.jpg') }}" alt="Third slide">
	    </div>
	  </div>
	  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
	    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
	    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>
	</div>
	

	<div class="main-sport-sec">
		<ul class="nav nav-tabs" id="myTab" role="tablist">
		  <li class="nav-item">
		    <a class="nav-link active" id="cricket-tab" data-toggle="tab" href="#cricket" role="tab" aria-controls="cricket" aria-selected="true"> <img class="tab-icon cricket-ic" src="{{ asset('images/cricket.png') }}" alt=""> Cricket</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" id="soccer-tab" data-toggle="tab" href="#soccer" role="tab" aria-controls="soccer" aria-selected="false"><img class="tab-icon soccer-ic" src="{{ asset('images/football.png') }}" > Soccer</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" id="tennis-tab" data-toggle="tab" href="#tennis" role="tab" aria-controls="tennis" aria-selected="false"> <img class="tab-icon tennis-ic" src="{{ asset('images/tennis-racket.png') }}" > Tennis</a>
		  </li>
		</ul>
		<div class="tab-content" id="myTabContent">
		  <div class="tab-pane fade show active" id="cricket" role="tabpanel" aria-labelledby="cricket-tab">
		  		@if(count($cricket) > 0)
		  		<ul class="highlights">
		  			@foreach($cricket as $c)
		  			@php $da = checkInplay($c->marketId); @endphp


		  			<li><a href="{{ url('mobile/fullmarket/4/'.$c->event_id.'/'.$c->marketId.'') }}"> 
		  				@if($da)
		  					@if($da->inplay)
		  					 <i class="fa fa-circle play_icon"></i>
		  					@else
		  					 <i class="fa fa-circle-thin play_icon"></i>
		  					  
		  					@endif
		  				@else	
		  					 <i class="fa fa-circle-thin play_icon"></i>
		  				@endif
		  				 {{ $c->event->name }} </a></li>
		  			@endforeach
		  		</ul>
		  		@else
		  			<p class="no_match">No Match Found</p>
		  		@endif


		  </div>
		  <div class="tab-pane fade" id="soccer" role="tabpanel" aria-labelledby="soccer-tab">
		  		@if(count($soccer) > 0)
		  		<ul class="highlights">
		  			@foreach($soccer as $s)
		  			@php $da = checkInplay($s->marketId); @endphp
		  			<li>
		  				<a href="{{ url('mobile/fullmarket/4/'.$s->event_id.'/'.$s->marketId.'') }}"> 
		  				@if($da)
		  					@if($da[0]->inplay)
		  					 <i class="fa fa-circle play_icon"></i>
		  					@else
		  					 <i class="fa fa-circle-thin play_icon"></i>
		  					  
		  					@endif
		  				@else	
		  					 <i class="fa fa-circle-thin play_icon"></i>
		  				@endif 
		  				{{ $s->event->name }} </a></li>
		  			@endforeach
		  		</ul>
		  		@else
		  			<p class="no_match">No Match Found</p>
		  		@endif

		  </div>
		  <div class="tab-pane fade" id="tennis" role="tabpanel" aria-labelledby="tennis-tab">
		  		@if(count($tennis) > 0)
		  		<ul class="highlights">
		  			@foreach($tennis as $t)
		  			@php $da = checkInplay($t->marketId); @endphp
		  			<li><a href="{{ url('mobile/fullmarket/4/'.$t->event_id.'/'.$t->marketId.'') }}">
		  				@if($da)
		  					@if($da[0]->inplay)
		  					 <i class="fa fa-circle play_icon"></i>
		  					@else
		  					 <i class="fa fa-circle-thin play_icon"></i>
		  					  
		  					@endif
		  				@else	
		  					 <i class="fa fa-circle-thin play_icon"></i>
		  				@endif
		  			 {{ $t->event->name }} </a></li>
		  			@endforeach
		  		</ul>
		  		@else
		  			<p class="no_match">No Match Found</p>
		  		@endif
		  </div>
		</div>
	</div>

</div>


@endsection