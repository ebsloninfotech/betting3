@extends('mobile.layouts.app')

@section('content')


<div class="main-body" style="padding-top: 25px;" >
	<p class="alert-msg"></p>
	<div class="matchDet">
		<h4>{{ $eventDetail->name }}</h4>
	</div>

	<div class="matchOdds">
		<h3>Match Odds 
			@php
				$minMax = minmax($eventDetail->event_type);
			@endphp
			@if(count($minMax)>0)
				<span class="minmaxbt">Min/Max : {{ $minMax['min'] }}/{{ $minMax['max'] }} </span>
			@endif
		</h3>
	</div>

	<div class="d-100 head-row">
		<div class="d-50">
			<p>Matched</p>
		</div>	
		<div class="d-25">
			<p>Back</p>
		</div>
		<div class="d-25">
			<p>Lay</p>
		</div>
	</div>
	<div class="singleMatchSelectionRow">
		@foreach($totalMarket[0]->runner as $value)	
			<div class="d-100 team-row row_{{ $value->selection_id }}" id="row_{{ $market }}"   event_type="{{ $eventDetail->event_type }}" event_id ="{{ $event }}" market_id="{{ $market }}" title="{{ $value->runner_name }}" name="" selection_id="{{ $value->selection_id }}" >
				@php 

				$ch2 = curl_init();

		        curl_setopt($ch2, CURLOPT_URL, 'http://128.199.24.35/v1-api/odds/getOdds/'.$market.'');
		        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
		        curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, 'GET');


		        $headers = array();
		        $headers[] = 'Accept: application/json';
		        $headers[] = 'Authorization: VKbsu';
		        curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);

		        $result2 = curl_exec($ch2);
		        if (curl_errno($ch2)) {
		            echo 'Error:' . curl_error($ch2);
		        }
		        curl_close($ch2);



		         $data = json_decode($result2);
				@endphp

				@if($data)
				<div class="d-50">
					<p class="t-name add-to-res">{{ $value->runner_name }}

						@auth
		   					@php
		   						$bets = currentBets($market); 
		   						$pl = matchPL($market,$value->selection_id); 

		   						

		   					@endphp
		   					@if(count($bets)>0)

		   						@if($pl['status'] == 'lose')
		   						<span class="matchPL{{ $value->selection_id }}  clsred d-block ">{{ $pl['net_profit'] }}</span>

		   						@else
		   						<span class="matchPL{{ $value->selection_id }} clsgreen d-block ">{{ $pl['net_profit'] }}</span>
		   						@endif
		   					@else
		   					<span class="matchPL{{ $value->selection_id }} d-block"></span>	
		   					@endif
		   				@endauth

					@if($exposure)
						{{-- 	@php 
							$user_id = Auth::user()->id;
							$towin = towin($user_id,$event,$value->selection_id);
							
						@endphp
						@if($towin[0] == true)
					<p class="t-name add-to-res">{{ $value->runner_name }}

							@if($towin[1] == 'plus')

							<span class="to-win" style="display: block;">{{ $towin[2] }}</span>
							<span class="to-lose" style="display: none;"></span>
							@else
							<span class="to-win" style="display: none;"></span>
							<span class="to-lose" style="display: block;">{{ $towin[2] }}</span>

							@endif
						@else
							<p class="t-name">{{ $value->runner_name }}
							<span class="to-win" style="display: none;"></span>
							<span class="to-lose" style="display: none;"></span>
						@endif
					</p> --}}
						{{-- 	@if($exposure->team1 == $value->selection_id)

								@if($exposure->amount <0 )
								<span class="to-win" style="display: none;"></span>
								<span class="to-lose" style="display: block;">{{ $exposure->amount }}</span>
								@else
								<span class="to-win" style="display: block;">{{ $exposure->amount }}</span>
								<span class="to-lose" style="display: none;"></span>
								@endif
							@else
								@if($exposure->team2 == $value->selection_id)
									@if($exposure->profit_amount < 0 )
										<span class="to-win" style="display: none;"></span>
										<span class="to-lose" style="display: block;">{{ $exposure->profit_amount }}</span>
										@else
										<span class="to-win" style="display: block;">{{ $exposure->profit_amount }}</span>
										<span class="to-lose" style="display: none;"></span>
									@endif
								@else
									@if($exposure->lose_amount < 0 )
										<span class="to-win" style="display: none;"></span>
										<span class="to-lose" style="display: block;">{{ $exposure->lose_amount }}</span>
										@else
										<span class="to-win" style="display: block;">{{ $exposure->lose_amount }}</span>
										<span class="to-lose" style="display: none;"></span>
									@endif
								@endif
							@endif --}}
						</p>
					@endif		
					
				</div>
				<div class="d-25">
					@if(count($data->runners[0]->ex->availableToBack)>2)	
						@if($data->runners[0]->ex->availableToBack[0])
		   					<a href="#" event_id ="{{ $event }}" type="back" market_id="{{ $market }}" title="{{ $value->runner_name }}" selection_id="{{ $value->selection_id }}"  class="@if($eventDetail->bet_status != 1) betDisabled @else disableds @endif btnBack text-decoration-none  "> -- </a>
		   				@else
		   					<a href="javascript:void(0)" class="btnBack text-decoration-none"> -- </a>
		   				@endif
					@else
		   				<a href="javascript:void(0)" class="btnBack text-decoration-none"> -- </a>
					@endif
				</div>
				<div class="d-25">
					@if(count($data->runners[0]->ex->availableToLay)>0)
						@if($data->runners[0]->ex->availableToLay[0])
		   					<a href="#" event_id ="{{ $event }}" type="lay" market_id="{{ $market }}" title="{{ $value->runner_name }}" selection_id="{{ $value->selection_id }}"  class="@if($eventDetail->bet_status != 1) betDisabled @else disableds @endif btnLay text-decoration-none "> -- </a>
		   				@else
		   					<a href="javascript:void(0)" class="btnLay text-decoration-none"> -- </a>
		   				@endif	
					@else
		   				<a href="javascript:void(0)" class="btnLay text-decoration-none"> -- </a>
					@endif
				</div>
				@else

					<div class="d-50">
						<p class="t-name add-to-res">{{ $value->runner_name }}</p>
					</div>
					<div class="d-25">
						<a href="javascript:void(0)" class="btnBack text-decoration-none"> -- </a>
					</div>
					<div class="d-25">
						<a href="javascript:void(0)" class="btnLay text-decoration-none"> -- </a>
					</div>


				@endif
			</div>
		@endforeach
	</div>
	<br>
	<br>
	@if(property_exists($fancy,'diamond'))
	<div class="fancy">		
		<div class="matchOdds">
			<h3>Fancy Market
				@php
				$minMax = minmax($eventDetail->event_type);
				@endphp
				@if(count($minMax)>0)
					<span class="minmaxbt">Min/Max : {{ $minMax['fancy_min'] }}/{{ $minMax['fancy_max'] }} </span>
				@endif
			</h3>
		</div>
		<div class="d-100 head-row">
			<div class="d-50">
				<p>Fancy </p>
			</div>	
			<div class="d-25">
				<p class="text-center">No</p>
			</div>
			<div class="d-25">
				<p class="text-center">Yes</p>
			</div>
		</div>
		<div class="fancysec singleMatchSelectionRow">
				@if(count($fancy->diamond)>0)
			@foreach($fancy->diamond as $f)
			<p class="fancyTitle">{{ $f->nat }}</p>	
			<div class="d-100 team-row fancyRow  fancy{{ $f->sid }}">
				<div class="d-50" style="display: list-item;">
					<p class="t-name add-to-res"> <span class="loseFancy">
						@auth
			   				@if(Auth::user()->hasRole('customer'))
			   					@if(loseFancy($f->sid) > 0)
			   						- {{ loseFancy($f->sid) }}

			   						<span class="fancyBookBt"><a href="javascript:void(0)" class="btn btn-danger btn-sm fetchSelectionBet" selection_id="{{ $f->sid }}" event_id="{{ $event }}" >Book</a></span>
			   					@endif
			   				@endif
			   			@endauth
					</span></p>
				</div>
				<div class="d-25">
					<a href="javascript:void(0)" type="lay" SelectionId="{{ $f->sid }}" class="sessionBeta placeFancyBet btnLay text-decoration-none" id="FancyLay{{ $f->sid }}"><span class="fancyPrize">{{ $f->l1 }}</span> <span class="fancySize">{{ $f->ls1 }}</span></a>
				</div>
				<div class="d-25">
					
					<a href="javascript:void(0)" type="back" SelectionId="{{ $f->sid }}" class="sessionBeta placeFancyBet btnBack text-decoration-none" id="FancyBack{{ $f->sid }}"> <span class="fancyPrize">{{ $f->b1 }}</span> <span class="fancySize">{{ $f->bs1 }}</span></a>

				</div>
			</div>
			@endforeach
		@endif
		</div>
		@endif

	</div>
</div>

@endsection


@section('after_script')


<script src="{{ asset('front/js/math.js') }}"></script>

<script>
	function c(u) {
        $('.singleMatchSelectionRow').find("#secRemain").html(MathUtil.decimal.divide(u, 10) + " sec remaining");
        if (u > 0) {
            u--;
            setTimeout(function () {
                c(u);
            }, 100);
        }
    }


    function d(u) {
        $('.loader').find("#betsecRemain").html(MathUtil.decimal.divide(u, 10) + " sec remaining");
        if (u > 0) {
            u--;
            setTimeout(function () {
                d(u);
            }, 100);
        }
    }

</script>


<div class="sessionbookedBet">
	<div class="modal fade" id="sessionModal" tabindex="-1" role="dialog" aria-labelledby="sessionModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="sessionModalLabel">Runs Position</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <table class="table">
	        	<tr>
	        		<th>Runs</th>
	        		<th>Amount</th>
	        	</tr>
	        	<tbody class="sessionBookTable">
	        		
	        	</tbody>
	        </table>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<script>



	function refreshMarket(){

		var eventArr = [];
		$('.singleMatchSelectionRow > .team-row').each(function() {
			 eventArr.push($(this).attr('selection_id'));
		});
		var event_type = $('.tab-pane.active').attr('event_type');

		if (typeof eventArr[0] !== 'undefined') {		
			$('.spark').removeClass('spark');
			$.ajax({
		        type: 'POST',
		        url: '/mobile/SingleEventsAccMarket',
		        headers: {
		                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		               },
		         data : { marketIds : '{{ $market }}', selection_id:eventArr},
		        success: function(res) {

		        		if(res[0] != null){

		                $.each(res[0].runners, function(i, item) {
		                	$('.row_'+item.selectionId+' > .d-25 > a').each(function(j,item2) { 
		                			if (j>0) {

		                				if(item.ex.availableToLay.length > 0){
		                						$(this).addClass('placeBtn');
		                						$(this).removeClass('disableds');
		                						$(this).html(item.ex.availableToLay[0].price);
		                					$(this).addClass('spark');
		                				}else{
		                					$(this).addClass('spark');
		                					$(this).html('--');

		                				}
		                			}else{

		                				if(item.ex.availableToBack.length > 0){

		                						$(this).addClass('placeBtn');
		                						$(this).removeClass('disableds');
		                				$(this).html(item.ex.availableToBack[0].price);
		                				$(this).addClass('spark');
		                				}else{
		                					$(this).addClass('spark');
		                				$(this).html('--');
		                				}

		                			}
		                	});

		                });

		        		}
		        }
		      });
			}	
	}	
		// });	
	

	

	$( document ).ready(function() {
		refreshMarket();
	});
	setInterval(function(){ 

		refreshMarket();
	}, 4000);


$('.fetchSelectionBet').click(function(){

	var event_id = $(this).attr('event_id');
	var selection_id = $(this).attr('selection_id');

	 $.ajax({
            type: 'POST',
            url: '/matchedSessionBet',
            headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   },
             data : { event_id : event_id,selection_id:selection_id},
            success: function(res) {

            	if(res[0].success == 'success'){
            		$('.sessionBookTable').html(res[0].data);
            		$('#sessionModal').modal('show');
            	}
            }
          });

});





	$('.team-row').on('click','.placeBtn',function() {
		var event_id = $(this).attr('event_id');
		var type = $(this).attr('type');
		var market_id = $(this).attr('market_id');
		var selection_id = $(this).attr('selection_id');
		var title = $(this).attr('title');
		var odds = $(this).text();
		$('.betInput').remove();

		var text = '<div class="betInput d-100"><div class="d-50"><input type="text" class="text-center oddinput" name="oddVal" value="'+odds+'" selection_id="'+selection_id+'" odd_type="'+type+'" readonly></div><div class="d-50"><input type="number" class="oddInsert" min="1" name="betAmount" value=""></div><ul id="stakePopupList" class="coin-list"> <li><a id="selectStake_1" href="#" stake="{{ user_default_stake() ? user_default_stake()->stake1 : 10  }}"> {{ user_default_stake() ? user_default_stake()->stake1 : 10  }}</a></li><li><a id="selectStake_2" href="#" stake="{{ user_default_stake() ? user_default_stake()->stake2 : 20  }}"> {{ user_default_stake() ? user_default_stake()->stake2 : 20  }}</a></li><li><a id="selectStake_3" href="#" stake="{{ user_default_stake() ? user_default_stake()->stake3 : 50  }}"> {{ user_default_stake() ? user_default_stake()->stake3 : 50  }}</a></li><li><a id="selectStake_4" href="#" stake="{{ user_default_stake() ? user_default_stake()->stake4 : 200  }}"> {{ user_default_stake() ? user_default_stake()->stake4 : 200  }}</a></li><li><a id="selectStake_5" href="#" stake="{{ user_default_stake() ? user_default_stake()->stake5 : 500  }}"> {{ user_default_stake() ? user_default_stake()->stake5 : 500  }}</a></li><li><a id="selectStake_6" href="#" stake="{{ user_default_stake() ? user_default_stake()->stake6 : 1000  }}"> {{ user_default_stake() ? user_default_stake()->stake6 : 1000  }} </a></li></ul><div class="d-50"><a href="#" class="cancelBt">Cancel</a></div><div class="d-50"><a href="#" class="placeBetBt disabled" selection_id="'+selection_id+'" type="'+type+'">Place Bet</a></div></div>';

		$(text).insertAfter('.row_'+selection_id);
	});
	

        $('.singleMatchSelectionRow').on('click','.placeBetBt',function(){
        	$('.loader').show();

        	d(50);

            @if(isset(Auth::user()->name))
                var user = "{{Auth::user()->username }}";
            @else

                var user = "";
            @endif 

            if (user) {

            	var b = []; 

            	$(".team-row").each(function(i) {

            		b[i] = $(this).attr('selection_id');

				   
				});


            	var selection_id = $(this).attr('selection_id');

                var a = [];
                    item = {};
                    item['event_id'] = $('.row_'+selection_id).attr('event_id');
                    item['event_type'] = $('.row_'+selection_id).attr('event_type');
                    item['market_id'] = $('.row_'+selection_id).attr('market_id');
                    item['event_name'] = '{{ $eventDetail->name }}';
                    item['selection_id'] = selection_id;
                    item['selections'] = b;
                    item['title'] = $('.row_'+selection_id).attr('title');
                    item['odds'] = $(this).closest("div.betInput").find("input[name='oddVal']").val();
                    item['bet_type'] = $(this).attr('type');  
                    item['bet_amount'] = $(this).closest("div.betInput").find("input[name='betAmount']").val(); 
                    a.push(item);

               $.ajax({
                type: 'POST',
                url: '/customer/BetPlaced',
                data: JSON.stringify(a),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                success: function(res) {

                	setTimeout(function() {
                	$('.loader').hide();
                    if(res[0].success==true){
                    	alert(res[0].msg);
                        location.reload();
                    }else{
                    	alert(res[0].msg);
                    }

                },4000);

                }
              });


            }else{
            	$('.loader').hide();
                alert('Please Login First');
            }

        });



@if(property_exists($fancy,'diamond'))
	setInterval(function(){ 
		$('.spark').removeClass('spark');
    $.ajax({
            type: 'POST',
            url: '/fancyBet',
            headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   },
             data : { market_id : '{{ $market }}'},
            success: function(res) {
                var t = '';
                $.each(res,function(i,item){
                	var layid = 'FancyLay'+item.sid;
                	var backid = 'FancyBack'+item.sid;
                	var layT = '<span class="fancyPrize">'+item.l1+'</span><span class="fancySize">'+item.ls1+'</span>';
                	var backT = '<span class="fancyPrize">'+item.b1+'</span><span class="fancySize">'+item.bs1+'</span>';
                	$('#'+layid+'').html(layT);
                	$('#'+backid+'').html(backT);
                	$('#'+layid+'').addClass('spark');
                	$('#'+backid+'').addClass('spark');

                	if($.isNumeric(item.l1)){

                		$('#'+layid+'').removeClass('disabled');
                	}else{
                		$('#'+layid+'').addClass('disabled');

                	}

                	if($.isNumeric(item.b1)){
                		$('#'+backid+'').removeClass('disabled');

                	}else{
                		$('#'+backid+'').addClass('disabled');

                	}

                	if(item.gstatus == 'SUSPENDED'){
                		$('#'+layid+'').addClass('disabled');
                		$('#'+backid+'').addClass('disabled');

                	}
                	
                	// console.log(item);
                })

            }
          });

}, 2000);

@endif

	$('.sessionBeta').click(function(){

		if($(this).hasClass('disabled')){
			return false;
		}else{

		$('.fancy_quick_bet').remove();
		$('.loseFancy').hide();
		var type = $(this).attr('type');
		var SelectionId = $(this).attr('SelectionId');
		var prize = $(this).find('span.fancyPrize').html();
		var size = $(this).find('span.fancySize').html();

		if(type == 'back'){
			var tabclass = 'backRow';

		}else{
			var tabclass = 'layRow';
		}

		var text = '<div class="fancy_quick_bet '+tabclass+'"><div><div class="row inprow"><div class="text-left"><div class="canclFancy"><a href="javascript:void(0)" class="btn btn-primary btn-sm">Cancel</a></div><div class="oddhead"><ul class="quick-bet-confirm"><li id="runs">'+prize+'</li><li id="odds" class="quick-bet-confirm-title">'+size+'</li></ul></div><div class="col-stake"><input type="'+type+'" prize="'+prize+'" size="'+size+'" SelectionId="'+SelectionId+'" id="inputStake" name="stakeAmount" type="text" value=""></div><div class="col-send"><a id="placeBet" class="btn btn-sm btn-site disabled" style="cursor:pointer;">Place Bets</a></div></div></div><div class="row m-0"><div id="stakePopupLists" class="col-stake_list"><ul><li><a  class="btn btn-default" id="selectStake_1" stake="{{ user_default_stake() ? user_default_stake()->stake1 : 10  }}" style="cursor:pointer;">{{ user_default_stake() ? user_default_stake()->stake1 : 10  }}</a></li><li><a  class="btn btn-default" id="selectStake_2" stake="{{ user_default_stake() ? user_default_stake()->stake2 : 20  }}" style="cursor:pointer;">{{ user_default_stake() ? user_default_stake()->stake2 : 20  }}</a></li><li><a  class="btn btn-default" id="selectStake_3" stake="{{ user_default_stake() ? user_default_stake()->stake3 : 50  }}" style="cursor:pointer;">{{ user_default_stake() ? user_default_stake()->stake3 : 50  }}</a></li><li><a  class="btn btn-default" id="selectStake_4" stake="{{ user_default_stake() ? user_default_stake()->stake4 : 100  }}" style="cursor:pointer;">{{ user_default_stake() ? user_default_stake()->stake4 : 100  }}</a></li><li><a  class="btn btn-default" id="selectStake_5" stake="{{ user_default_stake() ? user_default_stake()->stake5 : 200  }}" style="cursor:pointer;">{{ user_default_stake() ? user_default_stake()->stake5 : 200  }}</a></li><li><a  class="btn btn-default" id="selectStake_6" stake="{{ user_default_stake() ? user_default_stake()->stake6 : 1000  }}" style="cursor:pointer;">{{ user_default_stake() ? user_default_stake()->stake6 : 1000  }}</a></li></ul></div></div></div></div>';


		$(text).insertAfter($(this).closest('.fancyRow'));


		}
		
	});




	$('.singleMatchSelectionRow').on('click','#stakePopupLists > ul > li > a',function() {

		// console.log();
		$('.loseFancy').hide();

		var type = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('type');
		var size = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('size');

		if(type == 'back'){

			var stake = $(this).html();
		}else{

			var stake = (parseFloat($(this).html()) * parseFloat(size))/100 ;
		}
		$(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").val(stake);
		var selectionId = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('SelectionId');
		// $(this).closest('.fancyRow').find("th > p").append('<span class="loseFancy">'+stake+'</span>');
		$('.fancy'+selectionId+'> .d-50 > .t-name > span.loseFancy').html('(-'+stake+')');
		$('.fancy'+selectionId+'> .d-50 > .t-name > span.loseFancy').show();

		$(this).closest('.fancy_quick_bet').find("#placeBet").removeClass('disabled');


	});	



	$('.singleMatchSelectionRow').on('click','#stakePopupList > li > a',function() {

		$('.loseFancy').hide();

		var bet_type = $(this).closest('.betInput').find("input[name='oddVal']").attr('odd_type');
		var size = $(this).closest('.betInput').find("input[name='oddVal']").attr('value');
		var selection_id = $(this).closest('.betInput').find("input[name='oddVal']").attr('selection_id');

			var stake = $(this).html();
		// if(type == 'back'){

		// }else{

		// 	var stake = (parseFloat($(this).html()) * parseFloat(size))/100 ;
		// }

		var value = parseInt($(this).html());

		var v = (value*size)-value;		


		$('.singleMatchSelectionRow > .team-row').each(function(j,k){

			if($(this).hasClass('row_'+selection_id+'')){
				if(bet_type == 'back'){
					$('.row_'+selection_id+' > .d-50 > p > span').html(v.toFixed(2));
					$('.row_'+selection_id+' > .d-50 > p > span').removeClass('clsred');
					$('.row_'+selection_id+' > .d-50 > p > span').addClass('clsgreen');
				}else{

					$('.row_'+selection_id+' > .d-50 > p > span').html(v.toFixed(2));
					$('.row_'+selection_id+' > .d-50 > p > span').removeClass('clsgreen');
					$('.row_'+selection_id+' > .d-50 > p > span').addClass('clsred');
				}
			}else{

				var selection = $(this).attr('selection_id');

				if(bet_type == 'back'){

					$('.row_'+selection+' > .d-50 > p > span').html(value);
					$('.row_'+selection+' > .d-50 > p > span').removeClass('clsgreen');
					$('.row_'+selection+' > .d-50 > p > span').addClass('clsred');
				}else{

					$('.row_'+selection+' > .d-50 > p > span').html(value);
					$('.row_'+selection+' > .d-50 > p > span').removeClass('clsred');
					$('.row_'+selection+' > .d-50 > p > span').addClass('clsgreen');
				}
			}

		})





		// $(this).closest('.betInput').find("input[name='oddVal']").val(stake);
		// var selectionId = $(this).closest('.betInput').find("input[name='oddVal']").attr('selection_id');
		// // $(this).closest('.fancyRow').find("th > p").append('<span class="loseFancy">'+stake+'</span>');
		// $('.row_'+selectionId+'> .d-50 > .t-name > span').html('(-'+stake+')');
		// $('.row_'+selectionId+'> .d-50 > .t-name > span').show();

		$(this).closest('.betInput').find("#placeBet").removeClass('disabled');


	});	



		$('.singleMatchSelectionRow').on('click','.canclFancy > a',function() {

			$(this).closest('.fancy_quick_bet').remove();
			$('.loseFancy').hide();
		});



	$('.singleMatchSelectionRow').on('keyup','#inputStake',function() {


		var type = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('type');
		var size = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('size');

		if(type == 'back'){

			var val = $(this).val();
		}else{

			var val = (parseFloat($(this).val()) * parseFloat(size))/100 ;
		}
		var selectionId = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('SelectionId');
		if(val > 0){
			$(this).closest('.fancy_quick_bet').find("#placeBet").removeClass('disabled');
			// $(this).closest('.fancyRow').find("th > p").append('<span class="loseFancy">'+stake+'</span>');
			$('.fancy'+selectionId+' > .d-50 > .t-name > span.loseFancy').html('(-'+val+')');
			$('.fancy'+selectionId+' > .d-50 > .t-name > span.loseFancy').show();
		}else{
			$(this).closest('.fancy_quick_bet').find("#placeBet").addClass('disabled');
			$('.fancy'+selectionId+' > .d-50 > .t-name > span.loseFancy').hide();
		}

	});



		$('.singleMatchSelectionRow').on('click','#placeBet',function() {


			var selectionId = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('SelectionId');
			var type = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('type');
			var prize = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('prize');
			var size = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").attr('size');
			var stake = $(this).closest('.fancy_quick_bet').find("input[name='stakeAmount']").val();
			var event_id = {{ $event }};
			var market_id = {{ $market }};
			var event_type = {{ $eventDetail->event_type }};


			$('.singleMatchSelectionRow').find('.fancy_quick_bet').remove();


				var text = '<div id="fancyBetBar" class="fancy-quick-tr"><div class="d-100"><dl class="quick_bet-wrap quick_bet-progress"><dd id="progressBar" class="progress-bar" style="width: 100%;"></dd>											<dd class="progress-bar-txt">Placing your bets, Please wait <span id="secRemain">0 sec remaining…</span></dd></dl></div></div>';

				// $('.fancy'+selectionId+'').insertAfter(text);
				$(text).insertAfter('.fancy'+selectionId+'');

				c(30);

			 $.ajax({
                type: 'POST',
                url: '/customer/FancyBetPlaced',
                data: {selectionId:selectionId,type:type,prize:prize,size:size,stake:stake,event_id:event_id,market_id:market_id,event_type:event_type},
                headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                       },
                success: function(res) {
                	setTimeout(function() {
	                	if(res[0].success){
	                		
	                		alert(res[0].msg);
	                		location.reload();
	                	}else{
	                		alert(res[0].msg)
	                	}
                	},2000);
                }
              });


		});

</script>

@endsection