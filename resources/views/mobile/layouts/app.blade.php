<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	{{-- <meta name="viewport" content="width=device-width, initial-scale=1"> --}}
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>
	<title>Ferrari Exchange</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{ asset('mobile/css/main.css') }}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="ferrari">
	
	<header>
		
		<ul>
			<li class="li-tv_bet">
				{{-- <a id="openTV" class="a-open_tv ui-link" href="#" style="display: none;"><img src="/images/mobile/transparent.gif"></a> --}}
				<a id="openBetsBtn" class="a-open_bets ui-link" href="#"><img src="{{ asset('images/coin.png') }}">Bets</a>
			</li>
			@if(Auth::user()->hasRole('customer') || Auth::user()->hasRole('client'))
			<li class="main-wallet">
				<ul id="accountCredit" style="display: flex;">
					@if(Auth::user()->hasRole('customer'))
						<li>Main<span>&nbsp;</span><span class="betCredit">PTH @if(get_wallet_balance()) {{ number_format((float)get_wallet_balance(),2, '.', '') }} @else 0.00 @endif</span></li>
						<li>Exposure<span>&nbsp;</span><span class="totalExposure">@if(getTotalExposure()) {{ number_format((float)getTotalExposure(),2, '.', '') }}  @else 0.00 @endif</span></li>
                    @endif
                    @if(Auth::user()->hasRole('client'))
						<li><span>Main&nbsp;</span><span class="betCredit">PTH @if(get_wallet_balance()) {{ number_format((float)get_wallet_balance(),2, '.', '') }}  @else 0.00 @endif</span></li>
                    @endif
				</ul>
			</li>
			<li>
				
				<a class="a-refresh balanceRefresh ui-link" id="menuRefresh" href="#" title="Refresh Main Wallet"><i class="fa fa-refresh"></i></a>
			</li>
			@if(Auth::user()->hasRole('customer'))
			<li>
				
				<a class="a-refresh ui-link" id="setPop" href="#" title="Bet Stake Setting"><i class="fa fa-cog"></i></a>
			</li>

			<div id="set_pop" class="slip_set-pop d-none">
                        <div id="coinList" class="set-content">

                            
                            <dl id="editCustomizeStakeList" class="stake-set">

                                <dt>Stake</dt>
                                <form action="" id="betStakeForm">  
                                @csrf 
                                    @if(user_default_stake())  

                                        <dd><input id="stakeEdit_1" name="stakeEdit_1" class="stake_edit-input" type="number" value="{{ user_default_stake()->stake1 }}" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_2" name="stakeEdit_2" class="stake_edit-input" type="number" value="{{ user_default_stake()->stake2 }}" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_3" name="stakeEdit_3" class="stake_edit-input" type="number" value="{{ user_default_stake()->stake3 }}" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_4" name="stakeEdit_4" class="stake_edit-input" type="number" value="{{ user_default_stake()->stake4 }}" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_5" name="stakeEdit_5" class="stake_edit-input" type="number" value="{{ user_default_stake()->stake5 }}" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_6" name="stakeEdit_6" class="stake_edit-input" type="number" value="{{ user_default_stake()->stake6 }}" maxlength="7"></dd>  
                                    @else
                                        <dd><input id="stakeEdit_1" name="stakeEdit_1" class="stake_edit-input" type="number" value="" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_2" name="stakeEdit_2" class="stake_edit-input" type="number" value="" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_3" name="stakeEdit_3" class="stake_edit-input" type="number" value="" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_4" name="stakeEdit_4" class="stake_edit-input" type="number" value="" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_5" name="stakeEdit_5" class="stake_edit-input" type="number" value="" maxlength="7"></dd>
                                        <dd><input id="stakeEdit_6" name="stakeEdit_6" class="stake_edit-input" type="number" value="" maxlength="7"></dd>
                                    @endif
                                </form>
                            </dl>
                            
                            
                            <ul class="btn-wrap">
                                <li class="col-send"><a id="coinSave" class="btn-primary btn-sm" style="cursor:pointer;">Update</a></li>
                            </ul>
                        </div>
                    </div>


				@endif
			@endif
			{{-- <li><a class="a-setting ui-link" href="#" title="Setting"><img src="/images/mobile/transparent.gif"></a></li> --}}

		</ul>
		

	</header>
		@if(fetchNews())
	<div class="marqueerow">    
        <div class="marquee-box">
                <h4><i class="fa fa-microphone"></i> News</h4>
                <marquee>
                	@foreach(fetchNews() as $v)
                		<span>{{ date('d M Y',strtotime($v->created_at)) }}</span>
                		{{ $v->news }}
                		&nbsp;&nbsp;&nbsp;
                	@endforeach
                </marquee>
        </div>
    </div>
    @endif
		@yield('content')
	
	
	<div class="header">
	  <div class="container">
	    <nav class="bottom-nav">
	      <div class="bottom-nav-item {{ Request::path() == 'mobile/index' ? 'active' : '' }}">
	      	<a href="{{ url('mobile/index') }}" class="">	      		
		        <div class="bottom-nav-link">
		          <i class="fa fa-trophy"></i>
		          <span>Sports</span>
		        </div>
		    </a>
	      </div>
	      <div class="bottom-nav-item {{ Request::path() == 'mobile/inplay' ? 'active' : '' }} {{ Request::path() == 'mobile/today' ? 'active' : '' }} {{ Request::path() == 'mobile/tomorrow' ? 'active' : '' }}" >
	      	<a href="{{ url('mobile/inplay') }}" class="">	
	        	<div class="bottom-nav-link">
	          		<i class="fa fa-clock-o"></i>
	          		<span>In-Play</span>
	        	</div>
	    	</a>
	      </div>
	      <div class="bottom-nav-item">
	      	<a href="{{ url('mobile/index') }}" class="">	
		        <div class="bottom-nav-link">
		          <i class="fa fa-home"></i>
		          <span>Home</span>
		        </div>
		    </a>
	      </div>

	      <div class="bottom-nav-item {{ Request::path() == 'mobile/account' ? 'active' : '' }}">
	      	<a href="{{ url('mobile/account') }}" class="">	      		
		        <div class="bottom-nav-link">
		          <i class="fa fa-user"></i>
		          <span>Account</span>
		        </div>
	      	</a>
	      </div>
	    </nav>
	  </div>
	</div>



	<div class="sidepanel">
		<div id="sideHead" class="side-head">
			<h3 class="a-open_bet">
				<img src="{{ asset('mobile/img/casino-chips.png') }}">Open Bets
				<a id="closeSidePanel" class="close" href="#">X</a>
			</h3>

		</div>
			<ul class="betlist">
			</ul>
	</div>

	<div class="loader">
		<img src="{{ asset('mobile/img/load.gif') }}" alt="">
		<p class="loader-inner">please Wait <span id="betsecRemain"> 0 sec remaining…</span></p>
	</div>

	<script  src="https://code.jquery.com/jquery-3.5.1.min.js"  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="  crossorigin="anonymous"></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	
	<script src="{{ asset('mobile/js/custom.js') }}"></script>	

	@yield('after_script')

	@if(Auth::user()->hasRole('customer'))
	<script>

		 $('#setPop').click(function(){


		  if($('#set_pop').hasClass('d-none')){
		    $('#set_pop').removeClass('d-none');
		  }else{

		    $('#set_pop').addClass('d-none');
		  }


		 });



		  $('#coinSave').click(function(){

			        if ($("#stakeEdit_1").val() == "" || $("#stakeEdit_2").val() == "" || $("#stakeEdit_3").val() == ""|| $("#stakeEdit_4").val() == ""|| $("#stakeEdit_5").val() == ""|| $("#stakeEdit_6").val() == "") {
			            
			            alert('All Field Require');
			        } else {

			            var postData = $("#betStakeForm").serializeArray();


			             $.ajax({
			                      type: 'POST',
			                      url: '/UpdateBetStake',
			                      headers: {
			                                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			                             },
			                       data : postData,
			                      success: function(res) {
			                          // swal('','Successfully updated','success');
			                          $('#set_pop').addClass('d-none')
			                      }
			                    });
			        }



			 });


		  // setInterval(function(){ 


		  	$.ajax({
            type: 'get',
            url: '/checkSession',
            headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   },
            success: function(res) {
                
                if(res[0].success){

                }else{

                	alert('You have been logout due to inactive session or another screen login');
                	// location.reload();
                }

            }
          });

// }, 3000);


		  $(document).mouseup(function(e) 
			{
			    var container = $("#set_pop");

			    // if the target of the click isn't the container nor a descendant of the container
			    if (!container.is(e.target) && container.has(e.target).length === 0) 
			    {
			        container.addClass('d-none');
			    }
			});
		
	</script>

	@endif
	
    @auth

        @if(Auth::user()->pass_change == 'first')


                <div id="change_password" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Change Password</h4>
                            {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
                        </div>
                        <div class="modal-body">
                            <form  action="{{ url('chPass') }}" method="post">
                            @csrf
                            {{-- <input type="hidden" name="user_id" value="{{ Auth::user()->id }}"> --}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="password" name="old_password" placeholder="Old Password" class="form-control" required="true"> <br>
                                        <input type="password" name="new_password" placeholder="New Password" class="form-control" required="true"> <br>
                                        <input type="password" name="confirm_new_password" placeholder="Confirm New Password" class="form-control" required="true"><br>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="submit" value="Submit" class="btn btn-block btn-info">
                                        <p id="res_change_password"></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                  </div>
                </div>

                <script>
                    
                    $('#change_password').modal({backdrop: 'static', keyboard: false});
                    $('#change_password').modal('show');  
                </script>

        @endif


    @endauth
</body>
</html>