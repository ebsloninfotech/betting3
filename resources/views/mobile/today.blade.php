@extends('mobile.layouts.app')

@section('content')


<div class="main-body" style="padding:0;">


	<div class="d-100 tab-row">
		<div class="tab-wrap">
			<ul>
				<li id="inplay"><a href="{{ url('mobile/inplay') }}">In-Play</a></li>
				<li id="today"  class="select" ><a href="{{ url('mobile/today') }}">Today</a></li>
				<li id="tomorrow"><a href="{{ url('mobile/tomorrow') }}">Tomorrow</a></li>
			</ul>
		</div>

		<div class="inplay-sec">
			<div class="cric-sec">
				
				<p>Cricket</p>
				@if(count($today) > 0)
		  		<ul class="highlights">
		  			@foreach($today as $c)
		  			@if($c->event_type == 4)
		  			<li><a href="{{ url('mobile/fullmarket/4/'.$c->event_id.'/'.$c->marketId.'') }}"> 
		  				@if(checkInplayEvent($c->oddmarket->marketId))
		  					<span>In-Play</span> <br> <i class="fa fa-circle play_icon"></i> 
		  				@else
		  			 		<i class="fa fa-circle-thin play_icon"></i> 
		  			 	@endif
		  				{{ $c->name }} </a></li>
		  			@endif
		  			@endforeach
		  		</ul>
		  		@else
		  			<div class="text-center">No Match Found</div>
		  		@endif
			</div>
			<div class="soccer-sec">
				
				<p>Soccer</p>
				@if(count($today) > 0)
		  		<ul class="highlights">
		  			@foreach($today as $s)
		  			@if($s->event_type == 1)
		  			<li><a href="{{ url('mobile/fullmarket/4/'.$s->event_id.'/'.$s->marketId.'') }}"> 
		  				@if(checkInplayEvent($s->oddmarket->marketId))
		  					<span>In-Play</span> <br> <i class="fa fa-circle play_icon"></i> 
		  				@else
		  			 		<i class="fa fa-circle-thin play_icon"></i> 
		  			 	@endif
		  			 {{ $s->name }} </a></li>
		  			@endif
		  			@endforeach
		  		</ul>
		  		@else
		  			<div class="text-center">No Match Found</div>
		  		@endif
			</div>
			<div class="tennis-sec">
				
				<p>Tennis</p>
				@if(count($today) > 0)
		  		<ul class="highlights">
		  			@foreach($today as $t)
		  			@if($t->event_type == 2)
		  			<li><a href="{{ url('mobile/fullmarket/4/'.$t->event_id.'/'.$t->marketId.'') }}"> 
		  			 @if(checkInplayEvent($t->oddmarket->marketId))
		  					<span>In-Play</span> <br> <i class="fa fa-circle play_icon"></i> 
		  				@else
		  			 		<i class="fa fa-circle-thin play_icon"></i> 
		  			 	@endif
		  			 {{ $t->name }} </a></li>
		  			@endif
		  			@endforeach
		  		</ul>
		  		@else
		  			<div class="text-center">No Match Found</div>
		  		@endif
			</div>
		</div>
	</div>

</div>

@endsection