@extends('mobile.layouts.app')

@section('content')


<div class="main-body">
	
	<div class="user-wrap">

	    <div class="username-wrap">
	        <p class="account-id">
	            <span><i class="fa fa-user acc_user"></i> &nbsp;{{ $user->username }}</span>
	            {{-- <span class="time-zone">GMT+5:30</span> --}}
	        </p>
	    </div>

	    <ul class="menu-list">

	    	@if(Auth::user()->hasRole('customer'))
	        
		        <li class="">
		        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/myaccount/myprofile')}}">
		        		My Profile
			        </a>
			    </li>
		        
		        <li class="">
		        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/balance_overview')}}">
		        		Balance Overview
			        </a>
			    </li>
		        <li class="">
			        	<a href="#" class="a-open_bets">
			        	My Bets
			        </a>
			    </li>
		        <li class="">
		        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/bethistory')}}">
		        		Bets History
			        </a>
			    </li>

		        
		        <li class="">
		        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/myaccount/myaccountstatement')}}">
		        		Profit &amp; Loss
		        	</a>
		    	</li>       

		        
	        	<li class="">
	        		<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/myaccount/myloginhistory')}}">
	        			Activity Log
	        		</a>
	        	</li>

	        @endif


	        @if(Auth::user()->hasRole('client'))
	        
		        <li class="">
		        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/myaccount/myprofile')}}">
		        		My Profile
			        </a>
			    </li>
		        
		        <li class="">
		        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/balance_overview')}}">
		        		Balance Overview
			        </a>
			    </li>
		        <li class="">
			        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/downline')}}">
			        	Downline
			        </a>
			    </li>
		        <li class="">
		        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/reports')}}">
		        		Report
			        </a>
			    </li>

		        
		        <li class="">
		        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/myaccount/myaccountstatement')}}">
		        		Profit &amp; Loss
		        	</a>
		    	</li>       

		        
	        	<li class="">
	        		<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/myaccount/myloginhistory')}}">
	        			Activity Log
	        		</a>
	        	</li>

	        @endif


	        @if(Auth::user()->hasRole('admin'))
	        
		        <li class="">
		        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/myaccount/myprofile')}}">
		        		My Profile
			        </a>
			    </li>
		        <li class="">
			        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/downline')}}">
			        	Downline
			        </a>
			    </li>
		        <li class="">
		        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/reports')}}">
		        		Report
			        </a>
			    </li>
		        
	        	<li class="">
	        		<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/myaccount/myloginhistory')}}">
	        			Activity Log
	        		</a>
	        	</li>

	        @endif



	         @if(Auth::user()->hasRole('master'))
	        
		        <li class="">
		        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/myaccount/myprofile')}}">
		        		My Profile
			        </a>
			    </li>
		        <li class="">
			        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/downline')}}">
			        	Downline
			        </a>
			    </li>
		        <li class="">
		        	<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/reports')}}">
		        		Report
			        </a>
			    </li>
		        
	        	<li class="">
	        		<a target="_blank" href="{{url('/'.Auth::user()->roles[0]['name'].'/myaccount/myloginhistory')}}">
	        			Activity Log
	        		</a>
	        	</li>

	        @endif
	        
	    </ul>
	    
	    <a id="logout" class="logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">LOGOUT
	    </a>
	    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
	    
</div>

</div>

@endsection