@extends('mobile.layouts.app')

@section('content')


<div class="main-body" style="padding:0;">


	<div class="d-100 tab-row">
		<div class="tab-wrap">
			<ul>
				<li id="inplay"><a href="{{ url('mobile/inplay') }}">In-Play</a></li>
				<li id="today" ><a href="{{ url('mobile/today') }}">Today</a></li>
				<li id="tomorrow"  class="select"><a href="{{ url('mobile/tomorrow') }}">Tomorrow</a></li>
			</ul>
		</div>

		<div class="inplay-sec">
			<div class="cric-sec">
				
				<p>Cricket</p>
				@if(count($tomorrow) > 0)
		  		<ul class="highlights">
		  			@foreach($tomorrow as $c)
		  				@if($c->event_type == 4)
		  					<li><a href="{{ url('mobile/fullmarket/4/'.$c->event_id.'/'.$c->marketId.'') }}"> <i class="fa fa-circle-thin play_icon"></i> {{ $c->name }} </a></li>
		  				@endif
		  			@endforeach
		  		</ul>
		  		@else
		  			<div class="text-center">No Match Found</div>
		  		@endif
			</div>
			<div class="soccer-sec">
				
				<p>Soccer</p>
				@if(count($tomorrow) > 0)
		  		<ul class="highlights">
		  			@foreach($tomorrow as $s)
		  				@if($c->event_type == 1)
		  					<li><a href="{{ url('mobile/fullmarket/4/'.$s->event_id.'/'.$s->marketId.'') }}"> <i class="fa fa-circle-thin play_icon"></i> {{ $s->name }} </a></li>
		  				@endif
		  			@endforeach
		  		</ul>
		  		@else
		  			<div class="text-center">No Match Found</div>
		  		@endif
			</div>
			<div class="tennis-sec">
				
				<p>Tennis</p>
				@if(count($tomorrow) > 0)
		  		<ul class="highlights">
		  			@foreach($tomorrow as $t)
		  				@if($c->event_type == 2)
		  					<li><a href="{{ url('mobile/fullmarket/4/'.$t->event_id.'/'.$t->marketId.'') }}"> <i class="fa fa-circle-thin play_icon"></i> {{ $t->name }} </a></li>
		  				@endif
		  			@endforeach
		  		</ul>
		  		@else
		  			<div class="text-center">No Match Found</div>
		  		@endif
			</div>
		</div>
	</div>

</div>

@endsection